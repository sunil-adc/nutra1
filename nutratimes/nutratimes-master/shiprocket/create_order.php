
<?php

$curl = curl_init();

$auth_array = array("order_id"   => "12456",
                    "order_date" => "2019-07-24 11:11",
                    "pickup_location" => "bengaluru",
                    "channel_id" => "CUSTOM",
                    "comment" => "Nutra oil",
                    "billing_customer_name" =>  "Naturescope",
                    "billing_last_name" =>  "Nutratimes",
                    "billing_address" =>  "Axis Penmark",
                    "billing_address_2" =>  "jp  nagar 2nd phase",
                    "billing_city" =>  "bengaluru",
                    "billing_pincode" =>  "560078",
                    "billing_state" =>  "karnataka",
                    "billing_country" =>  "India",
                    "billing_email" =>  "vishwesh@adcanopus.com",
                    "billing_phone" =>  "9916446730",
                    "shipping_is_billing" =>  true,
                    "shipping_customer_name" =>  "Atul",
                    "shipping_last_name" =>  "Tiwari",
                    "shipping_address" =>  "J p naagr ",
                    "shipping_address_2" =>  "1st phase",
                    "shipping_city" =>  "bengaluru",
                    "shipping_pincode" =>  "560078",
                    "shipping_country" =>  "india",
                    "shipping_state" =>  "karnataka",
                    "shipping_email" =>  "atul@adcanopus.com",
                    "shipping_phone" =>  "9916446730",
                    "order_items" => array(
                                       "name" =>  "Nutratimes Hair oil",
                                       "sku"  =>  "hair oil",
                                       "units" =>  10,
                                       "selling_price" =>  "1500",
                                       "discount" =>  "",
                                       "tax"   =>  "",
                                       "hsn"   =>  441122
                                    ),

                    "payment_method" =>  "Prepaid",
                    "shipping_charges" =>  0,
                    "giftwrap_charges" =>  0,
                    "transaction_charges" =>  0,
                    "total_discount" =>  0,
                    "sub_total" =>  9000,
                    "length" =>  10,
                    "breadth" =>  15,
                    "height" =>  20,
                    "weight" =>  2.5

                  );


$json_data  = json_encode($auth_array);

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/orders/create/adhoc",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $json_data,
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "content-type: application/json",
    "Authorization: Bearer {{eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjI2MzE2MywiaXNzIjoiaHR0cHM6Ly9hcGl2Mi5zaGlwcm9ja2V0LmluL3YxL2V4dGVybmFsL2F1dGgvbG9naW4iLCJpYXQiOjE1NjkzODI0MDcsImV4cCI6MTU3MDI0NjQwNywibmJmIjoxNTY5MzgyNDA3LCJqdGkiOiJUeE9NV3FyRHpHdFVMUm9zIn0.JBetKKWHsqYT0qrlOLmX5ChWikc3_Fyz5YU4uvzy_j0}}",
    
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  $res_arr = json_decode($response);
  echo "<pre>";
  print_r($res_arr);
  
}

?>