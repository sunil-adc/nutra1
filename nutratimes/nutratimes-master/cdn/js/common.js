function page_loader(){
	var e="#pageLoader";
	var t="&data=send";
	var n=$(e).attr("page-redirecturl");
	var r=$(e).attr("page-datadiv");
	var i=$(e).attr("page-func");
	var s=$(e).attr("page-offset");
	var o=$(e).attr("page-perpage");
	var u=$(e).attr("page-total");
	var a=$(e).attr("page-parentdiv");
	var f=$(e).attr("page-number");
	var l=$(e).attr("page-loading");
	var c=$(e).attr("page-brand");
	var h=$(e).attr("page-category");
	var sc=$(e).attr("page-sub-category");
	var p=$(e).attr("page-price");
	var d=$(e).attr("page-whr");
	var sr=$(e).attr("page-sort");
	var sclr=$(e).attr("page-color");
	var sz=$(e).attr("page-size");
	
	
	if(typeof sz!="undefined"){
		t+="&sz="+sz
	}
	if(typeof p!="undefined"){
		t+="&p="+p
	}
	if(typeof sclr!="undefined"){
		t+="&clr="+sclr
	}
	if(typeof c!="undefined"){
		t+="&b="+c
	}
	if(typeof sc!="undefined"){
		t+="&sc="+sc
	}
	if(typeof h!="undefined"){
		t+="&c="+h
	}
	if(typeof sr!="undefined"){
		t+="&_sort="+sr
	}
	
	if(typeof i != 'undefined') {
		var v = n+i+"/"+s+"/"+o+"/"+f+"/1/"+d;
		$.ajax({
			url:v,
			type:"post",
			data:t,
			beforeSend:function(){
				$("#"+a).remove();
				$("#"+r).append("<div class='"+l+"'><div class='ajax-inner-loader'><div class='loading-img right'><img src='./images/preloader.gif' class='img-page-loader' /></div><div class='loading-text right'>Loading...</div><div class='clr'></div></div></div>")
			},
			success:function(e){
				$("."+l).remove();
				$("#"+r).append(e)
			}
		})
	}
}

function ajaxReg(e,t){
	var n=true,r="";
	var s=new Array;
	s[0] = "name";
	s[1] = "mobile";
	s[2] = "email";
	s[3] = "address";
	s[4] = "city";
	s[5] = "state";
	s[6] = "pincode";
	
	
	if($("#oos").val() == 1){
		$("#abosolute_reg_div").stop().slideDown().html("Opps! Product is out of stock.").delay(10e3).slideUp();
		return false
	}
			
	if(typeof $("#allsize-list").attr("class")!="undefined"){
		if($("#product-buy-now").attr("data-sizeid")!=""){
			r+="&size_id="+$("#product-buy-now").attr("data-sizeid");
			$("#size-error").removeClass("size-error-focus")
		} else {
			$("#abosolute_reg_div").slideDown().html("Please select the size first.").delay(10e3).slideUp();
			$("#size-error").addClass("size-error-focus");
			return false
		}
	}
	
	for(i in s){
		r+="&"+s[i]+"="+$("#"+s[i]).val();
		if ($("#"+s[i]).val()==""){
			$("#abosolute_reg_div").stop().slideDown().html("Please fill valid information.").delay(10e3).slideUp();
			$("#"+s[i]).css("border-color","red").focus();
			return false
		} else {
			$("#"+s[i]).css("border-color","#CFCFCF")
		}
		
		if(i==0){
			if(!$("#"+s[i]).val().match(/^[a-zA-Z\s]+$/) || $.trim($("#"+s[i]).val()) == ""){
				$("#abosolute_reg_div").stop().slideDown().html("Please enter correct name.").delay(10e3).slideUp();
				$("#"+s[i]).css("border-color","red").focus();
				return false
			}
		}
		
		if(i==1){
			if($("#"+s[i]).val().length!=10){
				$("#abosolute_reg_div").stop().slideDown().html("Mobile number must be 10 digits.").delay(10e3).slideUp();
				$("#"+s[i]).css("border-color","red").focus();
				return false
			}
		}

		if (i==2){
			if(!$("#"+s[i]).val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
				$("#abosolute_reg_div").stop().slideDown().html("Enter valid Email Address.").delay(10e3).slideUp();
				$("#"+s[i]).css("border-color","red").focus();
				return false
			}
		}
		
		if(i == 4){
			
			if(!$("#"+s[i]).val().match(/^[a-zA-Z\s]+$/) || $.trim($("#"+s[i]).val()) == ""){
				$("#abosolute_reg_div").stop().slideDown().html("Please enter correct city.").delay(10e3).slideUp();
				$("#"+s[i]).css("border-color","red").focus();
				return false
			}
			
		}


		if(i==6){
			if($("#"+s[i]).val().length!=6){
				$("#abosolute_reg_div").stop().slideDown().html("Pincode must be 6 digits.").delay(10e3).slideUp();
				$("#"+s[i]).css("border-color","red").focus();
				return false
			}
		}

	}
	if(e!=""){
		if(typeof $("#product-buy-now").val()!="undefined"){
			r+="&redirectcart=1&mode=add&pid="+$("#pid").val()+"&oos="+$("#oos").val()
		}
		if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
		r+="&form=submit";
		$.ajax({
			url:e,
			type:"post",
			data:r,
			beforeSend:function(){
				$("#register_btn").val("Please wait...")
			},
			success:function(e){
				$("#register_btn").attr("value","Sign Up");
				if (e=="added" || e=="done"){
					if(t==""){
						window.location.href=o+"cart"
					}else{
						window.location.href=t
					}
				} else if (e=="duplicate_mobile"){
					
					$("#abosolute_reg_div").html("Mobile number already registered. Please login ");
					window.location.href=o+"login/?msg=mae";
				} else if (e=="validation_err"){
					$("#abosolute_reg_div").stop().slideDown().html("Please fill valid information.").delay(10e3).slideUp()
				} else if (e=="oos"){
					$("#abosolute_reg_div").stop().slideDown().html("Opps! Product is out of stock.").delay(10e3).slideUp()
				} else {
					$("#abosolute_reg_div").stop().slideDown().html(e).delay(10e3).slideUp()
				}
			}
		})
	}
}
function ajaxPopupReg(e,t){
		var n=true,r="";
		var s=new Array, g=new Array, cerr=new Array;
		s[0] = "popup_name";
		s[1] = "popup_mobile";
		s[2] = "popup_email";
		s[3] = "popup_address";
		s[4] = "popup_city";
		s[5] = "popup_state";
		s[6] = "popup_pincode";
		
		g[0] = "name";
		g[1] = "mobile";
		g[2] = "email";
		g[3] = "address";
		g[4] = "city";
		g[5] = "state";
		g[6] = "pincode";
		
		cerr[0] = "Please enter your name";
		cerr[1] = "please enter your mobile";
		cerr[2] = "pleae enter your email";
		cerr[3] = "pleae enter your address";
		cerr[4] = "pleae enter your city";
		cerr[5] = "pleae select your state";
		cerr[6] = "pleae enter your pincode";
		
		if($("#popup-oos").val() == 1){
			alert("Opps! Product is out of stock.");
			return false
		}
		
		if(typeof $("#popup-allsize-list").attr("class") != "undefined"){
			if($("#popup_register_btn").attr("data-sizeid")!=""){
				r+="&size_id="+$("#popup_register_btn").attr("data-sizeid");
			} else {
				alert('Please select the size');
				return false
			}
		}
		
		for(i in s){
			r+="&"+g[i]+"="+$("#"+s[i]).val();
			if ($("#"+s[i]).val()==""){
				alert(cerr[i]);
				$("#"+s[i]).focus();
				return false
			}
			if (i==2){
				if(!$("#"+s[i]).val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
					alert("Enter valid Email Address.");
					return false
				}
			}
			if(i==1){
				if($("#"+s[i]).val().length!=10){
					alert("Mobile number must be 10 digits.");
					return false
				}
			}
			if(i==6){
				if($("#"+s[i]).val().length!=6){
					alert("Pincode must be 6 digits.");
					return false
				}
			}	
		}
		if(e!=""){
			if(typeof $("#popup_register_btn").val() != "undefined"){
				r+="&redirectcart=1&mode=add&pid="+$("#popup-pid").val()+"&oos="+$("#popup-oos").val()
			}
			if(typeof $("#popup-siteurl").val()!="undefined"){
				var o=$("#popup-siteurl").val()
			}
			r+="&form=submit";
			
			$.ajax({
				url:e,
				type:"post",
				data:r,
				beforeSend:function(){
					$("#popup_register_btn").val("Please wait...")
				},
				success:function(e){
					$("#popup_register_btn").attr("value","Submit");
					if(e=="added"||e=="done"){
						if(t==""){
							window.location.href=o+"cart"
						}else{
							window.location.href=t
						}
					} else if (e=="duplicate_mobile"){
						window.location.href=o+"login/?msg=mae";
						alert("Mobile Number is duplicate try login");
					} else if (e=="oos"){
						alert("Opps! Product is out of stock.");
					}  else if (e=="validation_err"){
						alert("Please fill valid information.");
					} else {
						alert(e);
					}
				}
			})
		}
	}
$(document).ready(function(e){
	$("#sort_by").change(function(e){
		var params = window.location.href;
		var sort_by_criteria = $("#sort_by").val();
		
		var arr_url = params.split('&');
		if(arr_url.length > 0){				 
			var flag = false;
			var s = "";
			 for(i=0;i<arr_url.length;i++){		
			 	var arr_get_var = arr_url[i].split('=');
				if(arr_get_var[0] == '_sort'){
					flag = true;					
					arr_get_var[1] = sort_by_criteria;
				}
				if(typeof arr_get_var[1]!="undefined"){
					s+="&"+arr_get_var[0]+"="+arr_get_var[1]
				} else {
					s+="&"+arr_get_var[0]
				}
			}
			if(flag==false){
				s+="&_sort="+sort_by_criteria
			}			 					
			
		 }
		 s = s.substr(1);
		window.location.href=s;
    })
});
function add_cart(e,t,n,r){
	
	$("#prod_reg_div").show();
	$("#name").focus();
	
	var i=n+"product/cart_handler";
	var s=$("#product-buy-now").attr("data-sizeid");
	$("#size-error").removeClass("size-error-focus");
	if(i!=""){
		if(typeof $("#allsize-list").attr("class")!="undefined"){
			if(typeof s!="undefined"&&s!=""){
				ajax_add_cart(e,t,s,i,n)
			} else {
				$("#size-error").addClass("size-error-focus")
			}
		} else if(typeof $("#allsize-list").attr("class")=="undefined"){
			ajax_add_cart(e,t,s,i,n)
		}
	}
}
function ajax_add_cart(e,t,n,r,i){
	
	data = $.ajax({
			url:r,
			type:"post",
			data:"&form=submit&mode="+e+"&pid="+t+"&size_id="+n,
			beforeSend:function(){
				$("#product-buy-now").html("Please wait...")
			}, success:function(e){
				
				$("#product-buy-now").hide();
				if(e=="not_login"){
					$("#product-buy-now").html("Buy Now");
					$("#abosolute_reg_div").fadeIn().html("Fill your details to Proceed").delay(10e3).fadeOut()
				} else if(e=="refresh"||e=="err"||e=="stock_not_available") {
					window.location.reload()
				} else if(e=="added"){
					window.location.href=i+"cart"
				}
			}
	})
}
function login_register(e){

	var m = $.trim($("#user-mobile").val());
	var p = $.trim($("#user-password").val());

	if(m.length!=10){
	    $("#abosolute_reg_div").html("Mobile number must be 10 digits.");
		$("#user-mobile").css("border-color","red").focus();
		return false
	}else{
		$("#abosolute_reg_div").html("");
		$("#user-mobile").css("border-color","#dedede").focus();
	}


	if(p.length < 6){
	    $("#abosolute_reg_div").html("Please enter correct password.");
		$("#user-password").css("border-color","red").focus();
		return false
	}else{

		$("#abosolute_reg_div").html("");
		$("#user-password").css("border-color","#dedede").focus();
	}

	var t = e + "login/login_register";
	if(t!=""){
		$.ajax({
			url:t,
			type:"post",
			data:"&form=submit&mobile="+m+"&password="+p+"&action=login",
			beforeSend:function(){
				$("#user-signin").val("Please wait...")
			},success:function(t){
				$("#user-signin").val("Sign In");
				if(t=="user_na"){
					$("#abosolute_reg_div").show().html("Mobile No. or password not matching.")
				} else if(t=="require_blank") {
					$("#abosolute_reg_div").show().html("Require fields are blank !!")
				}else if(t=="loggedin"){
					window.location.href=e
				} else {
					$("#abosolute_reg_div").show().html("refresh and try again")
				}
			}
		})
	}
}

function forget_password(e){
	
	var t = e + "recover/ra";
	var m = $("#mobile").val();
	
	if(m.length!=10){
		$("#abosolute_reg_div").html("Mobile number must be 10 digits.");
		$("#mobile").css("border-color","red").focus();
		return false
	}else{
		$("#abosolute_reg_div").html("");
		$("#mobile").css("border-color","#dedede").focus();
	}
	
	
	if(m!=""){
		$.ajax({
			url:t,
			type:"post",
			data:"&form=submit&submit=recover&mobile="+m,
			beforeSend:function(){
				$("#account-recover").val("Sending pasword...")
			},success:function(t){
				$("#account-recover").val("Submit");
				if(t=="done"){
					$("#abosolute_reg_div").show().html("<span style='color:green'>We have sent a new password to your Email account & Registered Mobile number.</span>"); 
					setTimeout(function(){ window.location.href= e+"login" }, 12000);
					$("#mobile").val("");
				} else if(t=="number_req") {
					$("#abosolute_reg_div").show().html("Mobile number required !!");
				}else if(t=="not_found"){
					$("#abosolute_reg_div").show().html("This number not found in our record !!");
					
				} else {
					//$("#abosolute_reg_div").hide();
					$("#abosolute_reg_div").html(t)
				}
			}
		})
	}
}


function changePasswordUser(t){

	var o = $.trim($("#old_password").val());
    var n = $.trim($("#new_password").val());
	var c = $.trim($("#confirm_password").val());


	if($.trim(o) == "" || o.length < 5 ){
		$("#change_err_div").html("Please enter correct Password");
		$("#old_password").css("border-color","red").focus();
		return false
	}else{
		$("#change_err_div").html("");
		$("#old_password").css("border-color","#dedede");
	}

	if($.trim(n) == "" || n.length < 8 ){
		$("#change_err_div").html("Minimum password length is 8");
		$("#new_password").css("border-color","red").focus();
		return false
	}else{
		$("#change_err_div").html("");
		$("#new_password").css("border-color","#dedede");
	}


	if($.trim(c) == "" || $.trim(n) !=  $.trim(c) ){
		$("#change_err_div").html("Your New and confirm password must be same");
		$("#confirm_password").css("border-color","red").focus();
		return false
	}else{
		$("#change_err_div").html("");
		$("#confirm_password").css("border-color","#dedede");
	}
	
	
	$.ajax({
		url:t,
		type:"post",
		data:"&submit=spas&old_password="+o+"&new_password="+n+"&confirm_password="+c,
		beforeSend:function(){
			$("#changesubmit").val("Changing password...")
		},success:function(e){
			$("#changesubmit").val("Submit");
			if(e=="done"){
				$('#form_chang_pass').trigger("reset");
				$("#change_err_div").show().html("<span style='color:green'>Your Account Password has been changed.</span>"); 
				
			} else if(e=="old_password") {
				$("#change_err_div").show().html("Your Current Password is wrong");
			}else if(e=="validation_err"){
				$("#change_err_div").show().html("Please check field and try again");
				
			}else if(e=="newpass_notmatch"){
				$("#change_err_div").show().html("Your Old Password & New password must be different.");	
			} else {
				//$("#abosolute_reg_div").hide();
				$("#abosolute_reg_div").html("refresh and try again");
			}
		}
	})
}


function update_proifle(t){

	var g = $.trim($("input[name='gender']:checked").val());
    var n = $.trim($("#name").val());
	var e = $.trim($("#email").val());


	if($.trim(g) == "" ){
		$("#profile_div_err").html("Please select gender");
		$("#gender").css("border-color","red").focus();
		return false
	}else{
		$("#profile_div_err").html("");
		$("#gender").css("border-color","#dedede");
	}

	if($.trim(n) == ""){
		$("#profile_div_err").html("Please enter correct name");
		$("#name").css("border-color","red").focus();
		return false
	}else{
		$("#profile_div_err").html("");
		$("#name").css("border-color","#dedede");
	}


	if(!$.trim(e).match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
		$("#profile_div_err").html("Please enter correct email");
		$("#email").css("border-color","red").focus();
		return false
	}else{
		$("#profile_div_err").html("");
		$("#email").css("border-color","#dedede");
	}
	
	$.ajax({
		url:t,
		type:"post",
		data:"&submit=spas&full_name="+n+"&email="+e+"&gender="+g,
		beforeSend:function(){
			$("#submit").val("Updating..")
		},success:function(e){
			$("#submit").val("Submit");
			if(e=="done"){
				$("#profile_div_err").show().html("<span style='color:green'>Account updated successfully.</span>"); 
				
			}else if(e=="validation_err"){
				$("#profile_div_err").show().html("Please check field and try again");
				
			} else {
				
				$("#profile_div_err").html("refresh and try again");
			}
		}
	})
}


function update_address(t){

	var a = $.trim($("#address").val());
    var c = $.trim($("#city").val());
	var s = $.trim($("#state").val());
	var p = $.trim($("#pincode").val());


	if($.trim(a) == "" || $.trim(a).length < 15){
		$("#address_div_err").html("Please enter complete address");
		$("#address").css("border-color","red").focus();
		return false
	}else{
		$("#address_div_err").html("");
		$("#address").css("border-color","#dedede");
	}

	if(!$.trim(c).match(/^[a-zA-Z]+$/)){
		$("#address_div_err").html("Please enter city");
		$("#city").css("border-color","red").focus();
		return false;
	}else{
		$("#address_div_err").html("");
		$("#city").css("border-color","#dedede");
	}


	if($.trim(s) == ""){
		$("#address_div_err").html("Please select state");
		$("#state").css("border-color","red").focus();
		return false;
	}else{
		$("#address_div_err").html("");
		$("#state").css("border-color","#dedede");
	}

	if($.trim(p) == ""){
		$("#address_div_err").html("Please enter pincode");
		$("#pincode").css("border-color","red").focus();
		return false;
	}else{
		$("#address_div_err").html("");
		$("#pincode").css("border-color","#dedede");
	}

	$.ajax({
		url:t,
		type:"post",
		data:"&submit=spas&address="+a+"&state="+s+"&city="+c+"&pincode="+p,
		beforeSend:function(){
			$("#submit").val("Updating..")
		},success:function(e){
			$("#submit").val("Submit");
			if(e=="done"){
				$("#address_div_err").show().html("<span style='color:green'>Address updated successfully.</span>"); 
				
			}else if(e=="validation_err"){
				$("#address_div_err").show().html("Please check field and try again");
				
			} else {
				
				$("#address_div_err").html("refresh and try again");
			}
		}
	})
}



function scroll_div(e){
	if(e!="")
		$("html, body").animate({scrollTop:$(e).offset().top},1e3)
}

(function(e){
	e.fn.outside=function(t,n){
		return this.each(function(){
			var r = e(this), i = this;
			e(document.body).bind(t,function s(r){
				if(r.target !== i &&!e.contains(i,r.target)){
					n.apply(i,[r]);
					if(!i.parentNode)e(document.body).unbind(t,s)
				}
			})
		})
	}
})(jQuery);

$(function(){
	$(".menu-listing").hover(function(){
		$(".sub-menu").hide();
		$("#"+$(this).attr("data-id")).show()
	});
	
	$(".subCtg").mouseleave(function(){
		$(".sub-menu").hide()
	});
	
	$(".product-thumb-image").hover(function(){
		var e = $(this).attr("side-big-thumb");
		var t = $(this).attr("org-image");
		$("#product-big-image").attr("src",e);
		$("#zoom-target").attr("href",t);
		var n = $("#zoom-target").easyZoom({parent:"div.zoom-container"})
	});
	
	$(".field, textarea").focus(function(){
		if(this.title==this.value){
			this.value=""
		}
	}).blur(function(){
		if(this.value==""){
			this.value=this.title
		}
	});
	$(".entity-header").click(function(e){
		$(this).next().slideToggle()
	});
	
	if($("#category-menu").length>0){
		$("#all-categories, #category-menu").hover(function(){
			$("#category-menu").show()
		}).bind("mouseleave",function(){
			$("#category-menu").hide()
		})
	}
	$("#product-suggestion, #main-master-search, .searchlabel, .search-list").outside("click",function(e){
		$("#product-suggestion").stop().hide()
	});
	
	$(document).on("keydown",".alpha_space",function(e){
		if(e.keyCode>=37&&e.keyCode<=40||e.keyCode>=65&&e.keyCode<=90||e.keyCode==32||e.keyCode==8||e.keyCode==46||e.keyCode==17||e.keyCode==9||e.keyCode==13||e.keyCode==17||e.keyCode==18){}else{e.preventDefault()}
	});
	
	$(document).on("keydown",".only_numeric",function(e){if(e.keyCode==46||e.keyCode==8||e.keyCode==9||e.keyCode==27||e.keyCode==13||e.keyCode==65&&e.ctrlKey===true||e.keyCode>=35&&e.keyCode<=39){
			return
		} else {
			if(e.shiftKey||(e.keyCode<48||e.keyCode>57)&&(e.keyCode<96||e.keyCode>105)){e.preventDefault()
			}
		}
	})
});

$(document).ready(function(e){
	$(".category-chkbox-sidebar").click(function(e){
		var t = $("input:checkbox:checked.category-chkbox-sidebar").map(function(){
			return this.value
		}).get();
		var n = "";
		for(i=0;i<t.length;i++){
			if(i!=t.length-1)n+=t[i]+"|";
			else n+=t[i]
		}
		var r = false; 
		var s = "";
		var o = window.location.href;
		var u = o.split("&");
		for(i=0;i<u.length;i++){
			var a = u[i].split("=");
			if(a[0]=="c"){
				r = true;
				a[1] = n
			}
			if(typeof a[1]!="undefined"){
				s+="&"+a[0]+"="+a[1]
			} else {
				s+="&"+a[0]
			}
		}
		if(r==false){
			s+="&c="+n
		}
		s = s.substr(1);
		window.location.href=s
	});
	
	$(".subcategory-chkbox-sidebar").click(function(e){
		var t = $("input:checkbox:checked.subcategory-chkbox-sidebar").map(function(){
			return this.value
		}).get();
		var n = "";
		for(i=0;i<t.length;i++){
			if(i!=t.length-1)n+=t[i]+"|";
			else n+=t[i]
		}
		var r = false; 
		var s = "";
		var o = window.location.href;
		var u = o.split("&");
		for(i=0;i<u.length;i++){
			var a = u[i].split("=");
			if(a[0]=="sc"){
				r = true;
				a[1] = n
			}
			if(typeof a[1]!="undefined"){
				s+="&"+a[0]+"="+a[1]
			} else {
				s+="&"+a[0]
			}
		}
		if(r==false){
			s+="&sc="+n
		}
		s = s.substr(1);
		window.location.href=s
	});
	
	$(".brand-chkbox-sidebar").click(function(e){
		var t = $("input:checkbox:checked.brand-chkbox-sidebar").map(function(){
			return this.value
		}).get();
		var n = "";
		for(i=0;i<t.length;i++){
			if(i!=t.length-1)
				n +=t[i]+"|";
			else 
				n+=t[i]
		}
		var r = false;
		var s = "";
		var o = window.location.href;
		var u = o.split("&");
		for(i=0;i<u.length;i++){
			var a = u[i].split("=");
			if(a[0]=="b"){
				r = true;
				a[1] = n
			}
			if(typeof a[1]!="undefined"){
				s+="&"+a[0]+"="+a[1]
			} else {
				s+="&"+a[0]
			}
		}
		if(r==false){
			s+="&b="+n
		}
		s = s.substr(1);
		window.location.href=s
	});
	
	$(".size-chkbox-sidebar").click(function(e){
		var t = $("input:checkbox:checked.size-chkbox-sidebar").map(function(){
			return this.value
		}).get();
		var n = "";
		for(i=0;i<t.length;i++){
			if(i!=t.length-1)
				n +=t[i]+"|";
			else 
				n+=t[i]
		}
		var r = false;
		var s = "";
		var o = window.location.href;
		var u = o.split("&");
		for(i=0;i<u.length;i++){
			var a = u[i].split("=");
			if(a[0]=="sz"){
				r = true;
				a[1] = n
			}
			if(typeof a[1]!="undefined"){
				s+="&"+a[0]+"="+a[1]
			} else {
				s+="&"+a[0]
			}
		}
		if(r==false){
			s+="&sz="+n
		}
		s = s.substr(1);
		window.location.href=s
	});
	
	$(".color-chkbox-sidebar").click(function(e){
		var t = $("input:checkbox:checked.color-chkbox-sidebar").map(function(){
			return this.value
		}).get();
		var n = "";
		for(i=0;i<t.length;i++){
			if(i!=t.length-1)
				n +=t[i]+"|";
			else 
				n+=t[i]
		}
		var r = false;
		var s = "";
		var o = window.location.href;
		var u = o.split("&");
		for(i=0;i<u.length;i++){
			var a = u[i].split("=");
			if(a[0]=="clr"){
				r = true;
				a[1] = n
			}
			if(typeof a[1]!="undefined"){
				s+="&"+a[0]+"="+a[1]
			} else {
				s+="&"+a[0]
			}
		}
		if(r==false){
			s+="&clr="+n
		}
		s = s.substr(1);
		window.location.href=s
	});
	
	$(".price-chkbox-sidebar").click(function(e){
		var t = $("input:checkbox:checked.price-chkbox-sidebar").map(function(){
			return this.value
		}).get();
		var n="";
		for(i=0;i<t.length;i++){
			if(i!=t.length-1)
				n+=t[i]+"|";
			else 
				n+=t[i]
		}
		var r = false;
		var s = "";
		var o = window.location.href;
		var u = o.split("&");
		for(i=0;i<u.length;i++){
			var a = u[i].split("=");
			if(a[0]=="p"){
				r = true;
				a[1] = n
			}
			if(typeof a[1]!="undefined"){
				s+="&"+a[0]+"="+a[1]
			} else {
				s+="&"+a[0]
			}
		}
		if(r==false){
			s+="&p="+n
		}
		s = s.substr(1);
		window.location.href=s
	})
});

$(window).scroll(function(){
	if ($(window).scrollTop()>=($(document).height()-$(window).height())*.95){
		page_loader()
	}
});

$(document).ready(function(e){
	function t(e){
		var t = $(e).val();
		var n = $("#siteurl").val();
		var r = "1=1";
		var i = $.cookie("search_cookie");
		if (t!=""){
			if(i!=t){
				$.cookie("search_cookie",t,{expires:10});
				var s = $.ajax({
							type: "POST",
							url: n+"/ajax/searchsuggestion/index/"+t,
							data: r,
							cache: false,
							beforeSend: function(){
								$("#product-suggestion").fadeIn().html('<div class="search-loader"></div>')
							},
							success:function(e){
								if(e==""){
									$("#product-suggestion").fadeIn().html('<div class="search_result">Search result not available.</div>')
								}else if(e=="refresh"){
									$("#product-suggestion").fadeIn().html('<div class="search_result">Something went wrong refresh the page.</div>')
								}else{
									$("#product-suggestion").fadeIn().html(e);
									$(".search_result").click(function(){
										$("#main-master-search").val($(this).attr("data-entity")).css("text-transform","uppercase");
										$("#search-form").submit()
									})
								}
							}
				})
			}
		}
		return false
	}
	
	$(document).on("submit","#search-form",function(){
		if($("#main-master-search").val().length<=0){
			$("#main-master-search").css("background-color","rgb(252, 237, 237)");
			$("#main-master-search").attr("placeholder","Enter something to start searching...");
			return false
		}
	});
	
	$(document).on("keyup","#search_brand",function(){
		var e = $(this).val(),t=0;
		$(".brand-list").each(function(){
			if($(this).attr("data-value").search(new RegExp(e,"i"))<0){
				$(this).hide()
			} else {
				$(this).show();
				t++
			}
		})
	});
	
	$(document).on("keyup","#search_size",function(){
		var e = $(this).val(),t=0;
		$(".size-list").each(function(){
			if($(this).attr("data-value").search(new RegExp(e,"i"))<0){
				$(this).hide()
			} else {
				$(this).show();
				t++
			}
		})
	});
	
	$(document).on("change",".chkloginreg",function(){
		if($(this).val()=="register"){
			$("#user-password").val("");
			$("#user-signin").attr("value","SIGN UP");
			$("#user-password").attr("disabled","disabled")
		} else {
			$("#user-password").removeAttr("disabled");
			$("#user-signin").attr("value","SIGN IN");
		}
	});
	
	$(document).on("keyup","#search_category",function(){
		var e = $(this).val(),t=0;
		$(".category-list").each(function(){
			if($(this).attr("data-value").search(new RegExp(e,"i"))<0){
				$(this).hide()
			} else {
				$(this).show();
				t++
			}
		})
	});
	
	$(document).on("keyup","#search_sub_category",function(){
		var e = $(this).val(),t=0;
		$(".sub-category-list").each(function(){
			if($(this).attr("data-value").search(new RegExp(e,"i"))<0){
				$(this).hide()
			} else {
				$(this).show();
				t++
			}
		})
	});
	
	$(document).on("click","#redeem_credit",function(){
		if($(this).is(":checked")){
			$("#redeem_point").val("1")
		} else {
			$("#redeem_point").val("0")
		}
	});
	
	$("#main-master-search").ajaxStop().keyup(function(){
		$("#main-master-search").css("background-color","#FFFFFF");
		$("#main-master-search").attr("placeholder","Search for products, brands");
		typingTimer=setTimeout(t(this),5e3)
	})
});

$(document).ready(function(e){
	$(".size-box").click(function(){
		$("#product-buy-now").attr("data-sizeid",$(this).attr("data-prodid"));
		$(".size-box").removeClass("size-box-selected");
		$(this).addClass("size-box-selected")
	});
	
	$(".popup-size-box").click(function(){
		$("#popup_register_btn").attr("data-sizeid",$(this).attr("data-prodid"));
		$(".popup-size-box").removeClass("size-box-selected");
		$(this).addClass("size-box-selected")
	});
	
	
	$(".crt-quantity").focus(function(){
		$("#update_quantity_"+$(this).attr("data-lnum")).show()
	});
	
	$(".del-item").click(function(){
		if(confirm("Are you sure you want to delete this item from your cart ?")){
			var e = $(this).attr("data-oid");
			var t = $(this).attr("data-cpid");
			var n = $(this).attr("data-lum");
			var r = 0;
			var i = $(this).attr("data-url");
			var s = $("#cart-error").attr("data-redirect-url");
			$.ajax({
				url:i+"cart/cartupd/1",
				type:"post",
				data:"&form=submit&oid="+e+"&cpid="+t+"&qty="+r,
				beforeSend:function(){
					$("#cart-error").slideDown().html("Removing one or more item from your cart...")
					$('#site_loading_icon').show();
				},
				success:function(e){
					if(e=="err"){
						$("#cart-error").slideDown().html("Unexpected error occure, refresh the page and try again.")
					} else if (e=='lkd') {
						$("#cart-error").slideDown().html("You can't delete items, please try again later")
					} else {
						window.location.href=s
					}
					$('#site_loading_icon').hide();
				}
			})
		}
	});
	
	$(".upd-quantity").blur(function(){
		var e = $(this).attr("data-lum");
		var t = $(this).attr("data-oid");
		var n = $(this).attr("data-cpid");
		var r = $(this).attr("data-cur-qty");
		var i = $("#quantity_"+e).val();
		var s = $(this).attr("data-url");
		var o = $("#cart-error").attr("data-redirect-url");
		if (i>=0&&i!=""){
			if(i!=r){
				if(i==0){
					if(!confirm("Are you sure you want to delete this item from your cart ?")){
						return false
					}
				}
				$.ajax({
					url:s+"cart/cartupd/1",
					type:"post",
					data:"&form=submit&oid="+t+"&cpid="+n+"&qty="+i,
					beforeSend:function(){
						$("#cart-error").slideDown().html("Updating Cart...")
						$('#site_loading_icon').show();
					},
					success:function(e){
						if(e=="err"){
							$("#cart-error").slideDown().html("Unexpected error occure, refresh the page and try again.")
						} else if(e=="qnty_err"){
							$("#cart-error").slideDown().html("You can't buy bulk items, Please change the quantity.")
						} else if(e=="stock_err"){
							$("#cart-error").slideDown().html("Stock is not available for requested quantity, Please change the quantity.")
						} else if(e=="lkd"){
							$("#cart-error").slideDown().html("You can't delete items, please try again later")
						} else {
							$("#cart-error").slideDown().html("Quantity Updated")
							window.location.href=o
						}
						$('#site_loading_icon').hide();
					}
				})
			}
		} else {
			$(".cart-error").slideDown().html("Quantity must be atleast 1")
		}
	})
});

/*$(".newsletter-two-submit").live("click", function(e) {
		var t = $("#nws_emailaddress").attr("data-siteurl");
		var n = $("#nws_emailaddress").val();
		var r = "&form=submit&gender="+$(this).attr("data-value")+"&email="+n;
		if (n != "" && typeof n != "undefined"){
			$("#nws_emailaddress").css("border","1px solid #A5A5A5");
			$.ajax({
				url:t+"ajax/userNewsletter/index/",
				type:"post",
				data:r,
				beforeSend:function(){
					$("#nws_emailaddress").attr("readonly","readonly");
					$(".newsletter-two-submit").attr("readonly","readonly")
				},
				success:function(e){
					$("#nws_emailaddress").removeAttr("readonly");
					$(".newsletter-two-submit").removeAttr("readonly");
					if (e == "done"){
						$("#nws_emailaddress").val("");
						alert("Congratulations! You will receive the vouchers in your email shortly.")
					}else if(e=="email_invalid"){
						alert("Email address is invalid.")
					}else if(e=="email_exist"){
						alert("Email address is already exist.")
					}else if(e=="refresh"){
						alert("Please refresh your page and try again.")
					}else if(e=="email_blank"){
						alert("You can't left blank email field.")
					}
				}
			})
		} else {
			$("#nws_emailaddress").css("border","1px solid red")
		}
	});
*/
$(document).ready(function(e){
	
	$("#gototopbg").click(function(){
		scroll_div("body")
	});

	$(window).scroll(function(){
		var e = $(".jvsidebar").height();
		var t = $(".wide-wrapper").height();
		if (e>t){
			$(".wide-wrapper").css("height",e);
		}
		
		var n = $(window).scrollTop();
		var r = $(document).height()-$(window).height()-$(window).scrollTop();
		var i = $(".footer").height();
		var s = $(".header").height();
		
		if (n >= screen.height){
			$("#gototopbg").stop().fadeIn(500)
		} else {
			$("#gototopbg").stop().fadeOut(500)
		}
		
		if(typeof $(".jvsidebar").offset()!="undefined"){
			var o = $(".jvsidebar").offset().top;
			if (o<n){
				$("#category-menu").attr("bottom-scroll",r+"=="+i);
				$("#category-menu").attr("top-scroll",n+"=="+i);
				
				if (n<=s){
					$(".jvsidebar").attr("second-condition",s);
					$(".jvsidebar").css({position:"fixed",top:s})
				} else if(r<=i){
					$(".jvsidebar").attr("first-condition",r-i-i*.1);
					$(".jvsidebar").css({position:"fixed",top:r-i-i*.1})
				} else {
					$(".jvsidebar").attr("third-condition",0);
					$(".jvsidebar").css({position:"fixed",top:0})
				}
			} else {
				$(".jvsidebar").css("position","static")
			}
		}
});
});


$(document).ready(function(e){
	$(".wish_list").click(function(e){
		var site = $(this).attr("data-url");
		var prod = $(this).attr("prod-id");
		var sess = $(this).attr("user-id");
		var usrprod = $(this).attr("up-id");
		var size = $(this).attr("size");
	    
		if(typeof size != 'undefined') {
			var r = "&form=submit&prod="+prod+"&u_id="+sess+"&up_id="+usrprod+"&size="+size;
		} else {
			var r = "&form=submit&prod="+prod+"&u_id="+sess+"&up_id="+usrprod;
		}
		
		$.ajax({
			url:site+"ajax/wishList/wishlist/1",
			type:"post",
			data:r,
			beforeSend:function(){
				
			},
			success:function(e){
			   if(e != ""){
				   window.location.href=site+"cart";
			   }else{
				   window.location.href=site+"cart";   
			   }
				
			}
		});
    });
});


  $(document).ready(function(e){
	$(".order_notpaid").click(function(e){
		var site = $(this).attr("siteurl");
		var o_id = $(this).attr("order-id");
		
		$.ajax({
					url:site+"orderhistory/pay_now",
					type:"post",
					data:"&odr_id="+o_id,
					beforeSend:function(){
						
					},
					success:function(e){
					   if(e != o_id){
						  window.location.href=site+"cart" 
					   }else{
						   window.location.href=site+"cart";   
					   }
						
					}
				})
				

			
    })
});


function change_resolved_status(checked_status,complaint_id,path){

$.ajax({	
url:path,
type:"post",
data:"&complaint_id="+complaint_id+"&checked_status="+checked_status,
beforeSend:function(){

},
success:function(e){
$("#complaint_status_li").html("Your complaint will be ressolved soon. Thank You.");
},
error:function(e){
alert("There is some Problem. Please try again")
}
})
}


function get_details_pincode(u){
	
	var pincd = $("#pincode").val();
	data = $.ajax({
			url:u,
			type:"post",
			data:"&pincode="+pincd,
			beforeSend:function(){
				
			}, success:function(e){				
			   var arr_reply = e.split('|');
			   $("#city").val(arr_reply[0]);
			   $("#state").val(arr_reply[1]);			   
		}
	})
}

$(document).ready(function(e) {
    $(".paywithcod").change(function(e) {
        var chk = 0;
		var siteurl = $(".cartpaymentform").attr("data-siteurl");
		
		if ($(this).val() == '1') {
			$(".cartpaymentform").attr("action", siteurl + "codconfirm/confirmorder");
			chk = 1;
		} else {
			$(".cartpaymentform").attr("action", siteurl + "payment/index");
			chk = 0;
		}
		
		var siteurl = $(this).attr("data-siteurl");
		$.ajax({
			url: siteurl + 'cart/convertOrd/' + chk,
			type: "post",
			data: "&formsbt=1",
			beforeSend: function(){
				$('#site_loading_icon').show();
			},
			success: function(e){
				
				if (e=='convrtd' || e=='onmrnoncod' || e=='ordblank') {
					window.location.href = siteurl + 'cart';
				} else {
					window.location.href = siteurl + 'cart';
				}
				$('#site_loading_icon').hide();
			},
			error: function(e){
				alert("There is some Problem. Please try again");
			}
		});
    });
});
$(window).scroll(function(){
    if ($(window).scrollTop() >= 100) {
        $('.menu').addClass('fixed-header');
    }
    else {
       $('.menu').removeClass('fixed-header');
    }
});

$(document).ready(function(e) {
    $(".cartSizeChange").click(function(e) {
		var siteurl 		= $(this).attr("data-siteurl");
		var data_size_div 	= $(this).attr("data-size-div");
		var data_order_id 	= $(this).attr("data-order-id");
		var data_prodid	 	= $(this).attr("data-prodid");
		var data_cartprodid	= $(this).attr("data-cartprodid");
		var data_size_id 	= $(this).attr("data-size-id");
		var data_size_name 	= $(this).attr("data-size-name");
		
		var v = '&data_order_id=' + data_order_id + 
				'&data_prodid=' + data_prodid + 
				'&data_cartprodid=' + data_cartprodid + 
				'&data_size_id=' + data_size_id + 
				'&data_size_name=' + data_size_name;
		
		
		if (typeof siteurl != 'undefined') {
			$.ajax({
				url: siteurl + 'cart/getSize/',
				type:"post",
				data:v,
				beforeSend:function(){
					$("#" + data_size_div).html('please wait...');
				},
				success:function(e){
					if (e == 'refresh') {
						window.location.href = siteurl + 'cart'
					} else {
						$("#" + data_size_div).html(e);
					}
				}
			});
		} else {
			alert('Please refresh the page');
		}     
    });

});

function chageSizeAjax(prodid, cartprodid, currentSize, siteurl) {
	if (typeof prodid != 'undefined' && typeof cartprodid != 'undefined') {
		var v = '&prodid=' + prodid + '&cartprodid=' + cartprodid + '&currentsize=' + currentSize;
		$.ajax({
			url: siteurl + 'cart/sizeUpdate/',
			type:"post",
			data: v,
			beforeSend:function(){
				$("#cart-error").slideDown().html("Updating Cart...")
			},
			success:function(e){
				window.location.href = siteurl + 'cart'
			}
		});
	} else {
		alert('Please refresh your page');
	}
}


	function contact_us(u) {
		

		if(!$("#name").val().match(/^[a-zA-Z]+$/)) {
			$("#contactus_div").html("<label style='color:red'>Please enter correct name.</label>");
			$("#name").css("border-color","red").focus();
			return false
		
		}else{
			var name = $("#name").val();
			$("#contactus_div").html("");
			$("#name").css("border-color","#dedede");
			
		}

		if(!$("#email").val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
			$("#contactus_div").html("<label style='color:red'>Please enter correct email address.<label>");
			$("#email").css("border-color","red").focus();
			return false
		
		}else{
			var email = $("#email").val();
			$("#contactus_div").html("");
			$("#email").css("border-color","#dedede");
			
		}
		
		if($("#mobile").val().length!=10){
			$("#contactus_div").html("<label style='color:red'>Please enter 10 digit mobile number.<label>");
			$("#mobile").css("border-color","red").focus();
			return false
		}else{
			var mobile = $("#mobile").val();
			$("#contactus_div").html("");
			$("#mobile").css("border-color","#dedede");
		}
		
		if($.trim($("#message").val()) == ""  ||  $("#message").val() == "" ){
			$("#contactus_div").html("<label style='color:red'>Please enter message.<label>");
			$("#message").css("border-color","red").focus();
			return false
		}else{
			var message = $("#message").val();
			$("#contactus_div").html("");
			$("#message").css("border-color","#dedede");
		}
		

		var v = '&submit=contac&name=' + name + '&email=' + email + '&mobile=' + mobile+ '&message=' + message;
		$.ajax({
			url: u ,
			type:"post",
			data: v,
			beforeSend:function(){
				$("#contact_sbmt").html("Submiting...")
			},
			success:function(e){
				
				$("#contact_sbmt").html("Submiting...");

				if(e == "done"){
					$('#contact_us_frm').trigger("reset");
					$("#contactus_div").html("<label style='color:green'>Your Query Submitted Successfully .<label>");	
				}else if(e == "validation_err"){
					$("#contactus_div").html("<label style='color:red'>Please try again.<label>");	
				}else{
					$("#contactus_div").html("<label style='color:red'>Please try again.<label>");	
				}
				
			}
		});
	
}


$(document).ready(function(e) {
    $(".order-details-history").click(function(e) {
		var siteurl 		= $(this).attr("data-siteurl");
		var data_id 		= $(this).attr("data-id");
		var data_order_id 	= $(this).attr("data-order-id");
		
		var v = '&data_order_id=' + data_order_id + 
				'&data_id=' + data_id;
		
		if (typeof siteurl != 'undefined') {
			$.ajax({
				url: siteurl + 'orderhistory/getOrderDetails/',
				type: "post",
				data: v,
				beforeSend:function(){
					$("#ajax_order_" + data_id).html('please wait...');
				},
				success:function(e){
					if (e == 'refresh') {
						alert('Please refresh the page and try again.')
					} else {
						$("#ajax_order_" + data_id).html(e);
					}
				}
			});
		} else {
			alert('Please refresh the page');
		}     
    });

});

function ajaxQukReg(e,t){
	var n=true,r="";
	var s=new Array;
	s[0] = "name";
	s[1] = "mobile";
	s[2] = "email";
	
	if(typeof $("#allsize-list").attr("class")!="undefined"){
		if($("#product-buy-now").attr("data-sizeid")!=""){
			r+="&size_id="+$("#product-buy-now").attr("data-sizeid");
			$("#size-error").removeClass("size-error-focus")
		} else {
			$("#abosolute_reg_div").slideDown().html("Please select the size first.").delay(10e3).slideUp();
			$("#size-error").addClass("size-error-focus");
			return false
		}
	}
	
	for(i in s){
		r+="&"+s[i]+"="+$("#"+s[i]).val();
		if ($("#"+s[i]).val()==""){
			$("#abosolute_reg_div").stop().slideDown().html("Please fill valid information.").delay(10e3).slideUp();
			$("#"+s[i]).css("border-color","red").focus();
			return false
		} else {
			$("#"+s[i]).css("border-color","#CFCFCF")
		}
		if (i==2){
			if(!$("#"+s[i]).val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
				$("#abosolute_reg_div").stop().slideDown().html("Enter valid Email Address.").delay(10e3).slideUp();
				$("#"+s[i]).css("border-color","red").focus();
				return false
			}
		}
		if(i==1){
			if($("#"+s[i]).val().length!=10){
				$("#abosolute_reg_div").stop().slideDown().html("Mobile number must be 10 digits.").delay(10e3).slideUp();
				$("#"+s[i]).css("border-color","red").focus();
				return false
			}
		}	
	}
	if(e!=""){
		if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
		r+="&quickform=submit";
		$.ajax({
			url:e,
			type:"post",
			data:r,
			beforeSend:function(){
				
			},
			success:function(e){
				$("#register_btn").attr("value","Sign Up");
				if (e=="added" || e=="done"){
					
				} else if (e=="refresh"){
					alert('please refresh your page and try again');
				} else if (e=="duplicate_mobile"){
					$("#abosolute_reg_div").stop().slideDown().html("Mobile Number alreadt exist try some other number").delay(10e3).slideUp()
				} else if (e=="validation_err"){
					$("#abosolute_reg_div").stop().slideDown().html("Please fill valid information.").delay(10e3).slideUp()
				}
			}
		})
	}
}

function cart_redirect(url, siteurl){
	if (typeof siteurl != 'undefined') {
		var v = 'form=cart&payurl=' + url + '&redeem_point=' + $("#redeem_point").val();
		$.ajax({
			url: siteurl + 'cart/get_cart_details/',
			type: "post",
			data: v,
			beforeSend:function(){
				
			},
			success:function(e){
				var obj = JSON.parse(e);
				if (typeof obj.msg != 'undefined') {
					if (typeof obj.msg == 'notlogin') {
						window.location.href = window.location.href;	
					}
					else if (obj.msg == 'deal_error') 
					{
						location.replace(siteurl+'cart');
					}
				}
				
				if (typeof obj.url != 'undefined') {
					if (typeof obj.url != '') {
						window.location.href = obj.url;	
					}
				}
				
				if (typeof obj.form != 'undefined') {
					if (typeof obj.form != '') {
						$("#jquery_cart_popup").html(obj.form).show();
					}
				}
			}
		});
	} else {
		alert('Please refresh the page');
	}   
}
function cart_payment_redirect(url, siteurl, th){
	if (typeof siteurl != 'undefined') {
		var v = 'form=cart&address=' + $(".quick-address").val() + 
				'&pincode=' +  $(".quick-pincode").val() + 
				'&city=' + $(".quick-city").val() +
				'&redeem_point=' + $("#quick_redeem_point").val() + 
				'&state=' + $(".quick-state").val();
		$.ajax({
			url: siteurl + 'cart/set_cart_details/',
			type: "post",
			data: v,
			beforeSend:function(){
				
			},
			success:function(e){
				if (e == 'refresh') {
					alert('Please refresh the page and try again.')
				} else if (e == 'leftblank') {	
					alert('All fields are mendatory')
				} else { 
					window.location.href = e;
				}
			}
		});
	} else {
		alert('Please refresh the page');
	}   
}

function chageprdAjax(siteurl) {
	
	if (typeof siteurl != 'undefined' ) {
		var v = '&1=1' ;
		
		
		$.ajax({
			url: siteurl + 'cart/prdupdate/',
			type:"post",
			data: v,
			beforeSend:function(){
				
				$("#cart-error").slideDown().html("Updating Cart...")
			},
			success:function(e){
				
				window.location.href = siteurl + 'cart'

				setTimeout(function(){ 
					location.reload();
				}, 1000);

				

			}  
		});
	} else {
		alert('Please refresh your page');
	}
}


function apply_promo(siteurl) {
	var promocode    = $("#promocode").val();
	var payment_type = $("#payment_type").val();
	var v = '&1=1' ;
	
	if ( $.trim(promocode) == 'ntrhr300' ) {
		
		if(payment_type == 1){
			
			$.ajax({
				url: siteurl + 'cart/prdupdate/',
				type:"post",
				data: v,
				beforeSend:function(){
					
					$("#cart-error").slideDown().html("Updating Cart...")
				},
				success:function(e){
					
					window.location.href = siteurl + 'cart'

					setTimeout(function(){ 
						location.reload();
					}, 1000);

					

				}  
			});
		}else{
			alert("Select Prepaid and apply Promo code. Not valid for COD.");
		}
	} else {
		alert('Promocode is not valid');
	}
}