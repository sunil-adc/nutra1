$('.sliderFor').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    fade: true,
    asNavFor: '.sliderNav'
});
$('.sliderNav').slick({
    asNavFor: '.sliderFor',
    dots: false,
    focusOnSelect: true,
    vertical: true,
    centerMode: true,
    centerPadding: '90px',
    slidesToShow: 2,
    responsive: [
        {
        breakpoint: 768,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 2
        }
        },
        {
        breakpoint: 480,
        settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
        }
        }
    ]

});