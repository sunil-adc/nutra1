function ajaxReg_hairoil(e,t){
	var n=true,r="";
	var s=new Array;
	s[0] = "name";
	s[1] = "mobile";
	s[2] = "email";
	s[3] = "address";
	s[4] = "city";
	s[5] = "state";
	s[6] = "pincode";
	
	
	for(i in s){
		r+="&"+s[i]+"="+$("#"+s[i]+t).val();
		if ($("#"+s[i]+t).val()==""){
			$("#"+s[i]+"_err"+t).stop().slideDown().html($("#"+s[i]+t).attr('data-attr')).delay(10e3).slideUp();
			$("#"+s[i]+t).css("border-color","red").focus();
			return false
		} else {
			$("#"+s[i]+t).css("border-color","#CFCFCF");
			$("#"+s[i]+"_err"+t).slideUp();
		}

		
		if(i==0){
			if(!$("#"+s[i]+t).val().match(/^[a-zA-Z\s]+$/) || $.trim($("#"+s[i]+t).val()) == ""){
				$("#"+s[i]+"_err"+t).stop().slideDown().html("Please enter correct name.").delay(10e3).slideUp();
				$("#"+s[i]+t).css("border-color","red").focus();
				return false
			}
		}
		
		if(i==1){
			if($("#"+s[i]+t).val().length!=10){
				$("#"+s[i]+"_err"+t).stop().slideDown().html("Mobile number must be 10 digits.").delay(10e3).slideUp();
				$("#"+s[i]+t).css("border-color","red").focus();
				return false
			}
		}

		if (i==2){
			if(!$("#"+s[i]+t).val().match(/^[\w]+([_|\.-][\w]{1,})*@[\w]{2,}([_|\.-][\w]{1,})*\.([a-z]{2,4})$/i)){
				$("#"+s[i]+"_err"+t).stop().slideDown().html("Enter valid Email Address.").delay(10e3).slideUp();
				$("#"+s[i]+t).css("border-color","red").focus();
				return false
			}
		}
		
		if(i == 4){
			
			if(!$("#"+s[i]+t).val().match(/^[a-zA-Z\s]+$/) || $.trim($("#"+s[i]+t).val()) == ""){
				$("#"+s[i]+"_err"+t).stop().slideDown().html("Please enter correct city.").delay(10e3).slideUp();
				$("#"+s[i]+t).css("border-color","red").focus();
				return false
			}
			
		}


		if(i==6){
			if($("#"+s[i]+t).val().length!=6){
				$("#"+s[i]+"_err"+t).stop().slideDown().html("Pincode must be 6 digits.").delay(10e3).slideUp();
				$("#"+s[i]+t).css("border-color","red").focus();
				return false
			}
		}

	}
	if(e!=""){
		r+="&redirectcart=1&mode=add&pid="+$("#pid").val()+"&oos="+$("#oos").val()
		
		if(typeof $("#siteurl").val()!="undefined"){
			var o=$("#siteurl").val()
		}
		r+="&form=submit";
		$.ajax({
			url:e,
			type:"post",
			data:r,
			beforeSend:function(){
				$("#register_btn"+t).val("Please wait...")
			},
			success:function(e){
				
				$("#register_btn"+t).attr("value","RUSH MY ORDER");
				if (e=="added" || e=="done"){
					
					window.location.href=o+"cart"
					
				} else if (e=="duplicate_mobile"){
					
					$("#global_err"+t).html("Mobile number already registered. Please login ");
					window.location.href=o+"login/?msg=mae";
				} else if (e=="validation_err"){
					$("#global_err"+t).stop().slideDown().html("Please fill valid information.").delay(10e3).slideUp()
				} else if (e=="oos"){
					$("#global_err"+t).stop().slideDown().html("Opps! Product is out of stock.").delay(10e3).slideUp()
				} else {
					$("#global_err"+t).stop().slideDown().html(e).delay(10e3).slideUp()
				}
			}
		})
	}
}