<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nutra Times Hair oil</title>
    <link href="./<?php echo S3_URL?>cdn/hairoil-lp/1/images/favicon.png" rel="icon" />  
    
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo S3_URL?>cdn/hairoil-lp/1/css/style.css">
    
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Playfair+Display&display=swap" rel="stylesheet">

    <!-- Taboola Pixel Code -->
    <script type='text/javascript'>
      window._tfa = window._tfa || [];
      window._tfa.push({notify: 'event', name: 'page_view', id: 1065107});
      !function (t, f, a, x) {
            if (!document.getElementById(x)) {
                t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
            }
      }(document.createElement('script'),
      document.getElementsByTagName('script')[0],
      '//cdn.taboola.com/libtrc/unip/1065107/tfa.js',
      'tb_tfa_script');
    </script>
    <noscript>
      <img src='https://trc.taboola.com/1065107/log/3/unip?en=page_view'
          width='0' height='0' style='display:none'/>
    </noscript>
    <!-- End of Taboola Pixel Code -->

</head>
<body>
    
    <!-- Header starts Here -->
    
    <header>
      <div class="notification-bar web_banner">
        <p>
          <strong>WARNING:-</strong> We currently have limited Bottles IN STOCK. Stocks sell out fast,
          <strong>act today!</strong>
          <a href="#registrationForm" class="btn btn-order go-to-form">Place My Order
            <i></i>
          </a>
        </p>
      </div>
    </header>
          
    <!-- Header ends Here -->

    <!-- Banner Starts Here -->

    <section id="banner">
      <div class="wrapper">
        <div class="bg_banner">
          <div class="container">
              <div class="col-md-12 mobile_hiden">
                  <div class="col-md-6 col-sm-6">
                    <!-- <img src="" alt="bottle" class="img-responsive"> -->
                  </div>
                  <div class="col-md-6 col-sm-6">
                      <!-- <div class="hero_text">
                        <div class="text"><h1><Strong>‘Nutra Times’ HAIR OIL</Strong></h1>
                          <h1>Powerful Plus</h1>
                          <h2><span class="price">Rs 699</span> <span class="strike"> <strike>2500</strike></span></h2>
                        </div>
                        <a href="#"><button class="btn buy_btn">Buy Now</button></a>
                      </div> -->
                      <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('http://www.amazepromos.com/policy/insurance/submit_frm','getinsured','1')">
                        <div class="col heading">
                          <h4>TELL US WHERE TO SEND</h4>                           
                      </div> 
                        <div class="form_items">       
                        
                            <div class="col">   
                            <div class="form-group">
                                <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                                <span class="help-block" id="name_err1"></span>
                            </div>
                        
                            <div class="form-group">
                                <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                                <span class="help-block" id="email_err1"></span>
                            </div>
                            
                            <div class="form-group">
                                
                                <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                                <span class="help-block" id="phone_err1"> </span>
                                
                            </div> 
            
                            <div class="form-group">
                                <input type="text" class="form-control datepicker" id="user_dob1" name="user_dob" placeholder="Address" autocomplete="off">
                                <span class="help-block" id="address_err"></span>
                            </div>
            
                            <div class="form-group">
                              <input type="text" class="form-control" id="city1" name="city" pattern="" placeholder="Enter City" data-attr="Please Enter Correct City">
                              <span class="help-block" id="city_err1"> </span>
                          </div>

                          <div class="form-group">
                            <select class="form-control" name="state" id="state1">
                              <option value="">Select State</option>
                              <option value="8">Andaman and Nicobar Islands</option>
                              <option value="5">Andra Pradesh</option>
                              <option value="6">Arunachal Pradesh</option>
                              <option value="7">Assam</option>
                              <option value="9">Bihar</option>
                              <option value="10">Chandigarh</option>
                              <option value="11">Chhattisgarh</option>
                              <option value="12">Dadar and Nagar Haveli</option>
                              <option value="13">Daman and Diu</option>
                              <option value="4">Delhi</option>
                              <option value="14">Goa</option>
                              <option value="15">Gujarat</option>
                              <option value="16">Haryana</option>
                              <option value="17">Himachal Pradesh</option>
                              <option value="18">Jammu and Kashmir</option>
                              <option value="19">Jharkhand</option>
                              <option value="2">Karnataka</option>
                              <option value="20">Kerala</option>
                              <option value="21">Lakshadeep</option>
                              <option value="22">Madya Pradesh</option>
                              <option value="1">Maharashtra</option>
                              <option value="23">Manipur</option>
                              <option value="24">Meghalaya</option>
                              <option value="25">Mizoram</option>
                              <option value="26">Nagaland</option>
                              <option value="27">Orissa</option>
                              <option value="29">Pondicherry</option>
                              <option value="28">Punjab</option>
                              <option value="30">Rajasthan</option>
                              <option value="31">Sikkim</option>
                              <option value="3">Tamil Nadu</option>
                              <option value="36">Telangana</option>
                              <option value="32">Tripura</option>
                              <option value="34">Uttar Pradesh</option>
                              <option value="33">Uttaranchal</option>
                              <option value="35">West Bengal</option>		
                            </select>
                            <span class="help-block" id="state_err1"> </span>
                          </div>

                          <div class="form-group">
                            <input type="number" class="form-control" id="pincode1" name="pincode" pattern="" placeholder="Enter Pincode" data-attr="Please Enter Correct Pincode">
                            <span class="help-block" id="city_err1"> </span>
                          </div>
                        <div class="submitbtncontainer">
                        <input type="submit" id="frm-sbmtbtn1" value="Rush My Order" name="submit" class="button">
                        </div>
                        <div class="secure_text">
                          SECURE 256-BIT SSL ENCRYPTION
                        </div>
                        <div class="secure">
                          <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/secure.jpg" alt="secure">
                        </div>
                        </div>
                        </div>
                     </form>
                      <!-- <div class="stamps"><img src="" alt="stamps" class="img-responsive"></div>         -->
                  </div>
              </div>
              <div class="col-sm-12 desktop-hidden">
                  
              </div>
                

          </div>

        </div>  
      </div>
    </section>

    <!-- banner Ends Here -->
    <div class="desktop-hidden">
      <form role="form" id="feedbackForm" class="feedbackForm" action="JavaScript:void(0)" onsubmit="insurance_jsfrm('http://www.amazepromos.com/policy/insurance/submit_frm','getinsured','1')">
        <div class="col heading">
          <h4>TELL US WHERE TO SEND</h4>                           
      </div> 
        <div class="form_items">       
        
            <div class="col">   
            <div class="form-group">
                <input type="text" class="form-control" id="name1" name="name" placeholder="Name" onkeyup="chck_valid('fname', 'Please enter correct firstname')" data-attr="Please enter correct firstname">
                <span class="help-block" id="name_err1"></span>
            </div>
        
            <div class="form-group">
                <input type="email" class="form-control" id="email1" name="email" placeholder="Email Address" onkeyup="chck_valid('email', 'Please enter correct email')" data-attr="Please enter correct email">
                <span class="help-block" id="email_err1"></span>
            </div>
            
            <div class="form-group">
                
                <input type="text" class="form-control only_numeric phone" id="phone1" name="phone" pattern="\d*" placeholder="Mobile Number" onkeyup="chck_valid('phone', 'Please enter correct Mobile Number')" data-attr="Please enter correct Mobile number">
                <span class="help-block" id="phone_err1"> </span>
                
            </div> 
  
            <div class="form-group">
                <input type="text" class="form-control datepicker" id="user_dob1" name="user_dob" placeholder="Address" autocomplete="off">
                <span class="help-block" id="address_err"></span>
            </div>
  
            <div class="form-group">
              <input type="text" class="form-control" id="city1" name="city" pattern="" placeholder="Enter City" data-attr="Please Enter Correct City">
              <span class="help-block" id="city_err1"> </span>
          </div>

          <div class="form-group">
            <select class="form-control" name="state" id="state1">
              <option value="">Select State</option>
              <option value="8">Andaman and Nicobar Islands</option>
              <option value="5">Andra Pradesh</option>
              <option value="6">Arunachal Pradesh</option>
              <option value="7">Assam</option>
              <option value="9">Bihar</option>
              <option value="10">Chandigarh</option>
              <option value="11">Chhattisgarh</option>
              <option value="12">Dadar and Nagar Haveli</option>
              <option value="13">Daman and Diu</option>
              <option value="4">Delhi</option>
              <option value="14">Goa</option>
              <option value="15">Gujarat</option>
              <option value="16">Haryana</option>
              <option value="17">Himachal Pradesh</option>
              <option value="18">Jammu and Kashmir</option>
              <option value="19">Jharkhand</option>
              <option value="2">Karnataka</option>
              <option value="20">Kerala</option>
              <option value="21">Lakshadeep</option>
              <option value="22">Madya Pradesh</option>
              <option value="1">Maharashtra</option>
              <option value="23">Manipur</option>
              <option value="24">Meghalaya</option>
              <option value="25">Mizoram</option>
              <option value="26">Nagaland</option>
              <option value="27">Orissa</option>
              <option value="29">Pondicherry</option>
              <option value="28">Punjab</option>
              <option value="30">Rajasthan</option>
              <option value="31">Sikkim</option>
              <option value="3">Tamil Nadu</option>
              <option value="36">Telangana</option>
              <option value="32">Tripura</option>
              <option value="34">Uttar Pradesh</option>
              <option value="33">Uttaranchal</option>
              <option value="35">West Bengal</option>		
            </select>
            <span class="help-block" id="state_err1"> </span>
          </div>

          <div class="form-group">
            <input type="number" class="form-control" id="pincode1" name="pincode" pattern="" placeholder="Enter Pincode" data-attr="Please Enter Correct Pincode">
            <span class="help-block" id="city_err1"> </span>
          </div>
        <div class="submitbtncontainer">
        <input type="submit" id="frm-sbmtbtn1" value="Rush My Order" name="submit" class="button">
        </div>
        <div class="secure_text">
          SECURE 256-BIT SSL ENCRYPTION
        </div>
        <div class="secure">
          <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/secure_mobile.jpg" alt="secure">
        </div>
        </div>
        </div>
      </form>
    </div>
    


    <div class="clearfix"></div>

      <!-- product details Starts Here -->


    <section id="details">
        <div class="container">
            <div class="col-md-12">
                <div class="wrapper">
                    <div class="col-md-12 textcentre">
                      <h1 id="details">Product Details</h1>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-6 product">
                          <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/bottle.png" alt="" class="img-responsive" data-toggle="modal" data-target="#myModal">                         
                           
                        </div>

                        <div class="col-md-6">
                            <div class="text_container margin70">
                                <h1 class="title">Nutratime Hair Oil</h1>
                                <h3>Nutratimes Anti Hairfall Oil + Shampoo</h3>
                                <span class="reviews blue">(892+ Positive Customer Reviews)</span>
                                <h2><span class="price">Rs 1289</span> <span class="strike"><strike>Rs.2998 </strike></span></h2>
                                <div class="divtext"><span class="icon"><img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/local_shipping.png" alt="Shipping">&nbsp </span>  Cash On Delivery [COD] available for Rs 1499</div>
                                <div class="divtext"><span class="icon"><img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/flight_takeoff.png" alt="Shipping"> &nbsp</span> <span>Seamlessly Quick Delivery In 6 Hours For Eligible Pincode</span>
                                </div>
                                  
                                  <div class="divtext">Normal Delivery in 2 to 4 days</div>
                                  <a href="#"><button class="btn buy_btn btn-grad">Place My Order</button></a><span class="alertTxt"> ( Hurry Only Few Left! )</span>
                                 
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                  <h3>description</h3>
                                </div>

                                <div class="card-body">
                                  <div class="blockquote mb-0">
                                    <p class="lineheight1_6">
                                      NUTRATIMES brings to you a revolutionary hair product to help you with balding scalp and hair loss. Nutratimes hair regrowth oil & shampoo combo is formulated especially for people suffering from hair loss and balding due to various reasons including genetic factors. The oil contains the natural goodness of BRAHMI, ASHWAGANDHA and BHRINGARAJ etc. These herbs stop hair fall and thinning of hair, and in return helps in hair regrowth and increasing its density. The shampoo contains ALOEVERA, TULASI, AMLA, NEEM and HIBISCUS which leaves your hair feeling fresh and replenished! For best results, it is recommended to use this combo of oil and shampoo provided by Nutratimes.</p>
                                    
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
      <!-- product details ends Here -->

    <div class="clearfix"></div>

      <!-- customer reviews section starts here  -->
    <section id="reviews" name="reviews">
      
      <div class="container ">

                <div class="col-md-12 marginb">
                      <div class="wrapper">

                        <div class="col-md-12">
                          <div class="heading textcentre">
                            <h1>Customer Reviews</h1>
                            <p>what our Customers Say About Us</p>
                        </div>

                        </div>

                        <div class="col-md-12">
                          <div class="overall_rating">
                              <h3>Nutratimes Hair oil Reviews</h3>
                              <p><strong>Average Customer Review</strong></p>
                              <div class="stars">
                                  <span class="yellow"><i class="fa fa-star" style="font-size:24px"></i></span>
                                  <span class="yellow"><i class="fa fa-star" style="font-size:24px"></i></span>
                                  <span class="yellow"><i class="fa fa-star" style="font-size:24px"></i></span>
                                  <span class="yellow"><i class="fa fa-star" style="font-size:24px"></i></span>
                                  <span class="yellow"><i class="fa fa-star-half-o" style="font-size:24px"></i></span>  
                                 <span class="blue">(892+ Positive Customer Reviews)</span>
                              </div>

                            <div class="individual_Rating">

                              <div id="5star" class="mt10">
                                 
                                <div class="stars">
                                    <span class="fs16"> 5 Star: </span>
                                    <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                    <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                    <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                    <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                    <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span> 
                                     <span class="blue">(315)</span>
                                </div>
                              </div>

                              <div id="4star" class="mt10">
                                  <div class="stars">
                                      <span class="fs16"> 4 Star: </span> 
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span> 
                                       <span class="blue">(448)</span>
                                  </div>

                              </div>

                              <div id="3star" class="mt10">
                                  <div class="stars">
                                      <span class="fs16"> 3 Star: </span> 
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span> 
                                       <span class="blue">(118)</span>
                                  </div>

                              </div>

                              <div id="2star" class="mt10">
                                  
                                  <div class="stars">
                                      <span class="fs16"> 2 Star: </span> 
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span> 
                                       <span class="blue">(10)</span>
                                  </div>
                              </div>

                              <div id="1star" class="mt10">
                                  
                                  <div class="stars">
                                      <span class="fs16"> 1 Star: </span> 
                                      <span class="yellow"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span>
                                      <span class="gray"><i class="fa fa-star" style="font-size: 18px;"></i></span> 
                                       <span class="blue">(1)</span>
                                  </div>

                              </div>

                            </div>
                          </div>
                        </div>
                        <div class="col-md-12 customer_reviews">
                          <div class="wrapper">

                            <div class="col-md-12 customer">

                              <div class="col-md-1">
                                <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/dev_commentor.jpeg" alt="commentor" class="img-responsive mt10">
                              </div>

                              <div class="col-md-11">
                                <span class="user_name">Dev</span> <span class="date">(12/07/2019) </span> <span class="stars yellow"><i class="fa fa-star"> <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i>
                                  <i class="fa fa-star"></i></i></span>
                                  <p class="mt10 lineheight1_6"> I was tired of being called baldy, baldy! Thank you Nutratimes for giving me this amazing product to help battle hair loss and boost hair growth.</p>
                              </div>
                            </div>

                            <div class="col-md-12 customer">
                                <div class="col-md-1">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/Sarah_commentor.jpeg" alt="commentor" class="img-responsive mt10">
                                </div>

                                <div class="col-md-11">
                                  <span class="user_name">Sara</span> <span class="date">(12/07/2019)</span> <span class="stars yellow"><i class="fa fa-star"> <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i></i></span>
                                    <p class="mt10 lineheight1_6">Have tried all types of hair oils and shampoos. From no sulphate and parabens to 100% natural to 95% organic and so on but nothing has worked except Nutratimes Anti Hairfall shampoo and oil. I have no hair fall episodes now!</p>
                                </div>

                              </div>

                              <div class="col-md-12 customer">
                                  <div class="col-md-1">
                                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/Arjun_commentor.jpeg" alt="commentor" class="img-responsive mt10">
                                  </div>

                                  <div class="col-md-11">
                                    <span class="user_name">Arjun</span> <span class="date">(12/07/2019)</span> <span class="stars yellow"><i class="fa fa-star"> <i class="fa fa-star"></i>
                                      <i class="fa fa-star"></i>
                                      <i class="fa fa-star"></i>
                                      <i class="fa fa-star"></i></i></span>
                                      <p class="mt10 lineheight1_6">My hair was thinning to a rate where I could feel my scalp. I tried everything but nothing worked. Some products just made the problem worse. Thank you Nutratimes for giving me this natural therapy for my hair</p>
                                  </div>

                              </div>

                              <div class="col-md-12 customer">
                                <div class="col-md-1">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/Arya_commentor.jpeg" alt="commentor" class="img-responsive mt10">
                                </div>

                                <div class="col-md-11">
                                  <span class="user_name">Arya</span> <span class="date">(12/07/2019)</span> <span class="stars yellow"><i class="fa fa-star"> <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i></i></span>
                                    <p class="mt10 lineheight1_6"> No more hair fall, split ends and dryness. Thank you Nutratimes hairoil Shampoo</p>
                                </div>

                              </div>

                              <div class="col-md-12 customer">
                                <div class="col-md-1">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/Farhad_commentor.jpeg" alt="commentor" class="img-responsive mt10">
                                </div>

                                <div class="col-md-11">
                                  <span class="user_name">Farhad</span> <span class="date">(12/07/2019)</span> <span class="stars yellow"><i class="fa fa-star"> <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i></i></span>
                                    <p class="mt10 lineheight1_6"> It is an all-in-one hair care and hair loss solution, I must say. It is my best bet!</p>
                                </div>

                              </div>

                              <div class="col-md-12 customer">
                                <div class="col-md-1">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/Priya_commentor.jpeg" alt="commentor" class="img-responsive mt10">
                                </div>

                                <div class="col-md-11">
                                  <span class="user_name">Priya</span> <span class="date">(12/07/2019)</span> <span class="stars yellow"><i class="fa fa-star"> <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i></i></span>
                                    <p class="mt10 lineheight1_6">Hair fall can kill self-confidence. I had a very bad case of hair fall during pregnancy and after delivering my first baby. One of my friends told me about Nutratimes Haioil & Shampoo and from then on I started using it. Great results! I suggest this hair oil to all new mothers who are facing hair fall problems.</p>
                                </div>

                              </div>

                              <div class="col-md-12 customer">
                                <div class="col-md-1">
                                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/1/images/sunil_commentor.jpeg" alt="commentor" class="img-responsive mt10">
                                </div>

                                <div class="col-md-11">
                                  <span class="user_name">sunil</span> <span class="date">(12/07/2019)</span> <span class="stars yellow"><i class="fa fa-star"> <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i></i></span>
                                    <p class="mt10 lineheight1_6">A foolproof product for hair loss and thinning hair. With this you don't have to worry about spending thousands on costly hair treatments!</p>
                                </div>

                              </div>

                          </div>
                        </div>
                      </div>
                </div>
      </div>
    </section>
    <div class="clearfix"></div>
      <!-- footer starts here  -->
    <footer>
      <div class="footer1">
        <div class="container">
          <div class="col-md-12">
              <div class="wrapper">

                <div class="col-md-12">
                    <p class="note"><strong>Note :</strong> These statements have not been evaluated by the Food and Drug Administration or any government agency. This product is not intended to diagnose, treat, cure, or prevent disease. As individuals differ, so will results.</p>
                </div>

                <div class="col-md-12">

                  <div class="nav_links">
                      <a href="https://www.buynutratimes.com/privacy-policy" class="open-popup-link">Privacy Policy </a> |
                      <a href="https://www.buynutratimes.com/terms-conditions" class="open-popup-link">Terms & Condition  </a> |
                      <a  href="https://www.buynutratimes.com/return-policy" class="open-popup-link">Return Policy   </a> |
                      <a  href="https://www.buynutratimes.com/disclaimer-policy" class="open-popup-link">Disclaimer Policy </a> |
                      <a href="https://www.buynutratimes.com/refund-policy" class="open-popup-link">Refund Policy  </a> |
                      <a href="https://www.buynutratimes.com/shipping-policy" class="open-popup-link">Shipping Policy</a>
                  </div>

                </div>

                <div class="col-md-12 mtb">
                    
                    <div class="col-md-12 col-xs-6 textcentre">
                      <h3>Contact us</h3>
                      <p class="lineheight1_6"> Email Us: <a href="#">support@nutratimes.com</a> <br>
                          Call Us: +91 08046328320 </p>
                    </div>
                    
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="footer2">
        <p>© 2019 <a href="https://www.nutratimes.com/" target="_blank"> nutratimes.com </a> | &nbsp  All rights reserved</p>
      </div>

    </footer>
    
    <!---Scripts------>
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>  


</body>
</html>