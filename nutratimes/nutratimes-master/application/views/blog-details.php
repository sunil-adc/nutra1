	<div class="container">
		<div class="row">
			<div id="content" class="col-md-9 col-sm-8 mb-xs-30 single-blog">

				<?php if(is_array($blogdetails) && count($blogdetails) > 0){ 
					foreach($blogdetails as $val){ ?>
					
					<div class="single-blog-content">
						<h1 class="post-title" style="    color: #2b5600; font-size:28px; margin-bottom:15px;"><b><?php echo $val->title?></b></h1>
							
						<div class="blog-img">
							<img src="<?php echo S3_URL.BLOG_IMAGE_FOLDER.strtolower($val->blog_image) ?>" alt="<?php echo $val->title?>" title="<?php echo $val->title ?>"  />
						</div>
						<div class="blog-detail mb-60">
							<?php echo $val->blogcontent ?>
							<hr>
						</div>
					</div>
			<?php 
					}	
				} 
			?>
			</div>
			<aside id="column-right" class="col-md-3 col-sm-4 hidden-xs sidebar-block" 
			style="position: sticky; top: 50px;">
				<a href="<?php echo SITE_URL."products" ?>" class="side-add">
					<img style="border: 1px solid #81af4d40;" src="<?php echo SITE_URL?>cdn/image/nutratimes-ad1.jpg" />
				</a>
			</aside>
		</div>
	</div>
