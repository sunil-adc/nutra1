<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
         </div>
         
         <?php
         if(isset($union_qry))
		 {
			 foreach($union_qry as $val)
			 {
				if($val->active_cnt == 'active_cnt')
					$active_cnt = $val->active;
				else if($val->active_cnt == 'pending')
					$pending = $val->active;
				else if($val->active_cnt == 'in_active_cnt')
					$in_active_cnt = $val->active;
				else if($val->active_cnt == 'viewed_cnt')
					$viewed_cnt = $val->active;	
				else if($val->active_cnt == 'replied_cnt')
					$replied_cnt = $val->active;	
			 }
		 }
		 
		 
		 ?>

       <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong>Search Keyword :</strong>
                    <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Search Field :</strong> 
                    <select name="search_field" id="search_field">
                        
                        
                        <option value="mobile" <?php echo ($search_field == "mobile") ? "selected='selected'" : ""; ?>>Mobile</option>
                        <option value="email" <?php echo ($search_field == "email") ? "selected='selected'" : ""; ?>>Email</option>
                        <option value="complaint" <?php echo ($search_field == "complaint") ? "selected='selected'" : ""; ?>>Complaint</option>
                    </select>
                    &nbsp;&nbsp;  
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
                <table>
                <thead>
        	<tr>
                <th align="right" width="10%">Pending</th>
                <th width="10%"><span class='color_green_bg'><?php echo $pending; ?></span></th>
                <th width="10%" align="right">Viewed</th>
                <th width="10%"><span class='color_green_bg'><?php echo $viewed_cnt; ?></span></th>
                <th width="10%" align="right">Replied</th>
                <th width="10%"><span class='color_green_bg'><?php echo $replied_cnt; ?></span></th>
          
                <th width="10%" align="right">Active</th>
                <th width="10%"><span class='color_green_bg'><?php echo $active_cnt; ?></span></th>
                <th width="10%" align="right">In-Active</th>
                <th width="10%"><span class='color_green_bg'><?php echo $in_active_cnt; ?></span></th>
                
            </tr>
            </thead>
        </table>
        <table>
        <thead>
          <tr>            
       		<th width="5%" nowrap="nowrap">#</th>
            <th width="10%" nowrap="nowrap">Mobile</th>
            <th width="20%" nowrap="nowrap">Email ID</th>
            <th width="25%" nowrap="nowrap">Complaints</th>
            <th width="15%" nowrap="nowrap">Date Created</th>
            <th width="10%" nowrap="nowrap">Complaint Status</th>
            <th width="10%" nowrap="nowrap">Reply</th>
            <th width="5%" nowrap="nowrap">Status</th>           
          </tr>
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {                 
				  $i = 1;
				  
		
			
				  foreach ($results as $key => $val) {
                        ?>
                        <tr>
                        	
                           <td><?php echo $i; ?></td>
                           <td><?php echo $val->mobile; ?></td>
                           <td><?php echo $val->email; ?></td>
                           <td><?php echo substr($val->complaint,0,100); ?></td>
                           <td><?php echo (strtotime($val->datecreated) > 0) ? time_diff ($val->datecreated) : "Not Available";?></td>
                           
                            <td><?php 
							if($val->complaint_status == 1)
							{
								echo "<span class='color_red_bg'>Pending</span>";
							}
							else if($val->complaint_status == 2)
							{
								echo "<span class='color_yellow_bg'>Viewed</span>";
							}
							else if($val->complaint_status >= 3)
							{
								echo "<span class='color_green_bg'>Replied</span>";
							}
							
							 ?></td>
                             <td>
                             <a href="<?php echo FULL_CMS_URL."/".strtolower($add_page)."/index/edit/".$val->$primary_field."/".$offset;?>/#atab" ><img src="<?php echo CMS_URL;?>images/icons/pencil.png" alt="Edit" /></a>
                             </td>
                             
                            <td id="status_<?php echo $i;?>"><a href="javascript:change_status('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','status','status_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->status == '1') ? "<span class='bg_green'>Active</span>" : "<span class='bg_red'>Inactive</span>"; ?></a></td>
                           
                       </tr>
                       <?php
						$i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="8" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>