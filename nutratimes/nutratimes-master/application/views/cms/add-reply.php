<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab align-left complain-box">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
          <label><span class="color_red">*</span> <span class="color_blue">Reply</span> </label>
          <textarea name="complaint"  class="text-input big-input" id="complaint" placeholder="Reply" ></textarea>
        </p>
        <p>
        <?php
		  if($customer_complaint_status <= 3)
		  {
		  ?>
          <span id="com_sts">
           <label><span class="color_red">*</span> <span class="color_blue">Ressolved Status</span> </label>
          <select name='complaint_status' id='complaint_status' class="text-input small-input">
          <option value="">Select</option>
          <option value="1">YES</option>
          <option value="0">NO</option>
          </select>
          
          <!--<input type="button" name="complaint_status" id="complaint_status" class="button button_resolve" value="Click this If the Complaint is resolved" onclick="javascript:change_resolved_status('true','<?php echo $complaint_id;?>','<?php echo FULL_CMS_URL;?>/<?php echo $add_page;?>/ajaxresolvedstatus')"   > 
-->          </span>
          <?php
		  }
		  
		  ?>
       
            
          
        </p>
        
        
       	<?php  
			echo form_hidden('mode',  'add' );
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden('complaint_id', $complaint_id);			
			echo form_hidden('name',  $customer_name );
			echo form_hidden('mobile', $customer_mobile);
			echo form_hidden('email', $customer_email);		
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo CMS_URL."/".$manage_page."/index/10/".$offset;?>'" type="button" value="Go Back" />  
         
        </p>
        <?php
		  if($customer_complaint_status > 3)
		  {
			  echo "This Complaint has been Ressolved";
		  }
		  ?>
		 
			  
        <p>
        
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
    
    <?php
	echo '<div class="complain-chat align-right">';
    if(isset($parent_history)) {
		
		foreach($parent_history as $val) {
			
			if($val->replied_employee) {
				?>
                 <div class="chat-box">
                    <div class="seajin-chat-box align-left">
                            <?php echo $val->complaint; ?>
                        <div class="clear"></div>
                        <div class="align-right complain-time"><?php echo  time_diff ($val->datecreated) ; ?></div>
                    </div>
                    <div class="left-triangle seajin-image align-right">
                         <a href="javascript:void(0)" title="<?php echo $val->username; ?>"><?php echo sub_string($val->username, 10); ?></a>
                    </div>
                    <div class="clear"></div>
                 </div>
			<?php
			} else {
			?> 
                <div class="chat-box">
                    <div class="customer-image align-left">
                        <a href="javascript:void(0)" title="<?php echo urldecode($customer_name); ?>"><?php echo sub_string(urldecode($customer_name), 10); ?></a>
                    </div>
                    <div class="customer-chat-box align-right">
                        <?php echo $val->complaint; ?>
                        <div class="clear"></div>
                        <div class="align-right complain-time"> <?php echo  time_diff ($val->datecreated) ; ?></div>
                    </div>  
                  </div>
            
			<?php
			}			
			
		
		}
		?>
        
	<?php	
	}
?>

 <div class="chat-box">
                    <div class="customer-image align-left">
                    	<a href="javascript:void(0)" title="<?php echo urldecode($customer_name); ?>"><?php echo sub_string(urldecode($customer_name), 10); ?></a>
                    </div>
                    <div class="customer-chat-box align-right">
                        <?php echo $customer_complaint; ?>
                        <div class="clear"></div>
                        <div class="align-right complain-time"> <?php echo  time_diff ($customer_date) ; ?></div>
                    </div>  
                  </div>
</div>
    	<div class="clear"></div>
  </div>
</div>
<div class="clear"></div>