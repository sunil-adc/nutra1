<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <p>
            <label><span class="color_blue">Name : </span>&nbsp;&nbsp;<?php echo $result_data['name'];?></label> 
        </p>
        <p>
            <label><span class="color_blue">Email : </span>&nbsp;&nbsp;<?php echo $result_data['email'];?></label>
        </p>
        <p>
            <label><span class="color_blue">Mobile : </span>&nbsp;&nbsp;<?php echo $result_data['mobile'];?></label>
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Payment Status : </span></label>
			<select name="payment_status" id="payment_status" class="text-input small-input">
            	<option value="">Select Option</option>
                <option value="1" <?php echo ($result_data['payment_status'] == 1) ? 'selected="selected"' : "";?>>Paid </option>
                <option value="0" <?php echo ($result_data['payment_status'] != 1) ? 'selected="selected"' : "";?>>Not Paid</option>
            </select> 
        </p>
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Payment Check Status : </span></label>
			<select name="payment_check_status" id="payment_check_status" class="text-input small-input">
            	<option value="">Select Option</option>
                <option value="1" <?php echo ($result_data['payment_check_status'] == 1) ? 'selected="selected"' : "";?>>Checked </option>
                <option value="0" <?php echo ($result_data['payment_check_status'] != 1) ? 'selected="selected"' : "";?>>Not Checked</option>
            </select> 
        </p>
       	<?php
            echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Changes" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
