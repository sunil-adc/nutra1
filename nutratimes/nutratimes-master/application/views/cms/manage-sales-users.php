<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
       <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
       </div>
      <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong>Search Keyword :</strong>
                    <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Search Field :</strong> 
                    <select name="search_field" id="search_field">
                        <option value="username" <?php echo ($search_field == "username") ? "selected='selected'" : ""; ?>>Username</option>
                        <option value="fname" <?php echo ($search_field == "fname") ? "selected='selected'" : ""; ?>>First Name</option>
                        <option value="lname" <?php echo ($search_field == "lname") ? "selected='selected'" : ""; ?>>Last Name</option>
                        <option value="email" <?php echo ($search_field == "email") ? "selected='selected'" : ""; ?>>Email</option>
                    </select>
                    &nbsp;&nbsp;  
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        <table>
        <thead>
          <tr>
       		<th width="16%">Username</th>
            <th width="15%">Role Name</th>
            <th width="15%">Email</th>
            <th width="15%">Full Name</th>
            <th width="10%">Created</th>
            <th width="10%">Updated</th>
            <th width="2%">Edit</th>
          </tr>
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {
                   $i = 1;
				   foreach ($results as $key => $val) {
                        ?>
                        <tr>
                            <td><?php echo $val->username; ?></td>
                            <td><a href="<?php echo FULL_CMS_URL."/addrole/index/edit/".$val->admin_role_id;?>/#atab"><?php echo $val->role_name; ?></a>
                            </td>
                            <td><?php echo $val->email; ?></td>
                            <td><?php echo $val->fname." ".$val->lname; ?></td>
                            <td><?php echo (strtotime($val->datecreated) > 0) ? time_diff ($val->datecreated) : "Not Available";?></td>
                            <td><?php echo (strtotime($val->dateupdated) > 0) ? time_diff ($val->dateupdated) : "Not Updated";?></td>
                            <td width="9%"><a href="<?php echo FULL_CMS_URL."/".strtolower($add_page)."/index/edit/".$val->$primary_field?>/#atab" title="Edit <?php echo $val->username; ?>"><img src="<?php echo CMS_URL;?>images/icons/pencil.png" alt="Edit" /></a></td>
                       </tr>
                       <?php
                    $i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="7" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>