<?php 
$arr_all = all_arrays(); ?>
<script>
	$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			inline: true
		});
	});
	
	function checkPubForm() {
		if ($("#pubs").val() == '') {
			$("#search").attr("action", "http://www.shophunk.com/cms/reportnetwise/page/");
		}
	}

</script>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <?php echo $label_from_to_date; ?>
        <div class="tab-content default-tab">
        <span id="quick_upd_msg"></span>
		<?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
			
	   ?>
       
         <form name="search" id="search" action="<?php echo $seach_form; ?>"  onsubmit="javascript:return checkPubForm()" method="post">
          <table>
            <tr>
                <td>
                    <strong><span class="color_red">*</span> From :</strong>
                    <input type="text" name="from_date" value="<?php echo (isset($from_date)) ? $from_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:10% !important" />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong><span class="color_red">*</span> To :</strong>
                    <input type="text" name="to_date" value="<?php echo (isset($to_date)) ? $to_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:10% !important"  />&nbsp;&nbsp;
                    
                    <strong><span class="color_red">*</span> Network :</strong>
                    <?php echo $net; ?>
                    
                    <strong><span class="color_red">*</span> Pubs :</strong>
                    <span id="ajaxPublisher">
                    	<select name="pubs" id="pubs" class="small-input" style="width:10% !important"><option value="">N/A</option></select>
                    </span>
                    
                    <strong><span class="color_red">*</span> Timezone :</strong>
                    <select name="timezone" id="timezone" class="text-input small-input datepicker" style="width:10% !important">
                    	<option value="0" <?php echo (isset($timezone) && $timezone == '0') ? 'selected' : '';?>>Asia/Calcutta</option>
                        <option value="-34200" <?php echo (isset($timezone) && $timezone == '-34200') ? 'selected' : '';?>>America/New_York</option>
                    </select>
                    
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        
        <table>
        <thead>
           <tr>
		    <th width="10%">Publisher</th>
		    <th style="text-align:center" width="5%">Pincode</th>
            <th style="text-align:center" width="5%">Type</th>
            <th style="text-align:center" width="5%">Goal</th>
            <th style="text-align:center" width="10%">Total Leads</th>
            <th style="text-align:center" width="10%">Leads Fired</th>
			<th style="text-align:center" width="10%">Total Sale</th>
            <th style="text-align:center" width="10%">Total Spent</th>
			<th style="text-align:center" width="10%">Total Revenue</th>
			<th style="text-align:center" width="10%">Total Margin</th>
			<th style="text-align:center" width="5%">Lead to Conversion</th>
			<th style="text-align:center" width="10%">CPS</th>
		  </tr>
        </thead>
        <tbody>
            <?php
			 $i = 1;
               if (isset($results)) {
				   if (count($final_pubname) > 0) {
				   		for($j = 0; $j < count($final_pubname); $j++) {
					   ?>
						<tr valign="middle">
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo $final_pubname[$j];?></td>
                            <td style="vertical-align:middle;text-align:center"><?php echo $pincode_type[$j];?>X</td>
                        	<td style="vertical-align:middle;text-align:center">
                        	<span class="<?php echo $handle_type[$j] == '1' ? 'color_green_bg': 'color_red_bg';?>">
								<?php echo $handle_type[$j] == '1' ? 'Manual': 'Automatic';?></span></td>
                        	<td style="vertical-align:middle;text-align:center"><?php echo $goal[$j];?></td>
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo $final_leads[$j];?></td>
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo $final_lead_fired[$j]; ?></td>
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo $final_sale[$j];?></td>
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo $final_spent[$j];?></td>
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo $final_revenue[$j];?></td>
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo $final_margin[$j];?></td>
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo $final_lead_to_conversion[$j];?> %</td>
							<td style="display:table-cell;vertical-align:middle;text-align:center"><?php echo round($final_cps[$j]);?></td>
					   </tr>
                       <tr>
                            <td colspan="12" class="limited_updation">
                                <form action="javascript:updateQuickPub('<?php echo $i?>')" class="quick_net_update" method="post">
                                    <div class="main_quick">
                                        <div class="align-left quick_pincode_type">
                                        Pincode : <?php
                                            if($sel_pp != false) {
                                                echo '<select name="pincode_type_'.$i.'" id="pincode_type_'.$i.'" >';
                                                foreach($sel_pp->result() as $v) {
                                                    $selected = $v->id == $pincode_type[$j] ? 'selected' : '';
                                                    echo '<option value="'.$v->id.'" '.$selected.'>'.$v->id.'X</option>';	
                                                }
                                                echo '</select>';
                                            }
                                       ?> 
                                        </div>
                                        <div class="align-left quick_handle_type">
                                        Handle : <select name="handle_type_<?php echo $i?>" id="handle_type_<?php echo $i?>" >
                                            <option>Select Option</option>
                                            <option value="1" <?php echo $handle_type[$j] == '1' ? 'selected' : ''; ?>>Manual</option>
                                            <option value="2" <?php echo $handle_type[$j] == '2' ? 'selected' : ''; ?>>Automatic</option>
                                        </select>    
                                        </div>
                                        <div class="align-left quick_fire_type">
                                            Fire Type :<select name="fire_type_<?php echo $i?>" id="fire_type_<?php echo $i?>" >
                                                <option value="0" <?php echo (isset($fire_type[$j]) && $fire_type[$j] == '0') ? "selected='selected'" : "";?>>
                                                Currency Basis</option>
                                                <option value="1" <?php echo (isset($fire_type[$j]) && $fire_type[$j] == '1') ? "selected='selected'" : "";?>>
                                                Percentage Basis</option>
                                            </select>
                                        </div>
                                        <div class="align-left quick_goal">
                                            Goal : <input type="text" id="goal_<?php echo $i?>" name="goal_<?php echo $i?>" class="quick_txt" value="<?php echo $goal[$j]?>" />
                                        </div>
                                        <div class="align-left quick_upd_btn">
                                            <input class="button" type="submit" id="sbmt_btn_<?php echo $i?>" name="sbmt_btn_<?php echo $i?>" value="SAVE" />
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                    <input type="hidden" id="search_form" name="search_form" value="<?php echo $seach_form; ?>" />
                                    <input type="hidden" id="data_id" name="data_id" value="<?php echo $i?>" />
                                    <input type="hidden" id="pub_name_<?php echo $i?>" name="pubname" value="<?php echo $final_pubid[$j];?>" />
                                    <input type="hidden" id="primary_id_<?php echo $i?>" name="primary_id" value="id" />
                                    <input type="hidden" id="primary_id_value_<?php echo $i?>" name="primary_id_value" value="<?php echo $id[$j];?>" />
                                    <input type="hidden" id="from_date_<?php echo $i?>" name="from_date" value="<?php echo $from_date;?>" />
                                    <input type="hidden" id="to_date_<?php echo $i?>" name="to_date" value="<?php echo $to_date;?>" />
                                    <input type="hidden" id="net_id_<?php echo $i?>" name="net_id" value="<?php echo $net_id;?>" />
                                    <input type="hidden" id="siteurl" name="siteurl" value="<?php echo FULL_CMS_URL;?>" />
                                </form>
                            </td>
                       </tr>
					   <?php
						$i++;
						}
				   }
			   } else {
					?>
					<tr>
                    	<td colspan="12" style="text-align:center">Start generating <?php echo $page_name;?></td>
                    </tr>
                    <?php
			   }
            ?>
        </tbody>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>
<?php
	if (isset($net_current) && $net_current != '') {
		?>
		<script type="text/javascript">
			$(document).ready(function(e) {
				$("#netGetPub").trigger("change");
			});
		</script>
		<?php
	}
?>