<?php $arr_all = all_arrays(); echo $from_date; echo $net;  ?>
<script>
  $(function() {
    $( ".datepicker" ).datepicker({
      dateFormat: 'yy-mm-dd',
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      inline: true
    });
  });
</script>
<script type="text/javascript">
  

  function change_status(status_value, lead_id, site_url){

    var status_val = status_value.value;
    
    if(status_val.value != "" && lead_id !="" ){


        $.ajax({
            url:site_url+"/smallform_lead/update_lead_status/"+lead_id+"/"+status_val,
            type:"post",
            data: "&name=status",
            beforeSend:function(){
              
            },
            success:function(e){
              
              if(e == "done"){

                alert("Status Updated");
                $("#lead_status_"+lead_id).val(status_val);

              
              }else{
                 
                 alert(e);

              }
            }
          
          });


    }else{
      alert("Lead id Error");
    }
  }

</script>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?>  Total - <?php echo $total_rows ?> </h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
      if( $this->session->flashdata('error') ) { 
        echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
      }else if( $this->session->flashdata('success') ) { 
        echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
      }
     ?>
       
         <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong><span class="color_red">*</span> From :</strong>
                    <input type="text" name="from_date" value="<?php echo (isset($from_date) && $from_date != "")   ? $from_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:15% !important" />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong><span class="color_red">*</span> To :</strong>
                    <input type="text" name="to_date" value="<?php echo (isset($to_date) && $to_date != "" ) ? $to_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:15% !important"  />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong>Utm Source :</strong>
                    <input type="text" name="net" value="<?php echo ($net != 0 && $net != '0' ? $net : $net); ?>" class="text-input small-input" autocomplete="off" style="width:15% !important" />
                    &nbsp;&nbsp;
                    
                    <select name="status" id="status" class="text-input small-input" style="width:15% !important" >
                      <option value=""> Status </option>
                      <option value="1" <?php echo $status == 1 ? 'selected' : ''  ?> > New Lead</option>
                      <option value="2" <?php echo $status == 2 ? 'selected' : ''  ?> > Follow Up</option>
                      <option value="3" <?php echo $status == 3 ? 'selected' : ''  ?> > Rejected</option>
                      <option value="4" <?php echo $status == 4 ? 'selected' : ''  ?> > Order Created </option>
                      <option value="5" <?php echo $status == 5 ? 'selected' : '' ?> > Reviewed  </option>
                    </select> 
                    <br><br/>
                    <strong>Phone :</strong>
                    <input type="text" name="phone" value="<?php echo ($phone != 0 && $phone != '0' ? $phone : $phone); ?>" class="text-input small-input" autocomplete="off" style="width:15% !important" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    

                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />&nbsp;&nbsp;&nbsp;

                    <input type="hidden" id="sel_query" name="sel_query" value="<?php echo $sel_query?>" />  

                    <input type="submit" class="button" id="download" name="download" value="Export" formaction="<?php echo $download_form?>" style="background: #959a8f">
                </td>
            </tr>        
          </table>
        </form>
        
        <table>
        <thead>
        <tr>
        <th width="5%">S no</th>
        <th width="10%">Name</th>
        <th width="10%">Phone</th>
        <th width="10%">Utm source</th>
        <th width="10%">status</th>
        <th width="10%">Create Order</th>
        <th width="10%">Date</th>
      </tr>
        </thead>
        <tbody>
            <?php
      
          if (isset($results)) {
             $srlno = 1;

            foreach ($results as $k ) {
              ?>
                 <tr valign="middle">
                 <td><?php echo $srlno ?></td>
                 <td><?php echo $k->name ?></td>
                 <td><?php echo $k->phone ?></td>
                 <td><?php echo $k->utm_source ?></td>
                 <td >
                   <select name="lead_status" id="lead_status_<?php echo $k->id?>" class="lead_status" onchange="change_status(this, '<?php echo $k->id?>', '<?php echo FULL_CMS_URL?>')" >
                     <option value="1" <?php echo ($k->status ==1 ) ? 'selected' : '' ?> >New Lead </option>
                     <option value="2" <?php echo ($k->status ==2 ) ? 'selected' : '' ?> >Follow Up </option>
                     <option value="3" <?php echo ($k->status ==3 ) ? 'selected' : '' ?> >Reject  </option>
                     <option value="4" <?php echo ($k->status ==4 ) ? 'selected' : '' ?> >Order Created  </option>
                     <option value="5" <?php echo ($k->status ==5 ) ? 'selected' : '' ?> >Reviewed  </option>
                   </select>
                 </td>
                 <td><a href="<?php echo FULL_CMS_URL.'/createorder?name='.$k->name.'&email='.$k->email.'&phone='.$k->phone.'&address='.$k->address.'&net='.$k->utm_source ?>" target="_blank">Click</a></td>
                 <td><?php echo $k->date_created ?></td>
                 </tr>
                  <?php
           $srlno++;
           }
         } else {
          ?>
          <tr>
                <td colspan="8" style="text-align:center">Start generating <?php echo $page_name;?></td>
              </tr>
          <?php
         }
         ?>
         <tr>
            <td colspan="8" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tbody>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>
<script type="text/javascript">
  function changePubs(th) {
    $.ajax({
      url: '<?php echo CMS_URL;?>ajaxGetPubs/index/' + th,
      type: 'post',
      data: 'data=send',
      beforeSend:function(){
        $("#ajaxPubs").html('please wait...');    
      },
      success:function(e){
        $("#ajaxPubs").html(e);
      }
    });
  }
</script>