<?php 	
	//PRODUCTS STATISTICS START
	if($this->session->userdata('admin_role_id') == 1 || $this->session->userdata('admin_role_id') == 7) {
    ?> 
    <div class="align-left chart-area">
        <div class="content-box" onclick="$('#contentainer_2').slideToggle();">
          <div class="content-box-header">
            <h3>Statistics for Orders</h3>
          </div>
          <div class="content-box-content" id="contentainer_2">
             <div id="order_chart" style="text-align:center">
                <img src="<?php echo CMS_URL?>images/preloader.gif" />
                &nbsp;&nbsp;&nbsp;Preparing the chart...
             </div>
          </div>  
        </div>
        <div class="content-box" onclick="$('#contentainer_1').slideToggle();">
          <div class="content-box-header">
            <h3>Statistics for products</h3>
          </div>
          <div class="content-box-content" id="contentainer_1">
             <div id="product_chart" style="text-align:center">
                <img src="<?php echo CMS_URL?>images/preloader.gif" />
                &nbsp;&nbsp;&nbsp;Preparing the chart...
             </div>
          </div>  
        </div>  
    </div>    
    
    <div class="align-right stats-area">
    	<div class="detailed-statistics-full grey">
           <div class="detailed-statistics-header blue">TOTAL REVENUE FOR THE DAY</div>
           <div class="detailed-statistics-body" id="today_revenue"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
           
        </div>
        <div class="detailed-statistics-full grey">
           <div class="detailed-statistics-header azure">EXPECTED REVENUE FOR THE DAY</div>
           <div class="detailed-statistics-body" id="expected_revenue"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
           
        </div>
        <div class="clear"></div>
        
        <div class="detailed-statistics grey align-left">
           <div class="detailed-statistics-header blue">Total Order</div>
           <div class="detailed-statistics-body" id="total_orders"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        </div>
        
         <div class="detailed-statistics grey align-right">
           <div class="detailed-statistics-header yellow">Not Delivered Order</div>
           <div class="detailed-statistics-body" id="total_notdelivered_orders"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
           
        </div>
        
        <div class="clear"></div>
        
        <div class="detailed-statistics-small grey align-left">
           <div class="detailed-statistics-header red">Total Sale</div>
           <div class="detailed-statistics-body" id="today_sale"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        	
        </div>
        <div class="detailed-statistics-small grey align-left" style="margin:0% 0% 0% 3%">
           <div class="detailed-statistics-header blue">New Leads</div>
           <div class="detailed-statistics-body" id="today_user"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        	
        </div>
        <div class="detailed-statistics-small grey align-right">
           <div class="detailed-statistics-header azure">Fired Leads</div>
           <div class="detailed-statistics-body" id="fire_leads"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        	
        </div>
        <div class="clear"></div>
    
    	<div class="detailed-statistics grey align-left">
           <div class="detailed-statistics-header green">Lead to Conversion</div>
           <div class="detailed-statistics-body" id="lead_to_conversion"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        	
        </div>
        <div class="detailed-statistics grey align-right">
           <div class="detailed-statistics-header purple">Growth Rate</div>
           <div class="detailed-statistics-body" id="growth_rate"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
        	
        </div>
        <div class="clear"></div>
        
        <div class="detailed-statistics grey align-left">
           <div class="detailed-statistics-header yellow">COD Qualify</div>
           <div class="detailed-statistics-body" id="cod_qualify"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
           
        </div>
        <div class="detailed-statistics grey align-right">
           <div class="detailed-statistics-header azure">QA Qualify</div>
           <div class="detailed-statistics-body" id="qa_qualify"><img src="<?php echo CMS_URL?>images/preloader.gif" /></div>
           
        </div>
        <div class="clear"></div>
    </div>
    </div>
    <div class="clear"></div>
    <script src="<?php echo S3_CMS_URL;?>scripts/highcharts.js"></script>
	<script src="<?php echo S3_CMS_URL;?>scripts/exporting.js"></script>
	<script type="text/javascript">load_chart('<?php echo FULL_CMS_URL;?>','<?php echo SITE_URL.INTER_USE_DIR;?>', 'product_chart', 'order_chart');</script>
	<?php
	}
	//ORDER STATISTICS END
?>
<div class="clear"></div>
