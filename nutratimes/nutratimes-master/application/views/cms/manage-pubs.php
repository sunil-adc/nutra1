<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$add_page; ?>"><?php echo $add_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
       <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
         <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
         </div>
         <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong>Search Keyword :</strong>
                    <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Search Field :</strong> 
                    <select name="search_field" id="search_field">
                        <option value="pubs">Publisher Name</option>
                        <option value="cpl">CPL</option>
                        <option value="pincode_type">Pincode Type</option>
                        <option value="goal">Goal</option>
                    </select>
                    &nbsp;&nbsp;
                    
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        <table>
        <thead>
          <tr>
       		<th width="15%">Publisher</th>
            <th width="15%">Network</th>
            <th width="10%">CPL</th>
            <th width="13%">Handle Type</th>
            <th width="13%">Pincode Type</th>
            <th width="15%">Goal</th>
			<th width="12%">Date Created</th>
            <th width="12%">Date Updated</th>
            <?php
				if($this->session->userdata('admin_role_id') == '1') {
					?><th width="5%">Status</th><?php
				}
			?>
            <th width="5%">Edit</th>
          </tr>
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {                 
				  $i = 1;
				  foreach ($results as $key => $val) {
                        ?>
                        <tr>
                            <td><a href="<?php echo FULL_CMS_URL."/".strtolower($add_page)."/index/edit/".$val->$primary_field?>#atab" 
                            	title="<?php echo $val->pubs;?>"><?php echo sub_string($val->pubs,40) ?></a></td>
                            <td><a href="javascript:void(0)" title="<?php echo $val->network;?>"><?php echo sub_string($val->network, 20); ?></a></td>
                            <td><?php echo $val->cpl; ?></td>
                            <td><span class="<?php echo $val->handle_type == '1' ? 'color_green_bg': 'color_red_bg';?>"><?php echo ($val->handle_type == 1) ? 'Manual' : 'Automatic';?></span></td>
                            <td><?php echo $val->pincode_type?>X</td>
                            <td><?php echo $val->goal; ?></td>
                            <td><?php echo (strtotime($val->datecreated) > 0) ? time_diff ($val->datecreated) : "Not Available";?></td>
                            <td><?php echo (strtotime($val->dateupdated) > 0) ? time_diff ($val->dateupdated) : "Not Updated";?></td>
                            <?php
            					if($this->session->userdata('admin_role_id') == '1') {
									?>
                            	<td id="status_<?php echo $i;?>"><a href="javascript:change_status('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','status','status_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->status == '1') ? "<span class='bg_green'>Active</span>" : "<span class='bg_red'>Inactive</span>"; ?></a></td>
                            	<?php
								}
							?>
                            <td ><a href="<?php echo FULL_CMS_URL."/".strtolower($add_page)."/index/edit/".$val->$primary_field?>#atab" title="Edit"><img src="<?php echo CMS_URL;?>images/icons/pencil.png" alt="Edit" /></a></td>
                       </tr>
                       <?php
						$i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="9" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="9" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>