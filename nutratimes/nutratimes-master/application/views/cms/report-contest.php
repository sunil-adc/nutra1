<?php $arr_all = all_arrays(); ?>
<script>
	$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			inline: true
		});
	});
</script>
<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
	   ?>
       
         <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong><span class="color_red">*</span> From :</strong>
                    <input type="text" name="from_date" value="<?php echo (isset($from_date)) ? $from_date : date("Y-m-d", strtotime("-2 day"));?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:20% !important" />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong><span class="color_red">*</span> To :</strong>
                    <input type="text" name="to_date" value="<?php echo (isset($to_date)) ? $to_date : date("Y-m-d", strtotime("-1 day"));?>" 
                    class="text-input small-input datepicker" autocomplete="off" style="width:20% !important"  />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong>Network :</strong>
                    <?php echo $net; ?>
                    <br /><br />
                    
                    <strong>Publisher :</strong>
                    <span id="ajaxPubs"><select name="pubid" id="pubid"><option value="0">Select Network</option></select></span>
                    
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong>BNR :</strong>
                    <input type="text" name="bnr" value="<?php echo (isset($bnr)) ? $bnr : '';?>" 
                    class="text-input small-input" autocomplete="off" style="width:20% !important"  />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        
        <table>
        <thead>
           <tr>
		    <th width="60%">Name</th>
            <th width="10%">Status</th>
            <th width="10%">Sale</th>
            <th width="10%">Lead</th>
            <th width="10%">Conversion</th>
		  </tr>
        </thead>
        <tbody>
            <?php
			   if (isset($lead)) {
			   		
					$arr = array();
					foreach ($sale->result() as $k => $v) {
						$arr[$v->prod_id] = $v->total_items;
					}
					
					foreach ($lead->result() as $key => $val) {
						if (isset($arr) && is_array($arr) && array_key_exists($val->prod_id, $arr)) {
							$sale_prod = $arr[$val->prod_id];
						} else {
							$sale_prod = 0;
						}
						?>
						<tr valign="middle">
							<td style="display:table-cell;vertical-align:middle">
								<?php echo $val->name;?>
							</td>
							<td style="display:table-cell;vertical-align:middle">
								<?php echo $val->status == 1 ? 'Active' : '<span class="color_red">Deactive</span>';?>
							</td>
							<td style="display:table-cell;vertical-align:middle">
								<?php echo $sale_prod;?>
							</td>
                            <td style="display:table-cell;vertical-align:middle">
								<?php echo $val->total_items;?>
							</td>
                            <td style="display:table-cell;vertical-align:middle">
								<?php 
									
									if ($sale_prod > 0) {
										echo number_format((($sale_prod * 100) / $val->total_items), 2);
									} else {
										echo 0;
									}
								?>%
							</td>
						</tr>
						<?php
				   }
			   } else {
					?>
					<tr>
                    	<td colspan="9" style="text-align:center">Start generating <?php echo $page_name;?></td>
                    </tr>
                    <?php
			   }
            ?>
        </tbody>
        </table>
    </div>
    <!-- End #tab1 -->
  </div>
</div>
<div class="clear"></div>
<script type="text/javascript">
	function changePubs(th) {
		$.ajax({
			url: '<?php echo CMS_URL;?>ajaxGetPubs/index/' + th,
			type: 'post',
			data: 'data=send',
			beforeSend:function(){
				$("#ajaxPubs").html('please wait...');		
			},
			success:function(e){
				$("#ajaxPubs").html(e);
			}
		});
	}
	
	<?php
		if (isset($_POST['net']) && $_POST['net'] != '') {
			?>
				changePubs('<?php echo $_POST['net'];?>')
			<?php
		}
	?>
</script>