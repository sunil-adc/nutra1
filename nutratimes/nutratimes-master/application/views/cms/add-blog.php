<?php $array_func = all_arrays();?>
<<script src="//cdn.ckeditor.com/4.5.6/full/ckeditor.js"></script>
<!--<script src="//cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>-->

<script type="application/javascript">
 function checkInputs(){ 
    var regex = /^[0-9]*$/;
	var title  	=document.getElementById("title").value;
	var url  	=document.getElementById("url").value;
	var blogcontent  =document.getElementById("blogcontent").value;
	var blog_image =document.getElementById("blog_image").value;
	var status      =document.getElementById("status").value;
    var page_title       =document.getElementById("page_title").value;
	var meta_description =document.getElementById("meta_description").value;


	if(title == "" || title.trim() ==""){
		alert("Title  is required");
		document.getElementById("title").focus();
		return false;
	}
	if(url == "" || url.trim() ==""){
		alert("URL  is required");
		document.getElementById("url").focus();
		return false;
	}
	
	
		
	if(status == "" || status.trim() ==""){
	   alert("Status field is required");
	   document.getElementById("status").focus();
	   return false;
	}
	if(page_title == "" || page_title.trim() ==""){
		alert("Page Title is required");
		document.getElementById("page_title").focus();
		return false;
	}
	if(meta_description == "" || meta_description.trim() ==""){
		alert("Meta Description is required");
		document.getElementById("meta_description").focus();
		return false;
	}

		
}

 </script>
<script>
$(document).ready(function(){
	$('#title').on('keyup', function() {
		var title_url = $("#title").val();
		var blog_url  = title_url.replace(/\s+/g, '-').toLowerCase();
		$("#url").val(blog_url);
		
	});});
 
</script>
<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
            echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
            if( $this->session->flashdata('error') ) { 
                echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
            }else if( $this->session->flashdata('success') ) { 
                echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
            }
        ?>


                    
        <fieldset>
        
        <p>
        <label><span class="color_red">*</span> <span class="color_blue">Blog Title </span></label>                           
                       
			<input type="text" id="title" name="title" placeholder="Enter Blog title" tabindex="1" class="text-input small-input" value="<?php echo _isset($result_data['title']); ?>">
         </p>                   

        <p>
        <label><span class="color_red">*</span> <span class="color_blue">URL </span></label>                           
                       
            <input type="text" id="url" name="url" placeholder="Blog Seo Url" tabindex="2" class="text-input small-input" value="<?php echo _isset($result_data['url']); ?>">
         </p>    
                        
        
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Blog Image * </span></label>                           
            <input type="file" name="blog_image" id="blog_image" placeholder="Blog Image" tabindex="3" class="text-input small-input">
            <label>Image size should not be less than 900*400 </label><br></br>
            <?php 
                if(isset($result_data['blog_image'])) {
                        echo "<img src='".S3_URL.BLOG_CROP_FOLDER.THUMB_PREFIX.strtolower($result_data['blog_image'])."' style='width:50%'/>";
                }
            ?>  
                               
         </p>         

         <p>
        <label><span class="color_red">*</span> <span class="color_blue">Meta Title </span></label>                           
                       
            <input type="text" id="page_title" name="page_title" placeholder="Meta title" tabindex="1" class="text-input small-input" value="<?php echo _isset($result_data['pagetitle']); ?>">
         </p>                   

        <p>
        <label><span class="color_red">*</span> <span class="color_blue">Meta Keyword </span></label>                           
                       
            <input type="text" id="meta_keyword" name="meta_keyword" placeholder="Meta Keyword" tabindex="1" class="text-input small-input" value="<?php echo _isset($result_data['meta_keyword']); ?>">
        </p>                   
         

         <p>

        <label><span class="color_red">*</span> <span class="color_blue">Meta Description </span></label>                           
         
            <textarea id="meta_description" name="meta_description" style="height:80px;" placeholder="Enter Description" tabindex="4" class="text-input small-input">
                <?php echo _isset($result_data['metadescription']); ?>
            </textarea>
        </p>    
        

        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Status </span></label>
            <select name="status" id="status" class="small-input" tabindex="5">
                <option value="" <?php echo ($result_data['status'] == NULL) ? 'selected="selectec"' : '';?>>Select Status</option>
                <option value="1" <?php echo ($result_data['status'] == '1') ? 'selected="selectec"' : '';?>>Active</option>
                <option value="0" <?php echo ($result_data['status'] == '0') ? 'selected="selectec"' : '';?>>Inactive</option>
            </select> 
        </p>
        
        <p>
            <label><span class="color_red">*</span> <span class="color_blue">Blog Content * </span></label>                           
                       
            <textarea id="blogcontent" name="blogcontent" placeholder="blogcontent" rows="16" tabindex="3" class="text-input small-input"><?php echo _isset($result_data['blogcontent']) ?></textarea>
            <script> CKEDITOR.replace( 'blogcontent' ); </script>
         </p>    
        

        <?php

                echo form_hidden('mode', (strtolower ($this->uri->segment(4)) == 'edit')  ? 'edit' : 'add');
                echo form_hidden('manage_page', $manage_page);
                echo form_hidden('add_page', $add_page);
                echo form_hidden($primary_field, $result_data[$primary_field]);
                echo form_hidden('cur_url', get_full_url());
            ?>


         <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" tabindex="6" onclick="return checkInputs()"/>
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        
        </fieldset>
        <div class="clear"></div>
        <?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
