<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$add_page; ?>/index/add/#atab"><?php echo $add_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
       <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows);?> records found in <?php echo $page_name;?></div>
       </div>
      <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong>Search Keyword :</strong>
                    <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <strong>Search Field :</strong> 
                    <select name="search_field" id="search_field">
                        <option value="prod_id" <?php echo ($search_field == "prod_id") ? "selected='selected'" : ""; ?>>Prod Id</option>
                        <option value="oos" <?php echo ($search_field == "oos") ? "selected='selected'" : ""; ?>>Out of Stock</option>
                        <option value="name" <?php echo ($search_field == "name") ? "selected='selected'" : ""; ?>><?php echo $page_name; ?> Name</option>
                    	<option value="seourl" <?php echo ($search_field == "seourl") ? "selected='selected'" : ""; ?>>SEO URL</option>
                    </select>
                    &nbsp;&nbsp;  
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                    
                    <?php
                    	if (array_constant_check($this->session->userdata('admin_role_id'), ADMIN_PRODUCT_EXCEL)) {
						?>
                            <input type="submit" class="button" id="btn_download_product" 
                            	name="btn_download_product" value="DOWNLOAD PRODUCT" />
                        <?php
						}
					?>
                </td>
            </tr>        
          </table>
        </form>
        <table>
        <thead>
          <tr>
       		<th width="4%">Product Id</th>
            <th width="38%"><?php echo $page_name; ?> Name</th>
            <th width="10%">Created</th>
            <th width="10%">Updated</th>
            <th width="5%">Is Contest</th>
            <th width="5%">OOS</th>
            <th width="10%">Verification</th>
            <th width="8%">Status</th>
            <th width="8%">Edit</th>
          </tr>
        </thead>
        <tbody>
            <?php	
               if (is_array($results) && count($results) > 0) {
                   $i = 1;
				   foreach ($results as $key => $val) {
                        ?>
                        <tr>
                            <td><?php echo $val->prod_id; ?></td>
                            <td><?php echo $val->name; ?></td>
                            <td><?php echo time_diff ($val->datecreated);?></td>
                            <td><?php echo (strtotime($val->dateupdated) > 0) ? time_diff ($val->dateupdated) : "Not Updated";?></td>
                            <td><?php echo ($val->contest == 0) ? '<span class="color_green">No</span>' : "<span class='color_red'>Yes</span>";?></td>
                            
                            <td id="oos_<?php echo $i;?>"><a href="javascript:change_oos('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','oos','oos_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->oos == 0) ? "<span class='color_green'>No</span>" : "<span class='color_red'>Yes</span>"; ?></a></td>
                            
                            <td id="verified_<?php echo $i;?>"><a href="javascript:change_verifiy('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','is_verified','verified_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->is_verified == '1') ? "<span class='color_green'>Verified</span>" : "<span class='color_red'>Not Verified</span>"; ?></a></td>
                            
                            <td id="status_<?php echo $i;?>"><a href="javascript:change_status('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','status','status_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->status == '1') ? "<span class='bg_green'>Active</span>" : "<span class='bg_red'>Inactive</span>"; ?></a></td>
                            <td>
                                <a href="<?php echo FULL_CMS_URL."/".strtolower($add_page)."/index/edit/".$val->$primary_field?>/#atab" title="Edit <?php echo $val->name; ?>"><img src="<?php echo CMS_URL;?>images/icons/pencil.png" alt="Edit" /></a>
                           </td>
                       </tr>
                       <?php
                    $i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="7" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="8" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>