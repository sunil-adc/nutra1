<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
	   ?>
       <div class="notification information png_bg">
       	  <div>Total <?php echo number_format($total_rows);?> records found in <?php echo $page_name;?></div>
       </div>
        <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                  <strong>Search Keyword :</strong>
                  <input type="text" name="search_txt" value="<?php echo $search_txt;?>" class="text-input small-input" />
                  &nbsp;&nbsp;&nbsp;&nbsp;
                  <strong>Search Field :</strong> 
                  <select name="search_field" id="search_field">
                    <option value="u.username" <?php echo ($search_field == "u.username") ? "selected='selected'" : ""; ?>>Username</option>
                    <option value="l.ip_address" <?php echo ($search_field == "l.ip_address") ? "selected='selected'" : ""; ?>>IP Addres</option>
                    <option value="l.browser_name" <?php echo ($search_field == "l.browser_name") ? "selected='selected'" : ""; ?>>Browser Name</option>
                    	
                  </select>
                  &nbsp;&nbsp;  
                  <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        <table>
        <thead>
          <tr>
       		<th width="20%">UserName</th>
       		<th width="20%">Role Name</th>
            <th width="20%">IP Address</th>
            <th width="20%">Browser </th>
            <th width="20%">Login Time</th>
          </tr>
        </thead>
        <tbody>
            <?php
               if (is_array($results) && count($results) > 0) {
                   $i = 1;
				   foreach ($results as $key => $val) {
                        ?>
                        <tr>
                            <td><?php echo $val->username; ?></td>
                            <td><?php echo $val->role_name; ?></td>
                            <td><?php echo $val->ip_address; ?></td>
                            <td><?php echo $val->browser_name; ?></td>
                            <td><?php echo (strtotime($val->date_time) > 0) ? time_diff ($val->date_time) : "Not Available";?></td>
                       </tr>
                       <?php
                    $i++;
					}
                } else {
                    ?>
                    <tr>
                        <td colspan="6" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                    </tr>
                    <?php
                }
            ?>
        </tbody>
        <tfoot>
          <tr>
            <td colspan="6" style="line-height: 1.5em; !important">
                <?php
                  if (count($results) > 0) {
                     echo $links;
                  }
                ?>
            </td>
          </tr>
        </tfoot>
        </table>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>