
<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.1/css/responsive.dataTables.min.css">

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script type="text/javascript">

  $(document).ready( function () {
      //$('#datatbl').dataTable();
  } );

  
</script>


<script>
	$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			inline: true
		});
		$('.content-box ul.content-box-tabs li a, .change_criteria').click(
			function() { 
				var _this = ".content-box ul.content-box-tabs li a";
				$(_this).parent().siblings().find("a").removeClass('current');
				$(_this).removeClass("default-tab");
				$(_this).removeClass("current");
				
				var currentTab = $(_this).attr('href');
				$(_this).addClass('current default-tab');
				$(currentTab).siblings().hide();
				$(currentTab).show();
				return false; 
			}
		);
		
	});
</script> 
<style>
	#main-content table thead th {
		font-size: 12px;
	}
</style>
<?php $arr_all = all_arrays(); ?>
<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <?php
		if(isset($results) || isset($only_amount) || isset($only_count) ) {
    	?>
        <ul class="content-box-tabs">
            <li><a href="#atab" onclick="javascript:$('#tab2').hide();$('#tab1').show();" class="">Start New Search</a></li>
            <li><a href="#atab" onclick="javascript:$('#tab1').hide();$('#tab2').show();" class="default-tab current">Search Result</a></li>
    	</ul>
        <?php
		}
	?>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    	<div class="tab-content default-tab" id="tab1">
		  <?php 
			echo form_open_multipart($form_submit);
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <div class="prod_full_div" style="padding-right:10px;">
            	<div class="align-left prod_leftside">
                	<div class="prod_box">
                    	<div class="prod_title prod_header">Date Info</div>
                    	<div class="prod_desc">                                            
                            <p>
                              <label> <span class="color_blue">Date type</span> </label>
                              <?php 
							  	$date_type = (isset($_REQUEST['date_type']) && $_REQUEST['date_type'] != "") ? $_REQUEST['date_type'] : '';
								echo custom_dropdown('date_type', '', $arr_all['ARR_DATE'], $date_type,'class="text-input small-input"');?>
                            </p>
                            <p>
                              <label> <span class="color_blue">From Date :</span> </label>
                              <input class="text-input small-input datepicker" autocomplete="off" type="text" name="from_date" 
                              value="<?php echo (isset($_REQUEST['from_date'])) ? $_REQUEST['from_date'] : date('Y-m-d', strtotime("-1 days")); ?>" id="from_date" /> 
                            </p>
                            <p>
                              <label> <span class="color_blue">To Date :</span> </label>
                              <input class="text-input small-input datepicker" autocomplete="off" type="text" name="to_date"  
                              value="<?php echo (isset($_REQUEST['to_date'])) ? $_REQUEST['to_date'] : date('Y-m-d'); ?>" id="to_date" /> 
                            </p>
                        </div>
                    </div>
                    <div class="prod_box">
                    	<div class="prod_title prod_header">Payment</div>
                    	<div class="prod_desc">  
                            <p>
                              <label> <span class="color_blue">Payment Mode :</span> </label>
                               <?php echo custom_dropdown('payment_mode', '', $arr_all['ARR_PAYMENT'], isset($_REQUEST['payment_mode']) ? $_REQUEST['payment_mode'] :'','class="text-input small-input"');?>
                            </p>
                            
                            <?php 
								if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 2) {
								?>
                                <p>
                                  <label> <span class="color_blue">Payment Status :</span> </label>
                                  <select name="payment_status" id="payment_status" class="text-input small-input">
                                    <option value="">Select Option</option>
                                    <option value="1" <?php echo (isset($_REQUEST['payment_status']) && is_numeric($_REQUEST['payment_status']) && $_REQUEST['payment_status'] == 1) ? 'selected' : ''; ?>>Paid</option>
                                    <option value="0" <?php echo (isset($_REQUEST['payment_status']) && is_numeric($_REQUEST['payment_status']) && $_REQUEST['payment_status'] == 0) ? 'selected' : ''; ?>>Not Paid</option>
                                  </select>
                                </p>
                                <?php
								}
								
								if (array_constant_check ($this->session->userdata('admin_role_id'), ADMIN_PAYMENT_ROLES) || 
									array_constant_check ($this->session->userdata('uid'), ADMIN_PAYMENT_USERS)) {
									?>
									<p style="display: none;">
                                      <label> <span class="color_blue">Payment Check Status :</span> </label>
                                      <select name="payment_check_status" id="payment_check_status" class="text-input small-input">
                                        <option value="">Select Option</option>
                                        <option value="1" <?php echo (isset($_REQUEST['payment_check_status']) && is_numeric($_REQUEST['payment_check_status']) && $_REQUEST['payment_check_status'] == 1) ? 'selected' : ''; ?>>Checked</option>
                                        <option value="0" <?php echo (isset($_REQUEST['payment_check_status']) && is_numeric($_REQUEST['payment_check_status']) && $_REQUEST['payment_check_status'] == 0) ? 'selected' : ''; ?>>Not Checked</option>
                                      </select>
                                    </p>
                                    <!--<p>
                                      <label> <span class="color_blue">COD Order type :</span> </label>
                                       <select name="cms_order" id="cms_order" class="text-input small-input">
                                       		<option value="">Select Option</option>
                                            <option value="0" <?php echo (isset($_REQUEST['cms_order']) && is_numeric($_REQUEST['cms_order']) && $_REQUEST['cms_order'] == 0) ? 'selected' : ''; ?>>By Customer</option>
                                            <option value="1" <?php echo (isset($_REQUEST['cms_order']) && is_numeric($_REQUEST['cms_order']) && $_REQUEST['cms_order'] == 1) ? 'selected' : ''; ?>>By Seajin Agent</option>
                                       </select>
                                    </p>-->
                            		<p>
                                      <label> <span class="color_blue">Mobile OR PC :</span> </label>
                                       <select name="is_pc_mobile" id="is_pc_mobile" class="text-input small-input">
                                       		<option value="">Select Option</option>
                                            <option value="0" <?php echo (isset($_REQUEST['is_pc_mobile']) && is_numeric($_REQUEST['is_pc_mobile']) && $_REQUEST['is_pc_mobile'] == 0) ? 'selected' : ''; ?>>From PC</option>
                                            <option value="1" <?php echo (isset($_REQUEST['is_pc_mobile']) && is_numeric($_REQUEST['is_pc_mobile']) && $_REQUEST['is_pc_mobile'] == 1) ? 'selected' : ''; ?>>From Mobile</option>
                                       </select>
                                    </p>
                            		  <!--<p>
                                      <label> <span class="color_blue">Is Fraud :</span> </label>
                                      <select name="is_fraud" id="is_fraud" class="text-input small-input">
                                        <option value="">Select Option</option>
                                        <option value="1" <?php echo (isset($_REQUEST['is_fraud']) && is_numeric($_REQUEST['is_fraud']) && $_REQUEST['is_fraud'] == 1) ? 'selected' : ''; ?>>Yes</option>
                                        <option value="0" <?php echo (isset($_REQUEST['is_fraud']) && is_numeric($_REQUEST['is_fraud']) && $_REQUEST['is_fraud'] == 0) ? 'selected' : ''; ?>>No</option>
                                      </select>
                                    </p>	-->
									<?php
								}
								?>
                         </div>
                    	</div>
                       <?php 
						  if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 2) {
						  ?>
                            <div class="prod_box">
                      <div class="prod_title prod_header">Output Type</div>
                    	<div class="prod_desc">
                        	<p>
                              <label> <span class="color_blue">Only Count : </span> </label>
                              <select name="only_count" id="only_count" class="text-input small-input">
                              	<option value="">Select Option</option>
								<option value="1" 
								<?php echo (isset($_REQUEST['only_count']) && $_REQUEST['only_count'] == 1) ? 'selected="selected"' : ''; ?>>Yes</option>
                              </select>
                            </p>
                            <p>
                              <label> <span class="color_blue">Only Amount : </span> </label>
                              <select name="only_amount" id="only_amount" class="text-input small-input">
                              	<option value="">Select Option</option>
								<option value="1" 
								<?php echo (isset($_REQUEST['only_amount']) && $_REQUEST['only_amount'] == 1) ? 'selected="selected"' : ''; ?>>Yes</option>
                              </select>
                            </p>
                            <!--<p>
                              <label> <span class="color_blue">Stats : </span> </label>
                              <select name="stats" id="stats" class="text-input small-input">
                              	<option value="">Select Option</option>
								<option value="1" 
								<?php //echo (isset($_REQUEST['stats']) && $_REQUEST['stats'] == 1) ? 'selected="selected"' : ''; ?>>ProductWise</option>
                              </select>
                            </p>-->
                            <p>
                              <label> <span class="color_blue">Order By : </span> </label>
                               <?php echo custom_dropdown('order_by', '', $arr_all['ARR_ORDERS'], isset($_REQUEST['order_by']) ? $_REQUEST['order_by'] :'','class="text-input small-input"');?>
                            </p>
                            <!--<p>
                              <label> <span class="color_blue">Product : </span> </label>
                              <?php echo custom_dropdown('product', '', $arr_all['ARR_YES_NO'], isset($_REQUEST['product']) ? $_REQUEST['product'] :'','class="text-input small-input"');?>
                            </p>-->
                            
              			</div>
                    </div>
                    	  <?php
						  }
					   ?>
                	<div class="prod_box">
                      <div class="prod_title prod_header">Status Wise</div>
                    	<div class="prod_desc">
                          <!--	<p>
                              <label> <span class="color_blue">Zipdial Status : </span> </label>
                              <?php echo custom_dropdown('zipdial_status', '', $arr_all['ARR_ZIPDIAL_STATUS'], isset($_REQUEST['zipdial_status']) ? $_REQUEST['zipdial_status'] : '','class="text-input small-input"', false, true);?>
                            </p>-->
							
						  <?php 
						  	if($this->session->userdata('admin_role_id') == 2) {
								?>
								            <!--<p>
                              <label> <span class="color_blue">QA Status : </span> </label>
                              <?php echo custom_dropdown('qa_status', '', $arr_all['ARR_QA_STATUS'], isset($_REQUEST['qa_status']) ? $_REQUEST['qa_status'] :'','class="text-input small-input"');?>
                            </p>-->
								<?php
							} else if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 5) {
						  		?>
                          		<!--<p>
                              <label> <span class="color_blue">QA Status : </span> </label>
                              <?php echo custom_dropdown('qa_status', '', $arr_all['ARR_QA_STATUS'], isset($_REQUEST['qa_status']) ? $_REQUEST['qa_status'] :'','class="text-input small-input"');?>
                            </p>  -->
                            <p>
                              <label> <span class="color_blue">Fire Status : </span> </label>
                              <?php echo custom_dropdown('fire_status', '', $arr_all['ARR_YES_NO'], isset($_REQUEST['fire_status']) ? $_REQUEST['fire_status'] :'','class="text-input small-input"');?>
                            </p>
                            
              
                <?php
              } 
                  ?>
              

                             	<p>
                              <label> <span class="color_blue">Delievery Status : </span> </label>
                              <?php echo custom_dropdown('delivery_status', '', $arr_all['ARR_DELIVERY_STATUS'], isset($_REQUEST['delivery_status']) ? $_REQUEST['delivery_status'] :'','class="text-input small-input"');?>
                            <!--</p>
                             	<p>
                              <label> <span class="color_blue">DND Status : </span> </label>
                              <?php echo custom_dropdown('dnd_status', '', $arr_all['ARR_YES_NO'], isset($_REQUEST['dnd_status']) ? $_REQUEST['dnd_status'] :'','class="text-input small-input"');?>
                            </p>-->
                
							<p style="display: none;">
                                <label><span class="color_red">*</span> <span class="color_blue">Not Paid Reason</span></label>
                                <?php echo custom_dropdown('notpaid_reason', '', $arr_all['ARR_NOTPAID'], isset($_REQUEST['notpaid_reason']) ? $_REQUEST['notpaid_reason'] :'','class="text-input small-input"');?> 
                            </p>
                            <p>
                              <label> <span class="color_blue">Caller Status : </span> </label>
                              <?php echo custom_dropdown('caller_status', '', $arr_all['ARR_CALLER_STATUS'], isset($_REQUEST['caller_status']) ? $_REQUEST['caller_status'] :'','class="text-input small-input"');?>
                            </p>
                            <!--<p>
                              <label> <span class="color_blue">Refund Status : </span> </label>
                              <?php echo custom_dropdown('refund_status', '', $arr_all['ARR_REFUND'], isset($_REQUEST['refund_status']) ? $_REQUEST['refund_status'] :'','class="text-input small-input"');?>
                            </p>-->
              			</div>
                    </div>
                 	<div class="prod_box">
                   <div class="prod_title prod_header">Customer Issue</div>
                      <div class="prod_desc">  
                	  <p>
                              <label> <span class="color_blue">Issue : </span> </label>
                              <?php echo custom_dropdown('issues', '', $arr_all['ARR_ISSUE'], isset($_REQUEST['issues']) ? $_REQUEST['issues'] :'','class="text-input small-input"');?>
                            </p>
                            
                      </div>
                   </div>
                 </div>
                 
                 <div class="align-left prod_leftside">
                	<div class="prod_box">
                    	<div class="prod_title prod_header">Customer Info</div>
                    	<div class="prod_desc">
                        	<p>
                              <label> <span class="color_blue">Customer Name : </span> </label>
                              <input class="text-input small-input" type="text" name="customer_name" id="customer_name" 
                              value="<?php echo (isset($_REQUEST['customer_name'])) ? $_REQUEST['customer_name'] : ''; ?>" /> 
                            </p>
                             <p>
                              <label> <span class="color_blue">Phone no :</span> </label>
                              <input class="text-input small-input" type="text" name="phone_no" id="phone_no" 
                              value="<?php echo (isset($_REQUEST['phone_no'])) ? $_REQUEST['phone_no'] : ''; ?>" /> 
                            </p>
                            <?php 
						  	if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 2) {
						  		?>
                                <p>
                                  <label> <span class="color_blue">Customer Email : </span> </label>
                                  <input class="text-input small-input" type="text" name="email" id="email" 
                                  value="<?php echo (isset($_REQUEST['email'])) ? $_REQUEST['email'] : ''; ?>" /> 
                                </p>
                                <?php
							}
							?>
                            <p>
                              <label> <span class="color_blue">City : </span> </label>
                              <input class="text-input small-input" type="text" name="city" id="city" 
                              value="<?php echo (isset($_REQUEST['city'])) ? $_REQUEST['city'] : ''; ?>" /> 
                            </p>
                           
                            
                        </div>
                    </div>
                    <div class="prod_box">
                   <div class="prod_title prod_header">Track Order</div>
                      <div class="prod_desc">  
                             <p>
                              <label> <span class="color_blue">Ref Id : </span> </label>
                              <input class="text-input small-input" type="text" name="refid" id="refid" 
                              placeholder="Ref Id 'OR' <?php echo SITE_NAME?> Order-Id" value="<?php echo (isset($_REQUEST['refid'])) ? $_REQUEST['refid'] : ''; ?>" /> 
                            </p>
                           <?php 
						  	if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 2) {
						  		?>
                                <p>
                                  <label> <span class="color_blue">Order Id Payment Gateway : </span> </label>
                                  <input class="text-input small-input only_numeric" type="text" name="order_id_pg" id="order_id_pg"
                                  placeholder="Only numbers are allowed" value="<?php echo (isset($_REQUEST['order_id_pg'])) ? $_REQUEST['order_id_pg'] : ''; ?>" /> 
                                </p>
                                <?php
							}
							?>
                           <p>
                              <label> <span class="color_blue">Tracking Number : </span> </label>
                              <input class="text-input small-input" type="text" name="tracking_number" id="tracking_number" 
                              value="<?php echo (isset($_REQUEST['tracking_number'])) ? $_REQUEST['tracking_number'] : ''; ?>" /> 
                            </p>
                            <p>
                              <label> <span class="color_blue">Product Name : </span></label>
                              <input class="text-input small-input" type="text" name="product_name" id="product_name" 
                              placeholder="Type more than 3 character for suggestion" autocomplete="off" 
                              value="<?php echo (isset($_REQUEST['product_name'])) ? $_REQUEST['product_name'] : ''; ?>" />
                              <input type="hidden" name="cms_url_hidden" id="cms_url_hidden" value="<?php echo FULL_CMS_URL?>" />
                              <input type="hidden" id="pid" name="pid" value="<?php echo (isset($_REQUEST['pid'])) ? $_REQUEST['pid'] : ''; ?>"/>
                              <div id="ajax_auto_suggestion"></div>	
                            </p>
                           
                           <?php 
						  	if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 2) {
						  		?>
                            <p>
                              <label> <span class="color_blue">Provider : </span> </label>
                               <?php echo custom_dropdown('provider', '', $arr_all['ARR_PROVIDER'], isset($_REQUEST['provider']) ? $_REQUEST['provider'] :'','class="text-input small-input"');?>
                            </p>
                            <?php
							}
							?>
                            
                            
                       </div>
                   </div>
                   <?php 
						//if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 2) {
          if ($this->session->userdata('admin_role_id') != 2) {
						
						if (array_constant_check($this->session->userdata('admin_role_id'), SEARCH_ORDER_NETWORK)) {						
						 ?>
                       <div class="prod_box">
                      <div class="prod_title prod_header">Network</div>
                    	<div class="prod_desc">  
                    		<p>
                              <label> <span class="color_blue">Network : </span> </label>
                              <input class="text-input small-input" type="text" name="net" id="net" 
                              value="<?php echo (isset($_REQUEST['net'])) ? $_REQUEST['net'] : ''; ?>" /> 
                            </p>
                            <p>
                              <label> <span class="color_blue">Publisher Id : </span> </label>
                              <input class="text-input small-input" type="text" name="pubid" id="pubid" 
                              value="<?php echo (isset($_REQUEST['pubid'])) ? $_REQUEST['pubid'] : ''; ?>" /> 
                            </p>
                            <p>
                              <label> <span class="color_blue">BNR : </span> </label>
                              <input class="text-input small-input" type="text" name="bnr" id="bnr" 
                              value="<?php echo (isset($_REQUEST['bnr'])) ? $_REQUEST['bnr'] : ''; ?>" /> 
                            </p>
                            <!--<p>
                              <label> <span class="color_blue">ZoneId : </span> </label>
                              <input class="text-input small-input" type="text" name="zoneid" id="zoneid" 
                              value="<?php echo (isset($_REQUEST['zoneid'])) ? $_REQUEST['zoneid'] : ''; ?>" /> 
                            </p>
                            <p>
                              <label> <span class="color_blue">Click Id :</span> </label>
                              <select name="clkid" id="clkid" class="text-input small-input">
                                <option value="">Select Option</option>
                                <option value="1" <?php echo (isset($_REQUEST['clkid']) && is_numeric($_REQUEST['clkid']) && $_REQUEST['clkid'] == 1) ? 'selected' : ''; ?>>Yes</option>
                                <option value="0" <?php echo (isset($_REQUEST['clkid']) && is_numeric($_REQUEST['clkid']) && $_REQUEST['clkid'] == 0) ? 'selected' : ''; ?>>No</option>
                              </select>
                            </p>-->
                            <!--<p>
                              <label> <span class="color_blue">IP : </span> </label>
                              <input class="text-input small-input" type="text" name="ip" id="ip" /> 
                            </p>-->
                        </div>
                   </div><?php
						}
						if ($this->session->userdata('admin_role_id') != 3 && $this->session->userdata('admin_role_id') != 5) {
							?>
                            <div class="prod_box">
                   			<div class="prod_title prod_header">Agents</div>
                      		<div class="prod_desc">  
                            <!--<p>
                              <label> <span class="color_blue">QA Agents : </span> </label>
                              <select name="qa_agents" id="qa_agents" class="text-input small-input">
                                <option value="">Select Option</option>
                                <?php 
                                  if($qa_agents->num_rows() > 0){
                                    foreach ($qa_agents->result() as $row){
                                        if(isset($_REQUEST['qa_agents'])) {
                                            $seleceted = ($row->id == $_REQUEST['qa_agents']) ? 'selected="selected"' : "";
                                        }
                                        echo '<option value="'.$row->id.'" '.$seleceted.'>'.$row->email.'</option>';
                                    }
                                    echo '</select>';
                                  }
                               ?>
                             </select>  
                            </p>-->
                            <p>
                              <label> <span class="color_blue">COD Agents : </span> </label>
                              <select name="cod_agents" id="cod_agents" class="text-input small-input">
								<option value="">Select Option</option>
								<?php 
                    $cod_agents_arr = array();
							  	  
                    if($cod_agents->num_rows() > 0){
								  	foreach ($cod_agents->result() as $row){
                      //$cod_agents_arr[$row->id] = substr($row->username, 0, strpos($row->username, "@")); 
                      $cod_agents_arr[$row->id] = $row->username; 
										$seleceted = "";
										if(isset($_REQUEST['cod_agents'])) {
											$seleceted = ($row->id == $_REQUEST['cod_agents']) ? 'selected="selected"' : "";
										}
										echo '<option value="'.$row->id.'" '.$seleceted.'>'.$row->email.'</option>';
									}
									echo '</select>';
								  }
							   ?>
                             </select>  
                            </p>
                  		</div>
                  			</div>          
                  			<?php
							}
						}
						?>
                  <div class="prod_box">
                   		<div class="prod_title prod_header">Location</div>
                      	<div class="prod_desc">  
                  			<p>
                              <label> <span class="color_blue">States </span> </label>
                                <select name="state" class="text-input small-input">
                                <option value="">Select Option</option>
								<?php
                                    if (count($arr_all['ARR_STATE']) > 0) {
                                        asort($arr_all['ARR_STATE']);
                                        foreach ($arr_all['ARR_STATE'] as $k => $v) {
                                            $selected = (isset($_REQUEST['state']) && ($_REQUEST['state'] == $k)) ? 'selected' : '';
                                            echo "<option value='".$k."' ".$selected.">".$v."</option>";
                                        }
                                    } else {
                                        echo "<option>Select Option</option>";
                                    }
                                ?>
                            </select>  
                            </p>
                  		</div>
                  </div>
                   
                </div>
              <div class="clear"></div>
       </div>               
        <?php	
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="s" id="s" value="Search Orders" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div> 
    	<div class="tab-content default-tab" id="tab2" style="overflow-x:scroll">
        <?php
		if(isset($only_count) && trim($only_count) == '1') {
			echo $search_criteria;
			if(isset($full_sql) && trim($full_sql) != "") {
				get_admin_file_download_links($full_sql, $this->session->userdata('admin_role_id'), $select_fields, $table_fields, $where_fields, $search_criteria);
			}
			?>
            <script>$('#tab1').hide();$('#tab2').show();</script>
            
            <div class="notification success png_bg"><a href="#" class="close"><img src="<?php echo CMS_URL?>/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div><a href="#atab" class="change_criteria" onclick="javascript:$('#tab2').hide();$('#tab1').show();">Click here to change the search criteria it.</a></div></div>
                
             <div class="prod_full_div" style="padding-right:10px;">
            	<div class="align-left prod_leftside">
                	<div class="prod_box">
                    	<div class="prod_title">Result</div>
                    	<div class="prod_desc">
                        	<br /><div><h5><span class="color_green">Total available record is :</span> <?php echo $total_record;?></h5></div><br /> 
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
			<?php
        } else if(isset($only_amount) && trim($only_amount) == '1') {
			echo $search_criteria;
			get_admin_file_download_links($full_sql, $this->session->userdata('admin_role_id'), $select_fields, $table_fields, $where_fields, $search_criteria);
			?>
            <script>$('#tab1').hide();$('#tab2').show();</script>
            <div class="notification success png_bg"><a href="#" class="close"><img src="<?php echo CMS_URL?>/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div><a href="#atab" class="change_criteria" onclick="javascript:$('#tab2').hide();$('#tab1').show();">Click here to change the search criteria it.</a></div></div>
                
                <div class="prod_full_div" style="padding-right:10px;">
            	<div class="align-left prod_leftside">
                	<div class="prod_box">
                    	<div class="prod_title">Result</div>
                    	<div class="prod_desc">
                        	<br /><div><h5><span class="color_green">Total amount is : </span> <?php echo ($total_amount > 0) ? $total_amount : 0;?></h5></div><br /> 
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
			<?php
		} else {
			if(isset($results)) {
				?>
				<div class="notification success png_bg"><a href="#" class="close"><img src="<?php echo CMS_URL?>/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>Total <span class="bg_red"><?php echo number_format($total_rows);?></span> orders matching with given criteria. <a href="#atab" class="change_criteria" onclick="javascript:$('#tab2').hide();$('#tab1').show();">Click here to change it.</a></div></div>
                
                <div class="notification information png_bg"><a href="#" class="close"><img src="<?php echo CMS_URL?>/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>Displaying record from <span class="bg_red"><?php echo $offset;?></span> to <span class="bg_red"><?php echo $perpage;?></span></div></div>
				<?php
                echo $search_criteria;
				if(isset($full_sql) && trim($full_sql) != "") {
					get_admin_file_download_links($full_sql, $this->session->userdata('admin_role_id'), $select_fields, $table_fields, $where_fields, $search_criteria);
				}
				
			?>
			<script>$('#tab1').hide();$('#tab2').show();</script>
            <table id="datatbl">
            <thead>
              <tr>
                <th width="8%">Ref ID</th>
                <th width="8%">PG Order ID</th>
                <th width="10%">Name</th>
                <th width="8%">Mobile</th>
                <th width="8%">City</th>
               <?php
                	if($this->session->userdata('admin_role_id') == '1') {
        						?>
            						<th width="4%">NET</th>
                        <th width="4%">PubId</th>
                        <!--<th width="4%">SubId</th>-->
                        <th width="4%">Fired</th>
                <?php
        					}
					
        					if (array_constant_check ($this->session->userdata('admin_role_id'), ADMIN_PAYMENT_ROLES) || 
        						array_constant_check ($this->session->userdata('uid'), ADMIN_PAYMENT_USERS)) {
        						?>
                    <th width="7%">Telecaller</th>
        						<!---<th width="7%">Payment Check Status</th>-->
                        <!--<th width="7%">Fraud</th>-->
                 <?php 
        					}
            		  ?>
                <th width="4%">Caller Status</th>
                <th width="5%">Payment Mode</th>
                <th width="8%">Payment Status</th>
                <th width="8%">Actions</th>
                <th width="20%">Order Date</th>
              </tr>
            </thead>
            <tbody>
                <?php
                   
                   if (is_array($results) && count($results) > 0) {
                       $i = 1;
                       foreach ($results as $key => $val) {
                            ?>
                            <tr>
                                <td><?php echo ($val->id != "") ? $val->id : "<span class='color_red'>N/A</span>" ; ?></td>
                                <td><?php echo ($val->order_id_pg != "") ? $val->order_id_pg : "<span class='color_red'>N/A</span>" ; ?></td>
                                <td><?php echo ($val->name != "") ? $val->name : "<span class='color_red'>N/A</span>" ; ?></td>
                                <td><?php echo ($val->mobile != "") ? $val->mobile : "<span class='color_red'>N/A</span>" ; ?></td>
                                <td><?php echo ($val->mobile != "") ? $val->city : "<span class='color_red'>N/A</span>" ; ?></td>
                                <?php
                					if ($this->session->userdata('admin_role_id') == 1 || $this->session->userdata('admin_role_id') == 7) {
									?>
                                <td><?php echo ($val->net != "") ? '<a href="javscript:void(0)" title="'.$val->net.'">'.sub_string($val->net, 10).'</a>' : "<span class='color_red'>N/A</span>" ; ?></td> 
                                <td><?php echo ($val->pubid != "") ? '<a href="javscript:void(0)" title="'.$val->pubid.'">'.sub_string($val->pubid, 10).'</a>' : "<span class='color_red'>N/A</span>" ; ?></td>
                                <!--<td><?php echo ($val->sub_id != "") ? '<a href="javscript:void(0)" title="'.$val->sub_id.'">'.sub_string($val->sub_id, 10).'</a>' : "<span class='color_red'>N/A</span>" ; ?></td>-->
                                
                                <td><?php echo ($val->fire_status == "1") ? "<span class='color_green_bg'>Yes</span>" : "<span class='color_red_bg'>No</span>"; ?></td>
                                	<?php
									}
									if (array_constant_check ($this->session->userdata('admin_role_id'), ADMIN_PAYMENT_ROLES) || 
										array_constant_check ($this->session->userdata('uid'), ADMIN_PAYMENT_USERS)) {
										?>
										<!--<td width="7%"><?php echo ($val->payment_check_status == "1") ? "<span class='color_green_bg'>Yes</span>" : "<span class='color_yellow_bg'>No</span>"; ?></td>-->

                    <td width="7%"><?php echo substr(($val->qa_user != 0 ? $cod_agents_arr[$val->qa_user] : $cod_agents_arr[$val->qa_user]), 0,7); ?></td>
                                        
                                        <!--<th id="fraud_status_<?php echo $i;?>"><a href="javascript:change_fraud_status('<?php echo FULL_CMS_URL?>','<?php echo $tablename;?>','<?php echo $primary_field;?>','<?php echo $val->$primary_field;?>','is_fraud','fraud_status_<?php echo $i?>','<?php echo $cur_controller; ?>');"><?php echo ($val->is_fraud == '1') ? "<span class='color_green_bg'>Yes</span>" : "<span class='color_red_bg'>No</span>"; ?></a></td></td>-->
                            		<?php 
									}
								?>
                                <td><?php echo substr(ucfirst(($arr_all['ARR_CALLER_STATUS'][$val->caller_status] )), 0,8); ?></td> 
                                <td><?php echo ($val->payment_mode == 2) ? "<span class='color_red_bg'>COD</span>" : "<span class='color_green_bg'>Prepaid</span>"; ?></td> 
                                <td><?php echo ($val->payment_status == 1) ? "<span class='color_green_bg'>Paid</span>" : "<span class='color_red_bg'>Not Paid</span>"; ?></td>
                                <td>
                                	<?php 
									if (array_constant_check ($this->session->userdata('admin_role_id'), ADMIN_PAYMENT_ROLES) || 
										array_constant_check ($this->session->userdata('uid'), ADMIN_PAYMENT_USERS)) {
										?>
										<!--<a href="<?php echo FULL_CMS_URL?>/updatecredits/index/<?php echo $val->user_id;?>/#atab" target="_blank">
										<img src="<?php echo CMS_URL?>images/coins.png" title="Update Credit Points" height="15px" /></a>-->
									
										<?php
									}
                                    ?>
                                    <a href="<?php echo FULL_CMS_URL?>/updatepaymentstatus/index/<?php echo $val->id;?>/#atab" target="_blank">
                                    <img src="<?php echo CMS_URL?>images/order_edit.png" title="Update Payment Status" height="17px" /></a>
                                    <a href="<?php echo FULL_CMS_URL?>/orderdetails/index/<?php echo $val->id;?>/#atab" target="_blank">
                                    <img src="<?php echo CMS_URL?>images/order_details.png" title="Order Details" height="14px" /></a>
                                </td>
                                <td><a href="javascript:void(0)" title="<?php echo date('d-M-Y H:i:s A',strtotime($val->order_date));?>">
								<?php echo ($val->order_date != "") ? date('d-M-Y H:i:s A',strtotime($val->order_date)) : "N/A"; ?></a></td>
                           </tr>
                           <?php
                        $i++;
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="14" style="text-align:center"><?php echo $page_name;?> Not available.</td>
                        </tr>
                        <?php
                    }
                ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan="14" style="line-height: 1.5em; !important;text-align:right">
                    <?php
                      if (count($results) > 0) {
                         echo $links;
                      }
                    ?>
                </td>
              </tr>
            </tfoot>
            </table>
        	<?php
			}
		}
	?>
    </div>
  </div>
</div>
<div class="clear"></div>
