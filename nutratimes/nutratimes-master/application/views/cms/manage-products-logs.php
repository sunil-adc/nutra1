<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$add_page; ?>/index/add/#atab"><?php echo $add_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
      <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong>Product Id:</strong>
                    <input type="text" name="prod_id" value="<?php echo $prod_id;?>" class="text-input small-input" />
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        <?php	
               if (is_array($results) && count($results) > 0) {
            		?>
        <table>
        <thead>
          <tr>
       		<th>Product Id</th>
            <th>User</th>
            <th>Role</th>
            <th>Action</th>
            <th>Date</th>
          </tr>
        </thead>
        <tbody>
            	<?php	
                   $i = 1;
				   foreach ($results as $key => $val) {
                        ?>
                        <tr>
                            <td><?php echo $val->name; ?></td>
                            <td><?php echo $val->username; ?></td>
                            <td><?php echo $val->role_name; ?></td>
                            <td><?php echo $val->action; ?></td>
                            <td><?php echo time_diff ($val->log_date);?></td>
                       </tr>
                       <?php
                    $i++;
					}
            ?>
        </tbody>
        </table>
        	<?php
			   } else {
			   	?>
					<table><tr><td style="text-align:center">Enter Product id to get product logs</td></tr></table>
				<?php
			   }
		?>
    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>