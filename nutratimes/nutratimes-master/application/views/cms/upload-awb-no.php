<?php $arr_all = all_arrays(); ?>
<div class="content-box">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
      <div class="prod_full_div" style="padding-right:10px;">
          <div class="align-left prod_leftside">
    	 	<div class="tab-content default-tab"> <?php echo form_open_multipart($form_submit); ?>
        <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
		<fieldset>
        <p>
          <label>Provider : </label>
          <?php echo custom_dropdown('provider', '', $arr_all['ARR_PROVIDER'], '','class="text-input small-input"');?>
        </p>
        
        <p>
          <label>Payment Mode : </label>
          <?php echo custom_dropdown('payment_mode', '', $arr_all['ARR_PAYMENT'], '','class="text-input small-input"');?>
        </p>
        <p>
          <label>AWB No(s). : </label>
          <textarea name="tracknums" id="tracknums" class="text-input small-input" rows="10" cols="10"></textarea>
        </p>
        <p>
          <?php 
				$data = array(
					  'name'        => 'submit',
					  'id'          => 'submit',
					  'value'       => 'Save Changes',
					  'class'       => 'button',
					);
				echo form_submit($data); 
		  ?>
        </p>
        <?php
            echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden('cur_url', get_full_url());
		?>	
      </fieldset>
      <div class="clear"></div>
      <?php echo form_close();?> </div>
  		  </div>
          <div class="align-left prod_leftside">
    	 	<table>
        <thead>
            <tr>
                <th width="50%">Provider</th>
                <th width="50%">Tracking Number</th>
            </tr>
        </thead>
   		<?php
			if ($tracking_count->num_rows() > 0) { 
				echo '<pre>';
				foreach ($tracking_count->result_array() as $val) {
					?>
				    <tr>
                    	<td>CASH Xpressbee</td>
                        <td><?php echo $val['cash_xpressbee'];?></td>
                    </tr>
					<!--<tr>
                    	<td>CASH DTDC</td>
                        <td><?php echo $val['cash_dtdc'];?></td>
                    </tr>
					<tr>
                    	<td>CASH FirstFlight</td>
                        <td><?php echo $val['cash_ff'];?></td>
                    </tr>
					<tr>
                    	<td>CASH Seajin</td>
                        <td><?php echo $val['cash_seajin'];?></td>
                    </tr>
					<tr>
                    	<td>CASH Ecom Express</td>
                        <td><?php echo $val['cash_ecom'];?></td>
                    </tr>
					<tr>
                    	<td>CASH Red Express</td>
                        <td><?php echo $val['cash_red'];?></td>
                    </tr>-->
					<tr>
                    	<td>COD Xpressbee</td>
                        <td><?php echo $val['cod_xpressbee'];?></td>
                    </tr>
					<!--<tr>
                    	<td>COD DTDC</td>
                        <td><?php echo $val['cod_dtdc'];?></td>
                    </tr>
					<tr>
                    	<td>COD FirstFlight</td>
                        <td><?php echo $val['cod_ff'];?></td>
                    </tr>
					<tr>
                    	<td>COD Seajin</td>
                        <td><?php echo $val['cod_seajin'];?></td>
                    </tr>
					<tr>
                    	<td>COD Ecom Express</td>
                        <td><?php echo $val['cod_ecom'];?></td>
                    </tr>
					<tr>
                    	<td>COD Red Express</td>
                        <td><?php echo $val['cod_red'];?></td>
                    </tr>-->
					<?php
				}
			}
		?>             
   </table>
  		  </div>
      	  <div class="clear"></div>
      </div>
  </div>
</div>
<div class="clear"></div>
