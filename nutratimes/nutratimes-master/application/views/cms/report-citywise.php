<script type="text/javascript">
	$(function() {
		$( ".datepicker" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			showButtonPanel: true,
			inline: true
		});
	});
</script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>



<script>
function citywise_chart(val_c, chart_div, text){	
	
	Highcharts.chart(chart_div, {
	  chart: {
	    plotBackgroundColor: null,
	    plotBorderWidth: null,
	    plotShadow: false,
	    type: 'pie'
	  },
	  title: {
	    text: text
	  },
	  tooltip: {
	    pointFormat: '{series.name}: <b>{point.y}</b>'
	  },
	  plotOptions: {
	    pie: {
	      allowPointSelect: true,
	      cursor: 'pointer',
	      dataLabels: {
	        enabled: true,
	        format: '<b>{point.name}</b>: {point.y}',
	        style: {
	          color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	        }
	      }
	    }
	  },
	  series: [{
	    name: 'USER',
	    colorByPoint: true,
	    data:  JSON.parse(val_c)
	  }]
	});

}
</script>

<?php $arr_all = all_arrays(); ?>

<div class="content-box" id="mtab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$add_page; ?>/index/add/#atab"><?php echo $add_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
        <?php 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
	    
			if (isset($total_rows)) {
				?>
			   <div class="notification information png_bg">
				  <div>Total <?php echo number_format($total_rows); ?> records found in <?php echo $page_name;?></div>
			   </div>
			   <?php
			}
			
			if (isset($only_sql)) {
				$total_qty = 0;
				$r = mysql_query($only_sql);
				if ($r) {
					while($v = mysql_fetch_object($r)) {
						$total_qty += $v->total_prod;
					}
				}
			
				if ($total_qty > 0) {
				?>
				   <div class="notification information png_bg">
					  <div>Total <?php echo number_format($total_qty); ?> Items ordered</div>
				   </div>
				   <?php
				}
			}
		?>
       
         <form name="search" id="search" action="<?php echo $seach_form; ?>" method="post">
          <table>
            <tr>
                <td>
                    <strong><span class="color_red">*</span> From :</strong>
                    <input type="text" name="from_date" value="<?php echo (isset($from_date)) ? $from_date : date("Y-m-d", strtotime("-1 day"));?>" 
                    class="text-input small-input datepicker" />&nbsp;&nbsp;&nbsp;&nbsp;
                    
                    <strong><span class="color_red">*</span> To :</strong>
                    <input type="text" name="to_date" value="<?php echo (isset($to_date)) ? $to_date : date("Y-m-d");?>" 
                    class="text-input small-input datepicker" />&nbsp;&nbsp;
                    
                    <input type="submit" class="button" id="btn_search" name="btn_search" value="GO" />
                </td>
            </tr>        
          </table>
        </form>
        
        <?php 
        	if(is_array($results) && count($results) > 0){
				
				foreach($results as $k ){
					$city_name[] = array("name" =>$k->city, "y" => (int)$k->total_lead );
				}
				$citywise_totallead_json = json_encode($city_name);
			

			if($delivery_query->num_rows() > 0 ){
				foreach ($delivery_query->result() as $v) {
					$deliver_arr[] = array(  "name" =>$v->city, "y" => (int)$v->total_lead);		
				}
				$citywise_deliverlead_json = json_encode($deliver_arr);	
			}

			if($state_lead_query->num_rows() > 0 ){
				foreach ($state_lead_query->result() as $v) {
					$state_lead_arr[] = array(  "name" => $arr_all['ARR_STATE'][$v->state], "y" => (int)$v->total_lead);		
				}
				$statewise_totallead_json = json_encode($state_lead_arr);	
			}

			if($state_delivery_query->num_rows() > 0 ){
				foreach ($state_delivery_query->result() as $v) {
					$state_deliver_arr[] = array(  "name" => $arr_all['ARR_STATE'][$v->state], "y" => (int)$v->total_lead);		
				}
				$statewise_deliverlead_json = json_encode($state_deliver_arr);	
			}

		?>
            
        <script>
            $(document).ready(function(e) {
               citywise_chart('<?php echo $citywise_totallead_json   ?>', 'chartContainer_total_lead', 'Total Lead CityWise' );
               citywise_chart('<?php echo $citywise_deliverlead_json ?>', 'chartContainer_total_delivered', 'Total Delivered Lead Citywise' );
               citywise_chart('<?php echo $statewise_totallead_json ?>', 'chartContainer_state_total_lead', 'Total Lead Statewise' );
               citywise_chart('<?php echo $statewise_deliverlead_json ?>', 'chartContainer_state_total_delivered', 'Total Delivered Lead Statewise' );
            });
        </script>
        
        <?php 
    		}
        ?>
        <br>

        <div id="chartContainer_total_lead" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
        <br><br/>
        <div id="chartContainer_total_delivered" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
        <br><br/>
        <div id="chartContainer_state_total_lead" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>
        <br><br/>
        <div id="chartContainer_state_total_delivered" style="height: 370px; max-width: 920px; margin: 0px auto;"></div>



    </div>
    <!-- End #tab1 -->
    
  </div>
</div>
<div class="clear"></div>