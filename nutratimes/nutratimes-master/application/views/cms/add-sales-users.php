<?php $all_array = all_arrays(); ?>
<div class="content-box" id="atab">
  <div class="content-box-header">
    <h3><?php echo $page_title; ?></h3>
    <h3 style="float:right"><a href="<?php echo FULL_CMS_URL."/".$manage_page; ?>"><?php echo $manage_page_title;?></a></h3>
    <div class="clear"></div>
  </div>
  <div class="content-box-content">
    <div class="tab-content default-tab">
      <?php echo form_open_multipart($form_submit); ?>
         <?php 
			echo validation_errors('<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>','</div></div>'); 
			if( $this->session->flashdata('error') ) { 
				echo '<div class="notification error png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('error').'</div></div>';
			}else if( $this->session->flashdata('success') ) { 
				echo '<div class="notification success png_bg"><a href="#" class="close"><img src="'.CMS_URL.'/images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a><div>'.$this->session->flashdata('success').'</div></div>';
			}
		?>
        <fieldset>
        <div class="prod_full_div" style="padding-right:10px;">
            <div class="align-left prod_leftside">
                <div class="prod_box">
                    <div class="prod_title">SALES</div>
                    <div class="prod_desc">
        				<p>
                            <label><span class="color_red">*</span> <span class="color_blue">COD / Sales Agent</span> </label>
                            <?php 
								if ( isset($result_data['cod_user']) && $result_data['cod_user'] != false) {
									foreach ($result_data['cod_user']->result() as $val) {
										echo $val->username;
										$coduserid = $val->id;
										$arr_cod_states = explode(",", $val->cod_states);
									}
								} else {
									$this->session->set_flashdata('error', 'Don\'t try to direct access the page');
									redirect(FULL_CMS_URL.'/'.$manage_page);
								}
							?> 
                        </p>
                        
                        <p>
                            <label><span class="color_red">*</span> <span class="color_blue">States</span> </label> 
                            <select name="cod_states[]" id="cod_states[]" size="10" multiple="multiple" class="small-input">
                            <option value="">Select Option</option>
							<?php
								if (count($all_array['ARR_STATE']) > 0) {
									asort($all_array['ARR_STATE']);
									
									foreach ($all_array['ARR_STATE'] as $k => $v) {
										$selected = (in_array($k, $arr_cod_states)) ? 'selected' : '';
										echo "<option value='".$k."' ".$selected.">".$v."</option>";
									}
								} else {
									echo "<option value=''>Select Option</option>";
								}
							?>
							</select> 
                        </p> 
                    </div>
                </div>
            </div>
        	<div class="clear"></div>
        </div>
        <?php
            echo form_hidden('mode', 'edit');
			echo form_hidden('sales_agents', $coduserid);
			echo form_hidden('manage_page', $manage_page);
			echo form_hidden('add_page', $add_page);
			echo form_hidden($primary_field, $result_data[$primary_field]);
			echo form_hidden('cur_url', get_full_url());
			// SET THE FULL URL USING SEGMENT ARRAY
		?>
        <p>
          <input class="button" type="submit" name="submit" id="submit" value="Save Data" />
          <input class="button" onclick="window.location.href='<?php echo FULL_CMS_URL."/".$manage_page?>'" type="button" value="Go Back" />
        </p>
        </fieldset>
        <div class="clear"></div>
		<?php echo form_close(); ?>
    </div>
  </div>
</div>
<div class="clear"></div>
