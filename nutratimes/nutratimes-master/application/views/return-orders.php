<?php
	// ECHO ALL CATEOGRIES
	echo $category;
?>
<div class="full-wrapper"><br />
<div class="profile-wraper">
  <div class="cart-wrapper-header"> My <?php echo SITE_NAME?> / PROFILE </div>
  <?php echo profile_sidebar(); ?>
  <div class="profile-right">
    <div class="account-infor">Order Returns</div>
    <div class="order-main">
        <div class="order-header">
          <div class="label-orderno order-title left">Order No.</div>
          <div class="label-item order-title left">Items</div>
          <div class="label-total order-title left">Total Amount</div>
          <div class="label-date order-title left">Date Placed</div>
          <div class="clr"></div>
        </div>
        <?php
        	if(isset($order_returns) && is_array($order_returns)) {
				foreach ($order_returns as $row) {
					?>
					<div class="order-content">
						<div class="label-orderno left"><?php echo $row->order_id; ?></div>
						<div class="label-item left"><?php echo $row->total_items; ?></div>
						<div class="label-total left"><?php echo _r($row->order_total); ?></div>
						<div class="label-date left"><?php echo date('d-M-Y H:i:s A', strtotime($row->dt_c)); ?></div>
                        <div class="clr"></div>                 
					</div>
					<?php
				}
			} else {
				?>
				<div class="order-content">
					<div class="order_blank">Till now you have not returns any order.</div>
                </div>
				<?php
			}
		?>
    </div>
  </div>
  <div class="clr"></div>
</div>
</div>