<?php
	// ECHO ALL CATEOGRIES
	//echo $category;
?>

<div class="container">
	<div class="row">
		<div id="content" class="col-md-12 col-sm-12">
			<div class="content">		
				<div class="top-view-heading">
					<h4><?php echo strtoupper(stripslashes($page_details->page_name));?></h4>
				</div>
				<?php 
				if($page_details->page_id == 2){
				?>	
					
				<div id="content" class="col-sm-6">
				
					<form action="javascript:void(0)" method="post" enctype="multipart/form-data"
					class="contact-us-form form-horizontal" onsubmit= contact_us('<?php echo SITE_URL?>info/saveContactUs')  id="contact_us_frm" >
					   <fieldset>			
					   <div id="contactus_div"></div>			   
						   <div class="form-group required">
							   <div class="col-xs-12 col-sm-12">
								   <label class="control-label" for="input-name">Your Name</label>
								   <div>
									   <input type="text" name="name" id="name" class="form-control">
								   </div>
							   </div>
							   <div class="col-xs-12 col-sm-12">
								   <label class="control-label" for="input-email">E-Mail Address</label>
								   <div>
									   <input type="text" name="email" id="email" class="form-control">
								   </div>
							   </div>
							   <div class="col-xs-12 col-sm-12">
								   <label class="control-label" for="input-email">Mobile</label>
								   <div>
									   <input type="text" name="mobile" id="mobile" class="form-control only_numeric" maxlength="10" >
								   </div>
							   </div>
						   </div>
						   <div class="form-group required">
							   <div class="col-xs-12">
								   <label class="control-label" for="input-enquiry">Enquiry</label>
								   <div>
									   <textarea name="message" rows="5" id="message" class="form-control"></textarea>
								   </div>
							   </div>
						   </div>
					   </fieldset>
					   <div class="buttons">
						   <div class="align-center">
							   <input class="btn btn-black" type="submit" id="contact_sbmt" name="submit" value="Submit">
						   </div>
					   </div>
				   </form>
				
			  </div>
				<?php }
			  ?>	
				
				<?php echo stripslashes($page_details->page_description);?>
			</div>
		</div>
	</div>
</div>
	
  