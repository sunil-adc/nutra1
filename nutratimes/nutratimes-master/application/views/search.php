<?php $all_array = all_arrays(); ?>
<div class="wide-wrapper right">
  <div class="product-wrap"> 
    <div class="breadcrumb-search">
            <a href="<?php echo SITE_URL?>">Home</a> &raquo;&nbsp;
            <?php 
			if (isset ($_REQUEST['c']) && trim($_REQUEST['c']) > 0) { 
				
				if(check_mysql_inject($_REQUEST['c'])){
					header('Location:'.SITE_URL);   
				}
				
				$breadcrumb_category = trim(str_replace("|", ",", $_REQUEST['c']), ",");
                
				$sql = "select cat_id, name from ".CATEGORY." where cat_id in (?)";
				$res_breadcrumb = $this->db->query($sql, array($breadcrumb_category));
				
				$i = 1;
				$total_rows = $res_breadcrumb->num_rows();
				if ($res_breadcrumb->num_rows() > 0) {
					foreach ($res_breadcrumb->result() as $val_breadcrumb) {
						?><a href="<?php echo SITE_URL?>search/?c=<?php echo $val_breadcrumb->cat_id;?>"><?php echo $val_breadcrumb->name;?></a><?php
						if ($i < $total_rows) { echo "&nbsp;/&nbsp;"; }
						$i++;
					}
					echo '&nbsp;&raquo;&nbsp;';
				}
            }
            if (isset ($_REQUEST['sc']) && trim($_REQUEST['sc']) > 0) { 
			    
				if(check_mysql_inject($_REQUEST['sc'])){
					header('Location:'.SITE_URL);   
				}
				
				$breadcrumb_sub_category = trim(str_replace("|", ",", $_REQUEST['sc']), ",");
                
				$sql = "select cat_id, parent_cat_id, name from ".CATEGORY." where cat_id in (?)";
				$res_breadcrumb = $this->db->query($sql, array($breadcrumb_sub_category));
				$i = 1;
				$total_rows = $res_breadcrumb->num_rows();
				if ($res_breadcrumb->num_rows() > 0) {
					foreach ($res_breadcrumb->result() as $val_breadcrumb) {
						?><a href="<?php echo SITE_URL?>search/?c=<?php echo $val_breadcrumb->parent_cat_id;?>&sc=<?php echo $val_breadcrumb->cat_id;?>"><?php echo $val_breadcrumb->name;?></a><?php if ($i < $total_rows) { echo "&nbsp;/&nbsp;"; }
						$i++;
					}
				}
            }
            ?>
    </div>
    <!-- FIRST MOBILE WRAPPER START -->
    <div class="product-details-mainwrap">
      <dl class="product-details-wrap">
        <!--<div class="product-img"><a href="<?php echo $category_banner_link;?>"><img src="<?php echo $category_banner;?>" height="176px" /></a>
        </div>-->
        <dt class="heading" id="search-header">
          <h1 class="heading-text">Search <span title="<?php echo $this->input->get('q');?>"><?php echo ucfirst(strtolower(trim($this->input->get('q')) != '' ? ": ".sub_string(trim($this->input->get('q')), 60) : ''));?></span></h1>
        	<dd class="sortbox right">
              <span>Sort By           
                <?php
                $sort_var = "";
                if(isset($_REQUEST['_sort'])){
                    $sort_var = $_REQUEST['_sort'];
                }
				echo custom_dropdown('sort_by', 'sort_by', $all_array['ARR_SORTBY'], $sort_var,'class="sort-select"',true); ?>
                </span>
            </dd>
        </dt>
        <?php
			if($pagination['available'] == 1) {
				echo "<div class='page-number'>Page - ".$pageno."</div>";
				echo '<ul class="wrap-products products-search">';
				$i = 1;
			 
				$total_result = count($pagination['result']);
				foreach ($pagination['result'] as $k => $v) {
					normal_product_list($v, $i);
					if($i == 4 && $total_result > 1) {
						$i = 0;
						echo '</ul><ul class="wrap-products products-search">';
					}
					$i++;
					$total_result--;
				}
				echo '</ul>';
				?>		
        <div class="clr"></div>
        <span id="wholedata">
        <?php
			// SET REQUEST VARIABLES - START
			$req_brand 	= isset($_GET['b']) ? $_GET['b'] : (isset($_REQUEST['b']) ? $_REQUEST['b'] : '');
			$req_cat 	= isset($_GET['c']) ? $_GET['c'] : (isset($_REQUEST['c']) ? $_REQUEST['c'] : '');
			$req_subcat = isset($_GET['sc']) ? $_GET['sc'] : (isset($_REQUEST['sc']) ? $_REQUEST['sc'] : '');
			$req_price 	= isset($_GET['p']) ? $_GET['p'] : (isset($_REQUEST['p']) ? $_REQUEST['p'] : '');
			$req_size 	= isset($_GET['sz']) ? $_GET['sz'] : (isset($_REQUEST['sz']) ? $_REQUEST['sz'] : '');
			$req_sort 	= isset($_GET['_sort']) ? $_GET['_sort'] : (isset($_REQUEST['_sort']) ? $_REQUEST['_sort'] : '');
			$req_color 	= isset($_GET['clr']) ? $_GET['clr'] : (isset($_REQUEST['clr']) ? $_REQUEST['clr'] : '');
			// SET REQUEST VARIABLES - END
		?>
        <div id="pageLoaderParent" class="ajax-page-loader"><a href="javascript:page_loader()" id="pageLoader" page-redirecturl="<?php echo FULL_SITE_URL.$cur_controller?>/" page-datadiv="wholedata" page-func="page" page-offset="<?php echo ($pagination['page']['offset'] + $pagination['page']['perpage']); ?>" page-perpage="<?php echo $pagination['page']['perpage']; ?>" page-total="<?php echo $pagination['page']['total']; ?>" page-parentdiv="pageLoaderParent" page-loading="ajax-page-loader" page-cat="" page-whr="<?php echo (isset($whr)) ? preg_replace('![^a-z0-9]+!i', '-', $whr) : '';?>" page-brand="<?php echo $req_brand; ?>" page-category="<?php echo $req_cat; ?>" page-sub-category="<?php echo $req_subcat; ?>" page-sort="<?php echo $req_sort; ?>" page-price="<?php echo $req_price; ?>" page-number="<?php echo $pageno;?>" page-color="<?php echo $req_color;?>" page-size="<?php echo $req_size;?>">Click here to load more...</a></div>
        </span>
			<?php
			} else {
				?><div class="ajax-page-loader">You’re being a little too picky, expand your search criteria to view the results.</div><?php
			}	
		?>
      </dl>
    </div>
    <!-- FIRST MOBILE WRAPPER END --> 
    
  </div>
</div>
<div class="clr"></div>
<?php
	$category = '';
	if (isset($_GET['c']) && trim($_GET['c']) != '' && isset($_GET['sc']) && trim($_GET['sc']) != '') {
		$category = str_replace("|", ",", $_GET['sc']);
		
		$sql = "select name, parent_cat_id from ".CATEGORY." where cat_id in (?)";
		$res = $this->db->query($sql, array($category));
		
		if ($res->num_rows() > 0){
			$category_name = $parent_cat_id = '';
			foreach ($res->result() as $val) {
				$category_name .= "".$val->name.",";
				$parent_cat_id .= "".$val->parent_cat_id.",";
			}
			$parent_cat_id = trim($parent_cat_id, ",");
			$category_name = trim($category_name, ",");
			
			
			$req_parent_category = str_replace("|", ",", $_GET['c']);
			if (strlen($req_parent_category) > 1) {
				$parent_cat_id = $req_parent_category;
			}
			
			$sql = "select name from ".CATEGORY." where cat_id in (?)";
			$res = $this->db->query($sql, array($parent_cat_id));
			if ($res->num_rows() > 0){
				$parent_category_name = '';
				foreach ($res->result() as $val) {
					$parent_category_name .= "".$val->name.",";
				}
				$parent_category_name = trim($parent_category_name, ",");
			}
		
		}
		?>
		<script type="text/javascript">
			window._atm_client_id = "6306";
			window._atm_params = new Object;
			_atm_params.t = "r";
			_atm_params.f="b";
			_atm_params.id="";
			_atm_params.cn="<?php echo $parent_category_name;?>";
			_atm_params.scn="<?php echo $category_name;?>";
			_atm_params.r="";
			_atm_params.custom_prop1="Desktop";
		
			var ast=document.createElement('script'); ast.type="text/javascript";
			ast.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//cdn.atomex.net/static/js/pxs/" + _atm_client_id + "/ast.js";
			document.body.appendChild(ast);
		</script>
        
        <script type='text/javascript'>
			rtgsettings ={
					'pdt_url': '',
					'pdt_category_list': '<?php echo $parent_category_name;?>',
					'pagetype': 'category',
					'key': 'DIR',
					'token': 'Shophunk_IN',
					'layer': 'iframe'
				};
			(function(d) {
				var s = d.createElement('script'); s.async = true;s.id='madv2014rtg';s.type='text/javascript';
				s.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.mainadv.com/Visibility/Rtgdir2-min.js';
				var a = d.getElementsByTagName('script')[0]; a.parentNode.insertBefore(s, a);
			}(document));
		</script>
		<?php
	
	} else if (isset($_GET['c']) && $_GET['c'] != '') {
		$category = str_replace("|", ",", $_GET['c']);
		
		
		$sql = "select name from ".CATEGORY." where cat_id in (?)";
		$res = $this->db->query($sql, array($category));
		if ($res->num_rows() > 0){
			$category_name = '';
			foreach ($res->result() as $val) {
				$category_name .= "".$val->name.",";
			}
			$category_name = trim($category_name, ",");
		}
		?>
		<script type="text/javascript">
			window._atm_client_id = "6306";
			window._atm_params = new Object;
			_atm_params.t = "r";
			_atm_params.f="b";
			_atm_params.id="";
			_atm_params.cn="<?php echo $category_name;?>";
			_atm_params.scn="";
			_atm_params.r="";
			_atm_params.custom_prop1="Desktop";
		
			var ast=document.createElement('script'); ast.type="text/javascript";
			ast.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + "//cdn.atomex.net/static/js/pxs/" + _atm_client_id + "/ast.js";
			document.body.appendChild(ast);
		</script>
        
        <script type='text/javascript'>
			rtgsettings ={
					'pdt_url': '',
					'pdt_category_list': '<?php echo $category_name;?>',
					'pagetype': 'category',
					'key': 'DIR',
					'token': 'Shophunk_IN',
					'layer': 'iframe'
				};
			(function(d) {
				var s = d.createElement('script'); s.async = true;s.id='madv2014rtg';s.type='text/javascript';
				s.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.mainadv.com/Visibility/Rtgdir2-min.js';
				var a = d.getElementsByTagName('script')[0]; a.parentNode.insertBefore(s, a);
			}(document));
		</script>
		<?php
	}
?>