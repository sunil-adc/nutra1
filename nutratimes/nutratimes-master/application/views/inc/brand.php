<?php
	if(count($brands) > 0) {
	?>
	<div class="full-wrapper">
	   <div class="brands">
        <ul>
          <?php
			foreach ($brands as $k => $v) {
			  ?>     
			  <li><a href="<?php echo SITE_URL.'search?&b='.$v->brand_id?>"> <img border="0" height="39" alt="<?php echo $v->name;?>" title="<?php echo $v->name;?>" src="<?php echo S3_URL.product_or_placeholder(BRAND_THUMB_IMG_DIR.THUMB_PREFIX.$v->image);?>"> </a> </li><?php
			}
           ?>
        </ul>
	  </div>
	</div>
	<?php
	}
?>