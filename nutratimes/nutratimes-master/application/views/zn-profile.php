<?php
	$all_array = all_arrays();
	// ECHO ALL CATEOGRIES	
?>

<div class="container">
  <div class="row">
    <aside id="column-left" class="col-md-3 col-sm-4 mb-xs-30 hidden-xs sidebar-block">
      <div class="list-group"> 
       <a href="<?php echo SITE_URL?>myaccount" class="list-group-item">My Account</a>
       <a href="<?php echo SITE_URL?>profile" class="list-group-item">Edit Account</a> 
       <a href="<?php echo SITE_URL?>changePasswordUser" class="list-group-item">Password</a>
       <a href="<?php echo SITE_URL?>fulladdress" class="list-group-item">Address Book</a>
       <a href="<?php echo SITE_URL?>orderhistory" class="list-group-item">Order History</a> 
        <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/recurring" class="list-group-item">Recurring payments</a>--> 
        <!--<a href="<?php echo SITE_URL?>returnorders" class="list-group-item">Returns</a> -->
        <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/newsletter" class="list-group-item">Newsletter</a>--> 
        <a href="<?php echo SITE_URL?>logout" class="list-group-item">Logout</a> </div>
    </aside>
    <div id="content" class="col-sm-9">
      <form method="post" action="javascript:void(0)" onsubmit="update_proifle('<?php echo SITE_URL?>profile/save')" class="form-horizontal">
        <fieldset>
        <div class="heading-bg mb-30">
          <h2 class="heading m-0">My Account Information</h2>
          <div id="profile_div_err" style="margin-top: 15px;color: red;font-size: 14px; font-weight: 600"></div>	
        </div>
        
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-email">Gender</label>
          <div class="col-sm-7"> <span class="form-control" style="border-color: #fff;">
            <input type="radio" name="gender" value="1" <?php echo ($user_details->gender == '1') ? "checked='checked'" : "";?> />
            &nbsp; Male &nbsp;&nbsp;
            <input type="radio" name="gender" value="2" <?php echo ($user_details->gender == '2') ? "checked='checked'" : "";?> />
            &nbsp; Female</span> </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-email">Full Name</label>
          <div class="col-sm-7">
            <input type="text" placeholder="Full Name" value="<?php echo ucfirst($user_details->name);?>" class="form-control alpha_space" id="name" name="name">
          </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-email">Mobile</label>
          <div class="col-sm-7">
            <input type="text" placeholder="Mobile Number" class="form-control" id="mobile" name="mobile" pattern="\d*" maxlength="10" value="<?php echo $user_details->mobile;?>" disabled="disabled">
          </div>
        </div>
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-email">E-Mail</label>
          <div class="col-sm-7">
            <input type="text" placeholder="Valid Email Address" value="<?php echo $user_details->email;?>" class="form-control" id="email" value="" name="email">
          </div>
        </div>
        <!--<div class="form-group required"> 
						<label class="col-sm-2 control-label" for="input-email">Address</label>
                        <div class="col-sm-8">
						<textarea placeholder="Address" class="form-control" id="address" name="address" class="form-control"><?php echo $user_details->address;?></textarea>
                        </div>
                    </div>
                    <div class="form-group required"> 
                        <label class="col-sm-2 control-label" for="input-email">City</label>
                        <div class="col-sm-8">
                        <input type="text" placeholder="City" class="form-control" id="city" name="city" value="<?php echo $user_details->city;?>">
                        </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-email">State</label>
                        <div class="col-sm-8">
                        <select name="state" id="state" class="form-control" >
                            <option value="">Select Option</option>
							<?php
								if (count($all_array['ARR_STATE']) > 0) {
									asort($all_array['ARR_STATE']);
									foreach ($all_array['ARR_STATE'] as $k => $v) {
										$selected = ($k == $user_details->state) ? "selected='selected'" : "";
										echo "<option value='".$k."' ".$selected.">".$v."</option>";
									}
								} else {
									echo "<option>Select Option</option>";
								}
							?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group required">
                        <label class="col-sm-2 control-label" for="input-email">Pincode</label>
                        <div class="col-sm-8">
                        <input type="text" placeholder="Pincode" class="form-control only_numeric" maxlength="6" id="pincode" name="pincode" value="<?php echo $user_details->pincode;?>">                   
                      </div>
                    </div>-->
        
        <div class="form-group">
          <input type="submit" value="Submit" id="submit" name="submit" class="btn btn-black" style="margin-left: 18%;margin-top: 30px;" />
        </div>
      </form>
      </fieldset>
    </div>
  </div>
</div>

<div class="scroll-top">
  <div id="scrollup"></div>
</div>
