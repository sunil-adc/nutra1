<?php
	// ECHO ALL CATEOGRIES
	//echo $category;
?>

<!--<div class="bread-crumb mb-30 center-xs">
  <div class="container">
    <div class="page-title">Order History</div>
    <div class="bread-crumb-inner right-side float-none-xs">
    <ul>
            <li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=common/home"><i class="fa fa-home"></i></a></li>
            <li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/account">Account</a></li>
            <li><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/order">Order History</a></li>
          </ul>
    </div>
  </div>
</div>-->
<div class="container">
  <div class="row"><aside id="column-left" class="col-md-3 col-sm-4 mb-xs-30 hidden-xs sidebar-block">
    <div class="list-group">
      <a href="<?php echo SITE_URL?>myaccount" class="list-group-item">My Account</a>
      <a href="<?php echo SITE_URL?>profile" class="list-group-item">Edit Account</a> 
      <a href="<?php echo SITE_URL?>changePasswordUser" class="list-group-item">Password</a>
      <a href="<?php echo SITE_URL?>fulladdress" class="list-group-item">Address Book</a>
      <a href="<?php echo SITE_URL?>orderhistory" class="list-group-item">Order History</a> 
      <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/recurring" class="list-group-item">Recurring payments</a>-->
      <!--<a href="<?php echo SITE_URL?>returnorders" class="list-group-item">Returns</a>-->
      <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/newsletter" class="list-group-item">Newsletter</a>-->
      <a href="<?php echo SITE_URL?>logout" class="list-group-item">Logout</a>
  
  </div>
  </aside>
      <div id="content" class="col-sm-9">     
       <div class="heading-bg mb-20"> 
       <h2 class="heading m-0">Order History</h2>
      </div> 
       <div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right">Order ID</td>
              <td class="text-left">Customer</td>
              <td class="text-right">Quantity</td>
              <td class="text-left">Payment Status</td>
              <td class="text-left">Payment Mode</td>
              <td class="text-right">Total</td>
              <td class="text-left">Date Added</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
          
          <?php
          if(isset($order_history) && is_array($order_history)) {
            $i = 1;
            foreach ($order_history as $row) {
          ?>  
          
           <tr>
              <td class="text-right"><?php echo "NTS0".$row->order_id; ?></td>
              <td class="text-left"><?php echo $row->name; ?></td>
              <td class="text-right"><?php echo $row->total_items; ?></td>
              <td class="text-left"><?php echo $row->payment_status == 1 ? '<span class="order_paid">PAID</span>' : '<span class="order_notpaid">PENDING</span>'; ?></td>
              <td class="text-left"><?php echo $row->payment_mode != 2 ? '<span class="order_paid">Online</span>' : '<span class="order_notpaid">COD</span>'; ?></td>
              <td class="text-right"><?php echo _r($row->order_total); ?></td>
              <td class="text-left"><?php echo date('d-M-Y', strtotime($row->dt_c)); ?></td>
              <td class="text-right"><a href="<?php echo SITE_URL?>orderhistory/detail/<?php echo $row->order_id?>" data-id="<?php echo $i?>" data-order-id="<?php echo $row->order_id?>" data-toggle="tooltip" title="View" class="btn btn-info"><i class="fa fa-eye"></i></a></td>
            </tr>
         <?php
            }
          } 
          ?>
         </tbody>
        </table>
      </div>
      <div class="row">
        <div class="col-sm-6 text-left"></div>
        <div class="col-sm-6 text-right">Showing 1 to 1 of 1 (1 Pages)</div>
      </div>
            <div class="buttons clearfix">
        <!--<div class="pull-right"><a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/account" class="btn btn-black">Continue</a></div>-->
      </div>
      </div>
    </div>
</div>
