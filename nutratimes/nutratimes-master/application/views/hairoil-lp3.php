<!DOCTYPE html>
<html lang="en">

<head>
  <title>Best Anti Hair Fall Oil and Shampoo brand in India - Nutratimes</title>
  <meta charset="utf-8">
  
  
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Buy Nutratimes anti hair fall oil and shampoo made up of 40 natural herbs at reasonable  prices in India.A perfect hair loss treatment for hair loss, baldness and damaged hairs.">
  <meta name="keywords" content="hair loss treatment,anti hair fall shampoo,hair fall shampoo,sulfate free shampoo,best organic shampoo,best shampoo for hair fall in India,best shampoo for hair growth and thickening,hair thickening shampoo.">


  <link href="<?php echo S3_URL?>cdn/hairoil-lp/3/img/favicon.png" rel="icon" />  
  
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.css"/>
    
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick-theme.css"/>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

  <link rel="stylesheet" type="text/css" href="<?php echo S3_URL?>cdn/hairoil-lp/3/css/styles.css">

  <link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-149373751-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-149373751-1');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 798083341 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-798083341"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-798083341');
</script>

<!-- Taboola Pixel Code -->
<script type='text/javascript'>
  window._tfa = window._tfa || [];
  window._tfa.push({notify: 'event', name: 'page_view', id: 1065107});
  !function (t, f, a, x) {
         if (!document.getElementById(x)) {
            t.async = 1;t.src = a;t.id=x;f.parentNode.insertBefore(t, f);
         }
  }(document.createElement('script'),
  document.getElementsByTagName('script')[0],
  '//cdn.taboola.com/libtrc/unip/1065107/tfa.js',
  'tb_tfa_script');
</script>
<noscript>
  <img src='https://trc.taboola.com/1065107/log/3/unip?en=page_view'
      width='0' height='0' style='display:none'/>
</noscript>
<!-- End of Taboola Pixel Code -->

</head>

<body>
  <?php $all_array = all_arrays(); ?>

 <div class="container-fluid">

    <div class="notification-bar web_banner">
        <p>
          <strong>WARNING:-</strong> We currently have limited Bottles IN STOCK. Stocks sell out fast,
          <strong>act today!</strong>
          <a href="#feedbackForm" class="btn btn-order go-to-form">Place My Order
            <i></i>
          </a>
        </p>
      </div>



    <nav class="navbar  navbar-default  navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand" href="#"><img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/nutratimes_hair_oil.png" alt="Logo" height="47px"></a>
          </div>    
        </div>
    </nav>
    

    <!-- Banner Starts Here -->

    <section id="banner">
          <div class="wrapper">
            <div class="bg_banner web_banner">
              <div class="container">
                    <div class="col-md-12 bannerText">
                       
                         <div class="col-md-4" style="float: right; margin-top: 1em; margin-right: -6em;">

                          <form role="form" id="feedbackForm" name="user_signup" class="feedbackForm" action="javascript:void(0)" onsubmit="javascript:return ajaxReg_hairoil('<?php echo SITE_URL?>buyregister', '1');" >
                            <div class="col heading">
                              <h4>TELL US WHERE TO SEND</h4>                           
                            </div> 
      
                            <div class="form_items">       
                            
                              <div class="col">   
                                    
                                  <div class="form-group">
                                      <input type="text" class="form-control" id="name1" name="name1" placeholder="Name" data-attr="Please enter correct Name" tabindex="1">
                                      <span class="help-block" id="name_err1"></span>
                                  </div>

                                  <div class="form-group">
                                    <input type="text" class="form-control only_numeric phone" id="mobile1" name="mobile1" pattern="\d*" maxlength="10" placeholder="Mobile Number" data-attr="Please enter correct Mobile number" tabindex="2">
                                    <span class="help-block" id="mobile_err1"> </span>
                                </div> 
                              
                                  <div class="form-group">
                                      <input type="text" class="form-control" id="email1" name="email1" placeholder="Email Address" data-attr="Please enter correct email" tabindex="3">
                                      <span class="help-block" id="email_err1"></span>
                                  </div>
                                  
                                  
                                  <div class="form-group">
                                      <input type="text" class="form-control" id="address1" name="adderss1" placeholder="Street Address" data-attr="Please Enter Correct Address" tabindex="4">
                                      <span class="help-block" id="address_err1"> </span>
                                  </div>
  
                                  <div class="form-group">
                                      <input type="text" class="form-control" id="city1" name="city1" placeholder="Enter City" data-attr="Please Enter Correct City" tabindex="5">
                                      <span class="help-block" id="city_err1"> </span>
                                  </div>
  
                                  <div class="form-group">
                                    <select class="form-control" name="state1" id="state1" tabindex="6">
                                      <option value="">Select State</option>
                                      <?php
                                        if (count($all_array['ARR_STATE']) > 0) {
                                          
                                          asort($all_array['ARR_STATE']);
                                          foreach ($all_array['ARR_STATE'] as $k => $v) {
                                            
                                            echo "<option value='".$k."' >".$v."</option>";
                                          }
                                        } else {
                                          echo "<option>Select Option</option>";
                                          echo "<option value='1'>ka</option>";
                                        }
                                      ?>
                                    </select>
                                    <span class="help-block" id="state_err1"> </span>
                                  </div>
  
                                  <div class="form-group">
                                    <input type="text" class="form-control" id="pincode1" name="pincode1" placeholder="Enter Pincode" data-attr="Please Enter Correct Pincode" tabindex="7">
                                    <span class="help-block" id="pincode_err1"> </span>
                                  </div>
                
                                   <input type="hidden" name="oos" value="0" id="oos" />
                                   <input type="hidden" name="pid" value="6" id="pid" />
                                   <input type="hidden" name="siteurl" value="<?php echo FULL_SITE_URL?>" id="siteurl" />

                                <div class="submitbtncontainer">
                                    <button type="submit" id="register_btn1" value="Submit" name="submit" class="button">
                                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/risk-form.gif" alt="buynow">
                                    </button>
                                </div>
      
                                <div class="secure_text">
                                  SECURE 256-BIT SSL ENCRYPTION
                                </div>
                                <div class="secure">
                                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/secure.jpg" alt="secure">
                                </div>
                              </div>
                            </div>
                          </form>

                        </div>

                     </div>
              </div>
    
            </div>
            <div class="clearfix"></div>
            <div class="m_banner">

            </div>  
          </div>
    </section>

    <!-- Banner Ends Here -->

    <div class="clearfix"></div>

    <!-- Mobile Form Starts Here -->

    <div class="mobilform">

        <form role="form" id="feedbackForm" name="user_signup" class="feedbackForm" action="javascript:void(0)" onsubmit="javascript:return ajaxReg_hairoil('<?php echo SITE_URL?>buyregister', '2');" >
          <div class="col heading">
            <h4>TELL US WHERE TO SEND</h4>                           
          </div> 

          <div class="form_items">       
          
            <div class="col">   
                <span class="help-block" id="global_err2"></span>
                <div class="form-group">
                    <input type="text" class="form-control" id="name2" name="name2" placeholder="Name" data-attr="Please enter correct Name" tabindex="1">
                    <span class="help-block" id="name_err2"></span>
                </div>

                <div class="form-group">
                  <input type="text" class="form-control only_numeric phone" id="mobile2" name="mobile2" pattern="\d*" placeholder="Mobile Number" data-attr="Please enter correct Mobile number" tabindex="2">
                  <span class="help-block" id="mobile_err2"> </span>
              </div> 
            
                <div class="form-group">
                    <input type="text" class="form-control" id="email2" name="email2" placeholder="Email Address" data-attr="Please enter correct email" tabindex="3">
                    <span class="help-block" id="email_err2"></span>
                </div>
                
                
                <div class="form-group">
                    <input type="text" class="form-control" id="address2" name="adderss2" placeholder="Street Address" data-attr="Please Enter Correct Address" tabindex="4">
                    <span class="help-block" id="address_err2"> </span>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" id="city2" name="city2" placeholder="Enter City" data-attr="Please Enter Correct City" tabindex="5">
                    <span class="help-block" id="city_err2"> </span>
                </div>

                <div class="form-group">
                  <select class="form-control" name="state2" id="state2" tabindex="6">
                    <option value="">Select State</option>
                    <?php
                      if (count($all_array['ARR_STATE']) > 0) {
                        
                        asort($all_array['ARR_STATE']);
                        foreach ($all_array['ARR_STATE'] as $k => $v) {
                          
                          echo "<option value='".$k."' >".$v."</option>";
                        }
                      } else {
                        echo "<option>Select Option</option>";
                        echo "<option value='1'>ka</option>";
                      }
                    ?>
                  </select>
                  <span class="help-block" id="state_err2"> </span>
                </div>

                <div class="form-group">
                  <input type="text" class="form-control" id="pincode2" name="pincode2" placeholder="Enter Pincode" data-attr="Please Enter Correct Pincode" tabindex="7">
                  <span class="help-block" id="pincode_err2"> </span>
                </div>

                 <input type="hidden" name="oos" value="0" id="oos" />
                 <input type="hidden" name="pid" value="6" id="pid" />
                 <input type="hidden" name="siteurl" value="<?php echo FULL_SITE_URL?>" id="siteurl" />

              <div class="submitbtncontainer">
                  <button type="submit" id="register_btn2" value="Submit" name="submit" class="button">
                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/risk-form.gif" alt="buynow">
                  </button>
              </div>

              <div class="secure_text">
                SECURE 256-BIT SSL ENCRYPTION
              </div>
              <div class="secure">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/2/images/secure.jpg" alt="secure">
              </div>
            </div>
          </div>
        </form>

    </div>

    <!-- Mobile Form Ends Here -->

    <div class="clerfix"></div>

    <!-- About Section Starts Here --> 
    
    <section class="container about mtop70">
      <div class="col-md-12 aboutSection">
        <h3> <span class="skyBlue"> HAIR LOSS </span> <b class="darkGreen"> AN INEVITABLE REALITY </b> </h3>

          <p class="mtop35 centerText">Millions of men around the world suffer Male Pattern Baldness, also known as, Androgenetic Alopecia. This condition is genetic and happens over a period of time, which is well before the age of 50. This condition is observed in women as well.</p>

          <p class="mtop35 centerText">There is no set age in men at which you can assert the onset of hair loss symptoms. It is noticed over a period of time and the symptoms are:</p>
        
        <br>
        <br>

        <div class="col-md-12  textcentre col-sm-12">

          <div class="col-md-6 col-sm-12">
            <div class="col-md-5 col-sm-12 floatnone">
              <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/thininghair.png" alt="" class="problemsimg">
            </div>

            <div class="col-md-6 col-sm-12 floatnone">
              <div class="problemsText mtop10"><strong>THINNING HAIR</strong></div>
            </div>
          </div>

          <div class="col-md-6 col-sm-12 textcentre mtop30">
            <div class="col-md-5 col-sm-12 floatnone">
              <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/hairBreakage.png" alt="" class="problemsimg">
            </div>

            <div class="col-md-6 col-sm-12 floatnone">
              <div class="problemsText mtop10"><strong>HAIR BREAKAGE</strong></div>
            </div>
          </div>

        </div>

        <div class="clearfix"></div>

        <br>
        <br>

        <div class="col-md-12 textcentre col-sm-12">

          <div class="col-md-6 col-sm-12 ">
            <div class="col-md-5 col-sm-12 floatnone">
              <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/pattern_baldness.png" alt="" class="problemsimg">
            </div>

            <div class="col-md-6 col-sm-12 floatnone">
              <div class="problemsText mtop10"><strong>PATTERN BALDNESS</strong></div>
            </div>
          </div>

          <div class="col-md-6 col-sm-12 mtop30 textcentre">
            <div class="col-md-5 col-sm-12 floatnone">
              <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/hair_loss.png" alt="" class="problemsimg">
            </div>

            <div class="col-md-6 col-sm-12 floatnone">
              <div class="problemsText mtop10"><strong>HAIR LOSS</strong></div>
            </div>
          </div>

        </div>

        <br>

        <div class="col-md-12 about2ndSection">
          <div class="col-md-6 description">
            <p>
              Everyone knows the importance of hair and multiple studies prove that hair loss negatively impacts almost every aspect of life. Men who experience thinning hair and hair loss have trouble finding well-paying jobs and attracting the opposite sex. They experience higher rates of depression, social anxiety and a lack of self confidence.
            </p>

          </div>

          <div class="col-md-6 centerText mtop30 mleft">
            <div class="col-md-6">
              <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/before.png" alt="">
              <div class="mtop10"><strong>Before</strong></div>
              
            </div>
 
            <div class="col-md-6 centerText mtop30">
              <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/after.png" alt="">
              <div class="mtop10">
                <strong>After</strong>
              </div>
            </div>
          </div>

          

        </div>
        
      </div>
    </section>

    <!--About Section Ends Here -->

    <div class="clearfix"></div>

    <!-- Problems Section Starts Here -->

    <section class="container problems" >
      
      <div class="col-md-12 description">
          <p class="mtop35 centerText font16"> <span class="skyBlue">NUTRATIMES</span> brings to you a revolutionary hair product to help you with balding scalp and hair loss. Nutratimes hair regrowth oil & shampoo combo is formulated especially for people suffering from hair loss and balding due to various reasons including genetic factors. The oil contains the natural goodness of <span class="skyBlue">BRAHMI, ASHWAGANDHA, BHRINGARAJ and other 40 herbs from kerala.</span> These herbs stop hair fall and thinning of hair, and in return helps in hair regrowth and increasing its density. The shampoo contains<span class="skyBlue"> ALOEVERA, TULASI, AMLA, NEEM and HIBISCUS </span>which leaves your hair feeling fresh and replenished! For best results, it is recommended to use this combo of oil and shampoo provided by Nutratimes.</p>
      </div>

    </section>

    <!-- Problems Section Ends Here -->



    <div class="clearfix"></div>

    <!-- Science Section Starts Here -->

    <section class="container science">
        <div class="col-md-12 centerText">
          <h3> <span class="skyBlue">THE SCIENCE BEHIND </span> 
             <span class="darkGreen"> <b> NUTRATIMES HAIR REGROWTH </b> </span> </h3>
        </div>

        <div class="col-md-12 centerText science2nd mtop20">
            <p class="font16"> <b class="darkGreen"> Nutratimes Ayurvedic Hairoil + Shampoo(Combo)</b> has been formulated by a team of leading dermatologists to support hair nourishment, regrowth, and immunity across various stages of the hair growth cycle.</p>
        </div>

        <div class="col-md-12 cycle">

            <div class="col-md-12 mtop30">
                <div class="col-md-6 stageOneSec">
                  <h4> <span class="skyBlue"> STAGE 1 </span> - ANAGEN - GROWING PHASE </h4>
                  <p>Nourishes the scalp and hair follicles from within, facilitating their emergence from the sebaceous glands.</p>
                    
                </div>
                <div class="col-md-6"></div>
            </div>

            <div class="col-md-12">
                <div class="col-md-6">
                    
                </div>
                <div class="col-md-6 stageTwoSec">
                  <h4> <span class="skyBlue"> STAGE 2 </span> - CATAGEN - REGRESSION PHASE </h4>
                  <p>Prevents hair fall and damage while Strengthening and promoting the growth of your existing hair.</p>
                </div>
                
            </div>

            <div class="col-md-12">
                <div class="col-md-6 stageThreeSec">
                  <h4> <span class="skyBlue"> STAGE 3 </span> - TELOGEN - RESTING PHASE</h4>
                  <p>This is the resting phase where around 10-15 percent of hairs will fall into. Whilst the old hair is resting, a new thick hair begins the growth with the help of Nutratimes Hairoil.</p>
                </div>
                <div class="col-md-6"></div>
            </div>

            <div class="col-md-12">
                <div class="col-md-6">
                    
                </div>
                <div class="col-md-6 stageFourSec">
                  <h4> <span class="skyBlue"> STAGE 4 </span> - EXOGEN - SHEDDING PHASE </h4>
                  <p>This is part of the resting phase where the old hair sheds and a new hair continues to grow.  But research shows that Nutratimes shampoo usage minimize hairs fall on daily basis and simultaneously Nutratimes Hairoil nourishes the hair roots with required nutrients to grow new hair..</p>
                </div> 
            </div>
       
            

        </div>

    </section>

    <!-- Science Section Ends Here -->

    <div class="clearfix"></div>

   <!-- Advertise Section Starts Here -->

   <section class="buy blue_gradient">

      <div class="container ">

          <div class="col-md-12 marginFourty">

             <div class="col-md-8">
                <h3>NUTRATIMES GUARANTEES HAIR REGROWTH </h3>
                <p>Place your order today and claim your Risk FREE BOTTLES, because time and hair loss wait for no one!</p>
                <a href="#feedbackForm" class="orderButton">
                  Place My Order
                </a>
              </div>

             <div class="col-md-4 mtop15">
                <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/bottle_tilt.png" alt="" >
             </div> 

          </div>

      </div>

   </section>

   <!-- Advertise Section Ends Here -->

   <div class="clearfix"></div>


    <!-- Benefits Section starts Here -->

    <section class="container benefitsHead">
        <div class="col-md-12 centerText">
          <h3 class="mtop70"> <span class="skyBlue"> BENEFITS OF </span> <b class="darkGreen"> NUTRATIMES FORMULA </b> </h3>
        </div>

    </section>

    <section class="benefits">

    </section>


    <!-- Benefits Section Ends Here -->
    
   

     <!-- Strengths Section Starts Here -->
      
     <section class="container strength">

       <div class="col-md-12 centerText">
        <h3 class="mtop70"> <span class="skyBlue"> CLINICAL STRENGTH </span> <b class="darkGreen"> HAIR REGROWTH MATRIX</b> </h3>
       </div>
      
       <div class="col-md-12 wrapper strengths mleft15">

              <div class="col-md-5">
              <div class="col-md-12">
                  <div class="col-md-5">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Brahmi-Olej.png" alt="">
                    </div>
                    <div class="col-md-7">
                      <h5 class="blue">BRAHMI</h5>
                      <p>Supports circulation of nutrition to hydrate and deep-conditioned the hair follicles. Through the goodness of Brahmi, Nutratimes Hairoil + Shampoo(Combo) helps to repair damaged hair and prevents further hair damage in future.</p>
                    </div>
              </div>                
              </div>     
           
              <div class="col-md-2"></div>
              <div class="col-md-5 mtop20">
                  <div class="col-md-12 ">
                      <div class="col-md-5">
                          <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Ashwagandha-Roots.png" alt="">
                        </div>
                        <div class="col-md-7">
                            <h5 class="blue">ASHWAGANDHA</h5>
                            <p>Ashwagandha in Nutratimes Hairoil + Shampoo(Combo) helps to build collagen and supports absorption of iron, which keeps your hair locks strong and healthy. This advanced formulation of Nutratimes Hairoil + Shampoo(Combo) also helpful in preventing split ends.</p>
                        </div>
                  </div>                
              </div> 
              
                          
        </div>
              
        
  
        <div class="col-md-12">
   
            <div class="col-md-5 mtop70 mtop20">
              <div class="col-md-12">
                <div class="col-md-5">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Bhringaraj.png" alt="">
                </div>

                <div class="col-md-7">
                  <h5 class="blue">BHRINGARAJ</h5>
                  <p>Nutratimes and its hair regrowth formulation contains ingredient like Bringaraj that boosts elasticity of the cortex. It prevents hair breakage by stimulating dormant follicles for hair regrowth.</p>
                </div>

              </div>             
              
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5 mtop70 mtop20">
              <div class="col-md-12">
                <div class="col-md-5">
                    <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/shankhpushpi.png" alt="">
                </div>

                <div class="col-md-7">
                    <h5 class="blue">SHANKHPUSHPI</h5>
                    <p>It boosts the hormonal equilibrium to support hair health and appearance. It also thickens and restores luster to the hair. Effectively works for Migrane & Headache.</p>
                </div>
                
              </div>               
                
            </div>
            
        </div>

      
      

     </section>

     <!-- Strengths Section Ends Here -->


    <!-- Strengths Section Starts Here --> 

    <section class="carouselSec">
          <div class="container carasouelContainer  desktop_view">
              <h3 class="carasouelHead centerText"> <span class="skyBlue"> CUSTOMER </span> <b class="darkGreen"> REVIEWS </b></h3>
              
              

              <h4 class="centerText"> JOIN THE REVOLUTION </h4>

              <div class="singleItem">
                <div class="sliderCards">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Arjun_commentor.jpeg" alt="">
                  <p>Arjun</p>
                  <div class="stars Yellow">
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star-half-o"></i></span>  
                  </div>

                  <br>

                  <p class="review">
                    When U are 30 and have a bald patch, people criticize you. Thank you Nutratimes, for this great product for hair regrowth.
                   

                  </p>
                  
                </div>
                <div class="sliderCards">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Arya_commentor.jpeg" alt="">
                  <p>Arya</p>
                  <div class="stars Yellow">
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star-o"></i></span>  
                  </div>

                  <br>

                  <p class="review">
                    Tried a lot of products for hair loss but none have been this effective. I thank Nutratimes for this!
                  </p>
                  
                </div>
                <div class="sliderCards">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Priya_commentor.jpeg" alt="">
                  <p>Priya</p>
                  <div class="stars Yellow">
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star-half-o"></i></span>
                      <span ><i class="fa fa-star-o"></i></span>  
                  </div>

                  <br>

                  <p class="review">
                    After shifting houses I went through hair fall due to the water change. It has been 2 months now since I am using this hair oil and the results are actually good. No more hair loss and bald scalp!
A natural herbal product with no side effects. Must try

                  </p>
                  
                </div>
                <div class="sliderCards">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/vijay_commentor.jpeg" alt="">
                  <p>Vijay</p>
                  <div class="stars Yellow">
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star-half-o"></i></span>  
                  </div>

                  <br>

                  <p class="review">
                    Before it was pay for a product that resulted in more hair fall but now with Nutratimes hair regrowth oil & shampoo I have got thick, long and strong hair.
                  </p>
                  
                </div>
                <div class="sliderCards">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Farhad_commentor.jpeg" alt="">
                  <p>Farhad</p>
                  <div class="stars Yellow">
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star-half-o"></i></span>  
                  </div>

                  <br>

                  <p class="review">
                    This product is worth every penny. I seeing amazing results and it is 100% natural!
                  </p>
                  
                </div>
                <div class="sliderCards">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/dev_commentor.jpeg" alt="">
                  <p>Dev</p>
                  <div class="stars Yellow">
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star"></i></span>
                      <span ><i class="fa fa-star-half-o"></i></span>  
                  </div>

                  <br>

                  <p class="review">
                    All I can say is this product is not a scam. Nutratimes hair regrowth oil & shampoo is indeed very effective!
                  </p>
                  
                </div>
              </div>
          </div>

    </section>

    <div class="col-md-12 mobile_view user_reviews">

          <div class="col-md-12 centerText" style="
          width: 100%;">
              <h3> <span class="skyBlue"> CUSTOMER </span> <b class="darkGreen"> REVIEWS </b></h3>
              
              

              <h4> JOIN THE REVOLUTION </h4>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Arjun_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Arjun</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15"> When U are 30 and have a bald patch, people criticize you. Thank you Nutratimes, for this great product for hair regrowth..</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Arya_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Arya</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">Tried a lot of products for hair loss but none have been this effective. I thank Nutratimes for this!</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Priya_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Priya</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">After shifting houses I went through hair fall due to the water change. It has been 2 months now since I am using this hair oil and the results are actually good. No more hair loss and bald scalp!
                      A natural herbal product with no side effects. Must try</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/vijay_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Vijay</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">Before it was pay for a product that resulted in more hair fall but now with Nutratimes hair regrowth oil & shampoo I have got thick, long and strong hair.</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/Farhad_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Farhad</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">This product is worth every penny. I seeing amazing results and it is 100% natural!</p>
                  </div>
                </div>
          </div>
          <div class="col-md-12">
              <div class="mobile-user-info">
                  <div class="user_block centerText">
                    
                    <div class="user_name">
                      <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/dev_commentor.jpeg">
                    </div>										
                    <i class="clearB"></i>
                  </div>
                  <div class="mobile-user_info">
                    <h6>Dev</h6>
                    <p class="centerText">2019-08-06</p>
                    <p class="centerText mtop15">All I can say is this product is not a scam. Nutratimes hair regrowth oil & shampoo is indeed very effective!</p>
                  </div>
                </div>
          </div>

    </div>

    <!-- Strengths Section Ends Here -->

    <div class="clearfix"></div>

    <!--Footer Section Starts Here  -->

    <footer class="mtop70">

        <div class="container centerText ">
              <div class="col-md-12 col-sm-12 floatnone displayblock">
                <div class="logo floatnone">
                  <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/nutratimes.png" alt="">
                </div>
              </div>

              <div class="col-md-12 col-sm-12">
                  <div class="nav_links">
                      <a href="https://www.buynutratimes.com/privacy-policy" class="open-popup-link">Privacy Policy </a> |
                      <a href="https://www.buynutratimes.com/terms-conditions" class="open-popup-link">Terms & Condition  </a> |
                      <a  href="https://www.buynutratimes.com/return-policy" class="open-popup-link">Return Policy   </a> |
                      <a  href="https://www.buynutratimes.com/disclaimer-policy" class="open-popup-link">Disclaimer Policy </a> |
                      <a href="https://www.buynutratimes.com/refund-policy" class="open-popup-link">Refund Policy  </a> |
                      <a href="https://www.buynutratimes.com/shipping-policy" class="open-popup-link">Shipping Policy</a>
                     
                  </div>

              </div>

              <div class="col-md-12 col-sm-12 floatnone">
                <div class="centerTex ">
                    <a href="https://www.facebook.com/nutratimesofficial/" target="_blank" class="facebook" title="Facebook"> <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/facebook.png" alt="facebok"> </a>
                    <a href="https://twitter.com/nutratimes" class="twitter" target="_blank" title="Twitter"> <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/twitter.png" alt="twitter"> </a>
                    <a href="https://in.pinterest.com/nutratimes/" target="_blank" class="pinterest" title="Pinterest"> <img src="<?php echo S3_URL?>cdn/hairoil-lp/3/img/pintrest.png" alt="pinterest"> </a>
                    
                </div>
                  
              </div>

              <div class="col-md-12">
                <h5>For Any Question Email Us : support@nutratimes.com</h5>
                <h5>Or call Us:+91 08046328320 - 08046328361 </h5>
                <h5>Timing: 9:30 Am To 6:30 PM IST ( Sunday is Holiday) </h5>
              </div>

              
              
        </div>

        <div class="footer2 col-md-12 col-sm-12" style="width: 100%">
            
                <p class="centerText" >© 2019 <a href="https://www.nutratimes.com/" target="_blank"> nutratimes.com </a> | &nbsp  All rights reserved</p>
            
              
          </div>

    </footer>

    <!--Footer Section Ends Here  -->

 </div>  

  <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
  
  <script type="text/javascript" src="<?php echo S3_URL?>cdn/hairoil-lp/3/js/main.js"></script>

    <script src="<?php echo S3_URL?>cdn/hairoil-lp/common.js" type="text/javascript"></script>


</body>

</html>