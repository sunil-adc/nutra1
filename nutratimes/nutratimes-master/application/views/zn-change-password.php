<?php
	// ECHO ALL CATEOGRIES
	
?>  



<div class="container">
    <div class="row">
  <aside id="column-left" class="col-md-3 col-sm-4 mb-xs-30 hidden-xs sidebar-block">
    <div class="list-group">
    <a href="<?php echo SITE_URL?>myaccount" class="list-group-item">My Account</a>
    <a href="<?php echo SITE_URL?>profile" class="list-group-item">Edit Account</a> 
    <a href="<?php echo SITE_URL?>changePasswordUser" class="list-group-item">Password</a>
    <a href="<?php echo SITE_URL?>fulladdress" class="list-group-item">Address Book</a>
    <a href="<?php echo SITE_URL?>orderhistory" class="list-group-item">Order History</a> 
    <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/recurring" class="list-group-item">Recurring payments</a>-->
    <a href="<?php echo SITE_URL?>returnorders" class="list-group-item">Returns</a>
    <!--<a href="http://panelss.in/Dropbox/trunk/organicstore/index.php?route=account/newsletter" class="list-group-item">Newsletter</a>-->
    <a href="<?php echo SITE_URL?>logout" class="list-group-item">Logout</a>
  </div>
  </aside>

 <div id="content" class="col-sm-9">      
      <form action="javascript:void(0)" onsubmit="changePasswordUser('<?php echo SITE_URL?>changePasswordUser/save')" method="post" enctype="multipart/form-data" class="form-horizontal" id="form_chang_pass">
        <fieldset>
            <div class="heading-bg mb-30">
              <h2 class="heading m-0">Change Password</h2>
              <div id="change_err_div" style="text-align: center;margin: 10px; color: red"></div>
            </div> 
            <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-password">Password</label>
            <div class="col-sm-6">
              <input type="password" name="old_password" placeholder="Old Password" id="old_password" class="form-control" />
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-confirm">New Password</label>
            <div class="col-sm-6">
              <input type="password" name="new_password" placeholder="New Password" id="new_password" class="form-control" />
              </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-3 control-label" for="input-confirm">Confirm Password</label>
            <div class="col-sm-6">
              <input type="password" name="confirm_password" placeholder="Confirm Password" id="confirm_password" class="form-control" />
              </div>
          </div>
        </fieldset>
        <div class="buttons clearfix">
          
            <input type="submit" name="submit" id="changesubmit" value="Submit" class="btn btn-black" style="margin-left: 26%;margin-top: 30px;" />
        </div>
      </form>
      </div>
    </div>
</div>
