<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class orderhistory extends CI_Controller {
	protected $data = array();
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		// CHECK USER IS LOGIN OR NOT
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL."login");
		}
	}
	
	public function index($is_ajax = 0) {
		$cur_controller 				= strtolower(__CLASS__);
		$this->data['cur_controller']	= $cur_controller;
		
		// FETCH CATEGORY
		$this->data['category'] = $this->general->get_nlevel_category($cur_controller);
		
		// FETCH ORDER HISTORY
		$this->replica_db->cache_off();
		$order_history = $this->replica_db->query("SELECT 
												o.name,
												o.payment_status,
												up.order_id, 
												sum(up.quantity * up.cash_price + up.shipping) order_total,
												count(up.id) total_items,
												o.dt_c 
										   FROM 
												".ORDER." o inner join ".USER_PRODUCTS." up on(o.id = up.order_id) 
										   WHERE 
												o.user = '".$_SESSION[SVAIZA_USER_ID]."' and 
												up.status != '0'  
											GROUP BY 
												up.order_id");
		
		if($order_history->num_rows() > 0) {
			foreach ($order_history->result() as $row) {
				$this->data['order_history'][] = $row;
			}
		} else {
			$this->data['order_history'] = false;
		}
		
		// LOAD ALL VIEWS
		$this->load_views();
	}
	 
	public function getOrderDetails() {
		$this->replica_db->cache_off();
		$order_history = $this->replica_db->query("SELECT 
														o.payment_status,
														up.id,
														up.order_id, 
														up.quantity,
														up.cash_price,
														up.shipping,
														p.name,
														o.dt_c 
												   FROM 
														".ORDER." o 
														inner join ".USER_PRODUCTS." up on (o.id = up.order_id)
														left join ".PRODUCT." p on (up.product = p.prod_id)  
												   WHERE 
														o.user = '".$_SESSION[SVAIZA_USER_ID]."' and 
														o.id = '".$_POST['data_order_id']."' and 
														o.payment_mode = 1 and 
														o.caller_status != 4 and 
														up.status != '0' and 
														o.delivery_status != '4'");
		
		if($order_history->num_rows() > 0) {
			echo '<div class="order_details_div-header">
					<div class="left product-name">Product Name</div>
					<div class="left product-price">Price</div>
					<div class="left product-qty">Quantity</div>
					<div class="left product-subtotal">Subtotal</div>
					<div class="clr"></div>
				  </div>';
				  
			$arr_shipping = array();
			$total_price = 0;
			foreach ($order_history->result() as $row) {
				echo '<div class="order_details_div">
						<div class="left product-name">'.$row->name.'</div>
						<div class="left product-price">'.$row->cash_price.'</div>
						<div class="left product-qty">'.$row->quantity.'</div>
						<div class="left product-subtotal">'.($row->cash_price * $row->quantity).'</div>
						<div class="clr"></div>
					  </div>';
				 $arr_shipping[] = $row->shipping;
				 $total_price += ($row->cash_price * $row->quantity);
			}
			$shipping = max($arr_shipping);
			echo '<div class="order_details_div">
						<div class="left product-name">&nbsp;</div>
						<div class="left product-price">&nbsp;</div>
						<div class="left product-qty">Shipping</div>
						<div class="left product-subtotal">'.$shipping.'</div>
						<div class="clr"></div>
					  </div>';
			echo '<div class="order_details_div">
						<div class="left product-name">&nbsp;</div>
						<div class="left product-price">&nbsp;</div>
						<div class="left product-qty">Total Amount</div>
						<div class="left product-subtotal">'.($shipping + $total_price).'</div>
						<div class="clr"></div>
					  </div>';
		} else {
			$this->data['order_history'] = false;
		}			
		exit;
	}
	
	function load_views() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-order-history',$this->data);
		//$this->load->view(FRONT_INC.'zn-history');
		$this->load->view(FRONT_INC.'zn-footer');
	}


	public function detail($order_id ) {
		
		$data['order_product_arr'] = "";
		$data['user_details_arr']  = "";
		$data['order_detail']	   = "";
		$order_product = $this->db->query('SELECT 
												o.id, o.payment_mode,  
												o.offer_percentage, up.product,
												p.name, 		up.cash_price, 
												up.cod_price, 	up.shipping, 
												up.contest, 	up.quantity,
												up.size, 		g.genre_name,
												o.dt_c
											FROM  
												'.ORDER.' o INNER JOIN  
												'.USER_PRODUCTS.' up ON (o.id = up.order_id) 
												INNER JOIN '.PRODUCT.' p ON (up.product = p.prod_id)
												LEFT JOIN '.GENRE.' g on (up.size = g.genre_id)
											WHERE 
												up.contest = 0 and 
												up.status = 1 and 
												up.order_id='.$order_id);
		
		if($order_product->num_rows() > 0) {
			foreach ($order_product->result() as $row ) {
				$data['order_detail'] =  $row;
				$data['order_product_arr'][] = $row;
			}
		}
		
		
		$user_details = $this->db->query('select name, email, mobile, address, city, state, pincode from '.USERS.' where user_id = '.$_SESSION[SVAIZA_USER_ID]);
	
		if($user_details->num_rows() > 0) {
			foreach ($user_details->result() as $row1 ) {
				$data['user_details_arr'] = $row1;
			}
		}
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common');
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('zn-order-history-detail',$data);
		//$this->load->view(FRONT_INC.'zn-history');
		$this->load->view(FRONT_INC.'zn-footer');
	
	}

}
