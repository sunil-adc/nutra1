<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class info extends CI_Controller {
	protected $data = array();
	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
	
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		$cur_controller 				= strtolower(__CLASS__);
		$this->data['cur_controller']	= $cur_controller;
		
		// FETCH CATEGORY
		$this->data['category'] = $this->general->get_nlevel_category($cur_controller);	
	}
	
	public function aboutus() {
		$this->render_content(1);
	}
	
	public function contactus() {
		$this->render_content(2);
	}
	
	public function returnpolicy() {
		$this->render_content(3);
	}
	
	public function faq() {
		$this->render_content(4);
	}
	
	public function sitemap() {
		$this->render_content(5);
	}
	
	public function privacypolicy() {
		$this->render_content(6);
	}
	
	public function feedback() {
		$this->render_content(7);
	}
	
	
	public function terms_conditions() {
		$this->render_content(8);
	}
	
	public function disclaimer_policy() {
		$this->render_content(9);
	}
	
	public function cancellationpolicy() {
		$this->render_content(10);
	}
	
	public function refundpolicy() {
		$this->render_content(11);
	}
	
	public function shippingpolicy() {
		$this->render_content(12);
	}
	
	private function render_content($page_id = 0) {
		if ($page_id > 0) {
			// LOAD STATIC PAGE CONTENT
			$query = $this->db->query("SELECT
											page_id, page_title, page_name, page_description, meta_title, meta_keywords, meta_description
										FROM
											".STATIC_PAGE."
										WHERE
											status = 1 and 
											page_id = ".$page_id);
			
			// GET THE PRODUCT DETAILS
			if ($query->num_rows() > 0) {
				foreach ($query->result() as $p) {
					$this->data['page_details'] = $p;
					$this->header['meta_title']   = $p->meta_title;
					$this->header['meta_desc']    = $p->meta_description;
					$this->header['meta_keyword'] = $p->meta_keywords;
				
				}
			} else {
				redirect(SITE_URL);
			}
		} else {
			redirect(SITE_URL);
		}
		
		// LOAD ALL REQUIRE VIEWS
		$this->load_views();
	}
	
	function load_views() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'zn-common', $this->header);
		$this->load->view(FRONT_INC.'zn-header');
		$this->load->view('staticpage',$this->data);
		$this->load->view(FRONT_INC.'zn-footer');
	}


    function saveContactUs(){
						
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|numeric');
	    $this->form_validation->set_rules('message', 'Message', 'required');

		
	

		if(isset($_POST['submit'])) {

			if ($this->form_validation->run()) {
			
				// TURN OFF THE CACHE FOR SINGLE QUERY
				$this->db->cache_off();
				// TURN OFF THE CACHE FOR SINGLE QUERY
					
						$data = array(
							   'name' => $_POST['name'],
							   'email' => $_POST['email'],
							   'mobile' => $_POST['mobile'],
							   'message' => $_POST['message']
							   
						);
						
						$this->db->insert(CONTACTUS,$data);
						$this->db->insert_id();
						$this->session->set_flashdata('success', 'Your Message Successfully received');
						
						$user_email = $_POST['email'];
						$user_name = $_POST['name'];
						
						echo "done";
						
						//$this->svaiza_email->email_contactus($user_email,$user_name);
											
			} else {
				echo "validation_err";
			}

		}
		//redirect(SITE_URL.'info/contactus');
	
				
	}
	
	
	 function saveAffilate(){
						
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('mobile', 'Mobile', 'required|numeric');
	    $this->form_validation->set_rules('message', 'Message', 'required');
		$this->form_validation->set_rules('cmpy_name', 'Company Name', 'required');
	

		if(isset($_POST['submit'])) {

			if ($this->form_validation->run()) {
			
				// TURN OFF THE CACHE FOR SINGLE QUERY
				$this->db->cache_off();
				// TURN OFF THE CACHE FOR SINGLE QUERY
					
						$data = array(
							   'name' => $_POST['name'],
							   'email' => $_POST['email'],
							   'mobile' => $_POST['mobile'],
							   'message' => $_POST['message'],
							   'company' => $_POST['cmpy_name']
							   
						);
						
						$this->db->insert(AFFILATE,$data);
						$this->db->insert_id();
						$this->session->set_flashdata('success', 'Your Message Successfully received');
											
			} else {
				$this->session->set_flashdata('error', '(*) denotes mandatory fields');
			}

		}
		redirect(SITE_URL.'info/affilate');
	
		
		
	}
}

