<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reportCodDelivery extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library(array('pagination', 'form_validation'));
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END				
	}
	public function index($perpage = 10, $offset = 0) {
		// DEFAULT PERPAGE RECORD
		$perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0) {
		
		// GET ALL ARRAY FROM HELPER
		$arr_all = all_arrays();
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['search_field'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "COD Delivery Report";
		$data['add_page_title']	= "COD Delivery Report";
		$data['page_name']	 	= "COD Delivery Report";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "reportcoddelivery";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		$data['seach_form']		= $full_path;
		
		unset($data['results']);
		
		if($this->input->post('btn_search') != "") {
			
			$this->form_validation->set_rules('from_date', 'From Date', 'required');
			$this->form_validation->set_rules('to_date', 'To Date', 'required');
		
			if ($this->form_validation->run() != false) {
				
				$data['from_date'] = $this->input->post('from_date');
				$data['to_date'] = $this->input->post('to_date');
				
				// CREATE SENT COUNT
				
				$total_sent = "select 
									count(o.id) as total_sent
								from
									".ORDER." o
								where 
									o.caller_status = 3 and 
									o.payment_mode = 2 and 
									o.provider = '".$this->input->post('provider')."' and 
									o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'"; 
				
				$data['total_sent'] = $this->db_function->count_record_using_count($total_sent, 'total_sent',  false, true, $this->replica_db, true);
				
				// CREATE TOTAL DELIVERED
				$total_delivered = "select 
									count(o.id) as total_delivered
								from
									".ORDER." o
								where 
									o.caller_status = 3 and
									o.delivery_status = 2 and
									o.payment_mode = 2 and 
									o.provider = '".$this->input->post('provider')."' and 
									o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'"; 
				
				$data['total_delivered'] = $this->db_function->count_record_using_count($total_delivered, 'total_delivered',  false, true, $this->replica_db, true);
				
				// CREATE TOTAL RETURN RECEIVED
				$total_recieved = "select 
									count(o.id) as total_recieved
								from
									".ORDER." o
								where 
									o.caller_status = 3 and
									o.delivery_status = 4 and
									o.payment_mode = 2 and 
									o.provider = '".$this->input->post('provider')."' and 
									o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'"; 
				
				$data['total_recieved'] = $this->db_function->count_record_using_count($total_recieved, 'total_recieved',  false, true, $this->replica_db, true);
				
				// CREATE TOTAL Pending
				$total_pending = "select 
									count(o.id) as total_pending
								from
									".ORDER." o
								where 
									o.caller_status = 3 and
									(o.delivery_status = 1 or o.delivery_status = 0)and
									o.payment_mode = 2 and 
									o.provider = '".$this->input->post('provider')."' and 
									o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'"; 
				
				$data['total_pending'] = $this->db_function->count_record_using_count($total_pending, 'total_pending',  false, true, $this->replica_db, true);
				
				// CREATE TOTAL RECEIVED
				$cntprd = " select 
								count(o.id) as cntprd
							from
								".ORDER." o 
								LEFT JOIN ".USER_PRODUCTS." up on (up.order_id = o.id) 
							where 
								o.caller_status = 3 and
								o.delivery_status = 2 and
								o.payment_mode = 2 and 
								up.status = 1 and 
								o.provider = '".$this->input->post('provider')."' and 
								o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'"; 
				
				$data['cntprd'] = $this->db_function->count_record_using_count($cntprd, 'cntprd',  false, true, $this->replica_db, true);
				
				// CREATE TOTAL RECEIVED
				$sumval = "	select 
								SUM(up.cod_price + up.shipping) AS sum_val
							from
								".ORDER." o 
								LEFT JOIN ".USER_PRODUCTS." up on (up.order_id = o.id) 
							where 
								o.caller_status = 3 and
								o.delivery_status = 2 and
								o.payment_mode = 2 and 
								up.status = 1 and 
								o.provider = '".$this->input->post('provider')."' and 
								o.sent_date between '".$data['from_date']."' and '".$data['to_date']."'"; 
				
				$data['delivered_amount'] = $this->db_function->count_record_using_count($sumval, 'sum_val',  false, true, $this->replica_db, true);
				
				
				$data['total_cost'] 			= 	($data['cntprd'] * PRODUCT_COST) + 
													($data['total_delivered'] * SHIPMENT_COST_DELIVERED) + 
													($data['total_recieved'] * RETURN_COST) + 
													($this->input->post('manpower') * MANPOWER_COST);
								
				$data['total_revenue'] 			= ($data['delivered_amount'] > 0) ? $data['delivered_amount'] : 0;
				$data['total_margin'] 			= $data['total_revenue'] - $data['total_cost'];
				$data['delivery_percentage'] 	= ($data['total_sent'] > 0) ? ($data['total_delivered'] * 100) / $data['total_sent'] : 0;
				$data['results']				= 1;
				$data['manpower']				= $this->input->post('manpower');
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect(FULL_CMS_URL.'/'.$data['cur_controller'].'/');
				exit;
			}
		}
		 
		$provider_where['payment_mode'] = '2';
		$data['provider'] = custom_dropdown('provider', '', $arr_all['ARR_PROVIDER'], $this->input->post('provider'),'class="text-input small-input"');
		
		// LOAD ALL REQUIRE VIEWS
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');	 
		$this->load->view(CMS_FOLDER_NAME.'/report-cod-delivery',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}