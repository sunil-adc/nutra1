<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addAdminUser extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		/*is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);*/
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ADMIN;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Admin User";
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['manage_page'] 		= "manageadminuser";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data']['id']			= NULL;
		$data['result_data']['adm_role_id']	= NULL;
		$data['result_data']['username']	= NULL;
		$data['result_data']['email']		= NULL;
		$data['result_data']['last_psw_cng']= NULL;
		$data['result_data']['fname']		= NULL;
		$data['result_data']['lname']		= NULL;
		$data['result_data']['fb_url']		= NULL;
		$data['result_data']['twt_url']		= NULL;
		$data['result_data']['status']		= NULL;
		$data['result_data']['datecreated']	= NULL;
		$data['result_data']['dateupdated']	= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	id, adm_role_id, username, fname, lname, email, fb_url, twt_url, 
																	status, last_psw_cng, datecreated, dateupdated', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		if ($this->session->userdata('admin_role_id') != '1') {
			if (isset($data['result_data']['adm_role_id']) && $data['result_data']['adm_role_id'] != '2' && 
				$data['result_data']['adm_role_id'] != '3' && $data['result_data']['adm_role_id'] != '9') {
				$this->session->set_flashdata('error', 'You dont have permission to look at that user profile.');
				redirect(CMS_URL.ADMIN_DEFAULT_CONTROLLER);
			}
		}
		
		$role_where['status'] = '1';
		$data['admin_role'] = $this->common_model->create_combo("adm_role_id", "adm_role_id", ADMIN_ROLE, $role_where, "role_name", "adm_role_id", $data['result_data']['adm_role_id'], "javascript:void(0);",'1');
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-admin-user',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		
		if ($this->session->userdata('admin_role_id') != '1') {
			//if ($_POST['adm_role_id'] != '2' && $_POST['adm_role_id'] != '3' && $data['result_data']['adm_role_id'] != '9') {
				//$this->session->set_flashdata('error', 'You dont have permission to assign role other than COD-Sales User / QA User');
				//redirect($_POST['cur_url']);
			//}
		}
		$primary_field = 'id';
		$tablename = ADMIN;
		
		// SET THE VALIDATION RULES
		if( strtolower($_POST['mode']) == 'add') { 		
			$this->form_validation->set_rules('password', 'Password', 'required');
		}
		
		$this->form_validation->set_rules('adm_role_id', 'Admin Role', 'required|greater_than[0]');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('fname', 'First Name', 'required');
		$this->form_validation->set_rules('lname', 'Last Name', 'required');
		
		if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST[$primary_field])) {
			$this->form_validation->set_rules('username', 'Username', 'required');
			$this->form_validation->set_rules('email', 'Email', 'required');
		} else {
			$this->form_validation->set_rules('username', 'Username', 'required|unique['.$tablename.'.username]');
			$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		}
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST[$primary_field])) { 		
					
					if($this->db_function->is_db_duplicate_field($tablename,$_POST[$primary_field],$primary_field,'username',$_POST['username'])==false){
						$this->session->set_flashdata('error', 'Username you try to assign is already assign to someone else *.');
						redirect($_POST['cur_url']);
						exit;
					}
					
					/*if($this->db_function->is_db_duplicate_field($tablename,$_POST[$primary_field],$primary_field,'email',$_POST['email'])==false){
						$this->session->set_flashdata('error', 'Email you try to assign is already assign to someone else *.');
						redirect($_POST['cur_url']);
						exit;
					}*/
					
					
					// SET THE WHERE CLAUSE
					$this->db->where($primary_field,$_POST[$primary_field]);
					
					// SET THE DATA
					if (strlen($_POST['password']) > 0 ) {
						$data['password'] = md5($_POST['password']);
						$data['last_psw_cng'] = date('Y-m-d H:i:s');
					}
					
					$data['adm_role_id'] = $_POST['adm_role_id'];
					$data['username'] = $_POST['username'];
					$data['fname'] = $_POST['fname'];
					$data['lname'] = $_POST['lname'];
					$data['email'] = $_POST['email'];
					$data['fb_url'] = $_POST['fb_url'];
					$data['twt_url'] = $_POST['twt_url'];
					$data['status'] = $_POST['status'];
					$data['dateupdated'] = date('Y-m-d H:i:s');					
					
					// UPDATE QUERY
					$this->db->update($tablename,$data);
					
					// IF USER IS UPDATING OVER INFORMATION THEN CHANGE SESSION DATA
					if($this->session->userdata('uid') == $_POST[$primary_field]) {
 						// UPDATE SESSION DATA ALSO SO IT WILL REFLECT EVERYWHERE
						$this->session->set_userdata('username', $data['fname'] . " " . $data['Lname']);
						$this->session->set_userdata('admin_role_id', $data['adm_role_id']);
						
						// OFF THE CACHE FOR THIS QUERY
						$this->db->cache_off();
						$role_query = $this->db->query("select role_details, role_name from ".ADMIN_ROLE." where adm_role_id = ".$data['adm_role_id']);
						if ($role_query->num_rows() > 0) {
							foreach ($role_query->result() as $role_data) {
								$this->session->set_userdata('admin_role_details', $role_data->role_details);
								$this->session->set_userdata('admin_role', $role_data->role_name);
							}
						}
					}
					$this->session->set_flashdata('success', 'Admin User edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					
					if($this->db_function->is_db_duplicate_field($tablename, $_POST[$primary_field], $primary_field, 'username',$_POST['username'])==false){
						$this->session->set_flashdata('error', 'Username you try to assign is already assign to someone else *.');
						redirect($_POST['cur_url']);
						exit;
					}
					
					// SET THE DATA FOR INSERTION
					$data = array(
									'adm_role_id' => $_POST['adm_role_id'],
									'username' => $_POST['username'],
									'password' => md5($_POST['password']),
									'fname' => $_POST['fname'],
									'lname' => $_POST['lname'],
									'email' => $_POST['email'],
									'fb_url' => $_POST['fb_url'],
									'twt_url' => $_POST['twt_url'],
									'status' => $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert($tablename,$data);
					$this->session->set_flashdata('success', 'Admin User added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, 'managesalesqa');
				$this->db->cache_delete(CMS_FOLDER_NAME, 'addsalesqa');
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
