<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class shiprocket_api extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->db->cache_off();
		
   }

     
    public function index(){
		
	}


	public function generate_token() {
		
		$auth_token = "";

		$curl = curl_init();

		$auth_json = array('email' =>  'atul@adcanopus.com',  'password' => 'atul@adcan');

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/auth/login",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 30,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => json_encode($auth_json),
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json",
		    "cache-control: no-cache"
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		  echo "cURL Error #:" . $err;
		  exit;
		} else {
		  
		 $auth_response = json_decode($response);
		 //print_r($auth_response);

			if ($auth_response->token != "") {

				$get_token = $auth_response->token;
				
				return $get_token;
			}else{
				return $get_token;
				
			}
				
		}
	}

	public function get_order_list($oid= "") {
		
		$auth_token = $this->generate_token();
		
		if($auth_token == ""){
			echo "Token Not generated";
			die();
		}

		$all_arrays = all_arrays(); 	
		$cond = "";
		
		$today_date = date('Y-m-d');
		$yesterday_date = date('Y-m-d',strtotime("-4 days"));
		 
		if($oid != ""){
			$cond = " o.id = ".$oid;
		}else{
			$cond = " Date_Format(o.qualified_date, '%Y-%m-%d') between '".$yesterday_date."' and '".$today_date."'";
		}
		

		$o_query = $this->db->query("Select 
								    o.id as oid,	o.name as cname, 		o.email,		o.mobile, 		
								    o.address,      o.offer_percentage,	o.status,	o.dt_u, 
									o.city,			o.area,			o.state,		o.alternate_phone,
									o.pincode,		o.payment_mode,	o.caller_status, o.delivery_status,
									o.provider,		o.tracking_number, o.sent_date,	 o.dt_c,
									up.product,		up.quantity,	
									up.size,		up.free_prod,	up.cod_price,	up.cash_price,	
									up.shipping,	up.contest,		up.datecreated,	up.id cp_id,
									p.name,			p.model_no,		p.seourl,       p.mrp,		
									p.cod_availability, p.is_combo,		
									p.combo_product
							FROM
									".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id) 
									left join ".PRODUCT." p on (up.product = p.prod_id)
							 WHERE
									up.status = 1 and
									o.caller_status = 2 and 
									o.provider = 6 and
									up.product = 6 and 
									".$cond
									) ;

																				 
		if($o_query->num_rows() > 0){

				foreach($o_query->result() as $k ){

					if($k->tracking_number == "" ){
						
						//Create Shiprocket Custom Order
						$curl = curl_init();

												
						  $ship_order_array = array('order_id' => $k->oid,
						  'order_date'=> $k->dt_c,
						  'pickup_location'=> 'Nuttimes',
						  'channel_id'=> '',
						  'comment'=> 'Nutra oil',
						  'billing_customer_name'=> $k->cname,
						  'billing_last_name'=> $k->cname,
						  'billing_address'=> $k->address,
						  'billing_address_2'=> $k->area,
						  'billing_city'=> $k->city,
						  'billing_pincode'=> $k->pincode,
						  'billing_state'=> $all_arrays['ARR_STATE'][$k->state],
						  'billing_country'=> 'india',
						  'billing_email'=> $k->email,
						  'billing_phone'=> $k->mobile,
						  'shipping_is_billing'=> true,
						  'shipping_customer_name'=> '',
						  'shipping_last_name'=> '',
						  'shipping_address'=> '',
						  'shipping_address_2'=> '',
						  'shipping_city'=> '',
						  'shipping_pincode'=> '',
						  'shipping_country'=> '',
						  'shipping_state'=> '',
						  'shipping_email'=> '',
						  'shipping_phone'=> '',
						  'order_items'=> array(

						  					  array('name'=> 'Nutra oil and Shampoo',
										      'sku' => 'Nutra oil and Shampoo',
										      'units'=> '1',
										      'selling_price'=> ($k->payment_mode == 2 ? $k->cod_price : $k->cash_price),
										      'discount'=> '',
										      'tax'=>'',
										      'hsn'=> ''
										  	  )
										    ),
						  'payment_method'=> ($k->payment_mode == 2 ? 'COD' : 'Prepaid'),
						  'shipping_charges'=> 0,
						  'giftwrap_charges'=> 0 ,
						  'transaction_charges'=> 0,
						  'total_discount'=> 0,
						  'sub_total' => ($k->payment_mode == 2 ? $k->cod_price : $k->cash_price),
						  'length' => '9',
						  'breadth'=> '10',
						  'height' => '18.5',
						  'weight' => '0.5'
						);
						
						curl_setopt_array($curl, array(
						  CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/orders/create/adhoc",
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "POST",
						  CURLOPT_POSTFIELDS => json_encode($ship_order_array),
						  CURLOPT_HTTPHEADER => array(
						    "Content-Type: application/json",
						    "Authorization: Bearer ".$auth_token
						  ),
						));

						$response = curl_exec($curl);
						$err = curl_error($curl);

						curl_close($curl);

						if ($err) {
							echo "cURL Error #:" . $err;
						    exit;
						} else {
						  
						  $order_response = json_decode($response);
						  echo "<pre>";
						  print_r($order_response);
						  
						  if($order_response->status_code == 1 && $order_response->shipment_id !="" ){
						  	$shipment_id = $order_response->shipment_id;
						  	$courier_id  = $order_response->courier_company_id;
						  	$order_id    = $order_response->order_id;
						  }else{
						  	echo "Shiprocket Order Create Error";
						  	exit;
						  }

						}

						//Generate AWB for shiprocket
						
						$awb_array = array('shipment_id' => $shipment_id, 'courier_id' => 43, 'status' => 'New');

						$curl1 = curl_init();

						curl_setopt_array($curl1, array(
						  CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/courier/assign/awb",
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 30,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "POST",
						  CURLOPT_POSTFIELDS => json_encode($awb_array),
						  CURLOPT_HTTPHEADER => array(
						    "Content-Type: application/json",
						    "Authorization: Bearer ".$auth_token
						  ),
						));

						$response1 = curl_exec($curl1);
						$err = curl_error($curl1);

						curl_close($curl1);

						if ($err) {
						  echo "cURL Error #:" . $err;
						  exit;	
						} else {
						  $awb_response =  json_decode($response1);
						  //print_r($awb_response);
						  //exit;
						}
						echo "<pre>";
						print_r($awb_response);

						if($awb_response->awb_assign_status == 1){

							if($this->assign_tracking_number($k->oid, $awb_response->response->data->awb_code)){

								echo "Tracking Number ".$awb_response->response->data->awb_code." updated for order-". $k->oid."<br>";
							}else{
								echo "Tracking Number Not updated for order-". $k->oid."Please check with Nutratimes Admin";
							}	

						}else{
							echo "Issue With Tracking Number Please contact Nutratimes Admin";
						}

					}
				}
			}else{
				
				echo "No Details Found, Please check caller status and Provider";
			}
		
	}


	public function assign_tracking_number($order_id="", $tracking_number=""){

		if($order_id != "" && $tracking_number != "" ){
		
			if($this->db->query("Update tbl_orders set delivery_status = 1, tracking_number ='".$tracking_number."', caller_status=3, delivery_status=1, sent_date='".date( 'Y-m-d' )."' where id = ".$order_id)){

				return true;
			}else{

				return false;
			}
		}

	}



	public function get_all_delivery_status(){
		
		$trackid_arr = array();
		$status_delv = "";

		$twodays_backdate = date('Y-m-d',strtotime("-3 days"));
		 
		$sql_q = $this->db->query("Select 
										  id, tracking_number, payment_mode, qa_user, dt_c, dt_u 
									From
										  ".ORDER."
									Where
										   provider = 6 and 
										   caller_status in (3) and 
										   delivery_status in (1,5) and
										   Date_Format(sent_date ,'%Y-%m-%d') between '2019-08-01' AND '".$twodays_backdate."'" );  
		

		if($sql_q->num_rows() > 0){
			
			$auth_token = $this->generate_token();

			foreach($sql_q->result() as $k){
				if($k->tracking_number != ""){	
					
					$status_delv = $this->get_delivery_status($k->tracking_number, $auth_token);

					if( $k->tracking_number != "" && $k->id !=""  ){

						if($status_delv != ""){									
							
							//UPDATE DELIVERY STATUS 
							
							if($status_delv == 7){

								if($k->payment_mode ==2 ){
									$this->db->query("Update tbl_orders set delivery_status = 2, payment_status = 1 where id = ".$k->id." and provider =6 limit 1");	

									echo "Delivery Status Updated For COD - order id ".$k->id."<br>";
								}else if($k->payment_mode ==1 ){

									$this->db->query("Update tbl_orders set delivery_status = 2 where id = ".$k->id." and provider = 6 limit 1");	
									
									echo "Delivery Status Updated For Prepaid- order id ".$k->id."<br>";
								}

							}

						}else{
							echo "Status API Response error.<br>";
						}
						
					}					

				}
			}
		}
	
	}


	public function get_delivery_status($tracking_number, $auth_token){

		
		if($auth_token == ""){
			echo "Token Not generated";
			die();
		}

	
		$curl = curl_init();

		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/courier/track/awb/".$tracking_number,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => false,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "GET",
		  CURLOPT_HTTPHEADER => array(
		    "Content-Type: application/json",
		    "Authorization: Bearer ".$auth_token
		  ),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		    return $err;
		} else {
		  $response;
		} 
		
		if($response != ""){

			$json_response = json_decode($response);
			
			return $json_response->tracking_data->shipment_status;

		}


		
	}


}
