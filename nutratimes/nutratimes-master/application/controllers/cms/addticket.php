<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addTicket extends CI_Controller {
	
	function __construct() {
		parent::__construct();	
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CACHE OFF
		$this->db->cache_off();
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= SUPPORT_TICKET;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Ticket";
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page'] 		= "supportticket";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "ticket_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	= NULL;
		$data['result_data']['ticket_id'] 				= NULL;
		$data['result_data']['order_id']				= NULL;
		$data['result_data']['order_id_pg']				= NULL;
		$data['result_data']['email']					= NULL;
		$data['result_data']['name']					= NULL;
		$data['result_data']['content']					= NULL;
		$data['result_data']['mobile']					= NULL;
		$data['result_data']['ticket_status']			= NULL;
		$data['result_data']['datecreated']				= NULL;
		$data['result_data']['dateupdated']				= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	ticket_id,
																	order_id,
																	order_id_pg,
																	name,
																	mobile,
																	content,
																	email,
																	ticket_status,
																	datecreated,
																	dateupdated', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;	
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-ticket',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function getorderid() {
		if (isset($_POST['order_id']) && $_POST['order_id'] != '' && is_numeric($_POST['order_id'])) {
			$res = $this->db->query("select id, order_id_pg, mobile, email, name from ".ORDER." where id = ".$_POST['order_id']);
			if ($res->num_rows() > 0) {
				foreach ($res->result() as $val) {
					echo $val->id."|".$val->order_id_pg."|".$val->mobile."|".$val->email."|".$val->name;
				}
			}
		}
		return 'blank';
		exit;
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('order_id', 'Order Id', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required');
		$this->form_validation->set_rules('ticket_status', 'Ticket Status', 'required');
		$this->form_validation->set_rules('content', 'Content', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				$_POST['mode'] = strtolower($_POST['mode']);
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['ticket_id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where("ticket_id", $_POST['ticket_id']);
					// SET THE DATA
					$data = array(
									'ticket_status' => $_POST['ticket_status'],
									'content' => $_POST['content'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(SUPPORT_TICKET, $data);
					$this->session->set_flashdata('success', 'Publisher edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'order_id' => $_POST['order_id'],
									'order_id_pg' => $_POST['order_id_pg'],
									'email' => $_POST['email'],
									'name' => $_POST['name'],
									'mobile' => $_POST['mobile'],
									'ticket_status' => $_POST['ticket_status'],
									'content' => $_POST['content'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(SUPPORT_TICKET, $data);
					$insert_id = $this->db->insert_id();
					$this->session->set_flashdata('success', 'Publisher added successfully');
					
					$arr_all_predefined_array = all_arrays();
					
					$this->svaiza_email->ticket_mail($_POST['email'], '<table width="100%" border="0">
					  <tr>
						<td>
							<table cellpadding="0" cellspacing="0" align="center" style="width:600px; border:0px none; font-family:\'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; font-size:12px; color:#333; border:1px solid #ffa200;">
							<tr style="background-color:#f6f6f6;">
								<td height="40" style="padding:10px 0px 10px 10px ;"><img src="'.SITE_URL.'/images/shp_logo.png" width="129" height="35" alt="'.$this->setting_site_name.'" title="'.$this->setting_site_name.'" /></td>
							</tr>
							<tr style="background-color:#006FDB; ">
								<td height="40" style="padding:0px 10px 0px 10px; font:bold 16px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; color:#FFF;">Subscription Complete</td>
							</tr>
							<tr>
								<td height="10" style="font:bold 14px \'Open Sans\', \'Trebuchet MS\', Helvetica, Arial, sans-serif; padding:10px 15px 10px 15px; ">Dear user,</td>
							</tr>
							<tr>
								<td height="10" style="padding:10px 15px 10px 15px;">
                                	Your support ticket is generated and we will update you soon on this <br>
                                    your Order id  : '.$_POST['order_id'].'<br>
                                    Ref id for the support ticket is : '.$arr_all_predefined_array['ARR_TICKET_STATUS'][$insert_id].'<br>
									Support ticket Details : '.$_POST['content'].'<br>
                                </td>
							</tr>
							<tr>
							  <td height="10" style="padding:0px 15px 0px 15px;">
									<br />
									Regards,<br />
									'.SITE_NAME.' Team
							  </td>
							</tr>
							<tr>
								<td height="10"></td>
							</tr>
							</table>
						</td>
					  </tr>
					</table>');
				}
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
