<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class homeManager extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library(array('form_validation', 'S3'));
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN END
		
	}
	
	public function index() {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= SETTINGS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		$cur_controller = strtolower(__CLASS__);
		
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		$data['menu'] = $menu;
		$data['page_title'] = "Home Manager";
		$data['manage_page_title'] = "Home Manager";
		$data['manage_page'] = $cur_controller;
		$data['add_file'] = $cur_controller;
		$data['form_submit'] = CMS_FOLDER_NAME."/".$cur_controller;	
		$data['menunum'] = 4;
		$data['leftmenu'] = 1;
		// EVERY PAGE NAME AND SOME DYNAMIC INFO
		
		// SET THE VARIABLE TO EMPTY
		$data['result_data']						= NULL;
		$data['result_data']['top_text_note']		= NULL;
		$data['result_data']['banner_top_wide']		= NULL;
		$data['result_data']['top_small_banner']	= NULL;
		$data['result_data']['down_first']			= NULL;
		$data['result_data']['down_second']			= NULL;
		$data['result_data']['down_third']			= NULL;
		$data['result_data']['down_four']			= NULL;
		$data['result_data']['search_top_banner']	= NULL;
		$data['result_data']['banner_top_wide_link']= NULL;
		$data['result_data']['top_small_banner_link']= NULL;
		$data['result_data']['down_first_link']		= NULL;
		$data['result_data']['down_second_link']	= NULL;
		$data['result_data']['down_third_link']		= NULL;
		$data['result_data']['down_four_link']		= NULL;
		$data['result_data']['header_tab_title_1']	= NULL;
		$data['result_data']['header_tab_link_1']	= NULL;
		$data['result_data']['header_tab_title_2']	= NULL;
		$data['result_data']['header_tab_link_2']	= NULL;
		$data['result_data']['header_tab_title_3']	= NULL;
		$data['result_data']['header_tab_link_3']	= NULL;
		$data['result_data']['header_tab_title_4']	= NULL;
		$data['result_data']['header_tab_link_4']	= NULL;
		$data['result_data']['header_tab_title_5']	= NULL;
		$data['result_data']['header_tab_link_5']	= NULL;
		$data['result_data']['header_tab_title_6']	= NULL;
		$data['result_data']['header_tab_link_6']	= NULL;
		$data['result_data']['header_tab_title_7']	= NULL;
		$data['result_data']['header_tab_link_7']	= NULL;
		$data['result_data']['search_top_banner_link']= NULL;
		

		
		// TURN OFF THE CACHE FOR SINGLE QUERY
		$this->db->cache_off();
		$settings = $this->db->query("select attr_name, attr_value from ".SETTINGS." where status = '1' and attr_name in ('banner_top_wide', 'top_small_banner', 'down_first', 'down_second', 'down_third', 'down_four', 'banner_top_wide_link', 'top_small_banner_link', 'down_first_link', 'down_second_link', 'down_third_link', 'down_four_link', 'search_top_banner_link', 'search_top_banner', 'top_text_note', 'homepage_banner_1', 'homepage_banner_2', 'homepage_banner_1_link', 'homepage_banner_2_link', 'header_tab_title_1', 'header_tab_link_1', 'header_tab_title_2', 'header_tab_link_2', 'header_tab_title_3', 'header_tab_link_3', 'header_tab_title_4', 'header_tab_link_4', 'header_tab_title_5', 'header_tab_link_5', 'header_tab_title_6', 'header_tab_link_6', 'header_tab_title_7', 'header_tab_link_7')");
		if($settings->num_rows() > 0) {
			foreach($settings->result() as $row) {
				$val[$row->attr_name] = $row->attr_value;		
				$data['result_data'][$row->attr_name] = $row->attr_value;
			}
		}
		
		$change_done = false;
		
		$s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
		$s3->putBucket(S3_BUCKET, S3::ACL_PUBLIC_READ);
		
		// CHECK IF FORM IS SUBMITTED START
		if($this->input->post('submit')) {
			$data = array();
			// FIRST BANNER TOP WIDE - START	
			if (isset($_FILES['banner_top_wide']['name']) && trim($_FILES['banner_top_wide']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['banner_top_wide']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					// DELETE EXISTING IMAGE
					
					// UPLOAD IT IN SERVER
					@move_uploaded_file($_FILES['banner_top_wide']['tmp_name'], "./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['banner_top_wide']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);
					
					
					// UPDATION
					$this->db->where("attr_name",'banner_top_wide');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// FIRST BANNER TOP WIDE - END
			
			// SECOND BANNER TOP SMALL - START
			if (isset($_FILES['top_small_banner']['name']) && trim($_FILES['top_small_banner']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['top_small_banner']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					
					// UPLOAD IT IN SERVER
					@move_uploaded_file($_FILES['top_small_banner']['tmp_name'],"./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['top_small_banner']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);
					
					// UPDATION
					$this->db->where("attr_name",'top_small_banner');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// SECOND BANNER TOP SMALL - END
			
			// FIRST BANNER BOTTOM SMALL - START
			if (isset($_FILES['down_first']['name']) && trim($_FILES['down_first']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['down_first']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					
					// UPLOAD IT IN SERVER
					@move_uploaded_file($_FILES['down_first']['tmp_name'],"./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['down_first']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);
					
					// UPDATION
					$this->db->where("attr_name",'down_first');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// FIRST BANNER BOTTOM SMALL - END
			
			// SECOND BANNER BOTTOM SMALL - START
			if (isset($_FILES['down_second']['name']) && trim($_FILES['down_second']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['down_second']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					
					// UPLOAD IT IN SERVER
					@move_uploaded_file($_FILES['down_second']['tmp_name'],"./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['down_second']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);
					
					// UPDATION
					$this->db->where("attr_name",'down_second');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// SECOND BANNER BOTTOM SMALL - END
			
			// THIRD BANNER BOTTOM SMALL - START
			if (isset($_FILES['down_third']['name']) && trim($_FILES['down_third']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['down_third']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					
					// UPLOAD IT IN SERVER
					@move_uploaded_file($_FILES['down_third']['tmp_name'],"./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['down_third']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);
					
					// UPDATION
					$this->db->where("attr_name",'down_third');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// THIRD BANNER BOTTOM SMALL - END
			
			// FOURTH BANNER BOTTOM SMALL - START
			if (isset($_FILES['down_four']['name']) && trim($_FILES['down_four']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['down_four']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					
					// UPLOAD IT IN SERVER
					move_uploaded_file($_FILES['down_four']['tmp_name'],"./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['down_four']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);
					
					// UPDATION
					$this->db->where("attr_name",'down_four');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// FOURTH BANNER BOTTOM SMALL - END
			
			// SEARCH PAGE BANNER BOTTOM SMALL - START
			if (isset($_FILES['search_top_banner']['name']) && trim($_FILES['search_top_banner']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['search_top_banner']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					
					// UPLOAD IT IN SERVER
					move_uploaded_file($_FILES['search_top_banner']['tmp_name'],"./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['search_top_banner']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);
					
					// UPDATION
					$this->db->where("attr_name",'search_top_banner');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// SEARCH PAGE BANNER BOTTOM SMALL - END
			
			// SIDEBAR BANNER 1 - START
			if (isset($_FILES['homepage_banner_1']['name']) && trim($_FILES['homepage_banner_1']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['homepage_banner_1']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					
					// UPLOAD IT IN SERVER
					move_uploaded_file($_FILES['homepage_banner_1']['tmp_name'],"./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['homepage_banner_1']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);
					
					// UPDATION
					$this->db->where("attr_name",'homepage_banner_1');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// SIDEBAR BANNER 1 - END
			
			// SIDEBAR BANNER 2 - START
			if (isset($_FILES['homepage_banner_2']['name']) && trim($_FILES['homepage_banner_2']['name']) != "") {
				// GET IMAGE EXTENSION
				$image_ext = allowed_extensions($_FILES['homepage_banner_2']['name']);
				if($image_ext != false) {
					// GET IMAGE NEW NAME
					$change_done = true;
					$new_img_name = get_rand_chracter('12','3').".".$image_ext;
					
					// UPLOAD IT IN SERVER
					move_uploaded_file($_FILES['homepage_banner_2']['tmp_name'],"./".CONTENT_DIR."/images/".$new_img_name);		
					
					// MOVE TO AMAZON S3 SERVER
					$s3->putObjectFile("./".CONTENT_DIR."/images/".$new_img_name, S3_BUCKET, CONTENT_DIR."/images/".$new_img_name, S3::ACL_PUBLIC_READ);
					
					// DELETE FROM OLD IMAGE FROM S3 BUCKET
					$s3->deleteObject(S3_BUCKET, CONTENT_DIR."/images/".$val['homepage_banner_2']);
					
					// DELETE EXISTING IMAGE
					unlink("./".CONTENT_DIR."/images/".$new_img_name);					
					
					// UPDATION
					$this->db->where("attr_name",'homepage_banner_2');
					$this->db->update(SETTINGS, array('attr_value' => $new_img_name));
				} else {
					// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
					$this->session->set_flashdata('error', 'Image extension is not allowed');
				}
			}
			// SIDEBAR BANNER 2 - END
			
			// CHANGE STATUS UPDATE
			$change_done = true;
			
			// ALL BANNER LINK CODING - START	
			$this->db->where("attr_name",'header_tab_title_1');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['header_tab_title_1']));
			
			$this->db->where("attr_name",'header_tab_link_1');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['header_tab_link_1']) != "") ? $_POST['header_tab_link_1'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'header_tab_title_2');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['header_tab_title_2']));
			
			$this->db->where("attr_name",'header_tab_link_2');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['header_tab_link_2']) != "") ? $_POST['header_tab_link_2'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'header_tab_title_3');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['header_tab_title_3']));
			
			$this->db->where("attr_name",'header_tab_link_3');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['header_tab_link_3']) != "") ? $_POST['header_tab_link_3'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'header_tab_title_4');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['header_tab_title_4']));
			
			$this->db->where("attr_name",'header_tab_link_4');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['header_tab_link_4']) != "") ? $_POST['header_tab_link_4'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'header_tab_title_5');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['header_tab_title_5']));
			
			$this->db->where("attr_name",'header_tab_link_5');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['header_tab_link_5']) != "") ? $_POST['header_tab_link_5'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'header_tab_title_5');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['header_tab_title_5']));
			
			$this->db->where("attr_name",'header_tab_link_6');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['header_tab_link_6']) != "") ? $_POST['header_tab_link_6'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'header_tab_title_6');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['header_tab_title_6']));
			
			$this->db->where("attr_name",'header_tab_link_6');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['header_tab_link_6']) != "") ? $_POST['header_tab_link_6'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'header_tab_title_7');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['header_tab_title_7']));
			
			$this->db->where("attr_name",'header_tab_link_7');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['header_tab_link_7']) != "") ? $_POST['header_tab_link_7'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'homepage_banner_1_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['homepage_banner_1_link']) != "") ? $_POST['homepage_banner_1_link'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'homepage_banner_2_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['homepage_banner_2_link']) != "") ? $_POST['homepage_banner_2_link'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'top_text_note');
			$this->db->update(SETTINGS, array('attr_value' => $_POST['top_text_note']));
			
			$this->db->where("attr_name",'banner_top_wide_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['banner_top_wide_link']) != "") ? $_POST['banner_top_wide_link'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'top_small_banner_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['top_small_banner_link']) != "") ? $_POST['top_small_banner_link'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'down_first_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['down_first_link']) != "") ? $_POST['down_first_link'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'down_second_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['down_second_link']) != "") ? $_POST['down_second_link'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'down_third_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['down_third_link']) != "") ? $_POST['down_third_link'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'down_four_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['down_four_link']) != "") ? $_POST['down_four_link'] : 'javascript:void(0)')));
			
			$this->db->where("attr_name",'search_top_banner_link');
			$this->db->update(SETTINGS, array('attr_value' => urlencode((trim($_POST['search_top_banner_link']) != "") ? $_POST['search_top_banner_link'] : 'javascript:void(0)')));
			// ALL BANNER LINK CODING - END
			
			
			// CHECK IF ARRAY IS NOT EMPTY THEN FIRE QUERY - START	
			if($change_done == true) {
				// DELETE THE RELATED CACHE 
				$this->db->cache_delete(CMS_FOLDER_NAME, $cur_controller);
				$this->db->cache_delete('default', 'index');
				// DELETE THE RELATED CACHE
				
				// SET AN FLASH MESSAGE
				$this->session->set_flashdata('success', 'Your homepage settings has been changed.');
			} else {
				$this->session->set_flashdata('error', 'Nothing changes happen...');
			}
			// CHECK IF ARRAY IS NOT EMPTY THEN FIRE QUERY - START
			redirect(FULL_CMS_URL.'/'.$cur_controller.'/');
		}
	
		
		
		// CHECK IF FORM IS SUBMITTED END
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/home-manager',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}
