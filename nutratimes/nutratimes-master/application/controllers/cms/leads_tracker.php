<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class leads_tracker extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library(array('pagination', 'form_validation'));
		
		// CHECK ADMIN IS LOGIN - START
		/*is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__); */
		// CHECK ADMIN IS LOGIN - END				
	}
	public function index($perpage = 10, $offset = 0) {
		// DEFAULT PERPAGE RECORD
		$perpage = 10;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0) {
		
		$data['label_from_to_date'] = '';
		
		// GET ALL ARRAY FROM HELPER
		$arr_all = all_arrays();
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['search_field'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "NetWise Leads Tracker";
		$data['add_page_title']	= "NetWise Leads Tracker";
		$data['page_name']	 	= "NetWise Leads Tracker";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "reportnetwise";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		$data['seach_form']		= $full_path;
		
		unset($data['results']);
		
		if($this->input->post('btn_search') != "") {
			
			$this->form_validation->set_rules('from_date', 'From Date', 'required');
			$this->form_validation->set_rules('to_date', 'To Date', 'required');
			$this->form_validation->set_rules('net', 'Network', 'required|numeric');
		
			if ($this->form_validation->run() != false) {
				
				// CACHE OFF
				$this->replica_db->cache_off();
				
				$sel_pp_query = $this->replica_db->query("select id from ".PARENT_PINCODE." where status = '1'");
				if($sel_pp_query->num_rows() > 0) {
					$data['sel_pp'] 	= $sel_pp_query;
				} else {
					$data['sel_pp'] 	= false;
				}
				
				$from_date = $this->input->post('from_date');
				$to_date = $this->input->post('to_date');
				$data['net'] = $this->input->post('net');
				
				$today_time = getdate(time());
				
				if ($this->input->post('from_date') == date('Y-m-d') && 
					$this->input->post('to_date') == date('Y-m-d') && 
					$this->input->post('timezone') != '0') {
				
					$data['from_date'] 	= date('Y-m-d 00:00:00', strtotime($this->input->post('from_date')));	
					$data['to_date'] 	= date('Y-m-d '.$today_time['hours'].':'.$today_time['minutes'].':'.$today_time['seconds'], 
													strtotime($this->input->post('to_date')));
													
					
					if ($this->input->post('timezone') != '') {
						$min_from_date  = date('Y-m-d 00:00:00', strtotime($this->input->post('from_date')));
						$min_to_date  	= date('Y-m-d 00:00:00', strtotime($this->input->post('to_date')));
						
						$str_from_date 	= strtotime($data['from_date']) + $this->input->post('timezone');
						$str_to_date 	= strtotime($data['to_date']) + $this->input->post('timezone');
						
						if ($str_from_date < strtotime($min_from_date) || strtotime($str_to_date) > strtotime($min_to_date)){
							
							// FIND TODAY SECONDS
							$today_seconds = strtotime(date('Y-m-d H:i:s')) - strtotime(date('Y-m-d 00:00:00'));
							
							if ($today_seconds > abs($this->input->post('timezone'))) {
								$add_seconds = $today_seconds - abs($this->input->post('timezone'));
							} else {
								$add_seconds = $today_seconds;
							}
							
							$str_from_date 	= strtotime($min_from_date);
							$str_to_date 	= strtotime($min_to_date) + $add_seconds;
							
						}
						
						$diff_timedate = $str_to_date - $str_from_date;
						$from_time = strtotime(date('Y-m-d H:i:s')) - $diff_timedate;
						
						$data['from_date'] 	= date('Y-m-d H:i:s', $from_time);
						$data['to_date'] 	= date('Y-m-d H:i:s');
					
						$data['label_from_to_date'] = '<div class="notification information png_bg"><div>Converted time <span class="bg_red">'.date('Y-m-d H:i A', $from_time).'</span> to <span class="bg_red">'.date('Y-m-d H:i A').'</span></div></div>';
						
					}
					$dateFormat = '%Y-%m-%d %H:%s:%i';
				} else {
					$dateFormat = '%Y-%m-%d';
					$data['from_date'] 	= $this->input->post('from_date');
					$data['to_date'] 	= $this->input->post('to_date');					
				}
				
				$sel_query = $this->replica_db->query("select pincode_type, handle_type, fire_type, goal, net, cpl, id AS net_id from ".PIXELS." where id = ".$data['net']);
				foreach ($sel_query->result() as $row) {
					
					$lead_to_conversion 	= '';
					$net 					= $row->net;
					$cpl 					= $row->cpl;
					$net_id 				= $row->net_id;
					$data['net_id'] 		= $row->net_id;
					$data['net_name'] 		= $row->net;
					$data['pincode_type'] 	= $row->pincode_type;
					$data['fire_type'] 		= $row->fire_type;
					$data['handle_type'] 	= $row->handle_type;
					$data['goal'] 			= $row->goal;
					$data['mysql']	= '';
					
					// COUNT THE LEADS
					$leads = "	select 
									COUNT(1) AS leads
								from
									".ORDER." o
								where 
									lower(o.net) = '".$net."' and 
									DATE_FORMAT(o.dt_c, '".$dateFormat."') between '".$data['from_date']."' and '".$data['to_date']."'";
					
					$data['leads'] = $this->db_function->count_record_using_count($leads, 'leads',  false, true, $this->replica_db, true);
					
					// COUNT THE LEADS FIRED
					$lead_fired = "	select 
									COUNT(1) AS lead_fired
								from
									".ORDER." o
								where 
									o.fire_status = 1 and 
									lower(o.net) = '".$net."' and 
									DATE_FORMAT(o.dt_c, '".$dateFormat."') between '".$data['from_date']."' and '".$data['to_date']."'";
										
					$data['lead_fired'] = $this->db_function->count_record_using_count($lead_fired, 'lead_fired',  false, true, $this->replica_db, true);
					
					// COUNT THE LEADS FIRED
					$sale = "	select 
									COUNT(1) AS sale
								from
									".ORDER." o
								where 
									o.payment_status = 1 and 
									lower(o.net) = '".$net."' and 
									DATE_FORMAT(o.dt_c, '".$dateFormat."') between '".$data['from_date']."' and '".$data['to_date']."'";
										
					$data['sale'] = $this->db_function->count_record_using_count($sale, 'sale',  false, true, $this->replica_db, true);
					
					// COUNT THE REVENUE FIRED
					$revenue = "select 
									SUM(up.cash_price * up.quantity + up.shipping) AS revenue
								from
									".ORDER." o 
									LEFT JOIN ".USER_PRODUCTS." up ON (o.id = up.order_id)
								where 
									up.contest = 1 and 
									o.payment_status = 1 and 
									up.status = '1' and 
									lower(o.net) = '".$net."' and 
									DATE_FORMAT(o.dt_c, '".$dateFormat."') between '".$data['from_date']."' and '".$data['to_date']."'";
					
					$data['revenue'] = $this->db_function->count_record_using_count($revenue, 'revenue',  false, true, $this->replica_db, true);
					
					$data['revenue']			= ($data['revenue'] > 0) ? $data['revenue'] : 0;
					$data['spent']				= $data['lead_fired'] * $cpl;
					$data['margin'] 			= $data['revenue'] - $data['spent'];
					
					if ($data['sale'] > 0) {
						$data['cps'] = $data['spent'] / $data['sale'];
					} else {
						$data['cps'] = 0;
					}
					
					if ($data['leads'] > 0) {
						$data['lead_to_conversion'] = number_format($data['sale'] * 100 / $data['leads'], 2);
					} else {
						$data['lead_to_conversion'] = 0;
					}
				}
				$data['network']				= $net;
				$data['results']				= 1;
			
				$data['to_date'] = $this->input->post('to_date');
				$data['timezone'] = $this->input->post('timezone');
				$data['from_date'] = $this->input->post('from_date');
				
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect(FULL_CMS_URL.'/'.$data['cur_controller'].'/');
				exit;
			}
		}
		
		$net_where['status'] = 1;
		$net_where['is_cpl_cps'] = 1;
		$data['net'] = $this->common_model->create_combo("net", "netGetPub", PIXELS, $net_where, "net", "id", $this->input->post('net'), "", "3", "", false, false, 'style="width:10% !important" data-cmsurl="'.FULL_CMS_URL.'"'); 
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');	 
		$this->load->view(CMS_FOLDER_NAME.'/leads-tracker',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function getPubs() {
		if (isset($_POST['net']) && $_POST['net'] != '') {
			$res = $this->db->query("select id, pubs from ".PUBS." where net_id in (".$_POST['net'].")");
			if ($res->num_rows() > 0) {
				echo '<select name="pubs" id="pubs" class="small-input" style="width:10% !important">';
				echo '<option value="">Select Option</option>';
				echo '<option value="all">All</option>';
				foreach ($res->result() as $val) {
					echo '<option value="'.$val->id.'">'.$val->pubs.'</option>';
				}
				echo '</select>';
			} else {
				echo '<select name="pubs" disabled="disabled" id="pubs" class="small-input" style="width:10% !important"><option value="">N/A</option></select>';
			}
		} else {
			echo '<select name="pubs" disabled="disabled" id="pubs" class="small-input" style="width:10% !important"><option value="">N/A</option></select>';
		}
		exit;
	}
}