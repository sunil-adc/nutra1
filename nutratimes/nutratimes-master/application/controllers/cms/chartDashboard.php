<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class chartDashboard extends CI_Controller {
	
	public $memcache;
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), "extra_call");
		// CHECK ADMIN IS LOGIN END
		
	}
	
	public function index($d){
		
		$today_sale = $today_users = $expected_revenue = $lead_to_conversion = $total_conversion = $total_prepaid_sale = $total_cod_sale = 0;
		$json_data = NULL;
		
		
		if($d == '1' ){
		
		   $from_date = date('Y-m-d');   
		   $to_date   = date('Y-m-d');
		       
		}else if($d == '2' ){
		
		   $fr_to = $_POST['sr_dt'];   
		   
		   if( $fr_to != ""){
			   $dt = explode(" - ", $fr_to);
			   if(is_array($dt) && count($dt) > 0 ){
				  $from_date = $dt[0];
				  $to_date   = $dt[1];
			   }
		   }
		}

		// COUNT TODAY SALE
		$total_prepaid_sale = $this->common_model->count_sale($this->replica_db, $from_date, $to_date);

		$total_cod = $this->common_model->count_cod_sale($this->replica_db, $from_date, $to_date);
		
		$total_conversion =  $total_prepaid_sale + $total_cod;

		// COUNT REVENUE DETAILS FOR TODAY
		$total_revenue_till_now = $this->common_model->count_revenue($this->replica_db, $from_date, $to_date);

		// COUNT TODAY LEADS / USERS
		$total_orders = $this->common_model->tot_orders($from_date, $to_date);

		// COUNT COD SEND
		$whr_cod_send = " o.payment_mode = 2 and ";
		$total_cod_send = $this->common_model->count_send_order($this->replica_db, $from_date, $to_date, $whr_cod_send);

		// COUNT PREPAID SEND
		$whr_prepaid_send = " o.payment_mode = 1 and ";
		$total_prepaid_send = $this->common_model->count_send_order($this->replica_db, $from_date, $to_date, $whr_prepaid_send);	

		/*$netwise_query = $this->db->query("Select id, net, payment_status, payment_mode From tbl_orders where date_format(dt_c, '%Y-%m-%d') between '".$from_date."' and '".$to_date."'");
		
		if($netwise_query->num_row() > 0 ){
			foreach ($netwise_leads->result() as $k) {
				
				$netwise_count[$k->net]['lead_count'] = $netwise_count[$k->net]['lead_count'] + 1 ;
				if($k->payment_status == 1){
					$netwise_count[$k->net]['lead_con']   = $netwise_count[$k->net]['lead_con'] + 1;
				}
			}
		}
		*/
		
		// GENERATE LEAD TO CONVERSION RATE
		if($total_leads > 0) {
			$lead_to_conversion = 100 * $total_leads / $total_leads;
		}
		
		 
		$stats_json = '{';
		$stats_json .= '"total_prepaid":'.$total_prepaid_sale.',';
		$stats_json .= '"total_cod":'.$total_cod.',';
		$stats_json .= '"total_conversion":'.$total_conversion.',';
		$stats_json .= '"total_revenue":'.$total_revenue_till_now.',';
		$stats_json .= '"total_cod_send":'.$total_cod_send.',';
		$stats_json .= '"total_prepaid_send":'.$total_prepaid_send.',';
		//$stats_json .= '"lead_to_conversion":'.$lead_to_conversion.',';
		//$stats_json .= '"fire_leads":'.$this->common_model->_get_fired_leads(1, $this->replica_db).',';		
		//$stats_json .= '"growth_rate":'.$this->common_model->_get_growth_rate($this->replica_db).',';		
		$stats_json .= '"cod_qualify":'.$this->common_model->qa_sales_action(1, 2, "caller_status", $this->replica_db).',';
		//$stats_json .= '"qa_qualify":'.$this->common_model->qa_sales_action(1, 2, "qa_status", $this->replica_db).',';
		//$stats_json .= '"total_orders":'.$this->common_model->total_orders().',';
		//$stats_json .= '"total_notdelivered_orders":'.$this->common_model->total_notdelivered_order().',';
		$stats_json .= '"total_leads":'.$total_orders;
		$stats_json .= '}';
		
		echo $stats_json;
		
		// EXIT FOR STOP EXECUTION 
		exit;
	}
}
