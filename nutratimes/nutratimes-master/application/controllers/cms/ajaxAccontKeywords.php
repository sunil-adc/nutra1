<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ajaxAccontKeywords extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	public function index() {
		
		if (isset($_POST['keyword_name']) && trim($_POST['keyword_name']) != "") {
			$this->db->cache_off();
			$res = $this->db->query("select * from ".ACCOUNT_KEYWORDS." where status = 1 and name like '%".trim($_POST['keyword_name'])."%'");
			if($res->num_rows() > 0) {
				foreach ($res->result() as $row) {
					if($row->status == 1) {
						echo "<div class='suggested_entity'><div class='color_blue suggested_entity_link suggested_entity_keyword' entity-data='".$row->name."'>".$row->name."</div></div>";		
					} else {
						echo "<div class='suggested_entity_error'><font class='color_red'>".$row->name." (It's disable, enable that product first.)</font></div>";
					}
				}
			} else {
				echo "<div class='suggested_entity_error'><font class='color_red'>Keyword not available.</font></div>";		
			}
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
		}		
		// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		exit;
		// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
	}
}