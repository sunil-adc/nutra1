<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bluedart_api extends CI_Controller
{
   public $global_country;
   public function __construct(){
        
		parent::__construct();
		$this->db->cache_off();
		
   }

     
    public function index(){
		
	}


	public function get_order_list($oid= "") {
		$all_arrays = all_arrays(); 	
		$cond = "";
		
		$today_date = date('Y-m-d');
		$yesterday_date = date('Y-m-d',strtotime("-2 days"));
		 
		if($oid != ""){
			$cond = " o.id = ".$oid;
		}else{
			$cond = " Date_Format(o.qualified_date, '%Y-%m-%d') between '".$yesterday_date."' and '".$today_date."'";
		}
		

		$o_query = $this->db->query("Select 
								    o.id as oid,	o.name as cname, 		o.email,		o.mobile, 		
								    o.address,      o.offer_percentage,	o.status,	o.dt_u, 
									o.city,			o.area,			o.state,		o.alternate_phone,
									o.pincode,		o.payment_mode,	o.caller_status, o.delivery_status,
									o.provider,		o.tracking_number, o.sent_date,	 o.dt_c,
									up.product,		up.quantity,	
									up.size,		up.free_prod,	up.cod_price,	up.cash_price,	
									up.shipping,	up.contest,		up.datecreated,	up.id cp_id,
									p.name,			p.model_no,		p.seourl,       p.mrp,		
									p.cod_availability, p.is_combo,		
									p.combo_product,  up.is_combo as user_combo,	
									up.combo_product as user_combo_prouct
							FROM
									".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id) 
									left join ".PRODUCT." p on (up.product = p.prod_id)
							 WHERE
									up.product = p.prod_id and 
									up.status = 1 and
									o.caller_status = 2 and 
									o.provider = 2 and
									".$cond
									) ;			 
		
		if($o_query->num_rows() > 0){

				foreach($o_query->result() as $k ){
					if($k->tracking_number == "" ){

						$api_url = "http://netconnect.bluedart.com/Ver1.8/ShippingAPI/WayBill/WayBillGeneration.svc";
						$soapParams = array(
											'trace' 							=> 1,  
											'style'								=> SOAP_DOCUMENT,
											'use'								=> SOAP_LITERAL,
											'soap_version' 						=> SOAP_1_2
											);

						$client = new SoapClient($api_url . "?wsdl", $soapParams);
						$client->__setLocation($api_url);
														
						$actionHeader = new SoapHeader('http://www.w3.org/2005/08/addressing','Action','http://tempuri.org/IWayBillGeneration/GenerateWayBill',true);
						$client->__setSoapHeaders($actionHeader);	

						$params = array(
						'Request' => 
							array (
								'Consignee' =>
									array (
										'ConsigneeAddress1' => $k->address,
										'ConsigneeAddress2' => $all_arrays['ARR_STATE'][$k->state],
										'ConsigneeAddress3'=> $k->area,
										'ConsigneeAttention'=> $k->cname,
										'ConsigneeMobile'=> $k->mobile,
										'ConsigneeName'=> $k->cname,
										'ConsigneePincode'=> $k->pincode,
										'ConsigneeTelephone'=> $k->mobile,
									)	,
								'Services' => 
									array (
										'ActualWeight' => '1',
										'CollectableAmount' => ($k->payment_mode == 2 ? $k->cod_price : 0),
										'Commodity' =>
											array (
												'CommodityDetail1'  => SITE_NAME,
												'CommodityDetail2'  => SITE_NAME,						
												'CommodityDetail3'  => 'India'
										),
										'CreditReferenceNo' => $k->oid,
										'DeclaredValue' => ($k->payment_mode == 2 ? $k->cod_price : $k->cash_price),
										'Dimensions' =>
											array (
												'Dimension' =>
													array (
														'Breadth' => '1',
														'Count'   => '1',
														'Height'  => '1',
														'Length'  => '1'
													),
											),
											'InvoiceNo' => $k->oid,
											'PackType' => '',
											'PickupDate' => $today_date,
											'PickupTime' => '1800',
											'PieceCount' => '1',
											'ProductCode' => 'A',
											'ProductType' => 'Dutiables',
											'SpecialInstruction' => '1',
											'SubProductCode' => ($k->payment_mode == 2 ? 'C' : 'P')
									),
									'Shipper' =>
										array(
											'CustomerAddress1' => substr(office_address,0,25),
											'CustomerAddress2' => substr(office_address,24,40),
											'CustomerAddress3' => substr(office_address,64,40),
											'CustomerCode' => '160915',
											'CustomerEmailID' => 'Vishwesh@adcanopus.com',
											'CustomerMobile' => office_number,
											'CustomerName' => 'Vishwesh',
											'CustomerPincode' => office_pincode,
											'CustomerTelephone' => office_number,
											'IsToPayCustomer' => '',
											'OriginArea' => 'BLR',
											'Sender' => '1',
											'VendorCode' => ''
										)
							),
							'Profile' => 
								 array(
								 	'Api_type' => 'S',
									//'LicenceKey'=>'9b40edffc20ce2e6fc1462a30fc48181',
									//'LoginID'=>'BLR00132',
									'LicenceKey'=> 'faa20d9ea765d3e4d3856c7faf72d0b8',
									'LoginID'   => 'BLR02248',
									'Version'   => '1.8')
									);

						// Here I call my external function
						//$result = $soap->__soapCall('GenerateWayBill',array($params));
						
						$result = $client->GenerateWayBill($params); 

						print_r($result->GenerateWayBillResult->Status->WayBillGenerationStatus);
						//echo $result->GenerateWayBillResult->DestinationArea."<br>";
						//echo $result->GenerateWayBillResult->DestinationLocation;
						//print_r($result); echo '</pre>';
						if($result->GenerateWayBillResult->AWBNo != ""){

							if($this->assign_tracking_number($k->oid, $result->GenerateWayBillResult->AWBNo, $result->GenerateWayBillResult->DestinationArea."-".$result->GenerateWayBillResult->DestinationLocation)){

								echo "Tracking Number ".$result->GenerateWayBillResult->AWBNo." updated for order-". $k->oid."<br>";
							}else{
								echo "Tracking Number Not updated for order-". $k->oid."Please check with Nutratimes Admin";
							}	

						}else{
							echo "Issue With Tracking Number Please contact Nutratimes Admin";
						}

					}
				}
			}else{
				
				echo "No Details Found, Please check caller status and Provider";
			}
		
	}


	public function assign_tracking_number($order_id="", $tracking_number="", $destination_code=""){

		if($order_id != "" && $tracking_number != "" && $destination_code!="" ){
		
			if($this->db->query("Update tbl_orders set delivery_status = 1, tracking_number ='".$tracking_number."', bluedart_area_code='".$destination_code."', caller_status=3, delivery_status=1, sent_date='".date( 'Y-m-d' )."' where id = ".$order_id)){

				return true;
			}else{

				return false;
			}
		}

	}



	public function get_all_delivery_status(){
		
		$trackid_arr = array();
		$status_delv = "";

		$twodays_backdate = date('Y-m-d',strtotime("-3 days"));
		 
		$sql_q = $this->db->query("Select 
										  id, tracking_number, payment_mode, qa_user, dt_c, dt_u 
									From
										  ".ORDER."
									Where
										   provider = 2 and 
										   caller_status in (3) and 
										   delivery_status in (1,5) and
										   Date_Format(sent_date ,'%Y-%m-%d') between '2019-03-01' AND '".$twodays_backdate."'" );  
		

		if($sql_q->num_rows() > 0){
			foreach($sql_q->result() as $k){
				if($k->tracking_number != ""){	
					
					$status_delv = $this->get_delivery_status($k->tracking_number);

					if( $k->tracking_number != "" && $k->id !=""  ){

						if($status_delv != ""){									
								
							/*$data = array(
								'status_code'    => $status_delv->Status,
								'comment'		 => $status_delv->Comment,
								'order_id' 	     => $k->id,
								'telecaller'     => $k->qa_user,
								'tracking_number'=> $k->tracking_number,
								'date'			 => date('Y-m-d', strtotime($status_delv->StatusDate)),
								'date_created'   => date('Y-m-d H:i:s')
							);
							
							// INSERT QUERY
							
							$this->db->insert(XPRESSBEE_TMP,$data);
							$xpres_id 	= $this->db->insert_id();
							*/	
						
							
							//UPDATE DELIVERY STATUS 

							if(strtolower($status_delv) == "dl"){

								if($k->payment_mode ==2 ){
									$this->db->query("Update tbl_orders set delivery_status = 2, payment_status = 1 where id = ".$k->id." and provider =2 limit 1");	

									echo "Delivery Status Updated For COD - order id ".$k->id."<br>";
								}else if($k->payment_mode ==1 ){

									$this->db->query("Update tbl_orders set delivery_status = 2 where id = ".$k->id." and provider =2 limit 1");	
									
									echo "Delivery Status Updated For Prepaid- order id ".$k->id."<br>";
								}

							}

						}else{
							echo "Status API Response error.<br>";
						}
						
					}					

				}
			}
		}
	
	}



	public function get_delivery_status($tracking_number){
	
		$url =  'http://api.bluedart.com/servlet/RoutingServlet?handler=tnt&action=custawbquery&loginid=BLR02248&lickey=df452935c5abba8756f0261c73b6f0fd&verno=1.3&scan=1&awb=awb&format=xml&numbers='.$tracking_number;
		$ch = curl_init( $url );
		
		# Setup request to send json via POST.

		//curl_setopt( $ch, CURLOPT_POSTFIELDS,  );
		//curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		# Return response instead of printing.
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		// Timeout in seconds
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		# Send request.
		$result = curl_exec($ch);
		curl_close($ch);
		
		$xml = new SimpleXMLElement($result);
		
		return $xml->Shipment->StatusType;

		
	}




}


