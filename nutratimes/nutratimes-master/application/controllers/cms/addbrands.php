<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addBrands extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library(array('form_validation', 'S3'));
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= BRANDS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['manage_page_title']	= "Manage Brands";
		$data['page_name']	 		= "Brands";
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page'] 		= "managebrands";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "brand_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	= NULL;
		$data['result_data']['description']				= NULL;
		$data['result_data']['status'] 					= NULL;
		$data['result_data']['name']					= NULL;
		$data['result_data']['seourl']					= NULL;
		$data['result_data']['status']					= NULL;
		$data['result_data']['datecreated']				= NULL;
		$data['result_data']['dateupdated']				= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	seourl,
																	brand_id, 
																	image,
																	name, 
																	status, 
																	description,
																	datecreated, 
																	dateupdated', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/ckeditor');
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-brands',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('seourl', 'SEO URL', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// GET BRAND ID
				$brand_id_img = (isset($_POST['brand_id']) && is_numeric($_POST['brand_id'])) ? $_POST['brand_id'] : NULL;
				
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['brand_id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where("brand_id",$_POST['brand_id']);
					// SET THE DATA
					$data = array(
									'name' => $_POST['name'],
									'seourl' => $_POST['seourl'],
									'status' => $_POST['status'],
									'description' => $_POST['description'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(BRANDS,$data);
					$this->session->set_flashdata('success', 'Brand edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'name' => $_POST['name'],
										'seourl' => $_POST['seourl'],
									'status' => $_POST['status'],
									'description' => $_POST['description'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(BRANDS,$data);
					$brand_id_img = $this->db->insert_id();
					$this->session->set_flashdata('success', 'Brand added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				
				if (isset ($_FILES['image']['name'])) {
					
					// SET AND INITIAL S3 CLASS AND FOR BUCKET
					$s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
					$s3->putBucket(S3_BUCKET, S3::ACL_PUBLIC_READ);
		
					$image_ext = allowed_extensions($_FILES['image']['name']);
					if($image_ext != false) {
						if($_FILES['image']['size'] > IMAGE_UPLOAD_MAX_SIZE) {
							// IF MORE THAN GENERATE FLASH MESSAGE
							$this->session->set_flashdata('error', 'File size must be less than or equals to '.
																	(IMAGE_UPLOAD_MAX_SIZE/1024/1024).' MB');
						} else {
							// CHECK IMAGE WE ARE TRYING TO UPLOAD THAT DIRECTORY IS AVAILABLE
							if (is_dir("../".BRAND_DIR_PATH) == false) {
								
								// DELETE THE CURRENT IMAGE FILE FROM HDD
								$brand_image = $this->db_function->get_single_value(BRANDS, 'image', "brand_id=".$brand_id_img, false, false);
								
								// FINALLY UPLOAD THE FILE AFTER CHECKING OF EVERYTHING 
								$new_img_name = get_rand_chracter('12','3').".".$image_ext;
								
								move_uploaded_file($_FILES['image']['tmp_name'],"./".BRAND_DIR_PATH.$new_img_name);		
								
								//CREATE THUMB FILE HERE 
								make_img_thumb("./".BRAND_DIR_PATH.$new_img_name, "./".BRAND_THUMB_IMG_DIR.THUMB_PREFIX.$new_img_name, 200);
								
								// COPY IMAGE TO AMAZON S3 SERVER
								$s3->putObjectFile("./".BRAND_DIR_PATH.$new_img_name, S3_BUCKET, BRAND_DIR_PATH.$new_img_name, S3::ACL_PUBLIC_READ);
								$s3->putObjectFile("./".BRAND_THUMB_IMG_DIR.THUMB_PREFIX.$new_img_name, S3_BUCKET, BRAND_THUMB_IMG_DIR.THUMB_PREFIX.$new_img_name, S3::ACL_PUBLIC_READ);
								
								// DELETE THE CURRENT IMAGE FILE FROM HDD
								unlink("./".BRAND_DIR_PATH.$new_img_name);
								unlink("./".BRAND_THUMB_IMG_DIR.THUMB_PREFIX.$new_img_name);
								
								// DELETE FROM OLD IMAGE FROM S3 BUCKET
								$s3->deleteObject(S3_BUCKET, BRAND_DIR_PATH.$brand_image);
								$s3->deleteObject(S3_BUCKET, BRAND_THUMB_IMG_DIR.THUMB_PREFIX.$brand_image);
								
								$brand_image = NULL;
								$brand_image['image'] = $new_img_name;
								
								// SET THE WHERE CLAUSE
								$this->db->where("brand_id", $brand_id_img);
								$this->db->update(BRANDS, $brand_image);
							}
						}
					}
				
				}
				
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
