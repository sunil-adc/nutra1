<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class searchorders_hairoil extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ($perpage = 30, $offset = 0) {
		
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
 
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Hairoil & Shampoo Orders";
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['page_title']	 		= "Search ".$data['page_name'];
		$data['manage_page'] 		= "searchorders";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/index";
		$data['mode'] 				= 'Search';
		
		
		$data['qa_agents'] = $this->replica_db->query("select id, username, email from ".ADMIN." where adm_role_id=2 and status=1");
		$data['cod_agents'] = $this->replica_db->query("select id, username, email from ".ADMIN." where adm_role_id=3 and status=1");


		// IF SEARCH REQUEST DETECT THEN SET THE PARAMETER AND PAGINATION
		if (isset($_REQUEST['s']) && trim($_REQUEST['s']) != "") {
			 			
			$search_string 		= $this->_create_search_query($_REQUEST);
			$search_criteria	= $search_string['search_criteria'];
			$select_fields		= $search_string['select_fields'];
			$table_fields		= $search_string['table_fields'];
			$where_condition	= $search_string['where_condition'];
			$order_by 			= $search_string['query_orderby'];
			$only_count 		= $search_string['only_count'];
			$only_amount 		= $search_string['only_amount'];
			
			// PREPARE QUERY FOR SEARCH DOWNLOAD RECORDS
			$data['full_sql'] 		= 	$search_string['query']; 
			$data['select_fields']	=	$select_fields;
			$data['table_fields']	=	$table_fields;
			$data['where_fields']	=	$where_condition;
			$data['search_criteria']=	$search_criteria;
			
			
			//$sel_query = $search_string['query']." group by o.id ".$order_by;
			$sel_query = $search_string['query']." group by o.id order by o.dt_c desc" ;
			
			$pg_query_string = "/?".$search_string['query_string'];
			
			// MODIFY FULL URL FOR PAGINATION
			$full_path .= $perpage;
			
			// SET FULL PATH
			// CONFIGURE PAGINATION PARAMETER
			
			// need to change here
			$data['only_count']				= NULL;
			$data['only_amount']			= NULL;
			$data['total_record']			= NULL;
			$data['total_amount']			= NULL;	
			$data['full_path'] 				= $full_path;
			$config['uri_segment'] 			= 5;
			$config['first_url'] 			= $full_path."/0/".$pg_query_string."#atab"; 
			$config['suffix'] 				= $pg_query_string."#atab";
			$config['total_rows'] 			= $this->db_function->count_record($sel_query, false, true, $this->replica_db, true);
			$config['per_page'] 			= $perpage;
			$config['base_url'] 			= $full_path;
			$choice 						= $config['total_rows'] / $config["per_page"];
			$config['num_links'] 			= 2;
			$config['full_tag_open'] 		= '<div id="paging" style="float:right; "><ul style="clear:left;">';
			$config['full_tag_close']	 	= '</ul></div>';
			$config['anchor_class'] 		= 'class="btn" ';
			$config['first_tag_open'] 		= '<li class="num_off">';
			$config['first_tag_close'] 		= '</li>';
			$config['last_tag_open']	 	= '<li class="num_off">';
			$config['last_tag_close'] 		= '</li>';
			$config['cur_tag_open'] 		= '<li class="num_on">';
			$config['cur_tag_close'] 		= '</li>';
			$config['num_tag_open'] 		= '<li class="num_off">';
			$config['num_tag_close'] 		= '</li>';
			$config['prev_tag_open'] 		= '<li class="num_off">';
			$config['prev_tag_close'] 		= '</li>';
			$config['next_tag_open'] 		= '<li class="num_off">';
			$config['next_tag_close'] 		= '</li>';
			$config['prev_link'] 			= 'PREVIOUS';
			$config['next_link'] 			= 'NEXT';
			$config['use_page_numbers'] 	= false;
			// PAGINATION PARAMETER VALUES END 
			
			
			if(isset($only_count) && trim($only_count) != "") {
				$counter 				= $this->replica_db->query($sel_query);
				$data['only_count'] 	= '1';
				$data['total_record'] 	= $counter->num_rows();
			} else if(isset($only_amount) && trim($only_amount) != "") {
				
				$amount 				= $this->replica_db->query($sel_query);
				$data['total_amount']	= 0;
				if($amount->num_rows() > 0) {
					foreach ($amount->result() as $amount_row) {
						$data['total_amount'] += ($amount_row->cash_price + $amount_row->shipping);
					}	
				}
				$data['only_amount'] 	= '1';
			} else {
				// PAGINATION PARAMETER INITIALIZE 
				$this->pagination->initialize($config);
				
				// SQL QUERY WITH OFFSET AND PERPAGE 
				$sql = $sel_query." limit ".$offset.", ".$perpage;
				
				// GET THE DATA FROM PAGINATION
				$data["total_rows"] = $config['total_rows'];
				$data["results"] = $this->db_function->get_data($sql, true, $this->replica_db, true);
				$data["links"] = $this->pagination->create_links();
			}
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// PAGE OFFSET DETAILS
		$data['offset'] = $offset;
		$data['perpage'] = $perpage;
		
		/*if (isset($sel_query) && $sel_query != '') {
			$data['sel_query'] = $sel_query;	
		}
		*/
			
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');	 
		$this->load->view(CMS_FOLDER_NAME.'/search-orders',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	public function __design($key = '', $value = ''){
		return '<div style="display:inline-block;margin:10px 15px 15px 0px"><span style="cursor: pointer;background: #FFEEC6;color: #624B0F;border-bottom: 1px solid #EBC875;border: 1px solid #FC0;padding: 2px 7px 2px 7px;font-size: 11px;-webkit-border-radius: 2px;-moz-border-radius: 2px;border-radius: 2px">'.$key.' : <span style="color:red">'.sub_string($value, 80).'</span></span></div>';
	}
	
	public function _create_search_query($arr) {
		
		$all_arrays = all_arrays();
		
		$pg_query_string 	= NULL;
		$orderby_string		= NULL;
		$search_string 		= NULL;
		$only_count			= NULL;
		$only_amount		= NULL;
		$search_criteria 	= '';
		
		$select_fields 		= "	o.id,
								o.order_id_pg,
								o.name,
								o.city,
								o.sub_id,
								o.clkid,
								o.email,
								o.is_fraud,
								o.fire_status,
								o.payment_check_status,
								o.mobile,
								o.caller_status,
								o.net,
								o.pubid,
								o.bnr, 
								o.qa_user, 
								o.payment_mode,
								o.payment_status,
								o.comments,
								o.bluedart_area_code,
								o.dt_c as order_date, 
								o.user as user_id ";
		
		if (isset($arr['product_name']) && trim($arr['product_name']) != "") {
			$select_fields 		.= ", p.product ";
		}
		
		$table_fields		= 	" ".ORDER." o ";
		$where_fields		=	" 1 = 1 and ";
		
		//FOR TELECALLER
		if ($this->session->userdata('admin_role_id') == 3 && $this->session->userdata('uid') != 24) {
			//$where_fields .= " o.qa_user = '".$this->session->userdata('uid')."' and o.caller_status != 4 and ";
			$where_fields .= " o.qa_user = '".$this->session->userdata('uid')."' and ";
		}

		if ((isset($arr['only_amount']) && trim($arr['only_amount']) != "") ||
			(isset($arr['product_name']) && trim($arr['product_name']) != "") || 
			(isset($arr['product']) && trim($arr['product']) != "")) {
			$select_fields	.= 	", p.cash_price, p.shipping ";
			$table_fields	.= 	" inner join ".USER_PRODUCTS." p on (o.id = p.order_id) ";
			
			$search_string .= " p.status = 1 and ";
			
		}else{

			$select_fields	.= 	", p.cash_price, p.shipping ";
			$table_fields	.= 	" inner join ".USER_PRODUCTS." p on (o.id = p.order_id) ";
			
			$search_string .= " p.status = 1 and ";

		}
		if (isset($arr['product_name']) && trim($arr['product_name']) != "") {
			$table_fields	.= 	" inner join ".PRODUCT." prod on (p.product = prod.prod_id) ";	
		}
		if (isset($arr['dnd_status']) && trim($arr['dnd_status']) != "") {
			$table_fields	.= 	" inner join ".USERS." u on (o.user = u.user_id) ";
		}
		
		 
		$query = "SELECT
						".$select_fields."
				  FROM
						".$table_fields."
				  WHERE 
				  		".$where_fields;
		
		if(is_array($arr)) {
			
			$arr = strip_slashes($arr);
			
			// CUSTOMER INFO SEARCH START //
			
			if (isset($arr['customer_name']) && trim($arr['customer_name']) != "") {
				$search_string .= " o.name = '".$arr['customer_name']."' and ";
				$pg_query_string .= "&customer_name=".$arr['customer_name'];
				$search_criteria .= 'Customer Name : '.$arr['customer_name'];		
			}
			if (isset($arr['state']) && trim($arr['state']) > 0) {
				$search_string .= " o.state = ".$arr['state']." and ";
				$pg_query_string .= "&state=".$arr['state'];
				$search_criteria .= $this->__design('State', $all_arrays['ARR_STATE'][$arr['state']]);
			}
			if (isset($arr['email']) && trim($arr['email']) != "") {
				$search_string .= " o.email = '".$arr['email']."' and ";
				$pg_query_string .= "&email=".$arr['email'];
				$search_criteria .= $this->__design('Email', $arr['email']);
			}
			if (isset($arr['city']) && trim($arr['city']) != "") {
				$search_string .= " o.city = '".$arr['city']."' and ";
				$pg_query_string .= "&city=".$arr['city'];
				$search_criteria .= $this->__design('City', $arr['city']);
			}
			/*if (isset($arr['zoneid']) && trim($arr['zoneid']) != "") {
				$search_string .= " o.zoneid = '".$arr['zoneid']."' and ";
				$pg_query_string .= "&zoneid=".$arr['zoneid'];
				$search_criteria .= $this->__design('Zone Id', $arr['zoneid']);
			}*/
			if (isset($arr['notpaid_reason']) && trim($arr['notpaid_reason']) != "") {
				$search_string .= " o.notpaid_reason = '".$arr['notpaid_reason']."' and ";
				$pg_query_string .= "&notpaid_reason=".$arr['notpaid_reason'];
				$search_criteria .= $this->__design('Not Paid Reason', $all_arrays['ARR_NOTPAID'][$arr['notpaid_reason']]);
			}
			if (isset($arr['phone_no']) && trim($arr['phone_no']) != "") {
				$search_string .= " o.mobile = '".$arr['phone_no']."' and ";
				$pg_query_string .= "&phone_no=".$arr['phone_no'];
				$search_criteria .= $this->__design('Phone No', $arr['phone_no']);
			}
			/*if (isset($arr['clkid']) && trim($arr['clkid']) != "") {
				if (isset($arr['clkid']) && $arr['clkid'] == '1') {
					$search_string .= " o.clkid != '' and o.clkid != '0' and ";	
				} else if (isset($arr['clkid']) && $arr['clkid'] == '0'){
					$search_string .= " (o.clkid == '' or  o.clkid == 0) and ";
				}
				$pg_query_string .= "&clkid=".$arr['clkid'];
				$search_criteria .= $this->__design('Click Id', (isset($arr['clkid']) && $arr['clkid'] == '1') ? 'Yes' : 'No');
			} */
			if (isset($arr['from_date']) && isset($arr['to_date']) && 
				trim($arr['from_date']) != "" && trim($arr['to_date']) != "") {
				if(isset($arr['date_type']) && trim($arr['date_type']) != "") {
					if($arr['date_type'] == 1) {
						$search_string .= "(o.qualified_date BETWEEN '".$arr['from_date']."' AND '".$arr['to_date']."') and ";
						$search_criteria .= $this->__design('Qualified date', $arr['from_date'].' to '.$arr['to_date']);		
					} else if($arr['date_type'] == 2) {
						$search_string .= "(o.sent_date BETWEEN '".$arr['from_date']."' AND '".$arr['to_date']."') and ";
						$search_criteria .= $this->__design('Sent date', $arr['from_date'].' to '.$arr['to_date']);	
					} else if($arr['date_type'] == 3) {
						$search_string .= "(o.sent_date BETWEEN '".$arr['from_date']."' AND '".$arr['to_date']."') and ";
						$search_criteria .= $this->__design('Sent date', $arr['from_date'].' to '.$arr['to_date']);	
					} else {
						$search_string .= "(DATE_FORMAT(o.refund_date, '%Y-%m-%d') BETWEEN '".$arr['from_date']."' AND '".$arr['to_date']."') and ";
						$search_criteria .= $this->__design('Refund date', $arr['from_date'].' to '.$arr['to_date']);		
					}
				} else {
					$search_string .= "(DATE_FORMAT(o.dt_c, '%Y-%m-%d') BETWEEN '".$arr['from_date']."' AND '".$arr['to_date']."') and ";
					$search_criteria .= $this->__design('Order date', $arr['from_date'].' to '.$arr['to_date']);
				}
				$pg_query_string .= "&date_type=".$arr['date_type']."&from_date=".$arr['from_date']."&to_date=".$arr['to_date'];
			}
			// CUSTOMER INFO SEARCH END //
			
			
			// OUTPUT TYPE START //
			if (isset($arr['only_count']) && trim($arr['only_count']) != "") {
				$only_count = "1";
				$search_criteria .= $this->__design('Only Count', 'Enable');
			}
			if (isset($arr['only_amount']) && trim($arr['only_amount']) != "") {
				$only_amount = "1";
				$search_string .= " o.payment_status = 1 and ";
				$search_criteria .= $this->__design('Only Amount', 'Enable');
			}
			if (isset($arr['stats']) && trim($arr['stats']) != "") {
				
			}
			if (isset($arr['order_by']) && trim($arr['order_by']) != "") {
				$order_by = ($arr['order_by'] == 1) ? 'DESC' : 'ASC';
				$orderby_string = " order by o.id ".$order_by;
				$pg_query_string .= "&order_by=".$arr['order_by'];
				$search_criteria .= $this->__design('Order by', $all_arrays['ARR_ORDERS'][$arr['order_by']]);
			} 
			
			// OUTPUT TYPE END //
			
			// STATUS WISE REPORT //
			/*if (isset($arr['qa_status']) && trim($arr['qa_status']) != "") {
				if(trim($arr['qa_status']) == '4') {
					$search_string .= " o.qa_status = '0' and ";
					$pg_query_string .= "&qa_status=0";
				} else {
					$search_string .= " o.qa_status = '".$arr['qa_status']."' and ";
					$pg_query_string .= "&qa_status=".$arr['qa_status'];
				}
				$search_criteria .= $this->__design('QA Status', $all_arrays['ARR_QA_STATUS'][$arr['qa_status']]);
			}*/
			if (isset($arr['fire_status']) && trim($arr['fire_status']) != "") {
				$search_string .= " o.fire_status = '".$arr['fire_status']."' and ";
				$pg_query_string .= "&fire_status=".$arr['fire_status'];
				$search_criteria .= $this->__design('Fire Status', $arr['fire_status']);
			}
			if (isset($arr['payment_check_status']) && trim($arr['payment_check_status']) != "") {
				$search_string .= " o.payment_check_status = '".$arr['payment_check_status']."' and ";
				$pg_query_string .= "&payment_check_status=".$arr['payment_check_status'];
				$search_criteria .= $this->__design('Payment Check Status', ($arr['payment_check_status'] == 1) ? 'Check' : 'Not Checked');
			}
			/*if (isset($arr['is_fraud']) && trim($arr['is_fraud']) != "") {
				$search_string .= " o.is_fraud = '".$arr['is_fraud']."' and ";
				$pg_query_string .= "&is_fraud=".$arr['is_fraud'];
				$search_criteria .= $this->__design('is fraud', ($arr['is_fraud'] == 1) ? 'Yes' : 'No');
			}*/
			/*
			if (isset($arr['zipdial_status']) && trim($arr['zipdial_status']) != "" && in_array($arr['zipdial_status'])) {
				$search_string .= " o.zipdial_status = '".$arr['zipdial_status']."' and ";
				$pg_query_string .= "&zipdial_status=".$arr['zipdial_status'];
				$search_criteria .= $this->__design('zipdial status', $all_arrays['ARR_ZIPDIAL_STATUS'][$arr['zipdial_status']]);
			}*/
			if (isset($arr['delivery_status']) && trim($arr['delivery_status']) != "") {
				if($arr['caller_status'] == 5) {
					$search_string .= " o.delivery_status != '2' and ";
				} else {
					$search_string .= " o.delivery_status = '".$arr['delivery_status']."' and ";
				}
				$pg_query_string .= "&delivery_status=".$arr['delivery_status'];
				$search_criteria .= $this->__design('Delivery status', $all_arrays['ARR_DELIVERY_STATUS'][$arr['delivery_status']]);
			}
			/*if (isset($arr['dnd_status']) && trim($arr['dnd_status']) != "") {
				$search_string .= " u.dnd_status = '".$arr['dnd_status']."' and ";
				$pg_query_string .= "&dnd_status=".$arr['dnd_status'];
			}*/
			if (isset($arr['caller_status']) && trim($arr['caller_status']) != "") {
				if($arr['caller_status'] == 5) {
					$search_string .= " o.caller_status != '3' and ";
				} else {
					$search_string .= " o.caller_status = '".$arr['caller_status']."' and ";
				}
				$pg_query_string .= "&caller_status=".$arr['caller_status'];
				$search_criteria .= $this->__design('Caller status', $all_arrays['ARR_CALLER_STATUS'][$arr['caller_status']]);
			}
			/*if (isset($arr['refund_status']) && trim($arr['refund_status']) != "") {
				$search_string .= " o.refund_status = ".$arr['refund_status']." and ";
				$pg_query_string .= "&refund_status=".$arr['refund_status'];
				$search_criteria .= $this->__design('Refund status', $all_arrays['ARR_REFUND'][$arr['refund_status']]);
			}*/
			// STATUS WISE REPORT //
			
			// NETWORK START //
			if (isset($arr['pubid']) && trim($arr['pubid']) != "") {
				$search_string .= " o.pubid = '".$arr['pubid']."' and ";
				$pg_query_string .= "&pubid=".$arr['pubid'];
				$search_criteria .= $this->__design('PubId', $arr['pubid']);
			}
			if (isset($arr['net']) && trim($arr['net']) != "") {
				$search_string .= " o.net = '".$arr['net']."' and ";
				$pg_query_string .= "&net=".$arr['net'];
				$search_criteria .= $this->__design('Net', $arr['net']);
			}
			if (isset($arr['bnr']) && trim($arr['bnr']) != "") {
				$search_string .= " o.bnr = '".$arr['bnr']."' and ";
				$pg_query_string .= "&bnr=".$arr['bnr'];
				$search_criteria .= $this->__design('BNR', $arr['bnr']);
			}
			if (isset($arr['tracking_number']) && trim($arr['tracking_number']) != "") {
				
				$arr['tracking_number'] = trim($arr['tracking_number'],",");
				$arr_tracking = explode(",", $arr['tracking_number']);
				
				foreach($arr_tracking as $k => $v){
					$arr_tracking[$k] = "'".preg_replace('/\s+/', '', $v)."'";
				}
				
				$str_tracking_number = implode(",", $arr_tracking);
				
				$search_string .= " o.tracking_number in (".trim(trim($str_tracking_number,","),", ").") and ";
				$pg_query_string .= "&tracking_number=".$arr['tracking_number'];
				$search_criteria .= $this->__design('Tracking number', trim(trim($str_tracking_number,","),", "));
			}
			if (isset($arr['product_name']) && trim($arr['product_name']) != "" && 
				isset($arr['pid']) && trim($arr['pid']) != "") {
				$search_string .= " p.status = '1' and p.product = '".$arr['pid']."' and ";
				$pg_query_string .= "&product_name=".$arr['product_name']."&pid=".$arr['pid'];
				$search_criteria .= $this->__design('Product Name', $arr['product_name']);
			}else{
				$search_string .= " p.status = '1' and p.product = 6 and ";
				//$pg_query_string .= "&product_name=".$arr['product_name']."&pid=6";
				//$search_criteria .= $this->__design('Product Name', $arr['product_name']);

			}

			if (isset($arr['refid']) && trim($arr['refid']) != "") {
				if (preg_match('/^[0-9, ]+$/', $arr['refid'])) {
					$search_string .= " o.id in (".trim(trim($arr['refid'],","),", ").") and ";
					$pg_query_string .= "&refid=".$arr['refid'];
					$search_criteria .= $this->__design('RefId', $arr['refid']);
				} else {
					$search_string .= " o.id in (0) and ";
					$pg_query_string .= "&refid=0";
					$search_criteria .= $this->__design('RefId', $arr['refid']);
				}
			}
			if (isset($arr['provider']) && trim($arr['provider']) != "") {
				$search_string .= " o.provider = '".$arr['provider']."' and ";
				$pg_query_string .= "&provider=".$arr['provider'];
				$search_criteria .= $this->__design('Provider', $all_arrays['ARR_PROVIDER'][$arr['provider']]);
			}
			if (isset($arr['order_id_pg']) && trim($arr['order_id_pg']) != "") {
				$search_string .= " o.order_id_pg = '".$arr['order_id_pg']."' and ";
				$pg_query_string .= "&order_id_pg=".$arr['order_id_pg'];
				$search_criteria .= $this->__design('Order Id PG', $arr['order_id_pg']);
			}
			/*if (isset($arr['qa_agents']) && trim($arr['qa_agents']) != "") {
				$search_string .= " o.qa_user = '".$arr['qa_agents']."' and ";
				$pg_query_string .= "&qa_agents=".$arr['qa_agents'];
				$search_criteria .= $this->__design('QA Agents', $arr['qa_agents']);
			}
			*/
			if (isset($arr['cod_agents']) && trim($arr['cod_agents']) != "") {
				$search_string .= " o.qa_user = '".$arr['cod_agents']."' and ";
				$pg_query_string .= "&cod_agents=".$arr['cod_agents'];
				$search_criteria .= $this->__design('COD Agents', $arr['cod_agents']);
			}
			/*if (isset($arr['cod_agents']) && trim($arr['cod_agents']) != "") {
				$search_string .= " o.cms_user = '".$arr['cod_agents']."' and ";
				$pg_query_string .= "&cod_agents=".$arr['cod_agents'];
				$search_criteria .= $this->__design('COD Agents', $arr['cod_agents']);
			}*/
			/*if (isset($arr['product']) && trim($arr['product']) != "") {
				$is_product = ($arr['product'] == 1) ? 0 : 1;
				$search_string .= " p.contest = ".$is_product." and p.status != 0 and ";
				$pg_query_string .= "&product=".$arr['product'];
				$search_criteria .= $this->__design('Product', (($arr['product'] == 1) ? 'Yes' : 'No'));
			}*/
			/*if (isset($arr['ip']) && trim($arr['ip']) != "") {
				$search_string .= " o.ip = '".$arr['ip']."' and ";
				$pg_query_string .= "&ip=".$arr['ip'];
				$search_criteria .= $this->__design('IP Address', $arr['ip']);
			}*/
			if (isset($arr['is_pc_mobile']) && trim($arr['is_pc_mobile']) != "") {
				$search_string .= " o.is_pc_mobile = '".$arr['is_pc_mobile']."' and ";
				$pg_query_string .= "&is_pc_mobile=".$arr['is_pc_mobile'];
				$search_criteria .= $this->__design('Lead from ', ($arr['is_pc_mobile'] == 0) ? 'PC' : 'Mobile');
			}
			if (isset($arr['issues']) && trim($arr['issues']) != "") {
				$search_string .= " o.issue = '".$arr['issues']."' and ";
				$pg_query_string .= "&issues=".$arr['issues'];
				$search_criteria .= $this->__design('Issues', $arr['issues']);
			}
			// NETWORK END //
			
			// PAYMENT START //
			if (isset($arr['payment_mode']) && trim($arr['payment_mode']) != "") {
				$search_string .= " o.payment_mode = '".$arr['payment_mode']."' and ";
				$pg_query_string .= "&payment_mode=".$arr['payment_mode'];
				$search_criteria .= $this->__design('Payment Mode', $all_arrays['ARR_PAYMENT'][$arr['payment_mode']]);
			}
			
			
			/*if ($this->session->userdata('admin_role_id') == 2) {
				$admin_cod_user = $this->db->query("select cod_user from ".ADMIN." where id = ".$this->session->userdata('uid'));
				if($admin_cod_user->num_rows() > 0) {
					foreach ($admin_cod_user->result() as $val_cms_user) {
						if (trim($val_cms_user->cod_user) != '') {
							$cms_cod_user[] = trim($val_cms_user->cod_user, ",");	
						}
					}
					
					if(isset($cms_cod_user) && is_array($cms_cod_user) && count($cms_cod_user) > 0) {
						$cms_cod_user = implode(", ",$cms_cod_user);
						$cms_where = " cms_user in (".$cms_cod_user.")";
						$search_criteria .= $this->__design('CMS User', $cms_cod_user);
					} else {
						$cms_cod_user = '0';	
						$cms_where = " cms_user < 0 ";
					}
				} else {
					$cms_cod_user = '0';	
					$cms_where = " cms_user < 0 ";
				}
				
				$search_string .= " o.payment_status = '0' and (o.caller_status != 4 and o.qa_status != 3 and o.delivery_status != 3) and ".$cms_where." and ";
				$pg_query_string .= "&payment_status=0";
				
			}  else if ($this->session->userdata('admin_role_id') == 3) {
				
				$cms_cod_states_where = ' 1 = 1 ';
				$admin_cod_states = $this->db->query("select cod_states from ".ADMIN." where id = ".$this->session->userdata('uid'));
				if($admin_cod_states->num_rows() > 0) {
					foreach ($admin_cod_states->result() as $val_cod_states) {
						if (trim($val_cod_states->cod_states) != '') {
							$cms_cod_states[] = trim($val_cod_states->cod_states, ",");	
						}
					}
					
					if(isset($cms_cod_states) && is_array($cms_cod_states) && count($cms_cod_states) > 0) {
						$cms_cod_states = implode(",",$cms_cod_states);
						$cms_cod_states_where = " o.state in (".$cms_cod_states.")";
						$search_criteria .= $this->__design('State', $cms_cod_states);
					}
				}
				
				$search_string .= " o.payment_status = '0' and (o.caller_status != 4 and o.qa_status != 3 and o.delivery_status != 3) and (o.cms_user = '' or o.cms_user = '".$this->session->userdata('uid')."') and ".$cms_cod_states_where." and ";
				$pg_query_string .= "&payment_status=0";
			} else */  if (isset($arr['payment_status']) && trim($arr['payment_status']) != "") {
				
				$search_string .= " o.payment_status = '".$arr['payment_status']."' and ";
				$pg_query_string .= "&payment_status=".$arr['payment_status'];
				$search_criteria .= $this->__design('Payment status', (($arr['payment_status'] == '1') ? 'Paid' : 'Not Paid'));
				
			} 
			/*
			else if (isset($arr['cms_order']) && trim($arr['cms_order']) != "") {
				
				if ($arr['cms_order'] == 0) {
					$search_string .= " o.cms_order = '".$arr['cms_order']."' and o.cms_user = 0 and ";
				} else if ($arr['cms_order'] == 1) {
					$search_string .= " o.cms_order = '".$arr['cms_order']."' and ";
				}
				$pg_query_string .= "&cms_order=".$arr['cms_order'];
				$search_criteria .= $this->__design('Order Type', (($arr['cms_order'] == 0) ? 'By Customer' : 'By Seajin Agent'));
			}*/
			// PAYMENT END //
			
			if (isset($arr['s']) && trim($arr['s']) != "") {
				$pg_query_string .= "&s=s";
			}
		}
		
		$search_string .= " 1 = 1 and ";
		$search_string = trim($search_string, "and ");
		$arr_return['select_fields']  	= $select_fields;
		$arr_return['table_fields']  	= $table_fields;
		$arr_return['where_fields']  	= $where_fields;
		$arr_return['where_condition']  = $search_string;
		$arr_return['query'] 			= $query .= $search_string;
		$arr_return['query_string'] 	= $pg_query_string;
		$arr_return['query_orderby'] 	= $orderby_string;
		$arr_return['only_count']		= $only_count;
		$arr_return['only_amount']		= $only_amount;
		$arr_return['search_criteria']	= $search_criteria;
		return $arr_return;
	}
}
