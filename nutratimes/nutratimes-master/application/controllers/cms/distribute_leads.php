<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class distribute_leads extends CI_Controller
{
   public $client_id;	
   public function __construct()
   {
        parent::__construct();
		$this->db->cache_off();
		//$this->output->enable_profiler(TRUE);
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		/*is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		*/
		// CHECK ADMIN IS LOGIN - END
		
		
   }


	public function test1($soap_key = ""){		 	
	 	/*
		$menu = $this->common_model->Menu_Array();
		$cur_controller 	= __CLASS__;
		
		// PAGE DETAILS
		$page_details['cur_controller'] = $cur_controller;
		$page_details['menu']           = $menu;
		
	    
		$from_date = date('Y-m-d',strtotime("-1 days")); 
		//$from_date = date('Y-m-d');
		
		$to_date   = date('Y-m-d');   
		
		if( $soap_key == "paft_key221"){
					
			$data['get_telecaller'] = $this->db_function->get_data("SELECT a.id, a.username FROM ".ADMIN_USERS." a LEFT JOIN ".ADMIN_ROLE." r On (a.adm_role_id= r.id)  WHERE  a.status =1 and (r.role_name = 'telecallers' OR r.id=2)");
			
			$no_of_telecaller = count($data['get_telecaller']);
			
		   //USER APPLICATION
			$sql = " SELECT 
						 up.id as upid
					 FROM 
						 ".USER_PRODUCT." up 
					 INNER JOIN 
						 ".U_FULL_DATA." ufd on (up.id = ufd.userprd_id) 
					 WHERE 
						 DATE_FORMAT(ufd.date_created, '%Y-%m-%d') between '".$from_date."' AND '".$to_date."' AND 
						 ufd.telecallerid = 0 AND 
						 ufd.app_status IN ('1','2')";
			
			$submit_apps  = $this->db->query($sql);
			$tot_app      = $submit_apps->num_rows();
			$distri_count = (int)($tot_app/$no_of_telecaller);
			$count_need   = ($distri_count*$no_of_telecaller);
					
			$submit_apps_proper = $this->db->query($sql." LIMIT ".$count_need);		
			if( $submit_apps_proper->num_rows > 0){
				if( $no_of_telecaller > 0){
					
					foreach($submit_apps_proper->result() as $row){
						$app_id[] = $row->upid;
					}
					
					$app_chunk = array_chunk($app_id, $distri_count, true);
					
					for($i=0; $i <= ($no_of_telecaller-1); $i++ ){
						$k = "('".implode("','", $app_chunk[$i])."')";
						if($k!=""){
							
							$this->db->query("UPDATE ".U_FULL_DATA." SET telecallerid=".$data['get_telecaller'][$i]->id." WHERE userprd_id IN ".$k);
							echo "Application Distributed Between Telecallers";
						}
					}
					
				}
			}
		
		}else{
		   echo "Not authozized";
		   die();	
		}
		*/
	}
	
	
	public function index($soap_key = ""){		 
	 	
	    
		$from_date = date('Y-m-d',strtotime("-1 days")); 
		//$from_date = "2018-09-01";//date('Y-m-d');
		
		$to_date   = date('Y-m-d');   
		
		if( $soap_key == "nut_ra221"){
		
			
			//Nutra KERLA, Andra Tamilnadu and Telanga Leads

			$sql_kerla = $this->db->query(" SELECT 
											 id, qa_user 
										 FROM 
											 ".ORDER."  
										 WHERE 
											 DATE_FORMAT(dt_c, '%Y-%m-%d') between '".$from_date."' AND '".$to_date."' AND 
											 qa_user = 0 AND state in (20, 5, 36, 3) ");


			if($sql_kerla->num_rows > 0){

				$this->db->query(" UPDATE ".ORDER." SET qa_user = 7 WHERE DATE_FORMAT(dt_c, '%Y-%m-%d') between '".$from_date."' AND '".$to_date."' AND 
						 qa_user = 0 AND state in (20, 5, 36, 3) ");

				echo "Kerla bangalore Andra and Telanga Leads =".$sql_kerla->num_rows."<br>";

			}
						 

			//NUtra USER APPLICATION
			$sql = " SELECT 
						 id, qa_user 
					 FROM 
						 ".ORDER."  
					 WHERE 
						 DATE_FORMAT(dt_c, '%Y-%m-%d') between '".$from_date."' AND '".$to_date."' AND 
						 qa_user = 0 ";
			
			$no_of_telecaller = 4;
			$pl_callers = array(5,7,15,17);
			
			$submit_apps  = $this->db->query($sql);
			$tot_app      = $submit_apps->num_rows();
			$distri_count = (int)($tot_app/$no_of_telecaller);
			$count_need   = ($distri_count*$no_of_telecaller);
					
			$submit_apps_proper = $this->db->query($sql." LIMIT ".$count_need);		
			if( $submit_apps_proper->num_rows > 0){
				if( $no_of_telecaller > 0){
					
					foreach($submit_apps_proper->result() as $row){
						$app_id[] = $row->id;
					}
					
					$app_chunk = array_chunk($app_id, $distri_count, true);
					
					for($i=0; $i <= ($no_of_telecaller-1); $i++ ){
						$k = "('".implode("','", $app_chunk[$i])."')";
						if($k!=""){
							$this->db->query("UPDATE ".ORDER." SET qa_user=".$pl_callers[$i]." WHERE id IN ".$k);
							echo "Leads Distributed Between Telecallers";
						}
					}
					
				}
			}
			
			
		}else{
		   echo "Not authozized";
		   die();	
		}
	
	}
	
	

}
?>