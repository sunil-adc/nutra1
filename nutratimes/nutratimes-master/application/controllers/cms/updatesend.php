<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class updateSend extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "edit", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Update Order to Send";
		$data['manage_page_title']	= $data['page_name'];
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "updatesend";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= 'Edit';
		
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');
		$this->load->view(CMS_FOLDER_NAME.'/update-send',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('sent_date', 'Sent Date', 'required');
		$this->form_validation->set_rules('tracknums', 'Order Id', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// ORDER STATUS DEFAULT TRUE
				$ord_status = true;
				$ord_logical_error = '';
				
				$trk_arr = explode("\n", $_REQUEST['tracknums']);
				foreach($trk_arr as $key=>$val){
					$val = trim($val);
					if ($val != '') {
						$order_status = $this->db_function->get_single_row(ORDER, "id, tracking_number, provider, payment_mode", "((payment_mode = 1 and payment_status = 1) or (payment_mode = 2 and payment_status = 0)) and (caller_status = 2 or caller_status = 6) and id = ".$val, $echo = false, $cache_off = true);
						
						if($order_status != false) {
							$id = '';
							if ($val > 0) {
								$this->db->where('id',$val); 
								$data = array(
									'status' => '1',
									'caller_status' => 3,
									'sent_date' => $_REQUEST['sent_date']
								);
								$this->db->update(ORDER,$data);
								
								// SEND MAIL TO USER
								$this->svaiza_email->dispatch_order($val, $order_status['tracking_number'], $order_status['payment_mode'], $order_status['provider']);
					
							}
						} else {
							$ord_logical_error .= $val.', ';
							$ord_status = false;
						}
					}
				}
				
			  	// DELETE ALL ORDER RELATED CACHE
				$this->common_model->delete_order_cache();
				
				if($ord_status == false) {
					$this->session->set_flashdata('error', 'Action completed with logical error, Data must be qualified before sent. (Id : '.(trim($ord_logical_error, ", ")).')');
				} else {
					$this->session->set_flashdata('success', 'Order status has been updated to sent date.');
				}
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
