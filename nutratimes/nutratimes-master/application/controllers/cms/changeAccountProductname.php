<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changeAccountProductName extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model(array (CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		
		// CHECK ADMIN IS LOGIN START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN END
		
	}
	
	public function prdSuggestion() {
		$str = '';
		if ( isset($_POST['product_name']) && trim($_POST['product_name']) != '') {
			$res = $this->db->query("select name from ".ACCOUNT_KEYWORDS." where lower(name) like '".$_POST['product_name']."%'");
			if ($res->num_rows() > 0) {
				$i = 0;
				foreach ($res->result() as $val) {
					$zebra_cls = ($i % 2 == 0) ? 'zebra_class' : '';
					$str .='<div class="suggested_entity">
						<div class="color_blue suggested_entity_link '.$zebra_cls.'" entity-data="'.$val->name.'" entity-dataid="">'.$val->name.'</div>
					</div>';
					$i++;
				}
			}
		}
		echo $str;
		exit;
	}
	
	public function index($divid="", $primary_id="", $mode="", $name_acc="") {
		
		// CHECK REQUIRED FIELED IS NOT BLANK.
		if( trim ($mode) == "label") { 
			
			// GET THE VALUE AND CHANGE IT
			// TURN OFF CACHE FOR ONE QUERY
			$acc_prod_name = $this->db_function->get_single_value(PRODUCT, 'name_acc', "prod_id=".$primary_id, false, true);
			
			// SEND THE RESPONCE IN CALLBACK
			?>
            <form method="post" action="javascript:void(0)">
                <input type="text" name="name_acc" id="txt_<?php echo $primary_id?>" class="account_keyword_suggestion text-input medium-input" 
                value="<?php echo $acc_prod_name?>" tabindex="1" />
                <input type="hidden" name="cms_url_hidden" id="cms_url_hidden" value="<?php echo FULL_CMS_URL?>" />
                <input type="submit" class="button small_ajax_button" id="save_<?php echo $primary_id?>" name="Save" value="Save" tabindex="2"
                onclick="javascript:display_ajax('<?php echo FULL_CMS_URL;?>', '<?php echo $divid;?>', '<?php echo $primary_id;?>', 'edit', $('#<?php echo 'txt_'.$primary_id;?>').val());">
                <div id="ajax_auto_suggestion" style="width:200px;margin:-3px 0px 0px 0px"></div>
            </form>
			<?php
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}else if( trim ($mode) == "copy") { 
			
			// GET THE VALUE AND CHANGE IT
			// TURN OFF CACHE FOR ONE QUERY
			$copy_prod_name = $this->db_function->get_single_value(PRODUCT, 'name', "prod_id=".$primary_id, false, true);
			
			// SET THE WHERE CLAUSE
			$this->db->where('prod_id',$primary_id);
			// SET THE DATA
			$data = array(
							'name_acc' => urldecode($copy_prod_name),
							'acc_dateupdated' => date('Y-m-d H:i:s')
						);
			// UPDATE QUERY
			$this->db->update(PRODUCT, $data);
			
			//DELETE THE CACHE FILE FOR ACCOUNT PRODUCT
			$this->db->cache_delete(CMS_FOLDER_NAME, 'accountproduct');
			
			?><a href="javascript:display_ajax('<?php echo FULL_CMS_URL;?>','acc_prod_id_<?php echo $primary_id;?>','<?php echo $primary_id;?>','label','')"><?php echo (urldecode($copy_prod_name) == "") ? 'Not Specified' : urldecode($copy_prod_name);?></a><?php
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else if( trim ($mode) == "edit") { 
			
			
			// SET THE DATA
			$data = array(
							'name_acc' => urldecode($name_acc),
							'acc_dateupdated' => date('Y-m-d H:i:s')
						);
			
			// SET THE WHERE CLAUSE
			$this->db->where('prod_id',$primary_id);
			
			// UPDATE QUERY
			$this->db->update(PRODUCT, $data);
			
			//DELETE THE CACHE FILE FOR ACCOUNT PRODUCT
			$this->db->cache_delete(CMS_FOLDER_NAME, 'accountproduct');
			
			// SEND THE RESPONCE IN CALLBACK
			?><a href="javascript:display_ajax('<?php echo FULL_CMS_URL;?>','acc_prod_id_<?php echo $primary_id;?>','<?php echo $primary_id;?>','label','')"><?php echo (urldecode($name_acc) == "") ? 'Not Specified' : urldecode($name_acc);?></a><?php
			
			if (urldecode($name_acc) == "") { ?> | <a href="javascript:display_ajax('<?php echo FULL_CMS_URL;?>','acc_prod_id_<?php echo $primary_id;?>','<?php echo $primary_id;?>','copy','')">Copy</a> <?php } 
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		} else {
			// ERROR OCCURE SO SEND REFRESH CALLBACK
			echo "refresh";
			
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
			exit;
			// EXIT IS REQUIRE WE ARE NOT LOADING VIEWS.
		}		
		// CHECK REQUIRED FIELED IS NOT BLANK.
	}
}