<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reportleadtoconversion extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		$this->load->helper('form');
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library(array('pagination', 'form_validation'));
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END				
	}
	public function index($perpage = 10, $offset = 0) {
		// DEFAULT PERPAGE RECORD
		$perpage = 100;
		
		// CALL PAGINATION FUNCTION
		$this->page($perpage, NULL, NULL, NULL, 0);
	}
	
	public function page($perpage = 10, $btn_search = NULL, $search_field = NULL, $search_txt = NULL, $offset = 0) {
		
		// GET ALL ARRAY FROM HELPER
		$arr_all = all_arrays();
		
		// DECLARE NULL VARIABLES 
		$query_string = NULL;
		$where = $whr = NULL;
		
		if($search_field == NULL && $search_txt == NULL && $btn_search > 0) {
			$offset = $btn_search;
			$btn_search = NULL;
		}
		
		$menu = $this->common_model->Menu_Array();
		$data['search_txt'] 	= "";
		$data['search_field'] 	= "";
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 		= $page_details['menu'];
		$data['cur_controller']	= $page_details['cur_controller'];
		$data['page_title']	 	= "Lead to Conversion report";
		$data['add_page_title']	= "Lead to Conversion report";
		$data['page_name']	 	= "Lead to Conversion report";
		$data['manage_page'] 	= strtolower(__CLASS__);
		$data['add_page'] 		= "reportleadtoconversion";
		$data['tablename']	 	= $tablename;
		$data['primary_field']	= "id";
		$data['seach_form']		= $full_path;
		
		unset($data['results']);
		
		if($this->input->post('btn_search') != "") {
			$total_lead = $total_conversion = $condition = 0;
			$this->form_validation->set_rules('from_date', 'From Date', 'required');
			$this->form_validation->set_rules('to_date', 'To Date', 'required');
		
			if ($this->form_validation->run() != false) {
				
				$data['from_date'] = $this->input->post('from_date');
				$data['to_date'] = $this->input->post('to_date');
				$data['net'] 		= $this->input->post('net');
				$data['pubid'] 		= $this->input->post('pubid');
				$data['bnr'] 		= $this->input->post('bnr');
				
				$whr = ' 1 = 1 and ';
				if (trim($data['net']) != '' && $data['net'] != "0") {
					$whr .= ' o.net = "'.$data['net'].'" and ';
					$condition = 1;
				}
				if (trim($data['pubid']) != '' && $data['pubid'] != "0") {
					$whr .= ' o.pubid = "'.$data['pubid'].'" and ';
					$condition = 1;
				}
				if (trim($data['bnr']) != '' && $data['bnr'] != "0") {
					$whr .= ' o.bnr = "'.$data['bnr'].'" and ';
					$condition = 1;
				}
				
				$this->replica_db->cache_off();
				if ($condition > 0) {
					$lead = $this->replica_db->query("
								  select 
										u.datecreated,
										count(u.user_id) total_user 
								  from 
										".USERS." u 
										inner join ".ORDER." o on (u.user_id = o.user)
								  where 
										left(u.datecreated, 10) between '".$data['from_date']."' and '".$data['to_date']."' and 
										".$whr." 1 = 1 
								  group by 
										left(u.datecreated, 10)");
				} else {
					$lead = $this->replica_db->query("
							  select 
									u.datecreated,
									count(u.user_id) total_user 
							  from 
									".USERS." u 
							  where 
									left(u.datecreated, 10) between '".$data['from_date']."' and '".$data['to_date']."'  
							  group by 
									left(u.datecreated, 10)");
				
				}
				
				if ($lead->num_rows() > 0) {
					foreach ($lead->result() as $val) {
						$data['total_lead'][] = array ($val->total_user, $val->datecreated);
					}
				}
				
				$conversion = $this->replica_db->query("select 
															count(o.id) as total_order, 
													  		o.dt_c
													  from 
															".ORDER." o 
													  where 
															o.payment_status = 1 and 
															left(o.dt_c, 10) between '".$data['from_date']."' and '".$data['to_date']."' and 
															".$whr." 1 = 1  
													  group by 
															left(o.dt_c, 10)");
				if ($conversion->num_rows() > 0) {
					foreach ($conversion->result() as $val_conversion) {
						$data['total_conversion'][] = array ($val_conversion->total_order, $val_conversion->dt_c);
					}
				}
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect(FULL_CMS_URL.'/'.$data['cur_controller'].'/');
				exit;
			}
		}
		
		$net_where['status'] = 1;
		$net_where['is_cpl_cps'] = 1;
		$data['net'] = $this->common_model->create_combo("net", "net", PIXELS, $net_where, "net", "net", $this->input->post('net'), "javascript:changePubs(this.value);", "4", '', true); 
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');	 
		$this->load->view(CMS_FOLDER_NAME.'/report-lead-conversion',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
}