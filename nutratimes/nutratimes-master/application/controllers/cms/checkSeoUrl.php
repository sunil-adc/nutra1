<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class checkSeoUrl extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), 'extra_call');
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $tablename = "", $seourl = "", $id = "1", $id_name = "1") {
		if($tablename != "" && $seourl != "") {
			// CACHE OFF FOR THIS QUERY
			$this->db->cache_off();
			$query = $this->db->query('select seourl from '.$tablename. ' where lower(seourl) = "'.strtolower(trim($seourl)).'" and '.$id_name.' != '.$id);
			if($query->num_rows() == 0) {
				// SUCCESS
				echo "1";
			} else {
				// DUPLICATE FOUND
				echo "0";
			}
		} else {
			echo "refresh";
		}
		exit;
	}
}