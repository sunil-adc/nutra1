<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class createorder extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), "extra_call");

		 
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ($id = NULL, $mode = "edit" ) {
		
		$data['name'] = $data['city'] = $data['email']= $data['phone'] = $data['state'] = $data['net'] = "" ;

		if(isset($_GET['name'])){

			$data['cname'] = $_GET['name'];
		}
		if(isset($_GET['phone'])){
			$data['phone'] = $_GET['phone'];
		}
		if(isset($_GET['email'])){
			$data['email'] = $_GET['email'];
		}
		if(isset($_GET['city'])){
			$data['city'] = $_GET['city'];

		}
		if(isset($_GET['state'])){
			$data['state'] = $_GET['state'];

		}

		if(isset($_GET['address'])){
			$data['address'] = $_GET['address'];

		}

		if(isset($_GET['net'])){
			$data['net'] = $_GET['net'];

		}
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['manage_page_title']	= "Create Order";
		$data['page_name']	 		= "Create Order";
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "searchorders/index";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/create-order',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	public function add() {
		
		
		date_default_timezone_set('Asia/Calcutta');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('area', 'Area', 'required');
		$this->form_validation->set_rules('state', 'State', 'required');
		$this->form_validation->set_rules('pincode', 'Pincode', 'required');
		$this->form_validation->set_rules('payment_status', 'Payment Status', 'required');
		
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				/**************************************************************************************/
				/******************CREATE NEW USER PROFILE IF MOBILE NUMBER IS NOT EXIST***************/
				/**************************************************************************************/
				if($this->general->is_duplicate_add(USERS, "mobile", $_POST['mobile'], true, false)) {
					$password	=	get_random_chracter(8, 15, true, true, true);
					$arr_post = $this->input->post();
					// SET THE DATA FOR INSERTION
					$data = array(
									'passwd'		=> md5($password),
									'name' 			=> $_POST['name'],
									'mobile' 		=> $_POST['mobile'],
									'email' 		=> $_POST['email'],
									'address'		=> $_POST['address'],
									'city'			=> $_POST['city'],
									'state'			=> $_POST['state'],
									'pincode'		=> $_POST['pincode'],
									'datecreated'	=> date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(USERS,$data);
					$user_id = $this->db->insert_id();
					
					
					// SEND EMAIL TO USER FOR REGISTER
					$this->svaiza_email->email_signup($user_id, $password);
				} else {
					$user_id = $this->db_function->get_single_value(USERS, 'user_id', "mobile='".$_POST['mobile']."'", false, false);
				}
				
				/**************************************************************************************/
				/**********************************CREATE NEW ORDER************************************/
				/**************************************************************************************/
				$data = array(
								'user' 		=> $user_id,
								'payment_status' => 1,
								'qualified_date' => date('Y-m-d'),
								'name'      => $_POST['name'],
								'mobile'    => $_POST['mobile'],
								'email'     => $_POST['email'],
								'caller_status' => '6',
								'address'   => $_POST['address'],
								'city'      => $_POST['city'],
								'area'      => $_POST['area'],
								'state'     => $_POST['state'],
								'alternate_phone' => $_POST['alternate_phone'],
								'pincode'   => $_POST['pincode'],
								'payment_status' => $_POST['payment_status'],
								'cms_order' => 1,
								'net'  		=> $_POST['net'],
								'qa_user'   => $this->session->userdata('uid'),
								'cms_order_user' => $this->session->userdata('uid'),
								'dt_c'      => date('Y-m-d H:i:s'),
								'payment_mode' => 1,
								'status'    => 1
							);
				// INSERT QUERY
				$this->db->insert(ORDER,$data);
				$order_id = $this->db->insert_id();
				
				// ADD IN LOG
				$this->common_model->order_logs($this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Order Created from CMS', '0', $order_id);
				
				$available_provider = $this->general->_payment_mode_wise_provider($_POST['pincode'], '1');
				if ($available_provider != false && is_numeric($available_provider)) {
					$this->general->_set_provider_order($available_provider, $order_id);
				}
				
				// DELETE ALL ORDER RELATED CACHE
				$this->common_model->delete_order_cache();
				
				// IF SUCCESSFUL THEN REDIRECT
				echo FULL_CMS_URL.'/orderproducts/index/'.$order_id.'/#addproduct';
			} else {
				echo 'require_blank';
			}
		} else {
			echo 'temp_error';	
		}
		
		// PUT EXIT FOR AJAX CALL
		exit;
	}
}