<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class uploadAwbNo extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "edit", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Upload AWB No.";
		$data['manage_page_title']	= $data['page_name'];
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "uploadawbno";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= 'Edit';
		
		$data['tracking_count'] = $this->db->query("select * from (
								
								(select count(awb_xpressbee_id) cash_xpressbee from ".AWB_XPRESSBEE." where status = 1 and payment_mode = 1) cash_xpressbee, 
								(select count(awb_xpressbee_id) cod_xpressbee from ".AWB_XPRESSBEE." where status = 1 and payment_mode = 2) cod_xpressbee) 
								");
			
			
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/datepicker');
		$this->load->view(CMS_FOLDER_NAME.'/upload-awb-no',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('provider', 'Provider', 'required');
		$this->form_validation->set_rules('payment_mode', 'Payment Mode', 'required');
		$this->form_validation->set_rules('tracknums', 'Sent Date', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// SET VARIABLES 
				$payment_mode 		= $_REQUEST['payment_mode'];
				$provider 			= $_REQUEST['provider'];
				$trk_arr 			= explode("\n",$_REQUEST['tracknums']);
				$tracking_number	= NULL;
				$all_arrays 		= all_arrays();
				$duplicate			= 0;
				$uploaded			= 0;
				
				if ($provider == 2) {
					$current_awbno = $this->common_model->get_awb_no_arr(AWB_BLUEDART);
				} elseif ($provider == 3) {
					$current_awbno = $this->common_model->get_awb_no_arr(AWB_DTDC);
				} elseif ($provider == 5) {
					$current_awbno = $this->common_model->get_awb_no_arr(AWB_FIRSTFLIGHT);
				} elseif ($provider == 7) {
					$current_awbno = $this->common_model->get_awb_no_arr(AWB_SEAJIN);
				} elseif ($provider == 8) {
					$current_awbno = $this->common_model->get_awb_no_arr(AWB_RED_EXPRESS);
				} elseif ($provider == 9) {
					$current_awbno = $this->common_model->get_awb_no_arr(AWB_ECOM_EXPRESS);
				}
			
				
				foreach($trk_arr as $key => $val){
					$awb = trim($val);
					if($payment_mode != 3){
						if(!in_array($awb, $current_awbno)) {
							if(trim($val) != "") {
								if($provider == 1 ){
									
									$this->db->insert(AWB_XPRESSBEE, array('awb_no' => $awb, "status" => 1, "payment_mode" => $payment_mode));					
									$uploaded++;
									
									//$this->session->set_flashdata('error', 'We are not providing any service from '.$all_arrays['ARR_PROVIDER'][$provider].' !!');
								} elseif ($provider == 2) {
									$this->db->insert(AWB_BLUEDART, array('awb_no' => $awb, "status" => 1, "payment_mode" => $payment_mode));					
									$uploaded++;
								} elseif ($provider == 3) {
									$this->db->insert(AWB_DTDC, array('awb_no' => $awb, "status" => 1, "payment_mode" => $payment_mode));
									$uploaded++;
								} elseif ($provider == 5) {
									$this->db->insert(AWB_FIRSTFLIGHT, array('awb_no' => $awb, "status" => 1, "payment_mode" => $payment_mode));
									$uploaded++;
								} elseif ($provider == 7) {
									$this->db->insert(AWB_SEAJIN, array('awb_no' => $awb, "status" => 1, "payment_mode" => $payment_mode));
									$uploaded++;
								} elseif ($provider == 8) {
									$this->db->insert(AWB_RED_EXPRESS, array('awb_no' => $awb, "status" => 1, "payment_mode" => $payment_mode));
									$uploaded++;
								} elseif ($provider == 9) {
									$this->db->insert(AWB_ECOM_EXPRESS, array('awb_no' => $awb, "status" => 1, "payment_mode" => $payment_mode));
									$uploaded++;
								}
							}
						} else {
							$duplicate++;
						}
					} else {
						
						$this->session->set_flashdata('error', 'We are not providing payment mode CBD anymore !!');
					}
				}
				
				
				if($duplicate == 0 && $uploaded >= 0) {
					$this->session->set_flashdata('success', 'Successfully code uploaded to '.$all_arrays['ARR_PROVIDER'][$provider].' !!');
				}else  if($duplicate > 0 && $uploaded == 0) {
					$this->session->set_flashdata('error', 'All tracking numbers are duplicate, all are ignored for <strong>'.$all_arrays['ARR_PROVIDER'][$provider].'</strong> !!');
				}else  if($duplicate > 0 && $uploaded >= 0) {
					$this->session->set_flashdata('error', '<strong>'.$duplicate.'</strong> tracking numbers are duplicate, <strong>'.$uploaded.'</strong> tracking numbers are uploaded for '.$all_arrays['ARR_PROVIDER'][$provider].'</strong> !!');
				}
				// DELETE ORDER CACHE
				$this->common_model->delete_order_cache();
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
