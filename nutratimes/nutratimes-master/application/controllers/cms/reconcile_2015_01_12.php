<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reconcile extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
		
		$this->db->cache_off();				
	}
	
	public function index ( $mode = "edit", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		define('MAX_RECONCILE_CHECK', '30');
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ORDER;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Reconcile";
		$data['manage_page_title']	= $data['page_name'];
		$data['page_title']	 		= $data['page_name'];
		$data['manage_page'] 		= "reconcile";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/index";
		$data['mode'] 				= 'Edit';
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		if (isset($_REQUEST['submit'])) {
			
			if(trim($_REQUEST['reconcile_data']) != "") {
				$arr_data = explode("\n", $_REQUEST['reconcile_data']);
				
				$responce_data = '<table border="1">';
				$responce_data .= '<tr>';
				$responce_data .= '<td>PG Order Id</td>';
				$responce_data .= '<td>Amount</td>';
				$responce_data .= '<td>Order Id</td>';
				$responce_data .= '<td>Comments</td>';
				$responce_data .= '<td>Satus</td>';
				$responce_data .= '<td>Information</td>';
				$responce_data .= '<td>Suggestion</td>';
				$responce_data .= '</tr>';
				
				$rk = 1;
foreach ($arr_data as $k => $v) {
	if (trim($v) != "" && $rk <= MAX_RECONCILE_CHECK){
		$arr_data_loop = explode("	", trim($v));
		
		$mobile_no 		= $arr_data_loop[0];
		$order_id_pg 	= $arr_data_loop[1];
		$amount 		= $arr_data_loop[2];
		
		$responce_data .= '<tr>';
		$responce_data .= '<td>'.$order_id_pg.'</td>';
		$responce_data .= '<td>'.$amount.'</td>';
		
		// CHECK ORDER ID NOT BLANK
		if ($order_id_pg > 0 && is_numeric($order_id_pg) && 
			$mobile_no > 0 && is_numeric($mobile_no)) {
			
			// SELECT ORDER DATA
			$this->db->cache_off();
			$res_order = $this->db->query("select 
												o.id as order_id,
												o.user,
												o.order_id_pg,
												o.payment_status,
												o.payment_mode,
												o.reconcile_check,
												up.contest,
												up.shipping,
												up.cod_price,
												up.cash_price,
												up.status,
												up.quantity
										   from 
												".ORDER." o left join ".USER_PRODUCTS." up on (up.order_id = o.id)
										   where 
										   		up.status = 1 and 
												o.mobile = '".$mobile_no."' and 
												o.order_id_pg = ".$order_id_pg);
			if ($res_order->num_rows() > 0) {
				
				$is_product = false;
				$is_contest = false;
				$reconcile_comment = $reconcile_product_contest = $reconcile_suggestion = $reconcile_status = $shipping = "";
				$amount_contest = $amount_product = $total_amount = 0;
				
				foreach ($res_order->result() as $val) {
					$order_id = $val->order_id;
					$reconcile_check = $val->reconcile_check;
					
					if ($val->contest == 1) {
						$is_contest = true;
						$amount_contest += $val->cash_price * $val->quantity;
					} else if ($val->contest != 1) {
						$is_product = true;
						$amount_product += $val->cash_price * $val->quantity;
					}
					$total_amount += $val->cash_price * $val->quantity;
					$shipping[] = $val->shipping;
					
					if ($val->user > 0) {
						$user_id = $val->user;
					}
				}
				
				$responce_data .= '<td>'.$order_id.'</td>';
				
				// ADD SHIPPING IN TOTAL AMOUNT
				$amount_shipping = max($shipping);
				$total_amount += $amount_shipping;
				
				if ($reconcile_check != '1' && $order_id > 0) {
					if ($is_contest == true && $is_product == true) {
						/************************************************************************/
						//***************** FOR CONTEST AND PRODUCT START ***********************/
						/************************************************************************/
						
						if ($user_id > 0) {
							$contest_payment_status = $product_payment_status = false;
						    
							if ($total_amount == $amount) {
								$this->db->cache_off();
								$res_points = $this->db->query("select user_id, points from ".USERS." where user_id = ".$user_id);
								if ($res_points->num_rows() > 0) {
									foreach ($res_points->result() as $val_points) {
										
										$used_credit_points = 0;
										
										$this->db->cache_off();
										$res_order = $this->db->query("select 
																			p.points 
																	   from 
																			".POINT_HISTORY." p inner join 
																			".ORDER." o on (o.id = p.order_id) 
																	   where 
																			o.user = ".$user_id);
										if ($res_order->num_rows() > 0) {
											foreach ($res_order->result() as $val_order) {
												$used_credit_points += $val_order->points;
											}
										}
										
										// COUNT THE TILL NOW CREDIT POINTS
										$order_amount = $this->order_credit_points($mobile_no, $order_id);
										$total_history_credit_points = $order_amount['contest_price'];
										$credit_point_hastobe_inaccount = ($total_history_credit_points - $used_credit_points);
										
										// CHECK FOR CONTEST
										if ($used_credit_points + $val_points->points + ($total_amount - $amount_product - $amount_shipping) == $credit_point_hastobe_inaccount) {
											// CHECK FOR CONTEST AMOUNT IF CREDIT ALREADY IN ACCOUNT THEN MAKE STATUS TRUE
											$contest_payment_status = true;
										
										} else if ($used_credit_points + $val_points->points + ($total_amount - $amount_product - $amount_shipping) == $credit_point_hastobe_inaccount + $used_credit_points) {
											
											// IF SAME CREDIT POINT IS MISSING WHICH WE HAVE TO GIVE THEN GIVE IT
											$contest_payment_status = true;
										}
										
										// CHECK FOR PRODUCTS
										if ($amount_product == ($amount - $amount_contest - $amount_shipping)) {
											$product_payment_status = true;
										}
										
										if($contest_payment_status == true && $product_payment_status == true) {
											
											$final_user_points = $amount_contest + $val_points->points;
											$this->db->query("update ".USERS." set points = ".$final_user_points." 
																 where user_id = ".$user_id);
											$this->db->query("update ".ORDER." set payment_status = 1, caller_status = 2, payment_check_status = 1, is_fraud = 0 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
											$reconcile_comment .= "User credit point updated to ".$final_user_points." (User Id : ".$user_id."), and order updated to paid";
											$reconcile_status .= "Success";
											
										} else {
											$this->db->query("update ".ORDER." set payment_status = 0, caller_status = 1, payment_check_status = 0, is_fraud = 1 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
											$reconcile_comment .= "Payment status updated to not paid and marked as fraud";
											$reconcile_suggestion .= "Check credit points";
											$reconcile_status .= "Error";
										}
									}
								} else {
									$reconcile_comment = "Unexpected Error User not found";
									$reconcile_suggestion = "UserId : ".$user_id." not available contact developers";
									$reconcile_status = "Fatal Error";
								}
							} else {
								$this->db->query("update ".ORDER." set payment_status = 0, caller_status = 1, payment_check_status = 0, is_fraud = 1 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
								$reconcile_comment = "Payment status updated to not paid and marked as fraud";
								$reconcile_suggestion = "Paid amount and Order amount is not matching.";
								$reconcile_status = "Error";
							}
						} else {
							$reconcile_comment = "Unexpected Error User not found";
							$reconcile_suggestion = "UserId : ".$user_id." not available contact developers";
							$reconcile_status = "Fatal Error";
						}
						/************************************************************************/
						/****************** FOR CONTEST AND PRODUCT END *************************/
						/************************************************************************/
						$reconcile_product_contest = "Order includes product and contest both";
					} else if ($is_contest == true && $is_product == false) {
						/************************************************************************/
						/************************ ONLY FOR CONTEST START ************************/ 
						/************************************************************************/
						if ($total_amount == $amount) {
							if ($user_id > 0) {
								$this->db->cache_off();
								$res_points = $this->db->query("select user_id, points from ".USERS." where user_id = ".$user_id);
								if ($res_points->num_rows() > 0) {
									foreach ($res_points->result() as $val_points) {
										$used_credit_points = 0;
										$this->db->cache_off();
										$res_order = $this->db->query("select 
																			p.points 
																	   from 
																			".POINT_HISTORY." p inner join 
																			".ORDER." o on (o.id = p.order_id) 
																	   where 
																			o.user = ".$user_id);
										if ($res_order->num_rows() > 0) {
											foreach ($res_order->result() as $val_order) {
												$used_credit_points += $val_order->points;
											}
										}
										
										// COUNT THE TILL NOW CREDIT POINTS
										$order_amount = $this->order_credit_points($mobile_no, $order_id);
										$total_history_credit_points = $order_amount['contest_price'];
										$credit_point_hastobe_inaccount = ($total_history_credit_points - $used_credit_points);
										
										// COMPARE CURRENT CREDIT POINTS WITH HAS TO BE CREDIT POINT
										if ($val_points->points == $credit_point_hastobe_inaccount) {
											$this->db->query("update ".ORDER." set payment_status = 1, caller_status = 2, payment_check_status = 1, is_fraud = 0 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
											$reconcile_comment .= "Order updated to Paid";
										} else if ($val_points->points != $credit_point_hastobe_inaccount) {
											
											// USED CREDIT POINT  + CURRENT CREDIT POINT == HASTO BE CREDIT POINT
											if ($used_credit_points + $val_points->points == $total_history_credit_points){
												
												$this->db->query("update ".ORDER." set payment_status = 1, 
																		caller_status = 2, payment_check_status = 1, is_fraud = 0 
																	where id = ".$order_id." and order_id_pg = ".$order_id_pg);
												
												$reconcile_comment = "Order updated to paid";
												$reconcile_status = "Success";
											
											} else if ($used_credit_points + $val_points->points == 0 && 
												$amount == $total_history_credit_points) {
												
												$this->db->query("update ".USERS." set points = ".$amount." 
																		where user_id = ".$user_id);
												
												$this->db->query("update ".ORDER." set payment_status = 1, 
																	caller_status = 2, payment_check_status = 1, is_fraud = 0
																  where id = ".$order_id." and order_id_pg = ".$order_id_pg);
												
												$reconcile_comment = "User credit point updated to ".$amount." 
																		(User Id : ".$user_id.") and Order updated to paid";
												$reconcile_status = "Success";
												
											} else if ($used_credit_points + $val_points->points + $amount == $total_history_credit_points){
												$total_amount = $val_points->points + $amount;
												$this->db->query("update ".USERS." set points = ".$total_amount." 
																	where user_id = ".$user_id);
												
												$this->db->query("update ".ORDER." set payment_status = 1, caller_status = 2, payment_check_status = 1, is_fraud = 0 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
												$reconcile_comment = "User credit point updated to ".$total_amount." (User Id : ".$user_id.") and Order updated to paid";
												$reconcile_status = "Success";
											} else {
												
												$this->db->query("update ".ORDER." set payment_status = 0, caller_status = 1, payment_check_status = 0, is_fraud = 1 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
												$reconcile_comment = "Payment status updated to not paid and marked as fraud";
												$reconcile_status = "Fatal Error";
											}
										}
									}
								} else {
									$reconcile_comment = "Unexpected Error User not found";
									$reconcile_suggestion = "UserId : ".$user_id." not available contact developers";
									$reconcile_status = "Fatal Error";
								}
							} else {
								$reconcile_comment = "Unexpected Error User not found";
								$reconcile_suggestion = "UserId : ".$user_id." not available contact developers";
								$reconcile_status = "Fatal Error";
							}
						} else {
							$this->db->query("update ".ORDER." set payment_status = 0, caller_status = 1, payment_check_status = 0, is_fraud = 1 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
							$reconcile_comment = "Payment status updated to not paid and marked as fraud";
							$reconcile_suggestion = "Paid amount and Order amount is not matching.";
							$reconcile_status = "Error";
						}
						$reconcile_product_contest = "Order includes contest only";
						/************************************************************************/
						//*********************** ONLY FOR CONTEST END *************************//
						/************************************************************************/
					} else if ($is_contest == false && $is_product == true) {
						/************************************************************************/
						//********************** FOR ONLY PRODUCT START *************************/
						/************************************************************************/
						
						if ($user_id > 0) {
							if ($total_amount == $amount) {
								$this->db->query("update ".ORDER." set payment_status = 1, caller_status = 2, payment_check_status = 1, is_fraud = 0 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
								$reconcile_comment = "Order payment status updated to paid";
								$reconcile_status = "Success";
							} else if ($total_amount > $amount) {
								$used_credit_points = 0;
								
								if ($user_id > 0) {
									$this->db->cache_off();
									$res_points = $this->db->query("select user_id, points from ".USERS." where user_id = ".$user_id);
									if ($res_points->num_rows() > 0) {
										foreach ($res_points->result() as $val_points) {
											$user_available_credit_point = $val_points->points;
										}
									}
								}
								
								$this->db->cache_off();
								$res_order = $this->db->query("select 
																	p.points 
															   from 
																	".POINT_HISTORY." p inner join 
																	".ORDER." o on (o.id = p.order_id) 
															   where 
																	o.user = ".$user_id);
								if ($res_order->num_rows() > 0) {
									foreach ($res_order->result() as $val_order) {
										$used_credit_points += $val_order->points;
									}
								}
									
								// COUNT THE TILL NOW CREDIT POINTS
								$order_amount = $this->order_credit_points($mobile_no, $order_id);
								$total_history_credit_points = $order_amount['contest_price'];
								$credit_point_hastobe_inaccount = ($total_history_credit_points - $used_credit_points);
								
								
								if ($credit_point_hastobe_inaccount == $user_available_credit_point) {
									if ($user_available_credit_point >= ($total_amount - $amount)) {
										
										$remaining_creditpoint = $user_available_credit_point - ($total_amount - $amount);
										$automated_used_creditpoints = $total_amount - $amount;
										
										$this->db->query("insert into ".POINT_HISTORY." set order_id = ".$order_id.", 
										points = ".$automated_used_creditpoints.", status = 1, dt_c = '".date('Y-m-d H:i:s')."'");
										
										$this->db->query("update ".USERS." set points = ".$remaining_creditpoint." 
															where user_id = ".$user_id);
										
										$this->db->query("update ".ORDER." set payment_status = 1, caller_status = 2, payment_check_status = 1, is_fraud = 0 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
										$reconcile_comment = "User credit point updated to ".$remaining_creditpoint." (User Id : ".$user_id.") and Order updated to paid";
										$reconcile_status = "Success";
									} else {
										$this->db->query("update ".ORDER." set payment_status = 0, caller_status = 1, payment_check_status = 0, is_fraud = 1 where id = ".$order_id." and order_id_pg = ".$order_id_pg);
										$reconcile_comment .= "Payment status updated to not paid and marked as fraud";
										$reconcile_suggestion .= "Order amount and available credit point is not matching payment is less than user has to pay check manual for more info";
										$reconcile_status .= "Error";
									}
								} else {
									$reconcile_comment = "No changes happen";
									$reconcile_suggestion = "User Available credit point is not matching with has ti be available credit point";
									$reconcile_status = "Error";
								
								}
							} else {
								$reconcile_comment = "As per automatic system, and as we know it may possible someone has deleted product this order check the logs for more details";
								$reconcile_suggestion = "Check ordered products (Include deleted products)";
								$reconcile_status = "Error";
							}
						} else {
							$reconcile_comment = "Unexpected Error User not found";
							$reconcile_suggestion = "UserId : ".$user_id." not available contact developers";
							$reconcile_status = "Fatal Error";
						}
						$reconcile_product_contest = "Order includes product only";
						/************************************************************************/
						//*********************** FOR ONLY PRODUCT END **************************/
						/************************************************************************/
					}
					$responce_data .= '<td>'.$reconcile_comment.'</td>';
					$responce_data .= '<td>'.$reconcile_status.'</td>';
					$responce_data .= '<td>'.$reconcile_product_contest.'</td>';
					$responce_data .= '<td>'.$reconcile_suggestion.'</td>';
					
					$this->db->query("insert into ".RECONCILE_LOG." set order_id = ".$order_id.", order_id_pg = ".$order_id_pg.", user_id = ".$user_id.", comment = '".$reconcile_comment."'");
					
					if ($order_id > 0 && $order_id_pg > 0) {
						$this->db->query("update ".ORDER." set reconcile_check = 1 where order_id_pg = ".$order_id_pg." and id = ".$order_id);
					}
				} else {
					$responce_data .= '<td>Already Reconciled</td>';
					$responce_data .= '<td>Error</td>';					
				}
			} else {
				$this->db->query("update ".ORDER." set is_fraud = 1 where mobile = ".$mobile_no);						
				$responce_data .= '<td>N/A</td>';
				$responce_data .= '<td>Order Id Pg not found</td>';
				$responce_data .= '<td>Fatal Error</td>';
				$responce_data .= '<td></td>';
				$responce_data .= '<td>Check manually</td>';
			}
		} else {
			$responce_data .= '<td>N/A</td>';
			$responce_data .= '<td>Order is pg must be greater than zero</td>';
			$responce_data .= '<td>Hard Fatal Error</td>';
			$responce_data .= '<td></td>';
			$responce_data .= '<td>Check manually</td>';
		}
		$responce_data .= '</tr>';
	}
	$rk++;
}
				$responce_data .= '</table>';
			}
			
			header("Content-Type:  application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=reconcile-".time().".xls");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			
			echo $responce_data;
			
			exit;
		}
		
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/reconcile',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	public function order_credit_points($mobile = 0, $order_id = 0) {
		if ($mobile > 0 && $order_id > 0) {
			$price['contest_price'] = 0;
			$price['product_price'] = 0;
			
			$res_order = $this->db->query("select 
												o.id,
												o.payment_status,
												o.payment_mode,
												up.cash_price,
												up.cod_price,
												up.shipping,
												up.quantity,
												up.contest,
												up.status
										  from 
												".ORDER." o inner join ".USER_PRODUCTS." up on (up.order_id = o.id) 
										  where 
												up.status = 1 and 
												(o.payment_status = 1 or o.id = ".$order_id.") and  
												o.status = 1 and 
												o.mobile = ".$mobile);	
			if ($res_order->num_rows() > 0) {
				foreach ($res_order->result() as $val) {
					if($val->contest == 1) {
						$price['contest_price'] += ($val->payment_mode == 1) ? $val->cash_price * $val->quantity : $val->cod_price * $val->quantity;
					} else {
						$price['product_price'] += ($val->payment_mode == 1) ? $val->cash_price * $val->quantity : $val->cod_price * $val->quantity;
					}
				}
			}
			return $price;
		}
		return false;
	}
}
