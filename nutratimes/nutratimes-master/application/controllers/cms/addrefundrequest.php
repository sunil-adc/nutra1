<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addRefundRequest extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library(array('form_validation', 'S3'));
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= REFUND_STATUS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_title']	 		= ucfirst($mode)." Category";
		$data['manage_page_title']	= "Add Refund Request";
		$data['page_name']	 		= "Refund Request";
		$data['manage_page'] 		= "addrefundrequest";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "refund_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// 
		if($mode == "edit" && ($id == NULL || $id < 1)) {
			$this->session->set_flashdata('error', 'Don\'t try to cheat the system, one or more require parameters are missing !!');
			redirect(FULL_CMS_URL."/dashboard");
			exit;
		}
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] 					= NULL;
		$data['result_data']['refund_id']		= NULL;
		$data['result_data']['user_id']			= NULL;
		$data['result_data']['order_id']		= NULL;
		$data['result_data']['mobile']			= NULL;
		$data['result_data']['name']			= NULL;
		$data['result_data']['email']			= NULL;
		$data['result_data']['status']			= NULL;
		$data['result_data']['datecreated']		= NULL;
		$data['result_data']['dateupdated']		= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			
			$query = $this->db->query("select 
											refund_id, 		user_id, 
											order_id, 		mobile, 
											name, 			email, 
											status, 			datecreated, 
											dateupdated	
									   from 
									   		".$tablename."  
									   where 
									   		".$data['primary_field'].' = '.$id.' 
									   group by 
									   		order_id');
			if ($query->num_rows() > 0) {
				$data['result_data'] = $query->row_array(); 
			} else {
				$data['result_data'] = false;	
			}
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-refund-request',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('order_id', 'Order id', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				$res = $this->db->query("select user, id, mobile, email from ".ORDERS." where id = ".$_POST['order_id']);
				if ($res->num_rows() > 0) {
					foreach ($res->result() as $val) {
						$user_id 	= $val->user_id;
						$order_id 	= $val->order_id;
						$mobile 	= $val->mobile;
						$name 		= $val->name;
						$email 		= $val->email;
					}
					
					if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['refund_id'])) { 		
						// SET THE WHERE CLAUSE
						$this->db->where('refund_id', $_POST['refund_id']);
						// SET THE DATA
						$data = array(
										'user_id' => $user_id,
										'order_id' => $order_id,
										'mobile' => $mobile,
										'name' => $name,
										'email' => $email,
										'status' => $_POST['status'],
										'datecreated' => date('Y-m-d H:i:s'),
										'dateupdated' => '0000-00-00 00:00:00'
									);
						// UPDATE QUERY
						$this->db->update(REFUND_STATUS,$data);
						$this->session->set_flashdata('success', 'Category edited successfully');
					} else if ( trim ($_POST['mode']) == 'add') { 			
						// SET THE DATA FOR INSERTION
						$data = array(
										'user_id' => $user_id,
										'order_id' => $order_id,
										'mobile' => $mobile,
										'name' => $name,
										'email' => $email,
										'status' => $_POST['status'],
										'dateupdated' => date('Y-m-d H:i:s')
									);
						// INSERT QUERY
						$this->db->insert(REFUND_STATUS,$data);
						$cat_id_img = $this->db->insert_id();
						$this->session->set_flashdata('success', 'Category added successfully');
					}
				} else {
					$this->session->set_flashdata('error', 'Order id invalid.');
					redirect($_POST['cur_url']);
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
