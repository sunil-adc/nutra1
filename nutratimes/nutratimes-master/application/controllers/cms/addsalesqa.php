<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addSalesQa extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "edit", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		if ($id == "" or $id == NULL || $id <= 0){
			redirect(FULL_CMS_URL.'/dashboard');
		}
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ADMIN;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Admin User";
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['manage_page'] 		= "managesalesqa";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data']['id']			= NULL;
		$data['result_data']['email']		= NULL;
		$data['result_data']['fname']		= NULL;
		$data['result_data']['lname']		= NULL;
		$data['result_data']['cod_user']	= NULL;
		$data['result_data']['status']		= NULL;
		$data['result_data']['datecreated']	= NULL;
		$data['result_data']['dateupdated']	= NULL;
		
		
		$cod_user = $this->db->query("select id, cod_user from ".ADMIN." where status = '1' and id = ".$id);
		if($cod_user->num_rows() > 0) {
			foreach ($cod_user->result() as $cod_user) {
				$data['result_data']['id'] = $cod_user->id;
				if ($cod_user->cod_user != "") {
					$data['result_data']['cod_user'] = explode(",",trim($cod_user->cod_user,","));
				} else {
					$data['result_data']['cod_user'] = array();
				}
			}
		} else {
			$data['result_data']['cod_user'] = false;
		}
		
		$qa_agents = $this->db->query("select fname, lname, email, id from ".ADMIN." where status = '1' and adm_role_id = 2");
		if($qa_agents->num_rows() > 0) {
			foreach ($qa_agents->result() as $qa_user) {
				$data['result_data']['qa_agents'][] = $qa_user; 
			}
		} else {
			$data['result_data']['qa_agents'] = false;
		}
		
		$sales_agents = $this->db->query("select fname, lname, email, id from ".ADMIN." where status = '1' and adm_role_id = 3");
		if($sales_agents->num_rows() > 0) {
			foreach ($sales_agents->result() as $sales_user) {
				$data['result_data']['sales_agents'][] = $sales_user; 
			}
		} else {
			$data['result_data']['sales_agents'] = false;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-sales-qa',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$primary_field = 'id';
		$tablename = ADMIN;
	
		$this->form_validation->set_rules('qa_user', 'QA Agent', 'required');
		$this->form_validation->set_rules('cod_user[]', 'Sales Agent', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				
				// SET THE WHERE CLAUSE
				$this->db->where($primary_field,$_POST['qa_user']);
				
				if(is_array($_POST['cod_user'])) {
					$cod_user = implode(",", $_POST['cod_user']);
				}
				
				// UPDATE QUERY
				$this->db->update($tablename,array("cod_user" => trim($cod_user,","), "dateupdated" => date('Y-m-d H:i:s')));
				
				$this->session->set_flashdata('success', 'User edited successfully');
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
