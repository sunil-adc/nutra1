<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addKeywords extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library(array('form_validation'));
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= ACCOUNT_KEYWORDS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_title']	 		= ucfirst($mode)." Accont Keywords";
		$data['manage_page_title']	= "Manage Accont Keywords";
		$data['page_name']	 		= "Accont Keywords";
		$data['manage_page'] 		= "managekeywords";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "keyword_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// 
		if($mode == "edit" && ($id == NULL || $id < 1)) {
			$this->session->set_flashdata('error', 'Don\'t try to cheat the system, one or more require parameters are missing !!');
			redirect(FULL_CMS_URL."/dashboard");
			exit;
		}
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] 					= NULL;
		$data['result_data']['keyword_id']		= NULL;
		$data['result_data']['name']			= NULL;
		$data['result_data']['status']			= NULL;
		$data['result_data']['datecreated']		= NULL;
		$data['result_data']['dateupdated']		= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	keyword_id, name, status, datecreated, dateupdated ', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-keywords',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				// GET BRAND ID
				$cat_id_img = (isset($_POST['keyword_id']) && is_numeric($_POST['keyword_id'])) ? $_POST['keyword_id'] : NULL;
				
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['keyword_id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where('keyword_id',$_POST['keyword_id']);
					// SET THE DATA
					$data = array(
									'name' => $_POST['name'],
									'status' => $_POST['status'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(ACCOUNT_KEYWORDS,$data);
					$this->session->set_flashdata('success', 'Keyword edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'name' => $_POST['name'],
									'status' => $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(ACCOUNT_KEYWORDS,$data);
					$keyword_id_img = $this->db->insert_id();
					$this->session->set_flashdata('success', 'Keyword added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
