<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addblog extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library(array('form_validation', 'S3'));
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		/*is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__); */
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= BLOG;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_title']	 		= ucfirst($mode)." Blog";
		$data['manage_page_title']	= "Manage Blog";
		$data['page_name']	 		= "Blog";
		$data['manage_page'] 		= "manageblog";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "blogid";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		// 
		if($mode == "edit" && ($id == NULL || $id < 1)) {
			$this->session->set_flashdata('error', 'Don\'t try to cheat the system, one or more require parameters are missing !!');
			redirect(FULL_CMS_URL."/dashboard");
			exit;
		}
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] 					= NULL;
		$data['result_data']['blogid']			= NULL;
		$data['result_data']['title']	    	= NULL;
		$data['result_data']['']				= NULL;
		$data['result_data']['status']			= NULL;
		$data['result_data']['date_created']	= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	blogid, title, url, blogcontent, blog_image, status, date_created, pagetitle, meta_keyword, metadescription ', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-blog',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('title', 'Title', 'required');
	    $this->form_validation->set_rules('url', 'URL', 'required');
	    $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_rules('page_title', 'Page Title', 'required');
        $this->form_validation->set_rules('meta_description', 'Meta Description', 'required');
		
		$blog_id = "";
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
							
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['blogid'])) { 		
					
					// SET THE WHERE CLAUSE
					$blog_id = $_POST['blogid'];

					$this->db->where("blogid",$_POST['blogid']);
					
					$lb_Arr=array();	
					$lb_Arr['title']     		= stripslashes(trim($this->input->post('title')));	 
					$lb_Arr['url']     			= stripslashes(trim($this->input->post('url')));	 
					$lb_Arr['blogcontent']     	= stripslashes(trim($this->input->post('blogcontent')));
					$lb_Arr['status']   		= addslashes(trim($this->input->post('status')));
					$lb_Arr['pagetitle']   		= addslashes(trim($this->input->post('page_title')));
					$lb_Arr['metadescription']  = addslashes(trim($this->input->post('meta_description')));	  
					$lb_Arr['meta_keyword']     = addslashes(trim($this->input->post('meta_keyword')));	  
					 
					// UPDATE QUERY
					$this->db->update(BLOG,$lb_Arr);
					$this->session->set_flashdata('success', 'Blog edited successfully');

				} else if ( trim ($_POST['mode']) == 'add') { 	

						if($this->db_function->get_data("SELECT blogid, title  FROM ".BLOG." WHERE title = '".trim($this->input->post('title'))."'")) {
						   
							   $this->session->set_flashdata('error', 'Blog Alredy existed, ');              
							   redirect(FULL_CMS_URL."/".$_POST['add_page']);

						}else{
						
							$lb_Arr=array();

							$lb_Arr['blogid']        	= "";
							$lb_Arr['title']     	 	= stripslashes(trim($this->input->post('title')));	
							$lb_Arr['url']     	     	= stripslashes(trim($this->input->post('url')));	 
							$lb_Arr['blogcontent']  	= stripslashes(trim($this->input->post('blogcontent')));			 
							$lb_Arr['status']   	 	= addslashes(trim($this->input->post('status')));
							$lb_Arr['pagetitle']   		= addslashes(trim($this->input->post('page_title')));
							$lb_Arr['meta_keyword']     = addslashes(trim($this->input->post('meta_keyword')));	  
							$lb_Arr['metadescription']  = addslashes(trim($this->input->post('meta_description')));
							$lb_Arr['date_created']     = date('Y-m-d H:i:s');

							$this->db->insert(BLOG,$lb_Arr);
							$blog_id = $this->db->insert_id();
							$this->session->set_flashdata('success', 'Blog Added successfully');

						}
				}


				if (isset ($_FILES['blog_image']['name'])) {
					
					$blog_img       = $_FILES['blog_image']['name'];
					$newimage    	= end(explode(".", $blog_img ));
					$newfilename 	= $_POST['url'];//preg_replace('/[^A-Za-z0-9\-]/', '', strtolower($this->input->post('title'))).".".$newimage;
					$lb_Arr['blog_image']   = strtolower($newfilename);			   
					$pathAndName 	= BLOG_IMAGE_FOLDER.strtolower($newfilename);
					$croppathAndName= BLOG_CROP_FOLDER.THUMB_PREFIX.$newfilename;

					$path 			= $_FILES['blog_image']['name'];
					$image_ext      = pathinfo($path, PATHINFO_EXTENSION);
					$allowed_types  = array('jpg', 'gif', 'png' , 'bmp' , 'jpeg');
					
					if($image_ext != false) {
						if($_FILES['blog_image']['size'] > IMAGE_UPLOAD_MAX_SIZE) {
							// IF MORE THAN GENERATE FLASH MESSAGE
							$this->session->set_flashdata('error', 'File size must be less than or equals to '.
																	(IMAGE_UPLOAD_MAX_SIZE/1024/1024).' MB');
						} else {
							// CHECK IMAGE WE ARE TRYING TO UPLOAD THAT DIRECTORY IS AVAILABLE
							if (is_dir("../".BLOG_IMAGE_FOLDER) == false) {
								
								// DELETE THE CURRENT IMAGE FILE FROM HDD
								$prod_image = $this->db_function->get_single_value(BLOG, 'blog_image', "blogid=".$blog_id, false, false);
								
								move_uploaded_file($_FILES['blog_image']['tmp_name'],$pathAndName);
					  		 	make_img_thumb("./".$pathAndName, "./".$croppathAndName, 700);
								
								// DELETE IMAGE FROM HDD
								//unlink("./".BLOG_IMAGE_FOLDER.$prod_image);
								//unlink("./".BLOG_CROP_FOLDER.THUMB_PREFIX.$prod_image);
								
								$p_image = NULL;
								$p_image['blog_image'] = $newfilename;

								// SET THE WHERE CLAUSE
								$this->db->where("blogid", $blog_id);
								$this->db->update(BLOG, $p_image);
							}
						}
					}
				}

				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				
				
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
