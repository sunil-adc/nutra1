<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class deleteCache extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_title']	 		= "Cache Management";
		$data['manage_page_title']	= "Cache Management";
		$data['page_name']	 		= "Cache Management";
		$data['manage_page'] 		= strtolower(__CLASS__);
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/del_cache";
		
		// INITIALIZE ARRAY VARIABLES
		$data['cache_size'] = byteFormat(calculate_dir_size($this->db->cachedir));
		$data['dir_size'] = _get_size_wise_directory($this->db->cachedir);
			
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/delete-cache',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	
	}
	
	public function del_cache() {
		// DELETE ALL CACHE FILES
		$this->db->cache_delete_all();
		
		$this->session->set_flashdata('success', 'All cache has been cleared');
		redirect(FULL_CMS_URL."/".strtolower(__CLASS__));
	}
	
	public function del_specific_cache($dir = "") {
		
		if($dir != "") {
			// EXPLODE STRING TO GET THE DIRECTORY NAME
			$arr_dir = explode('--', $dir);
			
			//DELETE N LEVEL FILE WHICH IS INSIDE DIRECTORY
			delete_n_level_file($this->db->cachedir.$arr_dir[0].'+'.$arr_dir[1]);
			
			// REMOVE DIRECTORY AT LAST
			rmdir($this->db->cachedir.$arr_dir[0].'+'.$arr_dir[1]);
			
			// SET FLASH MESSAGE FOR DELETION
			$this->session->set_flashdata('success', '<strong>'.$arr_dir[0].'+'.$arr_dir[1].'</strong> cache has been cleared');
		}
		// REDIRECTE TO CACHE MANAGEMENT PAGE
		redirect(FULL_CMS_URL."/".strtolower(__CLASS__));
		exit;
	}
}
