<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cron_net_pub extends CI_Controller {
	
	protected $final_responce;
	
	function __construct(){
		parent::__construct();
		$this->load->helper('email');
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		$this->load->model(array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));				
	}
	
	public function index() {
		
		$handle_type = '2';
		$final_responce = '<table width="90%" cellpadding="10" cellspacing="0" style="text-align:center;font-family:Verdana, Geneva, sans-serif;border:1px solid #66CC99">
			  <tr style="background:#66CC99;color:#FFF;">
				<td style="padding:10px 10px 10px 10px"><strong>NET/PUB</strong></td>
				<td style="padding:10px 10px 10px 10px"><strong>GOAL</strong></td>
				<td style="padding:10px 10px 10px 10px"><strong>CPS</strong></td>
				<td style="padding:10px 10px 10px 10px"><strong>CURRENT PINCODE</strong></td>
				<td style="padding:10px 10px 10px 10px"><strong>UPDATED PINCODE</strong></td>
			  </tr>';
		
		// CACHE OFF FOR NETWISE AND PUBWISE AUTO PIXEL CHANGE - START
		$this->replica_db->cache_off();
		$this->db->cache_off();
		// CACHE OFF FOR NETWISE AND PUBWISE AUTO PIXEL CHANGE - END
		
		$sel_query = $this->replica_db->query("select id, net, handle_type, cpl, pincode_type, goal, status from ".PIXELS." where handle_type = '".$handle_type."' and status = '1'");
		if ($sel_query->num_rows() > 0) {
			foreach ($sel_query->result() as $row) {
				// COUNT THE LEADS FIRED
				$lead_fired = "select COUNT(1) AS lead_fired from ".ORDER." o where o.fire_status = 1 and lower(o.net) = '".$row->net."'";		
				$data['lead_fired'] = $this->db_function->count_record_using_count($lead_fired, 'lead_fired',  false, true, $this->replica_db, true);
				
				// COUNT THE LEADS FIRED
				$sale = "select COUNT(1) AS sale from ".ORDER." o where o.payment_status = 1 and lower(o.net) = '".$row->net."'";					
				$data['sale'] = $this->db_function->count_record_using_count($sale, 'sale',  false, true, $this->replica_db, true);
				
				//COUNT SPENT
				$data['spent']	 = $data['lead_fired'] * $row->cpl;
				
				if ($data['sale'] > 0) {
					$data['cps'] = $data['spent'] / $data['sale'];
				} else {
					$data['cps'] = 0;
				}
				// GENERATE NETWISE REPORT
				$net_report = $this->common_model->net_pub_pixel_plus_minus(PIXELS,$data['sale'],$data['cps'],$row->goal,$row->id,$row->pincode_type,$row->net, '<strong>(NET)</strong>');
				$final_responce .= $net_report;
			}
		} else {
			$final_responce .= '<tr><td style="border-bottom:1px solid #66CC99" colspan="5">No Automatic Networks detected</td>';			
		}
		
		
		// GET ALL PUB REPORT
		$sel_pub_query = $this->replica_db->query("select id, pubs, handle_type, cpl, pincode_type, goal, net_id, status from ".PUBS." where handle_type = '".$handle_type."' and status = '1'");
		
		if ($sel_pub_query->num_rows() > 0) {
			foreach ($sel_pub_query->result() as $row_pubs) {
				// COUNT THE LEADS FIRED
				$pubs_lead_fired = "select COUNT(1) AS lead_fired from ".ORDER." o where o.fire_status = 1 and lower(o.pubid) = '".$row_pubs->pubs."'";		
				$data['pub_lead_fired'] = $this->db_function->count_record_using_count($pubs_lead_fired, 'lead_fired',  false, true, $this->replica_db, true);
				
				// COUNT THE LEADS FIRED
				$pubs_sale = "select COUNT(1) AS sale from ".ORDER." o where o.payment_status = 1 and lower(o.pubid) = '".$row_pubs->pubs."'";					
				$data['pub_sale'] = $this->db_function->count_record_using_count($pubs_sale, 'sale',  false, true, $this->replica_db, true);
				
				//COUNT SPENT
				$data['pubs_spent']	 = $data['pub_lead_fired'] * $row_pubs->cpl;
				
				if ($data['pub_sale'] > 0) {
					$data['pubs_cps'] = $data['pubs_spent'] / $data['pub_sale'];
				} else {
					$data['pubs_cps'] = 0;
				}
				
				// GENERATE NETWISE REPORT
				$pub_report = $this->common_model->net_pub_pixel_plus_minus(PUBS, $data['pub_sale'], $data['pubs_cps'], $row_pubs->goal, $row_pubs->id, $row_pubs->pincode_type, $row_pubs->pubs, '<strong>(PUB)</strong>');
				$final_responce .= $pub_report;
			}
		} else {
			$final_responce .= '<tr><td style="border-bottom:1px solid #66CC99" colspan="5">No Automatic Publishers detected</td>';			
		}
		
		$this->email->from('developer@seajintech.com', SITE_NAME.' Developers');
		$this->email->to('jaydip@seajintech.com');
		//$this->email->cc('anil@seajintech.com');
		$this->email->subject('Automatic Pincode Handling is working fine !!');
		$this->email->message('<div style="font-size:14px">'.$final_responce.'</div>');
		$this->email->send();
		exit;
	}
}