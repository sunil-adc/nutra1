<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addCpsNetworks extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= PIXELS;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "CPS Networks";
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page'] 		= "managecpsnetworks";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['result_data'][$data['primary_field']]	= NULL;
		$data['result_data']['net'] 					= NULL;
		$data['result_data']['pixel']					= NULL;
		$data['result_data']['handle_type']				= NULL;
		$data['result_data']['cpl']						= NULL;
		$data['result_data']['pincode_type']			= NULL;
		$data['result_data']['goal']					= NULL;
		$data['result_data']['status']					= NULL;
		$data['result_data']['datecreated']				= NULL;
		$data['result_data']['dateupdated']				= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	id, 			category_cps, 
																	net,			pixel, 
																	handle_type,	cpl, 
																	pincode_type, 	goal,			
																	category_pixels,status, 
																	datecreated,	dateupdated', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;	
		}
		
		$data['pixel_data'] = $this->db->query("select id from ".PARENT_PINCODE." where status = '1'");
		$data['categories'] = $this->db->query("select cat_id, name from ".CATEGORY." where status = '1' and parent_cat_id = 0");
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-cps-networks',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		if(count($_POST['category']) > 0) {
			foreach ($_POST['category'] as $k => $v) {
				if(!(is_numeric($v) && $v >= 0 && $v <= 100)) {
					$this->session->set_flashdata('error', 'Category CPS value must be numeric 0 to 100.');
					redirect($_POST['cur_url']);
					exit;		
				}
			}
		}
		
		
		$this->form_validation->set_rules('net', 'Network', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				$_POST['mode'] = strtolower($_POST['mode']);
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where("id",$_POST['id']);
					// SET THE DATA
					$data = array(
									'net' => $_POST['net'],
									'is_cpl_cps' => '2',
									'category_cps' => serialize($_POST['category']),
									'category_pixels' => serialize($_POST['category_pixels']),
									'status' => $_POST['status'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(PIXELS,$data);
					$this->session->set_flashdata('success', 'Network edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'net' => $_POST['net'],
									'is_cpl_cps' => '2',
									'category_cps' => serialize($_POST['category']),
									'category_pixels' => serialize($_POST['category_pixels']),
									'status' => $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(PIXELS,$data);
					$this->session->set_flashdata('success', 'Network added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, 'addpubs');
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again');
			redirect($_POST['cur_url']);	
		}
	}
}
