<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addProducts extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library(array('form_validation', 'S3'));
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
		
			
	}
	
	public function index ( $mode = "add", $prod_id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= PRODUCT;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Products";
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['manage_page'] 		= "manageproducts";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "prod_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] = NULL;
		$data['verifiedby'] = NULL;
		
		if ($mode == 'edit' && is_numeric($prod_id)) {
			$val = $this->db_function->get_single_row($tablename, 'search_keywords,
seourl,  	prod_id,			cat_id, 			sub_cat_id,		 brand, 		name, 					mrp, 
cod_price, 	sale_price, 		shipping, 		 	discount, 		 description,	small_description, 		verified_by,
model_no, 	warranty_msg, 		is_size, 			dateupdated, 	 contest, 		contest_description, 	priority, 	
title, 		meta_description,	meta_keywords, 		upsell, 		 free_prod, 	cash_back_amt, 		 	video_url, 
oos, 		clr_id, 			color_list, 	 	rating_avg,		 rating_total,	total_stock,			is_verified, 
is_combo,	combo_product,  	status,				datecreated, 	 cod_availability,	check_inventory ', $data['primary_field'].'='.$prod_id);
			
			$data['result_data'] = $val;
			
			if ($data['result_data']['verified_by'] > 0) {
				$data['verified_details'] = $this->db_function->get_single_row(ADMIN, "email, fname, lname", "id=".$data['result_data']['verified_by'],false,true);
			}
			
			// IF YOU ARE WORKING ON CACHING AND GET THE CACHE PRODUCT THAN CHECK AND REDIRECT AND CLEAR THE CACHE
			if (!is_array($data['result_data']) || count($data['result_data']) < 2){
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $data['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $data['add_page']);
				// DELETE THE CACHE FOR QUICK SEARCH
				$this->db->cache_delete(CMS_FOLDER_NAME, 'ajaxproductname');
				$this->session->set_flashdata('error', 'Out automated system detected that you are working on cache and that page was not available anymore so page is redirected automatically.');
				redirect(FULL_CMS_URL."/".$data['manage_page']."/#mtab");
				exit;
			}
			
			$prod_image = $this->db->query('select 
													prod_image_id, 
													prod_id,
													image, 
													status,
													isdefault, 
													datecreated, 
													dateupdated 
											from 
													'.PRODUCT_IMAGE.' 
											where 
													prod_id='.$prod_id);
			$data['prod_image'] = $prod_image;
		}
		
		// LOAD THE CATEGORY
		$cat_where['parent_cat_id'] = '0';
		$data['categories'] = $this->common_model->create_combo("cat_id", "cat_id", CATEGORY, $cat_where, "name", "cat_id", $data['result_data']['cat_id'], "javascript:get_subcat('".FULL_CMS_URL."',this.value,'','ajax_subcat');",'1', "", false, true);
		
		// SELECT FREE PRODUCTS
		$prod_where['prod_id != '] = ($data['result_data']['prod_id'] > 0) ? $data['result_data']['prod_id'] : '0';
		$prod_where['status'] = '1';
		$data['free_prod'] = $this->common_model->create_combo("free_prod", "free_prod", PRODUCT, $prod_where, "name", "prod_id", "", "javascript:void(0);",'11', "", true);
		
		$arr_product = $this->db->query('select prod_id, name from '.PRODUCT." where status = 1 and oos != 1 and contest = 0");
		foreach ($arr_product->result() as $val) {
			$data['arr_product'][] = $val;
		}
		
		// SELECT ALL BRAND
		$brand_where['status'] = '1';
		$data['brands'] = $this->common_model->create_combo("brand", "brand", BRANDS, $brand_where, "name", "brand_id", $data['result_data']['brand'], "javascript:void(0);", "3");		
		
		// SELECT ALL color
		$color_where['status'] = '1';
		$data['color'] = $this->common_model->create_combo("clr_id", "clr_id", COLORS, $color_where, "color_name", "color_id", $data['result_data']['clr_id'], "javascript:void(0);", "3");
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		//$this->load->view(CMS_FOLDER_NAME.'/ckeditor');
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-products',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
					
		$this->form_validation->set_rules('cat_id', 'Category Name', 'required|numeric');
		$this->form_validation->set_rules('name', 'Product Name', 'required');
		$this->form_validation->set_rules('seourl', 'SEO URL', 'required');
		$this->form_validation->set_rules('cod_availability','Select COD Options','required');
		$this->form_validation->set_rules('search_keywords','Search Keywords','required');
		
		if( trim($_POST['cod_availability'])  == 1){
			$this->form_validation->set_rules('cod_price','COD Price','required|numeric');
		}
		
		$this->form_validation->set_rules('status', 'Status', 'required|numeric');
		$this->form_validation->set_rules('oos', 'Out of Stock', 'required|numeric');
		$this->form_validation->set_rules('is_size', 'Size Availability', 'required|numeric');
		$this->form_validation->set_rules('mrp', 'MRP', 'required|numeric');
		$this->form_validation->set_rules('title', 'Page Title', 'required');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'required');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'required');
		$this->form_validation->set_rules('contest', 'Contest On this product', 'required');
		
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {	
				// GET PRODUCT ID
				$prod_id_img = (isset($_POST['prod_id']) && is_numeric($_POST['prod_id'])) ? $_POST['prod_id'] : NULL;
				
				if( trim(strtolower($_POST['mode'])) == 'edit' && is_numeric ($_POST['prod_id'])) { 
					
					$_POST['color_list'][] = $_POST['prod_id'];
					
					foreach ($_POST['color_list'] as $key_color_list => $val_color_list) {
						$color_mapped = '';
						foreach ($_POST['color_list'] as $key1_color_list => $val1_color_list) {
							if ($val1_color_list != $val_color_list) {
								$color_mapped .= $val1_color_list.",";
							}
						}
						$color_mapped = trim($color_mapped, ",");
						$this->db->query('update '.PRODUCT.' set color_list = "'.$color_mapped.'" where prod_id = '.$val_color_list);
					}
					
					// SET THE WHERE CLAUSE
					$this->db->where('prod_id',$_POST['prod_id']);
					// SET THE DATA
					$data = array(
									'cat_id' 				=> $_POST['cat_id'],
									'search_keywords' 		=> $_POST['search_keywords'],
									'sub_cat_id' 			=> $_POST['sub_cat_id'],
									'brand' 				=> $_POST['brand'],
									'name' 					=> $_POST['name'],
									'seourl' 				=> $_POST['seourl'],
									'model_no' 				=> $_POST['model_no'],
									'warranty_msg' 			=> $_POST['warranty_msg'],
									'priority' 				=> $_POST['priority'],
									'video_url' 			=> $_POST['video_url'],
									'free_prod' 			=> $_POST['free_prod'],
									'small_description' 	=> $_POST['small_description'],
									'is_size' 				=> $_POST['is_size'],
									'clr_id' 				=> $_POST['clr_id'],
									'mrp' 					=> $_POST['mrp'],
									'discount' 				=> $_POST['discount'],
									'cod_availability' 		=> $_POST['cod_availability'],
									'cash_back_amt' 		=> $_POST['cash_back_amt'],
									'cod_price' 			=> $_POST['cod_price'],
									'sale_price' 			=> $_POST['sale_price'],
									'shipping' 				=> $_POST['shipping'],
									'title' 				=> $_POST['title'],
									'meta_description' 		=> $_POST['meta_description'],
									'meta_keywords' 		=> $_POST['meta_keywords'],
									'contest' 				=> $_POST['contest'],
									'oos' 					=> $_POST['oos'],
									'contest_description' 	=> $_POST['contest_description'],
									'description' 			=> $_POST['description'],
									'status' 				=> $_POST['status'],
									'total_stock'			=> isset($_POST['total_stock']) ? $_POST['total_stock'] : 0,
									'check_inventory' 		=> (isset($_POST['contest']) && $_POST['contest'] == '1') ? 0 : $_POST['check_inventory'],
									'is_verified' 			=> 0,
									'dateupdated' 			=> date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(PRODUCT,$data);
					
					// ADD TO LOG TABLE
					$this->common_model->product_logs($_POST['prod_id'], $this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Edited');
					
					// UPDATE PRODUCT GENRE - START
					if(isset($_POST['genre']) && is_array($_POST['genre'])){
						$total_qty = 0;
						$genre_data = array();
						// FIRST CHECK FOR OLD DATA IN PRODUCT GENRE RELATED TO THIS PRODUCT
						$genredata = $this->db->query("select prod_genreid, genreid, genre_quantity from ".PRODUCT_GENRE." where prodid = ".$prod_id_img);
						
						if($genredata->num_rows() > 0) {
							
							foreach ($genredata->result() as $gv) {
								$arr_generedata[] = $gv->genreid;
								if (!in_array($gv->genreid, $_POST['genre'])) {
									$this->db->where('prod_genreid', $gv->prod_genreid);
									$this->db->delete(PRODUCT_GENRE);
								} else if (in_array($gv->genreid, $_POST['genre'])) {
									$del_key = array_search($gv->genreid, $_POST['genre']);
									if (trim($_POST['genre_size'][$del_key]) == '' || trim($_POST['genre_size'][$del_key]) == 0 ||
										trim($_POST['genre_size'][$del_key]) != '') {
										$this->db->where('prod_genreid', $gv->prod_genreid);
										$this->db->delete(PRODUCT_GENRE); 
									}
									
								}
							}
							
							foreach ($_POST['genre'] as $gk => $gv) {
								// IF GENERE ID IS NOT AVAILABLE IN DATABASE THEN INSERT 
								if (!in_array($gv, $arr_generedata)) {
									$this->db->insert(PRODUCT_GENRE, array('prodid' => $prod_id_img, 'genreid' => $gv, "genre_quantity" => ($_POST['genre_size'][$gk] > 0 && strlen($_POST['genre_size'][$gk]) > 0 && is_numeric($_POST['genre_size'][$gk])) ? $_POST['genre_size'][$gk] : 0));
								} else if (in_array($gv, $arr_generedata)) {
									$del_key = array_search($gv, $_POST['genre']);
									if (trim($_POST['genre_size'][$del_key]) == 0 || trim($_POST['genre_size'][$del_key]) != '') { 
										$this->db->insert(PRODUCT_GENRE, array('prodid' => $prod_id_img, 'genreid' => $gv, "genre_quantity" => ($_POST['genre_size'][$gk] > 0 && strlen($_POST['genre_size'][$gk]) > 0 && is_numeric($_POST['genre_size'][$gk])) ? $_POST['genre_size'][$gk] : 0));
									}
								}
							}
								
						} else {
						
							// ELSE ENTER NEW DATA IN PRODUCT GENRE
							foreach ($_POST['genre'] as $gk => $gv) {
								$genre_data[] = array("prodid" => $prod_id_img, "genreid" => $gv, "genre_quantity" => ($_POST['genre_size'][$gk] > 0 && strlen($_POST['genre_size'][$gk]) > 0 && is_numeric($_POST['genre_size'][$gk])) ? $_POST['genre_size'][$gk] : 0);
							}
							$this->db->insert_batch(PRODUCT_GENRE, $genre_data);
						}
					} else {
						// DELETE ALL GENRE FROM PRODUCT IF ITS NOT SET
						$this->db->where('prodid', $prod_id_img);
						$this->db->delete(PRODUCT_GENRE);
					}
					
					$this->session->set_flashdata('success', 'Product edited successfully');
					
				} else if ( trim (strtolower($_POST['mode'])) == 'add') { 			
					
					// SET THE DATA FOR INSERTION
					$data = array(
									'cat_id' 				=> $_POST['cat_id'],
									'search_keywords' 		=> $_POST['search_keywords'],
									'sub_cat_id' 			=> $_POST['sub_cat_id'],
									'brand' 				=> $_POST['brand'],
									'name' 					=> $_POST['name'],
									'seourl' 				=> $_POST['seourl'],
									'model_no' 				=> $_POST['model_no'],
									'warranty_msg' 			=> $_POST['warranty_msg'],
									'priority' 				=> $_POST['priority'],
									'video_url' 			=> $_POST['video_url'],
									'free_prod' 			=> $_POST['free_prod'],
									'small_description' 	=> $_POST['small_description'],
									'is_size' 				=> $_POST['is_size'],
									'clr_id' 				=> $_POST['clr_id'],
									'mrp' 					=> $_POST['mrp'],
									'discount' 				=> $_POST['discount'],
									'cod_availability' 		=> $_POST['cod_availability'],
									'cash_back_amt' 		=> $_POST['cash_back_amt'],
									'cod_price' 			=> $_POST['cod_price'],
									'sale_price' 			=> $_POST['sale_price'],
									'shipping' 				=> $_POST['shipping'],
									'title' 				=> $_POST['title'],
									'meta_description' 		=> $_POST['meta_description'],
									'meta_keywords' 		=> $_POST['meta_keywords'],
									'contest' 				=> $_POST['contest'],
									'total_stock'			=> isset($_POST['total_stock']) ? $_POST['total_stock'] : 0,
									'check_inventory' 		=> (isset($_POST['contest']) && $_POST['contest'] == '1') ? 0 : $_POST['check_inventory'],
									'oos' 					=> $_POST['oos'],
									'contest_description' 	=> $_POST['contest_description'],
									'description' 			=> $_POST['description'],
									'status' 				=> $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(PRODUCT,$data);
					$prod_id_img = $this->db->insert_id();
					
					$_POST['color_list'][] = $prod_id_img;
					
					foreach ($_POST['color_list'] as $key_color_list => $val_color_list) {
						$color_mapped = '';
						foreach ($_POST['color_list'] as $key1_color_list => $val1_color_list) {
							if ($val1_color_list != $val_color_list) {
								$color_mapped .= $val1_color_list.",";
							}
						}
						$color_mapped = trim($color_mapped, ",");
						$this->db->query('update '.PRODUCT.' set color_list = "'.$color_mapped.'" where prod_id = '.$val_color_list);
					}
					
					// ADD TO LOG TABLE
					$this->common_model->product_logs($prod_id_img, $this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Newly Added');
					
					// INSERT PRODUCT GENRE - START
					if(isset($_POST['genre']) && is_array($_POST['genre'])){
						$genre_data = array();
						foreach ($_POST['genre'] as $gk => $gv) {
							$genre_data[] = array("prodid" => $prod_id_img, "genreid" => $gv, "genre_quantity" => ($_POST['genre_size'][$gk] > 0 && strlen($_POST['genre_size'][$gk]) > 0 && is_numeric($_POST['genre_size'][$gk])) ? $_POST['genre_size'][$gk] : 0);
						}
						$this->db->insert_batch(PRODUCT_GENRE, $genre_data);
					}
					
					// SET FLASH MESSAGE
					$this->session->set_flashdata('success', 'Product added successfully');
				}
				
				
					
				if ((trim (strtolower($_POST['mode'])) == 'edit' && is_numeric ($_POST['prod_id'])) || trim(strtolower($_POST['mode'])) == 'add') {
					/* IMAGE UPLOAD CODE START */
					// CHECK FOR IMAGE NAME ARRAY SET OR NOT
					$new_img_name = NULL;
					if (is_array($_FILES['image']['name'])) {
						
						
						// SET AND INITIAL S3 CLASS AND FOR BUCKET
						$s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
						$s3->putBucket(S3_BUCKET, S3::ACL_PUBLIC_READ);
						
						// ACCESS IMAGE ONE BY ONE VIA ARRAY 
						for ($img = 0; $img < count($_FILES['image']['name']); $img++) {
							// CHECK IMAGE IS AVAILABLE OR NOT
							if (trim($_FILES['image']['name'][$img]) != "") {
								// CHECK CURRENT IMAGE EXTENSION IS VALID FOR UPLOAD
								$image_ext = allowed_extensions($_FILES['image']['name'][$img]);
								if($image_ext != false) {
									// CHECK IMAGE SIZE IS LESS OR EQUAL AS DEFINED
									if($_FILES['image']['size'][$img] > IMAGE_UPLOAD_MAX_SIZE) {
										// IF MORE THAN GENERATE FLASH MESSAGE
										$this->session->set_flashdata('error', 'File size must be less than or equals to '.
																				(IMAGE_UPLOAD_MAX_SIZE/1024/1024).' MB');
									} else {
									   // CHECK IMAGE WE ARE TRYING TO UPLOAD THAT DIRECTORY IS AVAILABLE
									   if (is_dir("../".PRODUCT_DIR_PATH) == false) {
										 // FINALLY UPLOAD THE FILE AFTER CHECKING OF EVERYTHING 
										 $new_img_name = get_rand_chracter('12','3').".".$image_ext;
											
										 move_uploaded_file($_FILES['image']['tmp_name'][$img],"./".PRODUCT_DIR_PATH.$new_img_name);		
											
										 //CREATE THUMB FILE HERE 
										 make_img_thumb("./".PRODUCT_DIR_PATH.$new_img_name, "./".PRODUCT_THUMB_120_150_PATH.$new_img_name, 120);
										 make_img_thumb("./".PRODUCT_DIR_PATH.$new_img_name, "./".PRODUCT_THUMB_200_300_PATH.$new_img_name, 250);
										 make_img_thumb("./".PRODUCT_DIR_PATH.$new_img_name, "./".PRODUCT_THUMB_350_450_PATH.$new_img_name, 350);
											
		// COPY IMAGE TO AMAZON S3 SERVER
		$s3->putObjectFile("./".PRODUCT_DIR_PATH.$new_img_name, S3_BUCKET, PRODUCT_DIR_PATH.$new_img_name, S3::ACL_PUBLIC_READ);
		$s3->putObjectFile("./".PRODUCT_THUMB_120_150_PATH.$new_img_name, S3_BUCKET, PRODUCT_THUMB_120_150_PATH.$new_img_name, S3::ACL_PUBLIC_READ);
		$s3->putObjectFile("./".PRODUCT_THUMB_200_300_PATH.$new_img_name, S3_BUCKET, PRODUCT_THUMB_200_300_PATH.$new_img_name, S3::ACL_PUBLIC_READ);
		$s3->putObjectFile("./".PRODUCT_THUMB_350_450_PATH.$new_img_name, S3_BUCKET, PRODUCT_THUMB_350_450_PATH.$new_img_name, S3::ACL_PUBLIC_READ);
										 
										 // DELETE THE CURRENT IMAGE FILE FROM HDD
										unlink("./".PRODUCT_DIR_PATH.$new_img_name);
										unlink("./".PRODUCT_THUMB_120_150_PATH.$new_img_name);
										unlink("./".PRODUCT_THUMB_200_300_PATH.$new_img_name);
										unlink("./".PRODUCT_THUMB_350_450_PATH.$new_img_name);
										 
										 $prod_image = NULL;
										 
										 if($img == 0 && trim (strtolower($_POST['mode'])) == 'add') {
										 	$prod_image['isdefault'] = 1;	
										 }
										 $prod_image['prod_id'] = $prod_id_img;
										 $prod_image['image'] = $new_img_name;
										 $prod_image['status'] = 1;
										 $this->db->insert(PRODUCT_IMAGE,$prod_image);
										 
										 // ADD TO LOG TABLE
										 $this->common_model->product_logs($prod_id_img, $this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'New Image Added');
					
									   } else {
											// IF DIRECTORY NOT AVAILABLE THAN GENERATE THE MESSAGE
											$this->session->set_flashdata('error', 'Directory not available, try again later');
									   }
									}							
								}
							}
						}
					
					}
					/* IMAGE UPLOAD CODE END */
				}
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				// DELETE THE CACHE FOR QUICK SEARCH
				$this->db->cache_delete(CMS_FOLDER_NAME, 'ajaxproductname');
				redirect(FULL_CMS_URL."/".$_POST['manage_page']."/#mtab");
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
	
	function delete_prod_img($prod_id, $img_id, $image_name, $method_name, $mode, $id){
		if($prod_id != "" && $img_id != "" && $image_name != "" && $method_name != "" && $mode != "" && $id != "") {
			$tablename = PRODUCT_IMAGE;
			
			// SET AND INITIAL S3 CLASS AND FOR BUCKET
			$s3 = new S3(AWS_ACCESS_KEY, AWS_SECRET_KEY);
			$s3->putBucket(S3_BUCKET, S3::ACL_PUBLIC_READ);
			
			// CHECK FOR IMAGE NAME
			$image_file_name = $this->db_function->get_single_value($tablename, 'image', 'prod_image_id = '.$img_id.' and 
																						  prod_id = '.$prod_id, false, true);
			if($image_file_name != false) {
				
				// DELETE OLD IMAGE FROM S3 BUCKET
				$s3->deleteObject(S3_BUCKET, PRODUCT_DIR_PATH.$image_file_name);
				$s3->deleteObject(S3_BUCKET, PRODUCT_THUMB_120_150_PATH.$image_file_name);
				$s3->deleteObject(S3_BUCKET, PRODUCT_THUMB_200_300_PATH.$image_file_name);
				$s3->deleteObject(S3_BUCKET, PRODUCT_THUMB_350_450_PATH.$image_file_name);
								
				// DELETE ENTRY FROM DATABASE
				$this->db->where('prod_image_id', $img_id);
				$this->db->where('prod_id', $prod_id);
				$this->db->delete($tablename);
				
				// UPDATE PRODUCT DATEUPDATED
				$data = array('dateupdated' => date('Y-m-d H:i:s'));
				$this->db->update(PRODUCT,$data);
				
				// ADD TO LOG TABLE
				$this->common_model->product_logs($prod_id, $this->session->userdata('uid'), $this->session->userdata('admin_role_id'), 'Image Deleted');
			
			
				// DELETE CACHE FILE FOR PRODUCTS
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				
				// SET FLASH MESSAGE FOR PRODUCT IMAGE
				$this->session->set_flashdata('success', 'Product image removed from the list');	
			} else {
				
				// DELETE OLD IMAGE FROM S3 BUCKET
				$s3->deleteObject(S3_BUCKET, PRODUCT_DIR_PATH.$image_name);
				$s3->deleteObject(S3_BUCKET, PRODUCT_THUMB_120_150_PATH.$image_name);
				$s3->deleteObject(S3_BUCKET, PRODUCT_THUMB_200_300_PATH.$image_name);
				$s3->deleteObject(S3_BUCKET, PRODUCT_THUMB_350_450_PATH.$image_name);
				
				// DELETE CACHE FILE FOR PRODUCTS
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, 'ajaxproductname');
				$this->session->set_flashdata('error', 'Database entry was not found so image was cleared for that.');
			}
		}
		redirect(FULL_CMS_URL."/".strtolower(__CLASS__).'/'.strtolower($method_name).'/'.strtolower($mode).'/'.$id."/#product_image_box");
		exit;
	}
}
