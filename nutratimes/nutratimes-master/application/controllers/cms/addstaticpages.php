<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addStaticPages extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= STATIC_PAGE;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Static Page";
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['manage_page'] 		= "managestaticpages";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "page_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] 					= NULL;
		$data['result_data']['page_id']			= NULL;
		$data['result_data']['page_title'] 		= NULL;
		$data['result_data']['page_name']		= NULL;
		$data['result_data']['page_description']= NULL;
		$data['result_data']['meta_title']		= NULL;
		$data['result_data']['meta_keywords']	= NULL;
		$data['result_data']['meta_description']= NULL;
		$data['result_data']['page_name']		= NULL;
		$data['result_data']['status']			= NULL;
		$data['result_data']['datecreated']		= NULL;
		$data['result_data']['dateupdated']		= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	page_id,  
																	page_title,
																	page_name,
																	page_description,
																	meta_title,
																	meta_keywords,
																	meta_description,
																	status, 
																	datecreated, 
																	dateupdated ', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		/*$this->load->view(CMS_FOLDER_NAME.'/ckeditor');*/
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-static-page',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('page_title', 'Name', 'required');
		$this->form_validation->set_rules('page_name', 'Page Name', 'required');
		$this->form_validation->set_rules('page_description', 'Page Descriptioname', 'required');
		$this->form_validation->set_rules('meta_title', 'Meta Title', 'required');
		$this->form_validation->set_rules('meta_keywords', 'Meta Keywords', 'required');
		$this->form_validation->set_rules('meta_description', 'Meta Description', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['page_id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where('page_id',$_POST['page_id']);
					// SET THE DATA
					$data = array(
									'page_title' => $_POST['page_title'],
									'page_name' => $_POST['page_name'],
									'page_description' => $_POST['page_description'],
									'meta_title' => $_POST['meta_title'],
									'meta_keywords' => $_POST['meta_keywords'],
									'meta_description' => $_POST['meta_description'],
									'status' => $_POST['status'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(STATIC_PAGE,$data);
					$this->session->set_flashdata('success', 'Static Page edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'page_title' => $_POST['page_title'],
									'page_name' => $_POST['page_name'],
									'page_description' => $_POST['page_description'],
									'meta_title' => $_POST['meta_title'],
									'meta_keywords' => $_POST['meta_keywords'],
									'meta_description' => $_POST['meta_description'],
									'status' => $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(STATIC_PAGE,$data);
					$this->session->set_flashdata('success', 'Static Page added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				redirect(FULL_CMS_URL."/".$_POST['manage_page']);
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *, remem ber page seourl must be unique for each page');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
