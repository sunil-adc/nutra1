<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
	}
	 
	public function index(){
		// UNSET ALL SESSION DATA FOR LOGOUT FUNCTION
		$this->memcache_driver->del_cache('cart_'.$_SESSION[SVAIZA_USER_ID]);
		$this->session->unset_userdata('svaiza_user_id');
		$this->session->unset_userdata('svaiza_name');
		$this->session->unset_userdata('svaiza_mobile');
		$this->session->unset_userdata('svaiza_email');
		$this->session->unset_userdata('svaiza_city');
		$this->session->unset_userdata('svaiza_state');
		$this->session->unset_userdata('svaiza_pincode');
		$this->session->unset_userdata(USER_ORDER_ID);
		
		$_SESSION[USER_ORDER_ID] = "";
		$_SESSION[SVAIZA_USER_ID] = '';
		$_SESSION[SVAIZA_MOBILE] = '';
		$_SESSION[SVAIZA_EMAIL] = '';
		
		redirect(SITE_URL);
	}
}
