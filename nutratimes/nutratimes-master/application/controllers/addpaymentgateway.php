<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addPaymentGateway extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->model( array(CMS_FOLDER_NAME.'/common_model', CMS_FOLDER_NAME.'/db_function'));
		$this->load->library('pagination');
		
		// CHECK ADMIN IS LOGIN - START
		is_Admin_Login($this->session->userdata('username'), $this->session->userdata('uid'), 
					   $this->session->userdata('admin_role'), $this->session->userdata('admin_role_id'), 
					   $this->session->userdata('admin_role_details'), __CLASS__);
		// CHECK ADMIN IS LOGIN - END
	}
	
	public function index ( $mode = "add", $id = NULL ) {
		$menu = $this->common_model->Menu_Array();
		
		// CORE DETAILS
		$cur_controller 	= strtolower(__CLASS__);
		$tablename 			= PAYMENT_GATEWAY;
		$full_path 			= FULL_CMS_URL."/".$cur_controller.'/'.__FUNCTION__.'/';
		
		// PAGE DETAILS
		$page_details['menu'] = $menu;
		$page_details['cur_controller'] = $cur_controller;
		
		// DETAILS WE WANT TO SEND IN VIEW
		$data['menu']	 			= $page_details['menu'];
		$data['cur_controller']		= $page_details['cur_controller'];
		$data['page_name']	 		= "Payment Gateway";
		$data['page_title']	 		= ucfirst($mode)." ".$data['page_name'];
		$data['manage_page_title']	= "Manage ".$data['page_name'];
		$data['manage_page'] 		= "managepaymentgateway";
		$data['add_page'] 			= strtolower(__CLASS__);
		$data['tablename']	 		= $tablename;
		$data['primary_field']		= "pg_id";
		$data['form_submit'] 		= FULL_CMS_URL."/".$cur_controller."/add_edit";
		$data['mode'] 				= ($mode == "add") ? ucfirst($mode) : 'Edit';
		
		
		// INITIALIZE ARRAY VARIABLES
		$data['result_data'] 						= NULL;
		$data['result_data']['pg_id']				= NULL;
		$data['result_data']['pg_name']				= NULL;
		$data['result_data']['status'] 				= NULL;
		$data['result_data']['product_priority']	= NULL;
		$data['result_data']['contest_priority']	= NULL;
		$data['result_data']['both_priority']		= NULL;
		$data['result_data']['name']				= NULL;
		$data['result_data']['status']				= NULL;
		$data['result_data']['datecreated']			= NULL;
		$data['result_data']['dateupdated']			= NULL;
		
		if ($mode == 'edit' && is_numeric($id)) {
			$val = $this->db_function->get_single_row($tablename, '	pg_id,  
																	pg_name,
																	product_priority,
																	contest_priority,
																	both_priority,
																	status,
																	datecreated,
																	dateupdated ', $data['primary_field'].'='.$id);
			$data['result_data'] = $val;
		}
		
		// HEADER CLASS
		$header['class']	=	strtolower(__CLASS__);
		
		// LOAD ALL REQUIRE VIEWS
		$this->load->view(CMS_FOLDER_NAME.'/header',$header);
		$this->load->view(CMS_FOLDER_NAME.'/ckeditor');
		$this->load->view(CMS_FOLDER_NAME.'/default-template',$page_details);
		$this->load->view(CMS_FOLDER_NAME.'/add-payment-gateway',$data);	 
		$this->load->view(CMS_FOLDER_NAME.'/footer');
	}
	
	function add_edit () {
		
		$this->form_validation->set_rules('pg_name', 'Payment Gateway Name', 'required');
		$this->form_validation->set_rules('product_priority', 'Product Priority', 'numeric');
		$this->form_validation->set_rules('contest_priority', 'Contest Priority', 'numeric');
		$this->form_validation->set_rules('both_priority', 'Both Priority', 'numeric');
		$this->form_validation->set_rules('status', 'Status', 'required');
		
		if( isset ($_POST['submit'])) {
			if ($this->form_validation->run() != false) {
				if( trim ($_POST['mode']) == 'edit' && is_numeric ($_POST['pg_id'])) { 		
					// SET THE WHERE CLAUSE
					$this->db->where('pg_id',$_POST['pg_id']);
					// SET THE DATA
					$data = array(
									'pg_name' => $_POST['pg_name'],
									'product_priority' => $_POST['product_priority'],
									'contest_priority' => $_POST['contest_priority'],
									'both_priority' => $_POST['both_priority'],
									'status' => $_POST['status'],
									'dateupdated' => date('Y-m-d H:i:s')
								);
					// UPDATE QUERY
					$this->db->update(PAYMENT_GATEWAY,$data);
					$this->session->set_flashdata('success', 'Payment Gateway edited successfully');
				} else if ( trim ($_POST['mode']) == 'add') { 			
					// SET THE DATA FOR INSERTION
					$data = array(
									'pg_name' => $_POST['pg_name'],
									'product_priority' => $_POST['product_priority'],
									'contest_priority' => $_POST['contest_priority'],
									'both_priority' => $_POST['both_priority'],
									'status' => $_POST['status'],
									'datecreated' => date('Y-m-d H:i:s')
								);
					// INSERT QUERY
					$this->db->insert(PAYMENT_GATEWAY,$data);
					$this->session->set_flashdata('success', 'Payment Gateway added successfully');
				}
				
				// DELETE CACHE AFTER ADD OR EDIT 
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['manage_page']);
				$this->db->cache_delete(CMS_FOLDER_NAME, $_POST['add_page']);
				redirect(FULL_CMS_URL."/".$_POST['manage_page']);
				exit;
			} else {
				$this->session->set_flashdata('error', 'Fields are required which denotes *.');
				redirect($_POST['cur_url']);
				exit;
			}
		} else {
			$this->session->set_flashdata('error', 'Something went wrong please try again later');
			redirect($_POST['cur_url']);	
		}
	}
}
