<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class orderhistorycod extends CI_Controller {
	protected $data = array();
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		$this->load->model('cart_driver');
		
		// USE THE REPLICA DATABASE FOR SEARCH ORDERS
		$this->replica_db = $this->load->database('replicadb', TRUE);
		
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		// CHECK USER IS LOGIN OR NOT
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL."login");
		}
	}
	
	public function index($is_ajax = 0) {
		$cur_controller 				= strtolower(__CLASS__);
		$this->data['cur_controller']	= $cur_controller;
		
		// FETCH CATEGORY
		$this->data['category'] = $this->general->get_nlevel_category($cur_controller);
		
		// FETCH ORDER HISTORY
		$this->replica_db->cache_off();
		$order_history = $this->replica_db->query("SELECT 
												o.payment_status,
												o.caller_status,
												up.order_id, 
												sum(up.quantity * up.cod_price) order_total,
												count(up.id) total_items,
												o.dt_c 
										   FROM 
												".ORDER." o inner join ".USER_PRODUCTS." up on(o.id = up.order_id) 
										   WHERE 
												o.user = '".$_SESSION[SVAIZA_USER_ID]."' and 
												o.payment_mode = 2 and 
												o.caller_status != 4 and 
												up.status != '0' and 
												o.delivery_status != '4'
										   GROUP BY 
												up.order_id");
		
		if($order_history->num_rows() > 0) {
			foreach ($order_history->result() as $row) {
				$this->data['order_history'][] = $row;
			}
		} else {
			$this->data['order_history'] = false;
		}
		
		// LOAD ALL VIEWS
		$this->load_views();
	}
	 
	function cancelcod($orderid = 0) {
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL."login");
		}
		
		if ($orderid > 0) {
			$res = $this->db->query("select payment_mode, caller_status from ".ORDER." where id = ".$orderid);
			if ($res->num_rows() > 0) {
				foreach ($res->result() as $val) {
					if ($val->payment_mode == 2 && $val->caller_status != 3 && $val->caller_status != 4) {
						if ($orderid > 0 && is_numeric($orderid)) {
							
							$this->db->query("update ".ORDER." set caller_status = 4 where id = ".$orderid);	
							
							// MINUS STOCK IF REQUIRE
							$this->cart_driver->product_stock_plus($orderid);
							
							// SEND CANCELLATION EMAIL TO USER
							$this->svaiza_email->cancel_order($orderid);
							
							// DELETE THE CACHE FOR THE ORDER HISTORY
							
							$this->db->cache_delete(__CLASS__, "index");
							
							// SET THE FLASH DATA
							$this->session->set_flashdata('success', 'Order cancelled successfully.');
							
							// REDIRECT
							redirect(SITE_URL.'orderhistorycod');
						} else {
							$this->session->set_flashdata('error', 'Some temporary error occure please try again.');
							redirect(SITE_URL.'orderhistorycod');
						}
					} else {
						$this->session->set_flashdata('error', 'Your order has been dispatched you can\'t cancel.');
						redirect(SITE_URL.'orderhistorycod');					
					}
				}
			} else {
				$this->session->set_flashdata('error', 'Some temporary error occure please try again.');
				redirect(SITE_URL.'orderhistorycod');
			}
		}
	}
	function load_views() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('order-history-cod',$this->data);
		$this->load->view(FRONT_INC.'history');
		$this->load->view(FRONT_INC.'footer');
	}
}
