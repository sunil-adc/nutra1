<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class addcomplaints extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		
		// PLACE GIANT FIREWALL FOR ALL REQUEST
		giantFirewall();
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		// SEND NETWORK AND PUB ID - START
		$this->general->checkNetworkPubId($this->input->get('net'), $this->input->get('pubid'), 
										  $this->input->get('bnr'), $this->input->get('clkid'),
										  $this->input->get('sub_id'), $this->input->get('prg_id'), 
										  $this->input->get('zoneid'));
		// SEND NETWORK AND PUB ID - START
		
		// CHECK USER IS LOGIN OR NOT - START
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			redirect(SITE_URL.'login');
		}

		// CHECK USER IS LOGIN OR NOT - END
	
	}

	public function index($seourl = NULL) {
		$cur_controller 		= strtolower(__CLASS__);
		
		// FETCH CATEGORY
		//$data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$data['cur_controller']	= $cur_controller;
		$data['mobile'] = $_SESSION[SVAIZA_MOBILE];
		$data['email'] = $_SESSION[SVAIZA_EMAIL];
		$data['name'] = $this->session->userdata('svaiza_name');
	
		$data_query = $this->db->query("select complaint_id,name,complaint,mobile,email,datecreated,complaint_status from ".COMPLAINT." where mobile='".$_SESSION[SVAIZA_MOBILE]."' and parent_reference_id=0 order by complaint_id,datecreated desc");
		if($data_query->num_rows() > 0)
		{
			foreach($data_query->result() as $r1)
			{
				$data['customer_name'] = $r1->name;
				$data['customer_mobile'] = $r1->mobile;
				$data['customer_email'] = $r1->email;
				$data['customer_complaint'] = $r1->complaint;
				$data['customer_date'] = $r1->datecreated;
				$data['complaint_id'] = $r1->complaint_id;
				$data['customer_complaint_status'] = $r1->complaint_status;
			}
		}
		
		if(isset($data['complaint_id']))
		{
			$history_query = $this->db->query("select c.complaint_id,c.name,c.complaint,c.datecreated,c.parent_reference_id,c.replied_employee,a.username from ".COMPLAINT." c  left JOIN  ".ADMIN." a on (c.replied_employee = a.id) where c.parent_reference_id = '".$data['complaint_id']."' order by c.datecreated desc");
			if($history_query->num_rows() > 0)
			{
				$a=0;
				foreach($history_query->result() as $a1)
				{
					$row[$a++] = $a1;
				}
				$data['parent_history'] = $row;
			}		
		}
		
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('add-complaints',$data);
		$this->load->view(FRONT_INC.'footer');
	}
	
	public function save() {
		
		
		$this->form_validation->set_rules('complaint', 'Complaints', 'trim|required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|numeric');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		

		if ($this->input->post('submit')) {
			if ($this->form_validation->run() != false) {
				
				$parent_reference_id = 0;
				
				//// GET PARENT REFERENCE_ID	
					
				$parent_query = $this->db->query("select complaint_id from ".COMPLAINT." where mobile='".trim($this->input->post('mobile'))."' and parent_reference_id = 0");
				
				
				
				if($parent_query->num_rows() > 0) 
				{
					foreach($parent_query->result() as $vl)
					{						
						$parent_reference_id = $vl->complaint_id;
						$parent_update = $this->db->query("update ".COMPLAINT." set complaint_status=1 where complaint_id='".$vl->complaint_id."'");
					}
				}
			
				// SET THE DATA
				$data_com = array('complaint' => trim($this->input->post('complaint')),
				
							  'mobile' => trim($this->input->post('mobile')),
							  'email' => trim($this->input->post('email')),
							  'name' => trim($this->input->post('name')),
							  'datecreated' => date('Y-m-d H:i:s'),
							  'parent_reference_id' => $parent_reference_id
				);
				// Insert QUERY
								
				$this->db->insert(COMPLAINT,$data_com);
			
				$this->session->set_flashdata('success', 'Complaint added successfully');
				
				$this->db->cache_delete(CMS_FOLDER_NAME, 'managecomplaints');
				$this->db->cache_delete(__CLASS__, "index");
				$this->db->cache_delete(__CLASS__, "save");			
			
						
				
			} else {
				$this->session->set_flashdata('error', '(*) denotes mandatory fields');
			}
		}
		redirect(SITE_URL.'addcomplaints');
	}
	
	public function ajaxresolvedstatus() {
	
		if($_POST['checked_status'] == 'true')
		{
			
			$this->db->query("update ".COMPLAINT." set complaint_status = 1 , dateupdated = now() where complaint_id = '".$_POST['complaint_id']."' ");
		}
		else
		{		
			$this->db->query("update ".COMPLAINT." set complaint_status = 1  , dateupdated = now() where complaint_id = '".$_POST['complaint_id']."'");
		}
		
		$this->db->cache_delete(CMS_FOLDER_NAME, 'managecomplaints');
		$this->db->cache_delete(__CLASS__, "index");
		$this->db->cache_delete(__CLASS__, "save");	
		$this->db->cache_delete(__CLASS__, "ajaxresolvedstatus");	
		
		redirect($_POST['cur_url']);
			
		exit;
	}
}
