<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class paidConfirm extends CI_Controller {
	
	protected $data;
	
	function __construct(){
		parent::__construct();
		$this->load->model('cart_driver');
		
		if(!is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			
			redirect(SITE_URL."login");
		}
	}
	
	public function index ($order_id = "") {
		
		// REDIRECT USER IF ORDER ID IS NOT SET
		if ($order_id == "" || !is_numeric($order_id)) {
			redirect(SITE_URL);
		}
		
		$cur_controller 		= strtolower(__CLASS__);
		
		// FETCH CATEGORY
		$this->data['category'] 		= $this->general->get_nlevel_category($cur_controller);
		$this->data['cur_controller']	= $cur_controller;
		
		$this->data['order_id'] = $order_id;
		$this->data['cart_catid'] = $this->cart_driver->getcartcatid($order_id);
		
		$this->data['order_product'] = $this->db->query('SELECT 
												o.payment_mode, up.product,
												p.name, 		up.cash_price, 
												up.cod_price, 	up.shipping, 
												up.contest, 	up.quantity,
												up.size, 		g.genre_name
											FROM  
												'.ORDER.' o INNER JOIN  
												'.USER_PRODUCTS.' up ON (o.id = up.order_id) 
												INNER JOIN '.PRODUCT.' p ON (up.product = p.prod_id)
												LEFT JOIN '.GENRE.' g on (up.size = g.genre_id)
											WHERE 
												up.contest = 0 and 
												up.status = 1 and 
												up.order_id='.$order_id);
		$this->data['user_details'] = $this->db->query('select name, email, mobile, address, city, state, pincode from '.USERS.' where user_id = '.$_SESSION[SVAIZA_USER_ID]);
 		
		$this->data['order_type'] = $this->common_model->_decide_product_type($order_id);
		if($this->data['order_type']['is_contest'] == true) {
			$this->data['contest_amount'] = 0;
			$contest_amount = $this->db->query('select amount from '.ORDER_PG.' where order_id = '.$order_id);
			if($contest_amount->num_rows() > 0) {
				foreach($contest_amount->result() as $row_contest_total) { 
					$this->data['contest_amount'] = $row_contest_total->amount;	
				}
			}
		}
		
		
		// UNSET SESSION ORDER ID
		$_SESSION[USER_ORDER_ID] = "";
		
		//--//Koushik 19-Dec-2014. clearing session after succesful cart
		$this->memcache_driver->del_cache('cart_'.$_SESSION[SVAIZA_USER_ID]);
		
		// LOAD ALL VIEWS
		$this->loadViews();
	}
	
	public function loadViews() {
		// LOAD ALL VIEWS	
		$this->load->view(FRONT_INC.'common');
		$this->load->view(FRONT_INC.'header');
		$this->load->view('paidconfirm',$this->data);
		$this->load->view(FRONT_INC.'footer');
	}
}