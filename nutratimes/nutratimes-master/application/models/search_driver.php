<?php
class search_driver extends CI_MODEL
{
	function __construct(){
		parent::__construct();	
	}
	
	function getAutomatedSidebar($sql) {
		$catid = array();
		$sub_cat_id = array();
		$brand = array();
		
		if(trim($sql) != "") {
			// EXECUTE THE QUERY
			$res = $this->db->query($sql);
			if($res->num_rows() > 0) {
				foreach ($res->result() as $row) {
					if(!in_array($row->cat_id, $catid)) {
						$catid[] = $row->cat_id;
					}
					if(!in_array($row->sub_cat_id, $sub_cat_id)) {
						$sub_cat_id[] = $row->sub_cat_id;
					}
					if(!in_array($row->brand, $brand)) {
						$brand[] = $row->brand;
					}
				}
				return array ('cat_id' => $catid, 'sub_cat_id' => $sub_cat_id, 'brand' => $brand);
			} else {
				return false;
			}
		}
		return false;
	}
	function getQutoedString($arr) {
		$str = "";
		if(isset($arr) && is_array($arr)) {
			foreach ($arr as $k => $v) {
				$str .= "'".$v."',";
			}
			return trim($str,",");
		}
		return false;
	}
	
	function getSidebarCategory($catid = '', $sub_catid = '') {
		if (is_array($catid)) {
			$total_category = count($catid);
			if($total_category > 1) {
				$string_catid = $this->getQutoedString($catid);
				$search_category = $this->db->query("select name, cat_id from ".CATEGORY." where status = 1 and cat_id in (".trim($string_catid,",").") order by name asc");
				return $search_category;
			} else if (is_array($sub_catid)) {
				if(count($sub_catid) > 0) {
					$string_sub_catid = $this->getQutoedString($sub_catid);
					$search_category = $this->db->query("select name, cat_id from ".CATEGORY." where status = 1 and cat_id in (".trim($string_sub_catid,",").") order by name asc");
					return $search_category;
				}
			}
		}
		return false;
	}
}
?>