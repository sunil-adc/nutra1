<?php
class cart_driver extends CI_MODEL
{
	function __construct(){
		parent:: __construct();
		$this->db->cache_delete('cart', 'index');
		date_default_timezone_set('Asia/Calcutta');
	}
	
	function get_cashback_points() {
		$cashback_amount = 0;
		$this->db->cache_off();
		$cart_data = $this->db->query("SELECT
											p.cash_back_amt
									   FROM
											".USER_PRODUCTS." up 
											inner join ".ORDER." o on (up.order_id = o.id) 
											inner join ".PRODUCT."	p on (up.product = p.prod_id) 
										  WHERE
											o.id = ".$_SESSION[USER_ORDER_ID]." and 
											up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
											up.status = 1 and 
											p.is_verified = 1 and 
											p.status = 1");
		
		if ($cart_data->num_rows() > 0) {
			foreach ($cart_data->result() as $val_cart_data) {
				if (isset($val_cart_data->cash_back_amt) && $val_cart_data->cash_back_amt > 0) {
					$cashback_amount +=	$val_cart_data->cash_back_amt;
				}
			}
		}
		return $cashback_amount > 0 ? $cashback_amount : false;
	}
	
	function _product_qty_instock_check($prod_id, $size, $qty) {
		$this->db->cache_off();
		$res = $this->db->query("select check_inventory, total_stock, is_size from ".PRODUCT." where status = 1 and oos != 1 and prod_id = ".$prod_id);
		if ($res->num_rows() > 0) {
			foreach ($res->result() as $val) {
				if ($val->check_inventory == 1) {
					if ($val->is_size == 1) {
						$res_size = $this->db->query("select genre_quantity, genreid from ".PRODUCT_GENRE." where status = 1 and prodid = ".$prod_id." and genreid = ".$size);
						if ($res_size->num_rows() > 0) {
							foreach ($res_size->result() as $val_size) {
								if ($val_size->genre_quantity < $qty) {
									return 'no';
								}
							}
						}
						return true;
					} else {
						if ($val->total_stock >= $qty) {
							return 'yes';
						} else {
							return 'no';
						}
					}
				} else {
					return 'yes';
				}
			}
		} else {
			return 'prod_na';	
		}
	}
	
	function _add_cart($pid, $sizeid="0") {

		date_default_timezone_set('Asia/Calcutta');

		// CACHE OFF FOR THIS QUERY
		$this->db->cache_off();
		
		// SELECT PRODUCT DETAILS AS PER REQUIREMENT
		$query = $this->db->query("select free_prod, is_combo, combo_product, cod_price, mrp, sale_price, shipping, contest from ".PRODUCT." where prod_id = ".$pid);
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$val_product = $row;
			}
			// SET THE DATA FOR INSERTION 
			
			// CHECK REALLY ORDER EXIST IN ORDER TABLE - START
			$order_exist = false;
			if(isset($_SESSION[USER_ORDER_ID]) && $_SESSION[USER_ORDER_ID] != "") {
				// CACHE OFF FOR THIS QUERY 
				$this->db->cache_off();
				
				$is_ord_exist = $this->db->query('select id from '.ORDER.' where id = '.$_SESSION[USER_ORDER_ID]);
				if ($is_ord_exist->num_rows() > 0) {
					$order_exist = true;
				}
			}
			// CHECK REALLY ORDER EXIST IN ORDER TABLE - END
			
			// SET ORDER ID IF NOT SET - START
			if(!isset($_SESSION[USER_ORDER_ID]) || $_SESSION[USER_ORDER_ID] == "" || $order_exist == false) {
				
				$user_data = $this->db->query("select 
												user_id,
												name,
												email,
												mobile,
												address,
												city,
												state,
												pincode
										   from 
										   		".USERS." 
										   where 
											     user_id = ".$_SESSION[SVAIZA_USER_ID]);
				if($user_data->num_rows() > 0) {
					// GET THE USER DETAILS
					foreach($user_data->result() as $row) {
						$val_users = $row;
					}
					
					$insnet = $inszoneid = $insprgid = $insbnr = $inspubid = $insclkid = $inssub_id = '';
					
					if ($this->session->userdata('clkid') != '') {
						$insclkid = $this->session->userdata('clkid');
					}
					if ($this->session->userdata('pubid') != '') {
						$inspubid = $this->session->userdata('pubid');
					}
					if ($this->session->userdata('bnr') != '') {
						$insbnr = $this->session->userdata('bnr');
					}
					if ($this->session->userdata('prg_id') != '') {
						$insprgid = $this->session->userdata('prg_id');
					}
					if ($this->session->userdata('sub_id') != '') {
						$inssub_id = $this->session->userdata('sub_id');
					}
					if ($this->session->userdata('zoneid') != '') {
						$inszoneid = $this->session->userdata('zoneid');
					}
					if ($this->session->userdata('net') != '') {
						$insnet = $this->session->userdata('net');
					}
					
					// ORDER TABLE DATA PROCESS
					unset($data);
					$data = array(
									'user'			=> $_SESSION[SVAIZA_USER_ID],
									'name' 			=> $val_users->name,
									'email' 		=> $val_users->email,
									'mobile' 		=> $val_users->mobile,
									'address'		=> $val_users->address,
									'city'			=> $val_users->city,
									'state'			=> $val_users->state,
									'pincode'		=> $val_users->pincode,
									'payment_mode'	=> '1',
									'net'			=> $insnet,
									'bnr'			=> $insbnr,
									'pubid'			=> $inspubid,
									'clkid'			=> $insclkid,
									'sub_id'		=> $inssub_id,
									'zoneid'		=> $inszoneid,
									'ip'			=> $this->input->ip_address(),
									'dt_c'			=> date('Y-m-d H:i:s')
								);
					
					// DECIDE FIRE STATUS FOR LEAD
					$fire_status = $this->_decide_fire_status($this->session->userdata('net'), $this->session->userdata('pubid'));
					if($fire_status == true) {
						$this->session->set_userdata('fire_status', '1');
						$data['fire_status'] = '1';
					}
					
					// INSERT QUERY
					$this->db->insert(ORDER,$data);
					$order_id = $this->db->insert_id();
					// ORDER TABLE DATA PROCESS
					
					// SET ORDER ID IN SESSION FOR FURTHER OPERATION
					$_SESSION[USER_ORDER_ID] = $order_id;
					
					// NOTE THE LOGS
					$this->common_model->order_logs(0, 0, 'Order Created by user it self', $_SESSION[SVAIZA_USER_ID], $order_id);
					
					// SELECT THE PROVIDER AS PER PINCODE
					$available_provider = $this->general->_get_provider_from_pincode($_SESSION[SVAIZA_USER_ID]);
					if ($available_provider != false && is_numeric($available_provider)) {
						$this->general->_set_provider_order($available_provider, $_SESSION[USER_ORDER_ID]);
					}
				}
			}
			// SET ORDER ID IF NOT SET - END
			
			// USER PRODUCT TABLE DATA PROCESS - START
			
			$cash_price = ($val_product->sale_price != "" && $val_product->sale_price > 0) ? $val_product->sale_price : $val_product->mrp;

			if($pid != 6 ){

				$sale_price = ($cash_price - ($cash_price * DISCOUNT_PERCENTAGE)/100 ) ;
			}else{
				$sale_price = $cash_price;
			}

			if (! $this->is_not_in_cart($_SESSION[USER_ORDER_ID], $pid, $sizeid)) {
				unset($user_product);
				

				$user_product = array(
								'session_user_id'	=> $_SESSION[SVAIZA_USER_ID],
								'order_id' 			=> $_SESSION[USER_ORDER_ID],
								'product' 			=> $pid,
								'is_combo' 			=> $val_product->is_combo,
								'combo_product' 	=> $val_product->combo_product,
								'quantity' 			=> 1,
								'size' 				=> $sizeid,
								'free_prod'			=> ($val_product->free_prod != "") ? $val_product->free_prod : '',
								'cod_price'			=> ($val_product->cod_price != "") ? $val_product->cod_price : '',
								'cash_price'		=> $sale_price, //($val_product->sale_price != "" && $val_product->sale_price > 0) ? $val_product->sale_price : $val_product->mrp,
								'shipping'			=> ($val_product->shipping != "") ? $val_product->shipping : '',
								'contest'			=> ($val_product->contest != "") ? $val_product->contest : '',
								'datecreated'		=> date('Y-m-d H:i:s')
							);
				// INSERT QUERY
				$this->db->insert(USER_PRODUCTS,$user_product);
				// USER PRODUCT TABLE DATA PROCESS - END
				
				// NOTE THE LOGS
				$msg = strlen(trim($sizeid)) > 0 ? 'Product added in cart (Prod Id : '.$pid.' & Size : '.$sizeid.')' : 'Product added in cart (Prod Id : '.$pid.')';
				$this->common_model->order_logs(0, 0, $msg, $_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
		
			} else {
				unset($user_product);
				
				// SET SIZE IN UPDATE QUERY IF IT IS SET
				$upd_prod_whr = ($sizeid > 0) ? " and size = ".$sizeid : '';
					
				$this->db->query("update 
										".USER_PRODUCTS." 
								  set 
								  		cod_price = ".$val_product->cod_price.",
										cash_price = ".$sale_price.",
										status = 1, 
										quantity = 1  
								  where 
								  		product = ".$pid." and 
										order_id = ".$_SESSION[USER_ORDER_ID].$upd_prod_whr);
				// USER PRODUCT TABLE DATA PROCESS - END
			
				// NOTE THE LOGS
				$this->common_model->order_logs(0, 0, 'Order Product Updated Added one more quantity (Prod Id : '.$pid.' & Size : '.$sizeid.')', $_SESSION[SVAIZA_USER_ID], $_SESSION[USER_ORDER_ID]);
				
			}
		}
		return $_SESSION[USER_ORDER_ID];
	}
	
	function is_not_in_cart($order_id, $pid, $sizeid='') {
		
		// CACHE OFF FOR THIS QUERY
		$this->db->cache_off();
		if(isset($sizeid) && $sizeid > 0) {
			$this->db->where('size', $sizeid);
		}
		$this->db->where('product', $pid);
		$this->db->where('order_id', $order_id);
		$this->db->from(USER_PRODUCTS);
		$count = $this->db->count_all_results();
		
		
		if($count >= 1) {
			return true;
		} else {
			return false;
		}
		
	}
	function _decide_fire_status() {
		if (!($this->session->userdata('fire_status')) && $this->session->userdata('fire_status') != '1') {
			if($this->session->userdata('svaiza_pincode') != "") {
				if ($this->session->userdata('pubid') != '') {
					return $this->_check_inside_publisher($this->session->userdata('pubid'), 
														  $this->session->userdata('net') ? $this->session->userdata('net') : '');
				} else if ($this->session->userdata('net') != "") {
					return $this->_check_inside_network($this->session->userdata('net'));
				}
			}
		}
		return '0';	
	}
	
	public function _check_inside_network($net) {
		// CACHE OFF AND CHECK WHETHER PUBLISHER IS REALLY AVAILABLE
		$this->db->cache_off();
		$this->db->where('lower(net)', $net);
		$this->db->from(PIXELS);
		$is_net_available =  $this->db->count_all_results();
		
		// IF AVAILABLE THEN CHECK FOR PINCODE
		if($is_net_available > 0) {
			$this->db->cache_off();
			/*$sel_pincode = $this->db->query("SELECT 
												net,
												id
											FROM 
												".PIXELS."
											 WHERE 
												lower(net) = '".strtolower($net)."'");*/

			$sel_pincode = $this->db->query("SELECT 
												p.net,
												p.id, 
												pp.id as fire_type,
												pp.pincode 
											 FROM 
												".PIXELS." p inner join ".PARENT_PINCODE." pp on (p.pincode_type = pp.id)
											 WHERE 
												lower(p.net) = '".strtolower($net)."'");

			if($sel_pincode->num_rows() > 0) {
				foreach ($sel_pincode->result() as $row) {
					
					//return $this->change_log_count('net_id='.$row->id.' and pub_id=0' , $row->fire_type, $row->id, '');
					//return $this->_check_current_pincode_exist($this->session->userdata('svaiza_pincode'), $row->pincode);
					return true;
				}
			}
		}
		return false;
	}

	
	public function _check_inside_publisher($pubid = "", $net = "") {
		if($pubid != "") {
			// CACHE OFF AND CHECK WHETHER PUBLISHER IS REALLY AVAILABLE
			$this->db->cache_off();
			$is_pub_available = 0;
			
			$res = $this->db->query("select count(p.id) total_pubs from ".PUBS." p inner join ".PIXELS." n on (p.net_id = n.id) where lower(p.pubs) = '".$pubid."' and n.net = '".$net."'");
			if ($res->num_rows() > 0) {
				foreach ($res->result() as $val_pub) {
					$is_pub_available = $val_pub->total_pubs;
				}
			}
			
			// IF AVAILABLE THEN CHECK FOR PINCODE
			if($is_pub_available > 0) {
				$this->db->cache_off();
				$sel_pincode = $this->db->query("SELECT 
													p.pubs,
													p.net_id, 
													pp.id as fire_type,
													pp.pincode 
												 FROM 
													".PUBS." p inner join ".PARENT_PINCODE." pp on (p.pincode_type = pp.id)
												 WHERE 
													lower(p.pubs) = '".strtolower($pubid)."'");
				if($sel_pincode->num_rows() > 0) {
					foreach ($sel_pincode->result() as $row) {
						
						//return $this->change_log_count('pub_id='.$row->id, $row->fire_type, $row->net_id ,$row->id);
						//return $this->_check_current_pincode_exist($this->session->userdata('svaiza_pincode'), $row->pincode);
						return true;
					}
				}
			} else if ($net != "") {
				return $this->_check_inside_network($net);
			}
		}
		return false;
	}
	
	public function _check_current_pincode_exist($pincode, $pincode_array) {
		$pincode_array = explode(",", trim(trim(trim($pincode_array),','),'@'));
		$is_exist = array_search($pincode, $pincode_array);
		if($is_exist != false) {
			return true;	
		}
		return false;
	}
	
	public function _get_cps_category_pixel() {
		if(is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			
			if (trim($this->session->userdata('net')) != "") {
				$category_wise_cps = $this->general->get_network_categorywise_cps($this->session->userdata('net'));
				if($category_wise_cps != false) {
					$all_categories_cps_pixels = unserialize($category_wise_cps->category_pixels);
					
					$cart_data = $this->db->query("SELECT 
														sum(up.cash_price * up.quantity) as total_amount, c.cat_id 
													FROM 
														".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id), 
														".PRODUCT."	p left join ".CATEGORY." c on (p.cat_id = c.cat_id) 
													WHERE 
														o.id = ".$_SESSION[USER_ORDER_ID]." and 
														up.product = p.prod_id and 
														up.status = 1 
													GROUP BY 
														c.cat_id");
													
					if($cart_data->num_rows() > 0) {
						$total_cps_cost = 0;
						foreach ($cart_data->result() as $row) {
							// GET TOP LEVEL CATEGORY FROM CURRNET CATEGORY
							$parent_cat_id = $this->general->get_top_level_category($row->cat_id);
							
							// GET CPS PERCENTAGE
							$cps_pixels = $all_categories_cps_pixels[$parent_cat_id];
							
							$category_total[] = str_replace("{TOTAL_AMOUNT}", $row->total_amount, str_replace("{IP_ADDR}", $_SERVER['REMOTE_ADDR'], str_replace("{ORDID}", $_SESSION[USER_ORDER_ID], $cps_pixels)));				
						}
						return $category_total;
					}
				}
			}
		}
		return false;
	}
	
	public function _get_cps_amount() {
		if(is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			
			if (trim($this->session->userdata('net')) != "") {
				$category_wise_cps = $this->general->get_network_categorywise_cps($this->session->userdata('net'));
				if($category_wise_cps != false) {
					$all_categories_cps = unserialize($category_wise_cps->category_cps);
							
					$cart_data = $this->db->query("SELECT 
														up.product,	 up.cash_price, up.quantity, c.cat_id 
													FROM 
														".USER_PRODUCTS." up inner join ".ORDER." o on (up.order_id = o.id), 
														".PRODUCT."	p left join ".CATEGORY." c on (p.cat_id = c.cat_id) 
													WHERE 
														o.id = ".$_SESSION[USER_ORDER_ID]." and 
														up.product = p.prod_id and 
														up.status = 1 
													GROUP BY 
														c.cat_id");
													
					if($cart_data->num_rows() > 0) {
						$total_cps_cost = 0;
						foreach ($cart_data->result() as $row) {
							$cps_cost = 0;
							
							// GET TOP LEVEL CATEGORY FROM CURRNET CATEGORY
							$parent_cat_id = $this->general->get_top_level_category($row->cat_id);
							
							// GET CPS PERCENTAGE
							$cps_percentage = $all_categories_cps[$parent_cat_id];
									
							// GET FINAL CPS FOR CURRENT PRODUCT
							if($cps_percentage > 0) {
								$cps_cost = (($row->cash_price * $row->quantity) * $cps_percentage) / 100;	
							} else {
								$cps_cost = 0;
							}
							
							// INCREASE TOTAL REDEMPTION
							$total_cps_cost += $cps_cost;
						}
					}
					return ($total_cps_cost > 0) ? $total_cps_cost : '0';
				}
			}
		}
		return false;
	}
	
	
	public function _cps_pixel_fire() {
		if (trim($this->session->userdata('net')) != "") {
			if($this->session->userdata('cps_fired') != "1") {
				$total_cps_amount = $this->cart_driver->_get_cps_amount();
				$category_pixel = $this->cart_driver->_get_cps_category_pixel();
				$this->cart_driver->_update_cps_pixelFired();
				
				if($category_pixel != false) {
					$this->session->set_userdata('cps_fired_pixels', $category_pixel);
				}
				$this->session->set_userdata('cps_fired','1');
			}
		}
	}
	
	public function _update_cps_pixelFired() {
		if(is_user_login($_SESSION[SVAIZA_USER_ID],
						  $_SESSION[SVAIZA_MOBILE],
						  $_SESSION[SVAIZA_EMAIL])) {
			if (isset($_SESSION[USER_ORDER_ID]) && $_SESSION[USER_ORDER_ID] > 0) {
				$this->db->query("update tbl_orders set fire_status = 1 where id = ".$_SESSION[USER_ORDER_ID]);	
			}
		}
	}
	
	public function fire_pixel_3rd_party($ord_id = "0") {
		$arr = array();
		
		// PRE INITIALIZE VARIABLE
		$arr['net_pub_script'] = $arr['shopaaj_script'] = "";
		
		if($ord_id > 0 && is_numeric($ord_id)) {
			$ord_id = $ord_id;
		} else {
			if ($_SESSION[USER_ORDER_ID] > 0) {
				$ord_id = $_SESSION[USER_ORDER_ID];
			} else {
				return false;
			}
		}
		$this->db->cache_off();
		$res = $this->db->query("select fire_status, net, zoneid, pubid, clkid from ".ORDER." where id = ".$ord_id);
		if($res->num_rows() > 0) {
			foreach ($res->result() as $val) {
				if($val->fire_status == 1) {
					if(isset($val->pubid) && $val->pubid != '0' && strlen(trim($val->pubid)) > 0) {
						$this->db->cache_off();
						$res = $this->db->query("SELECT n.id, n.net, p.pixels pub_pixel, p.net_type, n.pixel, n.status FROM ".PIXELS." n INNER JOIN ".PUBS." p ON ( n.id = p.`net_id` ) WHERE LOWER( p.pubs ) =  '".$val->pubid."' and LOWER( n.net ) =  '".$val->net."'");
						if ($res->num_rows() > 0){
							foreach ($res->result() as $pub_val) {
								if (trim($pub_val->pub_pixel) != "") {
									$pixel_code = $pub_val->pub_pixel;
								} else {
									$pixel_code = $pub_val->pixel;
								}
								
								if ($pub_val->net_type == '1') {
									// PREPARE THE URL
									$curl_url = str_replace("{ZONEID}", $val->zoneid, str_replace("{CLKID}", $val->clkid, str_replace("{PUB_ID}", $val->pubid, str_replace("{IP_ADDR}", $_SERVER['REMOTE_ADDR'], str_replace("{ORDID}", $ord_id, str_replace("{USER_ID}", $_SESSION[SVAIZA_USER_ID], $pixel_code))))));
									
									// S2S
									$curl = curl_init();
									curl_setopt_array($curl, array(
										CURLOPT_RETURNTRANSFER 	=> 1,
										CURLOPT_URL 			=> trim($curl_url),
										CURLOPT_POST 			=> 1
									));
									$resp = curl_exec($curl);
									curl_close($curl);
									$arr['net_pub_script'] = $resp;
								} else {
									$arr['net_pub_script'] = str_replace("{ZONEID}", $val->zoneid, str_replace("{CLKID}", $val->clkid, str_replace("{PUB_ID}", $val->pubid, str_replace("{IP_ADDR}", $_SERVER['REMOTE_ADDR'], str_replace("{ORDID}", $ord_id, str_replace("{USER_ID}", $_SESSION[SVAIZA_USER_ID], $pixel_code))))));
								}
							}
						} else if(isset($val->net) && $val->net != '0' && trim($val->net) != ""){
							$this->db->cache_off();
							$res = $this->db->query("SELECT n.id, n.net, n.pixel, n.status, n.net_type FROM ".PIXELS." n WHERE LOWER(n.net) =  '".$val->net."'");
							if ($res->num_rows() > 0){
								foreach ($res->result() as $net_val) {
									if ($net_val->net_type == '1') {
										// PREPARE THE URL
										$curl_url = str_replace("{ZONEID}", $val->zoneid, str_replace("{CLKID}", $val->clkid, str_replace("{PUB_ID}", $val->pubid, str_replace("{IP_ADDR}", $_SERVER['REMOTE_ADDR'], str_replace("{ORDID}", $ord_id, str_replace("{USER_ID}", $_SESSION[SVAIZA_USER_ID], $net_val->pixel))))));
										
										// S2S
										$curl = curl_init();
										curl_setopt_array($curl, array(
											CURLOPT_RETURNTRANSFER 	=> 1,
											CURLOPT_URL 			=> trim($curl_url),
											CURLOPT_POST 			=> 1
										));
										$resp = curl_exec($curl);
										curl_close($curl);
										$arr['net_pub_script'] = $resp;
									} else {
										$arr['net_pub_script'] = str_replace("{ZONEID}", $val->zoneid, str_replace("{CLKID}", $val->clkid, str_replace("{PUB_ID}", $val->pubid, str_replace("{IP_ADDR}", $_SERVER['REMOTE_ADDR'], str_replace("{ORDID}", $ord_id, str_replace("{USER_ID}", $_SESSION[SVAIZA_USER_ID], $net_val->pixel))))));
									}
								}
							}
						}
					} else if(isset($val->net) && $val->net != '0' && trim($val->net) != ""){
						$this->db->cache_off();
						$res = $this->db->query("SELECT n.id, n.net, n.pixel, n.status, n.net_type FROM ".PIXELS." n WHERE LOWER(n.net) =  '".$val->net."'");
						
						if ($res->num_rows() > 0){
							foreach ($res->result() as $net_val) {
								if ($net_val->net_type == '1') {
									// PREPARE THE URL
									$curl_url = str_replace("{ZONEID}", $val->zoneid, str_replace("{CLKID}", $val->clkid, str_replace("{PUB_ID}", $val->pubid, str_replace("{IP_ADDR}", $_SERVER['REMOTE_ADDR'], str_replace("{ORDID}", $ord_id, str_replace("{USER_ID}", $_SESSION[SVAIZA_USER_ID], $net_val->pixel))))));
									
									// S2S
									$curl = curl_init();
									curl_setopt_array($curl, array(
										CURLOPT_RETURNTRANSFER 	=> 1,
										CURLOPT_URL 			=> trim($curl_url),
										CURLOPT_POST 			=> 1
									));
									$resp = curl_exec($curl);
									
									$error_msg = curl_error($curl);
									
									curl_close($curl);
									$arr['net_pub_script'] = $resp; 
								} else {
									$arr['net_pub_script'] = str_replace("{ZONEID}", $val->zoneid, str_replace("{CLKID}", $val->clkid, str_replace("{PUB_ID}", $val->pubid, str_replace("{IP_ADDR}", $_SERVER['REMOTE_ADDR'], str_replace("{ORDID}", $ord_id, str_replace("{USER_ID}", $_SESSION[SVAIZA_USER_ID], $net_val->pixel))))));

						
								}
							}
						}
					}
					
					$pub_shopaaj = '';
					if (isset($val->pubid) && trim ($val->pubid) != '') {
						$pub_shopaaj = '&pubid='.$val->pubid;
					}
					
					$insclkid_Str = '';
					if (isset($val->clkid) && trim ($val->clkid) != '') {
						$insclkid_Str = '&clkid='.$val->clkid;
					}
					
					$arr['shopaaj_script'] = '';
				
					return $arr;
				}
			}
		}
		return false;
	}
	
	public function lock_order($order_id=0) {
		/*if($order_id > 0) {
			$this->db->query("update ".USER_PRODUCTS." set is_locked = 1 where order_id = ".$order_id);
			$this->db->query("update ".ORDER." set is_locked = 1 where id = ".$order_id);
			
			// NOTE THE LOGS
			$this->common_model->order_logs(0, 0, 'Order Locked', $_SESSION[SVAIZA_USER_ID], $order_id);
				
		}*/
	}
	
	public function unlock_order($order_id=0) {
		/*if($order_id > 0) {
			$this->db->query("update ".USER_PRODUCTS." set is_locked = 0 where order_id = ".$order_id);
			$this->db->query("update ".ORDER." set is_locked = 1 where id = ".$order_id);
			
			// NOTE THE LOGS
			$this->common_model->order_logs(0, 0, 'Order UnLocked ', $_SESSION[SVAIZA_USER_ID], $order_id);
		}*/
	}
	public function product_stock_minus() {
		$cart_data = $this->db->query("SELECT
											up.product,				up.quantity,	
											up.size,				p.prod_id,
											p.total_stock,			p.is_size,
											p.check_inventory,		pg.prod_genreid,
											pg.genre_quantity 
									   FROM
											".USER_PRODUCTS." up inner join ".PRODUCT." p on (p.prod_id = up.product) 
									   		left join ".PRODUCT_GENRE." pg on (up.size = pg.genreid)
									   WHERE
											up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
											up.order_id = ".$_SESSION[USER_ORDER_ID]." and 
											up.product = p.prod_id and 
											up.status = 1 and 
											p.is_verified = 1 and 
											p.status = 1");											

		if($cart_data->num_rows() > 0) {
			foreach ($cart_data->result() as $val) {
				if ($val->check_inventory == 1) {
					if ($val->is_size == 1) {
						$this->db->query("update ".PRODUCT_GENRE." set genre_quantity = genre_quantity - ".$val->quantity." where prodid = ".$val->product." and prod_genreid = ".$val->prod_genreid);	
					} else {
						$this->db->query("update ".PRODUCT." set total_stock = total_stock - ".$val->quantity." where prod_id = ".$val->product);
					}
				} else {
				}
			}
		}
	}
	
	public function product_stock_plus($order_id = 0) {
		if ($order_id > 0) {
			$cart_data = $this->db->query("SELECT
											up.product,				up.quantity,	
											up.size,				p.prod_id,
											p.total_stock,			p.is_size,
											p.check_inventory,		pg.prod_genreid,
											pg.genre_quantity 
									   FROM
											".USER_PRODUCTS." up inner join ".PRODUCT." p on (p.prod_id = up.product) 
									   		left join ".PRODUCT_GENRE." pg on (up.size = pg.genreid)
									   WHERE
											up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
											up.order_id = ".$order_id." and 
											up.product = p.prod_id and 
											up.status = 1 ");											

			if($cart_data->num_rows() > 0) {
				foreach ($cart_data->result() as $val) {
					if ($val->check_inventory == 1) {
						if ($val->is_size == 1) {
							$this->db->query("update ".PRODUCT_GENRE." set genre_quantity = genre_quantity + ".$val->quantity." where prodid = ".$val->product." and prod_genreid = ".$val->prod_genreid);	
						} else {
							$this->db->query("update ".PRODUCT." set total_stock = total_stock + ".$val->quantity." where prod_id = ".$val->product);
						}
					} else {
					}
				}
			}
		}
		return false;
	}
	
	public function getcartcatid($order_id = 0) {
		$catid = '';
		$cart_data = $this->db->query("SELECT 
											up.product, p.cat_id, c.name 
										FROM
											".USER_PRODUCTS." up 
											inner join ".PRODUCT." p on (p.prod_id = up.product) 
											inner join ".CATEGORY." c on (c.cat_id = p.cat_id) 
									   WHERE
											up.session_user_id = ".$_SESSION[SVAIZA_USER_ID]." and 
											up.order_id = ".$order_id." and 
											up.status = 1 ");											

		if($cart_data->num_rows() > 0) {
			foreach ($cart_data->result() as $val) {
				$catid .= $val->name.",";
			}
		}
		
		return trim($catid, ",");
	}
	
	
	
	
}
?>